// JavaScript Document
/////particle

jQuery(window).scroll(function () {
        "use strict";
        var c_scrollTop = jQuery(window).scrollTop();
        ///// Fixed Menu

        if (jQuery('body').width() < 992 - 17) {
                if (c_scrollTop > 0) {
                        jQuery('header').addClass('scrolled1');
                        jQuery('header').addClass('scrolled');
                        var hights = jQuery('header').height();
                        jQuery('html').css('margin-top', hights + 50);
                } else {
                        jQuery('header').removeClass('scrolled1');
						jQuery('header').removeClass('scrolled');
                        jQuery('html').css('margin-top', 0);
                }
        } else {
                if (c_scrollTop > 5) {
                        jQuery('header').addClass('scrolled1');
                        var hights = jQuery('header').height();
                        jQuery('html').css('margin-top', hights);
                        if (c_scrollTop > 160) {
                                jQuery('header').addClass('scrolled');
                                jQuery('html').css('margin-top', 240);
                        }
                        else {
                                jQuery('header').removeClass('scrolled');
                                jQuery('html').css('margin-top', 47);
                                if (c_scrollTop < 47) {
                                        jQuery('html').css('margin-top', c_scrollTop);
                                }
                        }
                } else {
                        jQuery('header').removeClass('scrolled1');
                        jQuery('html').css('margin-top', c_scrollTop);
                }

        }

});

jQuery(document).ready(function () {
    if(jQuery('.zoom_image').length){
        jQuery('.zoom_image').elevateZoom({
                gallery: 'gal1',
                zoomType: "inner",
                cursor: "crosshair",
                zoomWindowFadeIn: 500,
                zoomWindowFadeOut: 800,
        });
    }
// Search
        jQuery('.search_link').on('click', function () {
                jQuery('.s_dropdown').toggle();
                jQuery(this).toggleClass('active');
        });
        // Star Rating
        jQuery('.product_srar a').hover(function () {
                if (!jQuery(this).parent('.product_srar').hasClass('disabled')) {
                        jQuery(this).prevAll().toggleClass('on');
                }

        });
        // Custom Check
        //pre checked
        jQuery('.checkbox-items .item input').each(function () {
                if (jQuery(this).attr('checked')) {
                        jQuery(this).siblings('label').addClass('active');
                }
        });
        /// Toggle List View
        jQuery('.list-view-btn').click(function (e) {
                e.preventDefault();
                jQuery('.product_list_main .item').addClass('item_list_view');
                jQuery('.grid-switch').removeClass('active');
                jQuery(this).addClass('active');
        });
        jQuery('.grid-view-btn').click(function (e) {
                e.preventDefault();
                jQuery('.product_list_main .item').removeClass('item_list_view');
                jQuery('.grid-switch').removeClass('active');
                jQuery(this).addClass('active');
        });
        //click check
        jQuery('.checkbox-items .item label').click(function () {
                if (jQuery(this).siblings('input').prop('checked')) {
                        jQuery(this).removeClass('active');
                        jQuery(this).siblings('input').prop('checked', false);
                } else {
                        jQuery(this).addClass('active');
                        jQuery(this).siblings('input').prop('checked', 'true');
                }
        });
        ///////////

        jQuery('.cat_btn').click(function () {
                jQuery('.category_selector').addClass('on');
                jQuery('body').addClass('menu_open');
        });
        jQuery('.category_selector .close_bt').click(function () {
                jQuery('.category_selector').removeClass('on');
                jQuery('body').removeClass('menu_open');
        });
        jQuery('.filt_btn').click(function () {
                jQuery('.filter_selector').addClass('on');
                jQuery('body').addClass('menu_open');
        });
        jQuery('.filter_selector .close_bt').click(function () {
                jQuery('.filter_selector').removeClass('on');
                jQuery('body').removeClass('menu_open');
        });
        ////////////////
        ////Quantity-increaser
        jQuery('.qminus').click(function () {
                var currentQty = jQuery(this).siblings('.mquantity').val();
                if (currentQty > 1) {
                        jQuery(this).siblings('.mquantity').val(currentQty - 1);
                }
        });
        jQuery('.qplus').click(function () {
                var currentQty2 = jQuery(this).siblings('.mquantity').val();
                jQuery(this).siblings('.mquantity').val(parseInt(currentQty2) + 1);
        });
        //// tool tip
        jQuery(function () {
                jQuery('[data-toggle="tooltip"]').tooltip({
                        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'}
                );
        })
        ///////Tab
        jQuery('.sole_tab li a').on('click', function (e) {
                e.preventDefault();
                jQuery('.sole_tab li').removeClass('active');
                jQuery(this).parent('li').addClass('active');
                var sl_tab = jQuery(this).data('tab');
                jQuery('.soles_tab_c').hide();
                jQuery('#' + sl_tab).show();
        });
        ////////////Profile Menu
        var viewportWidth = jQuery(window).width();
        if (viewportWidth < 768) {
                jQuery('.profile_menu h3').on('click', function () {
                        jQuery(this).siblings('ul.dp').slideToggle();
                });
        }
////
		//////////////////
		/////Filter Mobile Category
		jQuery('.left-filtr h4').click(function(){
			//jQuery('.left-filtr h4').not(this).siblings('.filter-section').slideUp();
			jQuery(this).toggleClass('opend');
			jQuery(this).siblings('.filter-section').slideToggle();
			 
		});
        jQuery('.jumper').click(function () {
                var targets = jQuery(this).attr('href');
                jQuery('html,body').stop().animate({
                        scrollTop: jQuery(targets).offset().top + 150
                }, 1500);
        });
        jQuery('.collapseble > ul > li > a').each(function () {
                if (jQuery(this).siblings().size() > 0) {
                        jQuery(this).addClass('expnd');
                }
        });
        jQuery('.collapseble > ul > li > a').click(function (e) {
                e.preventDefault();
                if (jQuery(this).siblings().size() > 0) {
                        jQuery('.collapseble > ul > li > a').not(this).siblings('p , ul, h3').slideUp();
                        jQuery('.collapseble > ul > li > a').not(this).parent('li').removeClass('active');
                        jQuery(this).siblings('p , ul , h3').slideToggle();
                        jQuery(this).parent('li').toggleClass('active');
                }
        });
        jQuery('.left_menu_collapse > li > a').click(function (e) {
                e.preventDefault();
                if (jQuery(this).siblings().size() > 0) {
                        jQuery('.left_menu_collapse > li > a').not(this).siblings('ul').slideUp();
                        jQuery('.left_menu_collapse > li > a').not(this).parent('li').removeClass('active');
                        jQuery(this).siblings('ul').slideToggle();
                        jQuery(this).parent('li').toggleClass('active');
                }
        });
        //////////////////////////////
        //////address
        jQuery('.drop_down_tabs a').click(function (e) {
                e.preventDefault();
                jQuery('.drop_down_tabs a').removeClass('active');
                jQuery(this).addClass('active');
                var targer_tab = jQuery(this).data('link');
                jQuery('.menu_tabs .item').hide();
                jQuery('.menu_tabs .' + targer_tab).show();
        });
        jQuery('.main_menu .nav li a').click(function (e) {
//                e.preventDefault();
                jQuery('.menu_drop_downs').hide();
                var targer_tab = jQuery(this).data('link');
                jQuery('.menu_drop_downs.' + targer_tab).show();
                jQuery(this).parent('li').addClass('active');
        });
        jQuery('.main_menu .nav li a').mouseenter(function () {
                jQuery('.menu_drop_downs').hide();
                jQuery('.main_menu .nav li').removeClass('active');
        });
        jQuery(document).mouseup(function (e) {
                var $container = jQuery('.menu_drop_downs');
                // var $container2 = jQuery('.s_dropdown');
                // if the target of the click isn't the container nor a descendant of the container
                if (!$container.is(e.target) && $container.has(e.target).length === 0) {
                        $container.hide();
                        jQuery('.main_menu .nav li').removeClass('active');
                        //if(jQuery('.search_link').hasClass('active')){
                        //	$container2.hide();
                        //	jQuery('.search_link').removeClass('active');
                        //}
                }
        });
        //////address
        jQuery('.map_togles a').click(function (e) {
                e.preventDefault();
                jQuery('.map_togles a').removeClass('active');
                jQuery(this).addClass('active');
                var address_key = jQuery(this).data('target');
                jQuery('.map_container > div').removeClass('active');
                jQuery('.map_container > div.' + address_key).addClass('active');
                ;
        });
        jQuery('#myCarousel').on('slide.bs.carousel', function (e) {
                var slideTo = jQuery(e.relatedTarget).index();
                jQuery('.slider_icon_show').carousel(slideTo);
        });
        jQuery('.down_arrow').click(function (e) {
                e.preventDefault();
                jQuery('html,body').stop().animate({
                        scrollTop: jQuery('.hm_about').offset().top
                }, 1500);
        });
        ///////////////////Menu
        ////Menubar
        jQuery('.nav_btn_group').click(function () {
                jQuery('.side_menu').toggleClass('menu_shown');
                jQuery(this).toggleClass('active');
                jQuery('.menu_overlay').toggleClass('active');
                jQuery('body').toggleClass('menu_open');
        });
        jQuery('.menu_overlay').click(function () {
                jQuery('.side_menu').toggleClass('menu_shown');
                jQuery(this).toggleClass('active');
                jQuery('.nav_btn_group').toggleClass('active');
        });
        ///////
        jQuery('li.has-child').click(function () {
                jQuery('li.has-child').not(this).find('ul').slideUp();
                jQuery('li.has-child').not(this).removeClass('active');
                jQuery(this).find('ul').slideToggle();
                jQuery(this).toggleClass('active');
        });
		jQuery('.side_menu li.has-child > a').click(function (e) {
			e.preventDefault();
		});
        //////////////////
        /////scroll
        if (jQuery('.part_scroller').length) {
                jQuery('.part_scroller').scrollbar();
        }

////////Radio Script
/////////////////////////////////////////////////////
        jQuery('.radio_group label').click(function () {
                if (!jQuery(this).parent('.radio_group').hasClass('multi_select')) {
                        jQuery('.radio_group label').removeClass('active');
                }
                jQuery(this).toggleClass('active');
        });
        ////////Checkbox Script
        /////////////////////////////////////////////////////

        jQuery(".datepicker").datepicker();
        /////Main Nav
        jQuery('.mobile_menu').click(function () {
                jQuery('.mobile_nav').slideToggle();
                //jQuery('.main_wrapper').toggleClass('moved');

        });
        jQuery('.parent_menu a').click(function () {
                var menu_child = jQuery(this).data('menu');
                jQuery('.' + menu_child).addClass('active');
        });
        jQuery('.second_level h3 a').click(function () {
                var menu_close = jQuery(this).data('menu');
                jQuery('.' + menu_close).removeClass('active');
        });
        /////////////////////////////
        /////Mobile Arow
        jQuery('.mob_arrow').click(function () {
                jQuery('.header_top_bar ul.right').toggleClass('show');
                jQuery(this).toggleClass('active');
        });
        ///Search_box

        jQuery('.search').click(function () {
                jQuery('.search_form').toggleClass('show');
                jQuery(this).toggleClass('active');
        });
        jQuery('.s_close').click(function () {
                jQuery('.search_form').toggleClass('show');
                jQuery(this).toggleClass('active');
        });
        ////////////////////
        ///////Calender

        var viewportWidth = jQuery(window).width();
        if (viewportWidth > 660) {
                var Rows = 2;
        } else {
                var Rows = 1;
        }
        jQuery('.home_product_slider').slick({
                slidesToShow: 3,
                //autoplay: true,
                //autoplaySpeed: 2000,
                rows: Rows,
                slidesToScroll: 1,
                pauseOnHover: true,
                arrows: true,
                responsive: [
                        {
                                breakpoint: 1024,
                                settings: {
                                        slidesToShow: 2,
                                        rows: 1,
                                }

                        },
                        {
                                breakpoint: 800,
                                settings: {
                                        slidesToShow: 2,
                                        rows: 1,
                                }
                        },
                        {
                                breakpoint: 768,
                                settings: {
                                        slidesToShow: 1,
                                        rows: 1,
                                }
                        },
                        {
                                breakpoint: 600,
                                settings: {
                                        slidesToShow: 2,
                                        rows: 1,
                                }
                        },
                        {
                                breakpoint: 480,
                                settings: {
                                        slidesToShow: 2,
                                        rows: 1,
                                }
                        }
                ]
        });
        jQuery('.test_slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                pauseOnHover: true,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 2000,
                dots: true,
                asNavFor: '.testi_img_slider',
                responsive: [
                        {
                                breakpoint: 800,
                                settings: {
                                        slidesToShow: 1

                                }

                        },
                        {
                                breakpoint: 600,
                                settings: {
                                        slidesToShow: 1,
                                        autoplaySpeed: 8000,
                                }
                        }
                ]
        });
        jQuery('.blog_slider').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                pauseOnHover: true,
                arrows: false,
                autoplay: false,
                dots: false,
                asNavFor: '.testi_slider',
                responsive: [
                        {
                                breakpoint: 768,
                                settings: {
                                        slidesToShow: 3

                                }

                        },
                        {
                                breakpoint: 600,
                                settings: {
                                        slidesToShow: 2,
                                        arrows: true,
                                }
                        },
                        {
                                breakpoint: 420,
                                settings: {
                                        slidesToShow: 1,
                                        arrows: true,
                                }
                        }
                ]
        });
        jQuery('.big_image_slider').slick({
                slidesToShow: 1,
                //autoplay: true,
                //autoplaySpeed: 2000,
                slidesToScroll: 1,
                pauseOnHover: true,
                arrows: false,
                asNavFor: '.thumbnail_image_slider',
        });
        if (jQuery('.big_image_slider').length) {
                jQuery('.big_image_slider').slickLightbox({
                        caption: 'caption'
                });
        }
        jQuery('.thumbnail_image_slider').slick({
                slidesToShow: 5,
                //autoplay: true,
                //autoplaySpeed: 2000,
                slidesToScroll: 1,
                pauseOnHover: true,
                arrows: true,
                asNavFor: '.big_image_slider',
                focusOnSelect: true,
                responsive: [
                        {
                                breakpoint: 1024,
                                settings: {
                                        slidesToShow: 4
                                }

                        },
                        {
                                breakpoint: 800,
                                settings: {
                                        slidesToShow: 4

                                }
                        },
                        {
                                breakpoint: 768,
                                settings: {
                                        slidesToShow: 4

                                }
                        },
                        {
                                breakpoint: 600,
                                settings: {
                                        slidesToShow: 3

                                }
                        },
                        {
                                breakpoint: 480,
                                settings: {
                                        slidesToShow: 2

                                }
                        }
                ]
        });
        $window = jQuery(window);
        if ($window.width() < 968) {
                jQuery('.new-arrival-slider').slick({
                        slidesToShow: 3,
                        autoplay: true,
                        autoplaySpeed: 2000,
                        slidesToScroll: 2,
                        pauseOnHover: true,
                        arrows: true,
                        responsive: [
                                {
                                        breakpoint: 600,
                                        settings: {
                                                slidesToScroll: 1,
                                                slidesToShow: 2

                                        }
                                },
                                {
                                        breakpoint: 400,
                                        settings: {
                                                slidesToScroll: 1,
                                                slidesToShow: 2

                                        }
                                }
                        ]
                });
        }


});
//////
jQuery(window).scroll(function () {
        /*if (jQuery(window).scrollTop() <= 0){
         jQuery('header').removeClass('fixed');
         jQuery('html').css('margin-top',0);
         }
         else {
         jQuery('header').addClass('fixed')
         jQuery('html').css('margin-top',67);

         }*/
});
////////////////////////////////////////////////
/////////////////////////////
//////////////ANIMATIONS
/////////////////////////////
////////////////////////////////////////////////
jQuery(document).ready(function ($) {
        'use strict';
        function myanimations(doelement, doclass) {
                jQuery(doelement).each(function () {
                        if (jQuery(this).offset().top + 50 < (jQuery(window).height() + jQuery(window).scrollTop()) &&
                                (jQuery(this).offset().top + jQuery(doelement).outerHeight()) + 170 > jQuery(window).scrollTop())
                        {
                                jQuery(this).addClass('animated');
                                jQuery(this).addClass(doclass);
                        } else {
                                //jQuery(this).removeClass(doclass);
                        }
                });
        }
//element, animation, hover/false


        myanimations('.step_item, .dropdown-menu, .to_up, .testimoni_section h2, .newsletter_section h4, .footer_links .col-xs-6', 'fadeInUp');
        myanimations('.fast_delivery, .featured h1, .ad_banner, .adrow .col-sm-4, .latest_products h2', 'fadeInUp');
        myanimations('.different h3, .from-blog h2', 'fadeInUp');
        myanimations('.quick_enquirys', 'fadeInUp animdelay1');
        myanimations('.return, .footer-contact', 'fadeInUp animdelay');
        myanimations('.newsletter_row', 'fadeInUp animdelay1');
        myanimations('.menu_dropdown', 'fadeIn');
        myanimations('.newsletter_section p, .email_form, .from-blog .blog_item p, .product_list_home', 'fadeIn animdelay');
        myanimations('.home_product_slider, .brand_row .col-xs-6, .shop_by_brand .orthic-col, .different p', 'fadeIn animdelay');
        myanimations('.from_right', 'fadeInRight');
      //myanimations('.top-bar', 'fadeInDown');
        myanimations('.content-body h5', 'fadeInDown animdelay');
        myanimations('.content-body h1, .logo img', 'fadeInUp');
        myanimations('.product_list_main .item, .listings_sidebar .filter-section ul li, .checkbox-items label', 'fadeIn');
        myanimations('.about_body p, .about_body img, .blog_slider', 'fadeIn');
        myanimations('.zoomin, .latest_products h5, .newsletter_section h3', 'zoomIn');
        myanimations('.inner_caption h1, .inner_main_head, .flex-grid-3 .item.contain, .inner_head_style2, .contact_add .contact_header', 'fadeInUp');
        // myanimations('.package_slider .item', 'fadeInUp');
        /////

});
jQuery(window).scroll(function () {
        "use strict";
        function myanimations(doelement, doclass) {
                jQuery(doelement).each(function () {
                        if (jQuery(this).offset().top + 50 < (jQuery(window).height() + jQuery(window).scrollTop()) &&
                                (jQuery(this).offset().top + jQuery(doelement).outerHeight()) + 220 > jQuery(window).scrollTop())
                        {
                                jQuery(this).addClass('animated');
                                jQuery(this).addClass(doclass);
                        } else {
                                //jQuery(this).removeClass(doclass);
                        }
                });
        }
        myanimations('.step_item, .dropdown-menu, .to_up, .testimoni_section h2, .newsletter_section h4, .footer_links .col-xs-6', 'fadeInUp');
        myanimations('.fast_delivery, .featured h1, .ad_banner, .adrow .col-sm-4, .latest_products h2', 'fadeInUp');
        myanimations('.different h3, .from-blog h2', 'fadeInUp');
        myanimations('.quick_enquirys', 'fadeInUp animdelay1');
        myanimations('.return, .footer-contact', 'fadeInUp animdelay');
        myanimations('.newsletter_row', 'fadeInUp animdelay1');
        myanimations('.menu_dropdown', 'fadeIn');
        myanimations('.newsletter_section p, .email_form, .from-blog .blog_item p, .product_list_home', 'fadeIn animdelay');
        myanimations('.home_product_slider, .brand_row .col-xs-6, .shop_by_brand .orthic-col, .different p', 'fadeIn animdelay');
        myanimations('.from_right', 'fadeInRight');
       //yanimations('.top-bar', 'fadeInDown');
        myanimations('.content-body h5', 'fadeInDown animdelay');
        myanimations('.content-body h1, .logo img', 'fadeInUp');
        myanimations('.product_list_main .item, .listings_sidebar .filter-section ul li, .checkbox-items label', 'fadeIn');
        myanimations('.about_body p, .about_body img, .blog_slider', 'fadeIn');
        myanimations('.zoomin, .latest_products h5, .newsletter_section h3', 'zoomIn');
        myanimations('.inner_caption h1, .inner_main_head, .flex-grid-3 .item.contain, .inner_head_style2, .contact_add .contact_header', 'fadeInUp');
        //Number Animation
        function numberanimations(doelement, animnumber, aftertext) {
                jQuery(doelement).each(function () {
                        if (jQuery(this).offset().top + 50 < (jQuery(window).height() + jQuery(window).scrollTop()) &&
                                (jQuery(this).offset().top + jQuery(doelement).outerHeight()) + 300 > jQuery(window).scrollTop())
                        {
                                if (!jQuery(doelement).hasClass('triggered')) {
                                        jQuery(doelement).addClass('triggered');
                                        jQuery(doelement).animateNumber(
                                                {
                                                        number: animnumber,
                                                        numberStep: function (now, tween) {
                                                                var floored_number = Math.floor(now),
                                                                        $target = jQuery + (tween.elem);
                                                                jQuery(doelement).text(floored_number + aftertext);
                                                        }
                                                },
                                        1000
                                                );
                                }
                        }
                });
        }
        numberanimations('.counter_1', 2000, '+');
        numberanimations('.counter_2', 1500, '+');
        numberanimations('.counter_3', 100, '%');
        numberanimations('.counter_4', 100, '%');
});