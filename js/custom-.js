/*=====================================================================================
 $(document).ready(function()
 ======================================================================================*/

$(window).load(function () {
});



$(document).ready(function () {

        $('#UserAddress_bill_country').change(function () {
                var country = $(this).val();
                if (country != '') {
                        $.ajax({
                                type: "POST",
                                url: baseurl + "ajax/selectState",
                                data: {country: country}
                        }).done(function (data) {
                                if (data == 0) {
                                        $("#UserAddress_bill_state").val($("#UserAddress_bill_state option:first").val());
//                                        $('#UserAddress_bill_state option[value=  ]').prop('selected', true);
                                        $('.b_state1').val('');
                                        $('.cn_ext').hide();
                                        $('.not_ext').show();
                                } else {
                                        $('.b_state1').val('');
                                        $('.not_ext').hide();
                                        $('.cn_ext').show();
                                        $('#UserAddress_bill_state').html(data);
                                }
                        });
                } else {
                        $('#UserAddress_bill_state').html("<option value=''>--Select--</option>");
                }
        });
        $('#UserAddress_ship_country').change(function () {
                var country = $(this).val();
                if (country != '') {
                        $.ajax({
                                type: "POST",
                                url: baseurl + "ajax/selectState",
                                data: {country: country}
                        }).done(function (data) {
                                if (data == 0) {
                                        $('.s_state1').val('');

                                        $('.ship_cn_ext').hide();
                                        $('.ship_not_ext').show();
                                } else {
                                        $('.s_state1').val('');
                                        $('.ship_not_ext').hide();
                                        $('.ship_cn_ext').show();
                                        $('#UserAddress_ship_state').html(data);
                                }
                        });
                } else {
                        $('#UserAddress_ship_state').html("<option value=''>--Select--</option>");
                }
        });


        $('#UserAddress_country').change(function () {
                var country = $(this).val();
                if (country != '') {
                        $.ajax({
                                type: "POST",
                                url: baseurl + "ajax/selectState",
                                data: {country: country}
                        }).done(function (data) {
                                if (data == 0) {

                                        $('#UserAddress_state').attr('required', false);
                                        $('#UserAddress_bill_state').attr('required', true);
                                        $('.cn_ext').hide();
                                        $('.not_ext').show();
                                } else {

                                        $('#UserAddress_state').attr('required', true);
                                        $('#UserAddress_bill_state').attr('required', false);
                                        $('.not_ext').hide();
                                        $('.cn_ext').show();

                                        $('#UserAddress_state').html(data);
                                }
                        });
                } else {
                        $('#UserAddress_state').html("<option value=''>--Select--</option>");
                }
        });

        $('#UserAddress_country').change(function () {
                var country = $(this).val();
                if (country != '') {
                        $.ajax({
                                type: "POST",
                                url: baseurl + "ajax/SelectPhonecode",
                                data: {country: country}
                        }).done(function (data) {
                                $('#UserAddress_phonecode').html(data);
                        });
                } else {
                        $('#UserAddress_phonecode').html("<option value=''>--Code--</option>");
                }
        });

        $('#UserAddress_bill_country').change(function () {
                var country = $(this).val();
                if (country != '') {
                        $.ajax({
                                type: "POST",
                                url: baseurl + "ajax/SelectPhonecode",
                                data: {country: country}
                        }).done(function (data) {
                                $('#UserAddress_bill_phonecode').html(data);
                        });
                } else {
                        $('#UserAddress_bill_phonecode').html("<option value=''>--Code--</option>");
                }
        });
        $('#UserAddress_ship_country').change(function () {
                var country = $(this).val();
                if (country != '') {
                        $.ajax({
                                type: "POST",
                                url: baseurl + "ajax/SelectPhonecode",
                                data: {country: country}
                        }).done(function (data) {
                                $('#UserAddress_ship_phonecode').html(data);
                        });
                } else {
                        $('#UserAddress_ship_phonecode').html("<option value=''>--Code--</option>");
                }
        });
        /*----------------------- BG-Image Wrapper Starts ---------------------------*/

        $(".img-wrapper-old").each(function () {
                var imageUrl = $(this).find('img').attr("src");
                $(this).find('img').css("visibility", "hidden");
                $(this).css('background-image', 'url(' + imageUrl + ')').css("background-repeat", "no-repeat").css("background-size", "cover").css("background-position", "50% 50%");
        });

        $(".img-wrapper").each(function () {
                var imageUrl = $(this).find('img').attr("src");
                var imageUrl = imageUrl.replace(/ /g, '%20');
                var imageUrl = imageUrl.replace("(", '%28');
                var imageUrl = imageUrl.replace(")", '%29');
                $(this).find('img').css("visibility", "hidden");
                $(this).css('background-image', 'url(' + imageUrl + ')').css("background-repeat", "no-repeat").css("background-size", "contain").css("background-position", "50% 50%");
        });

        $(".img-wrapper-contain").each(function () {
                var imageUrl = $(this).find('img').attr("src");
                var imageUrl = imageUrl.replace(/ /g, '%20');
                var imageUrl = imageUrl.replace("(", '%28');
                var imageUrl = imageUrl.replace(")", '%29');
                $(this).find('img').css("visibility", "hidden");
                $(this).css('background-image', 'url(' + imageUrl + ')').css("background-repeat", "no-repeat").css("background-size", "contain").css("background-position", "50% 50%");
        });

        if ($('.header-svg-left').length > 0) {
                $('.header-svg-left').each(function () {
                        var path = document.querySelector('.header-svg-left path');
                        var length = path.getTotalLength();
                        path.style.strokeDasharray = length + ' ' + length;
                        path.style.strokeDashoffset = length;
                });
        }
        if ($('.header-svg-right').length > 0) {
                $('.header-svg-right').each(function () {
                        var path = document.querySelector('.header-svg-right path');
                        var length = path.getTotalLength();
                        path.style.strokeDasharray = length + ' ' + length;
                        path.style.strokeDashoffset = length;
                });
        }

        $('.top-cart').hover(function () {
                $('.cart_drop_down').addClass('active');
        }, function () {
                $('.cart_drop_down').removeClass('active');
        });

        /*----------------------- BG-Image Wrapper Ends ---------------------------*/

        /*----------------------- Testimonial Slide Starts ------------------------*/

        $('.fea-pro-slider').slick({
                slidesToShow: 4,
                dots: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slidesToScroll: 1,
                pauseOnHover: true,
                arrows: false,
                nav: false,
                nav: true,
                        responsive: [
                                {
                                        breakpoint: 1200,
                                        settings: {
                                                centerMode: false,
                                                slidesToShow: 3
                                        }
                                },
                                {
                                        breakpoint: 650,
                                        settings: {
                                                centerMode: false,
                                                slidesToShow: 2
                                        }
                                },
                                {
                                        breakpoint: 500,
                                        settings: {
                                                centerMode: false,
                                                slidesToShow: 1
                                        }
                                }
                        ]
        });

        /*----------------------- Testimonial Slide Ends ------------------------*/



        /*----------------------- Gift Slide Starts ------------------------*/

        $('.gift-img-slider').slick({
                slidesToShow: 4,
                dots: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slidesToScroll: 1,
                pauseOnHover: true,
                arrows: false,
                nav: true,
                responsive: [
                        {
                                breakpoint: 1200,
                                settings: {
                                        centerMode: false,
                                        slidesToShow: 3
                                }
                        },
                        {
                                breakpoint: 750,
                                settings: {
                                        centerMode: false,
                                        slidesToShow: 2
                                }
                        },
                        {
                                breakpoint: 500,
                                settings: {
                                        centerMode: false,
                                        slidesToShow: 1
                                }
                        }
                ]
        });

        /*----------------------- Gift Slide Ends ------------------------*/



        /*----------------------- Gallery Slide Sync Starts -------------------------*/

        $('.client-img-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                nav: false,
                draggable: false,
                asNavFor: '.client-det-slider'
        });

        /*----------------------- Gallery Slide Sync Ends -------------------------*/






        /*----------------------- Product Slide Starts ------------------------*/

        $('.client-det-slider').slick({
                slidesToShow: 1,
                dots: false,
                slidesToScroll: 1,
                autoplay: true,
                pauseOnHover: true,
                nav: true,
                asNavFor: '.client-img-slider',
                arrows: true
        });

        /*----------------------- Product Slide Ends ------------------------*/


        /*----------------------- Service Details Slide Starts ------------------------*/

        $('.you-like-slider').slick({
                slidesToShow: 6,
                dots: false,
                autoplay: true,
                autoplaySpeed: 3000,
                slidesToScroll: 1,
                pauseOnHover: true,
                nav: true,
                arrows: true,
                appendArrows: $(".you-like-main h3"),
                responsive: [
                        {
                                breakpoint: 1200,
                                settings: {
                                        centerMode: false,
                                        slidesToShow: 5
                                }
                        },
                        {
                                breakpoint: 991,
                                settings: {
                                        centerMode: false,
                                        slidesToShow: 4
                                }
                        },
                        {
                                breakpoint: 767,
                                settings: {
                                        centerMode: false,
                                        slidesToShow: 3
                                }
                        },
                        {
                                breakpoint: 550,
                                settings: {
                                        centerMode: false,
                                        slidesToShow: 2
                                }
                        },
                        {
                                breakpoint: 400,
                                settings: {
                                        centerMode: false,
                                        slidesToShow: 1
                                }
                        }
                ]
        });


        $('.details-thumb').slick({
                slidesToShow: 4,
                dots: false,
                autoplay: false,
                autoplaySpeed: 3000,
                slidesToScroll: 1,
                pauseOnHover: false,
                nav: true,
                arrows: true,
                vertical: true,
                verticalSwiping: true,
                responsive: [
                        {
                                breakpoint: 991,
                                settings: {
                                        vertical: false,
                                        verticalSwiping: false,
                                        slidesToShow: 4,
                                        slidesToScroll: 1
                                }
                        },
                        {
                                breakpoint: 767,
                                settings: {
                                        vertical: true,
                                        verticalSwiping: true,
                                        slidesToShow: 3,
                                        slidesToScroll: 1
                                }
                        },
                        {
                                breakpoint: 550,
                                settings: {
                                        vertical: false,
                                        verticalSwiping: false,
                                        slidesToShow: 3,
                                        slidesToScroll: 1
                                }
                        }

                ]
        });

        /*----------------------- Service Details Slide Ends ------------------------*/


        $('.search-large-btn').click(function () {
                $(this).closest('.header-menu').addClass('search-active');
        });
        $(document).click(function () {
                $('.header-menu').removeClass('search-active');
        });
        $('.header-search, .search-large-btn').click(function (event) {
                event.stopPropagation();
        });



        /*----------------------- Side Menu Starts ---------------------------*/


        $('.side-menu-ul > li > a').click(function (e) {
                var $parent = $(this).parent('li');
                var $sibiul = $(this).siblings('ul');
                if ($parent.hasClass('open')) {
                        $sibiul.slideToggle();
                        $parent.toggleClass('open active');
                } else {
                        $('.side-menu-ul > li.open > ul').slideUp();
                        $('.side-menu-ul > li.open').removeClass('open active');
                        $sibiul.slideToggle();
                        $parent.toggleClass('open active');
                }
        });
        $('.side-price h3 a').click(function (e) {
                $(this).closest('.side-price').find('.side-price-box').slideToggle();
                $(this).toggleClass('active');
        });


        /*----------------------- Side Menu Ends ---------------------------*/


        /*----------------------- Price Slider Starts ---------------------------*/

        /* $( "#slider-range" ).slider({
         range: true,
         min: 100,
         max: 90000,
         values: [ 1500, 90000 ],
         slide: function( event, ui ) {
         $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
         $( ".min_value" ).html( "<i class='fa fa-rupee'></i> " + ui.values[ 0 ]);
         $( ".max_value" ).html( "<i class='fa fa-rupee'></i> " + ui.values[ 1 ]);
         }
         });
         $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
         " - $" + $( "#slider-range" ).slider( "values", 1 ) );
         $( ".min_value" ).html( "<i class='fa fa-rupee'></i> " + $( "#slider-range" ).slider( "values", 0 ));
         $( ".max_value" ).html( "<i class='fa fa-rupee'></i> " + $( "#slider-range" ).slider( "values", 1 ));*/
        $("#slider-range").slider({range: true,
                min: 1,
                max: 1000,
                values: [1, 1000],
                slide: function (event, ui) {
                        var min_amount = $("#amount").val(ui.values[0]);
                        var max_amount = $("#amount1").val(ui.values[1]);
                        $('.min_val').val(ui.values[ 0 ]);
                        $('.max_val').val(ui.values[ 1 ]);
                        var categ_id = $("#cat_name").val();
                        $(".min_value").html("<i class='fa fa-usd'></i> " + ui.values[ 0 ]);
                        $(".max_value").html("<i class='fa fa-usd'></i> " + ui.values[ 1 ]);
                },
//                stop: function (event, ui) {
//                        alert(2);
////                        productFilter();
//                }
        });
        $("#amount").val($("#slider-range").slider("values", 0));
        $("#amount1").val($("#slider-range").slider("values", 1));
        $(".min_value").html("<i class='fa fa-usd'></i> " + $("#slider-range").slider("values", 0));
        $(".max_value").html("<i class='fa fa-usd'></i> " + $("#slider-range").slider("values", 1));
        $(".add_to_cart").click(function () {
                var id = $(this).attr('id');
                var canname = $("#cano_name_" + id).val();
                var qty = $(".qty_" + id).val();
              //  addtocart(canname, qty);
        });

        /*----------------------- Price Slider Ends ---------------------------*/

//        $("#img_01").elevateZoom({gallery: 'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true});

        $("#img_01").bind("click", function (e) {
                var ez = $('#img_01').data('elevateZoom');
                $.fancybox(ez.getGalleryList());
                return false;
        });


        /*----------------------- Ripple Effect Light Starts ---------------------------*/

//        Waves.init();
//        Waves.attach('.butter ', ['waves-float', 'waves-light']);


        /*----------------------- Ripple Effect Light Ends ---------------------------*/



        /*----------------------- Input Style Starts ------------------------*/

        (function () {
                // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
                if (!String.prototype.trim) {
                        (function () {
                                // Make sure we trim BOM and NBSP
                                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                                String.prototype.trim = function () {
                                        return this.replace(rtrim, '');
                                };
                        })();
                }

                [].slice.call(document.querySelectorAll('.input__field')).forEach(function (inputEl) {
                        // in case the input is already filled..
                        if (inputEl.value.trim() !== '') {
                                classie.add(inputEl.parentNode, 'input--filled');
                        }

                        // events:
                        inputEl.addEventListener('focus', onInputFocus);
                        inputEl.addEventListener('blur', onInputBlur);
                });

                function onInputFocus(ev) {
                        classie.add(ev.target.parentNode, 'input--filled');
                }

                function onInputBlur(ev) {
                        if (ev.target.value.trim() === '') {
                                classie.remove(ev.target.parentNode, 'input--filled');
                        }
                }
        })();

        /*----------------------- Input Style Ends ------------------------*/


        /*----------------------- Gallery Fancy Box Starts ----------------------*/

//
//        $("a.fancybox").fancybox({
//                helpers: {
//                        overlay: {
//                                locked: false
//                        }
//                }
//        });


        /*----------------------- Gallery Fancy Box Ends ----------------------*/


        /*----------------------- Carousel Caption Lettering Starts ---------------------------*/

//        $(".latest-event span").lettering('words');

        /*----------------------- Carousel Caption Lettering Ends ---------------------------*/


        /*----------------------- Fade This Starts ---------------------------*/

//        $(window).fadeThis({
//                reverse: false
//        });

        /*----------------------- Fade This Ends ---------------------------*/


        /*----------------------- Date Picker Starts ------------------------*/

        $('ul li.dropdown.drop-hover').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
        }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(300);
        });

//        $('input.date-pickers').datepicker();

        /*----------------------- Date Picker Ends ------------------------*/


        /*----------------------- Menu Lock Starts ---------------------------*/

        //    $(window).scroll(function() {
        //        var scroll = $(window).scrollTop();
        //    });
        //    var didScroll;
        //    var lastScrollTop = 0;
        //    var delta = 2;
        //    var navbarHeight = $('header').outerHeight();
        //    $(window).scroll(function(event) {
        //        didScroll = true;
        //    });
        //    setInterval(function() {
        //        if (didScroll) {
        //            hasScrolled();
        //            didScroll = false;
        //        }
        //    }, 250);
        //
        //    function hasScrolled() {
        //        var st = $(this).scrollTop();
        //        if (Math.abs(lastScrollTop - st) <= delta)
        //            return;
        //        if (st > lastScrollTop && st > navbarHeight) {
        //            $('header').removeClass('show_header').addClass('hide_header');
        //        } else {
        //            if (st + $(window).height() < $(document).height()) {
        //                $('header').removeClass('hide_header').addClass('show_header');
        //            }
        //        }
        //        lastScrollTop = st;
        //    }


        /*----------------------- Menu Lock Ends ---------------------------*/







        /*----------------------- Contact Map Starts ---------------------------*/

        //Google Map
        //    var map;
        //    var iconBase = 'images/'
        //
        //    function initialize() {
        //        var styles = [{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"simplified"},{"hue":"#ff0000"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#d9ecff"},{"saturation":55},{"lightness":30},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"hue":"#ffffff"},{"saturation":50},{"lightness":50},{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#f7c884"},{"saturation":100},{"lightness":-22},{"visibility":"on"}]}];
        //        var mapOptions1 = {
        //            zoom: 16,
        //            scrollwheel: false,
        //            disableDefaultUI: true,
        //            center: new google.maps.LatLng(25.202660 + 0.00002, 55.278255 + 0.001)
        //        };
        //        map = new google.maps.Map(document.getElementById('contact_map_id'),
        //            mapOptions1);
        //
        //        var myLatlng = new google.maps.LatLng(25.202660, 55.278255);
        //        var marker1 = new google.maps.Marker({
        //            position: myLatlng,
        //            map: map,
        //            title: 'Bonefire',
        //            icon: iconBase + 'map-icon.png'
        //        });
        //
        //        map.setOptions({
        //            styles: styles
        //        });
        //    }
        //    google.maps.event.addDomListener(window, 'load', initialize);
        //    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //        initialize();
        //    });

        /*----------------------- Contact Map ends ---------------------------*/


});

function getcarttotal() {
        $.ajax({
                type: "POST",
                cache: 'false',
                async: false,
                url: baseurl + 'Cart/Getcarttotal',
                data: {}
        }).done(function (data) {
                $(".amount").html(data);
                hideLoader();
        });
}
function getcartcount() {
        $.ajax({
                type: "POST",
                cache: 'false',
                async: false,
                url: baseurl + 'Cart/Getcartcount',
                data: {}
        }).done(function (data) {
                $(".cart_items").html(data);
                hideLoader();
        });
}
function removecart(cartid, canname) {

        $.ajax({
                type: "POST",
                cache: 'false',
                async: false,
                url: baseurl + 'Cart/Removecart',
                data: {cartid: cartid, cano_name: canname}
        }).done(function (data) {
                getcartcount();
                getcarttotal();
                //$(".cart_box").html(data);
                //alert(data);
                if (data == 'Cart box is Empty') {
                        window.location.href = baseurl + "Cart/Mycart";
                } else {
                        location.reload();
                }
                hideLoader();
        });
}

function getcartdata() {
        $.ajax({
                type: "POST",
                cache: 'false',
                async: false,
                url: baseurl + 'Cart/Selectcart',
                data: {}
        }).done(function (data) {
                $(".cart_box").html(data);
                //$(".cart_box").show('fast');
                hideLoader();
        });
}