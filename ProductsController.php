<?php

class ProductsController extends Controller {

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
            ),
        );
    }

    public function actionCategory($name) {
     
//var_dump(Yii::app()->session);
//exit;
        if (!Yii::app()->request->isAjaxRequest) {
            unset(Yii::app()->session['toy_style']);
            unset(Yii::app()->session['width_fitting']);
            unset(Yii::app()->session['color']);
            unset(Yii::app()->session['size']);
            unset(Yii::app()->session['max']);
            unset(Yii::app()->session['min']);
            unset($_SESSION['category_new']);
        }
        $parent = ProductCategory::model()->findByAttributes(array('canonical_name' => $name));
        if (empty($parent)) {
            $this->render('ProductNotfound');
            return FALSE;
        }
        $category = ProductCategory::model()->findAllByAttributes(array('parent' => $parent->parent));
        $cats = ProductCategory::model()->findAllByattributes(array('parent' => $parent->id));
        if (isset($_POST['pagesize'])) {
            $pagesize = $_POST['pagesize'];
            Yii::app()->session['pagesize'] = $pagesize;
        }
        if (isset($_POST['category'])) {
            $categ = $_POST['category'];
            Yii::app()->session['sort_id'] = $_REQUEST['category'];
        } else {
            if (Yii::app()->request->isAjaxRequest) {
                $categ = Yii::app()->session['sort_id'];
            } else {
                Yii::app()->session['sort_id'] = 1;
                $categ = Yii::app()->session['sort_id'];
            }
        }
        if (isset($_POST['category'])) {
            $get_sort_value = $_POST['category'];
            if ($get_sort_value == 1) {
                $srt = ' id DESC ';
            } else if ($get_sort_value == 2) {
                $srt = 'price ASC';
            } else if ($get_sort_value == 3) {
                $srt = 'price DESC';
            } else if ($get_sort_value == 4) {
                $srt = 'product_name ASC';
            } else if ($get_sort_value == 5) {
                $srt = 'product_name DESC';
            }
        } else {
            $srt = ' id DESC ';
        }
        if (!isset($pagesize)) {
            $pagesize = 15;
        }
        if (isset(Yii::app()->session['min'])) {
            $min = Yii::app()->session['min'];
        }
        else
        
        if (isset(Yii::app()->session['max'])) {
            $max = Yii::app()->session['max'];
        }
       

        $final_condition = Yii::app()->Menu->MenuCategories($cats, $srt, $parent, $categ, $min , $max, $pagesize);

        if ($final_condition != '') {
            $dataProvider = new CActiveDataProvider('Products', array(
                'criteria' => array(
                    'condition' => $final_condition,
                ),
                'pagination' => array(
                    'pageSize' => $pagesize,
                ),
                'sort' => array(
                    'defaultOrder' => $srt,
                )
                    )
            );
        } else {
            $dataProvider = NULL;
        }
        $getproducts = Yii::app()->Menu->AllProducts($cats, $parent, $categ, $min, $max, $pagesize);
        if (isset(Yii::app()->session['temp_product_filter'])) {
            unset(Yii::app()->session['temp_product_filter']);
            unset(Yii::app()->session['temp_product_filter_check']);
        }
        $this->render('index', array('dataProvider' => $dataProvider, 'getproducts' => $getproducts, 'parent' => $parent, 'category' => $category, 'name' => $name, 'pagesize' => $pagesize));
    }

    public function actionSale() {
        if (!Yii::app()->request->isAjaxRequest) {
            unset(Yii::app()->session['toy_style']);
            unset(Yii::app()->session['width_fitting']);
            unset(Yii::app()->session['color']);
            unset(Yii::app()->session['size']);
            unset(Yii::app()->session['max']);
            unset(Yii::app()->session['min']);
        }
        $product_exist = Products::model()->findAll(array("condition" => 'discount_rate!="" and status=1'));
        if (isset($_POST['pagesize'])) {
            $pagesize = $_POST['pagesize'];
            Yii::app()->session['pagesize'] = $pagesize;
        }

        if (!isset($pagesize)) {
            $pagesize = 15;
        }

        if (isset(Yii::app()->session['min'])) {
            $min = Yii::app()->session['min'];
        }

        if (isset(Yii::app()->session['max'])) {
            $max = Yii::app()->session['max'];
        }
        if (isset($_POST['sorting'])) {
            $categ = $_POST['sorting'];
            Yii::app()->session['sort_id'] = $_REQUEST['sorting'];
        } else {
            if (Yii::app()->request->isAjaxRequest) {
                $categ = Yii::app()->session['sort_id'];
            } else {
                //Yii::app()->session['sort_id'] = 1;
                $categ = Yii::app()->session['sort_id'];
            }
        }
        if (isset($_POST['sorting'])) {
            $get_sort_value = $_POST['sorting'];
            if ($get_sort_value == 1) {
                $srt = ' id DESC ';
            } else if ($get_sort_value == 2) {
                $srt = 'price ASC';
            } else if ($get_sort_value == 3) {
                $srt = 'price DESC';
            } else if ($get_sort_value == 4) {
                $srt = 'product_name ASC';
            } else if ($get_sort_value == 5) {
                $srt = 'product_name DESC';
            }
        } else {
            $srt = ' id DESC ';
        }
        if (isset(Yii::app()->session['sorting'])) {

        }
        if (!empty($product_exist)) {
            foreach ($product_exist as $product_data) {
                $pids[] = $product_data->id;
            }
            $finalcondition = Yii::app()->Menu->SearchByProducts($pids, $parent, $categ, $min = '', $max = '', $pagesize);
            $getproducts = Products::model()->findAll("(" . $finalcondition . ") AND status = 1");
            $dataProvider = new CActiveDataProvider('Products', array(
                'criteria' => array(
                    'condition' => $finalcondition,
                ),
                'pagination' => array(
                    'pageSize' => $pagesize,
                ),
                'sort' => array(
                    'defaultOrder' => $srt,
                )
                    )
            );
        } else {
            $dataProvider = NULL;
            $getproducts = NULL;
        }
        if ($dataProvider != NULL && $getproducts != NULL) {
            $this->render('sale', array('dataProvider' => $dataProvider, 'getproducts' => $getproducts, 'parent' => $parent, 'category' => $category, 'name' => $name, 'pagesize' => $pagesize));
        } else {
            $this->render('sale', array('dataProvider' => $dataProvider, 'getproducts' => $getproducts, 'parent' => $parent, 'category' => $category, 'name' => $name, 'pagesize' => $pagesize));
        }
        exit;
    }

    public function actionDetail($name) {
       
        $product_view = new ProductViewed;
        $prduct = Products::model()->findByAttributes(array('canonical_name' => $name, 'status' => 1));
        $recently = '';
//        $related_products = explode(",", $prduct->related_products);
        $product_category = explode(",", $prduct->category_id);
        $related_condition = '';
        foreach ($product_category as $p_cat) {
            if ($p_cat != '') {
                $related_condition .= "FIND_IN_SET('$p_cat',`category_id`) OR ";
            }
        }
        $related_condition = rtrim($related_condition, ' OR');
        if ($related_condition != '') {
            $related_products = Products::model()->findAll(array('condition' => 'status = 1 AND (' . $related_condition . ') AND id !=' . $prduct->id . ' order by id desc', 'limit' => 8));
        }
        if (Yii::app()->session['user'] != '' && Yii::app()->session['user'] != NULL) {
            $user_id = Yii::app()->session['user']['id'];
            $product_view_exist = ProductViewed::model()->findByAttributes(array('user_id' => $user_id, 'product_id' => $prduct->id));
            if ($product_view_exist == NULL) {
                $product_view->date = date('Y-m-d');
                $product_view->product_id = $prduct->id;
                $product_view->session_id = NULL;
                $product_view->user_id = $user_id;
                if ($prduct->id != '') {
                    $product_view->save(FALSE);
                }
            }
            $cat = $prduct->enquiry_sale;
            if ($cat == 1) {
                $viewed = ProductViewed::model()->findAll(array('limit' => 8));
            } else {
                $viewed = ProductViewed::model()->findAll(array('limit' => 8));
            }
        } else {
            if (!isset(Yii::app()->session['temp_user'])) {
                Yii::app()->session['temp_user'] = microtime(true);
            }
            $sessonid = Yii::app()->session['temp_user'];
            $product_view_exist = ProductViewed::model()->findByAttributes(array('session_id' => $sessonid, 'product_id' => $prduct->id));

            if (empty($product_view_exist) && $product_view_exist == NULL) {
                $product_view->date = date('Y-m-d');
                $product_view->product_id = $prduct->id;
                $product_view->session_id = $sessonid;
                $product_view->user_id = NULL;
                if ($prduct->id != '') {
                    $product_view->save(FALSE);
                }
            }
            $cat = $prduct->enquiry_sale;
            if ($cat == 1) {
                $recently = Products::model()->findAll(array('condition' => 'status = 1 AND enquiry_sale = 1 AND NOT FIND_IN_SET(2, category_id) AND NOT FIND_IN_SET(8, category_id) order by id desc', 'limit' => 8));
                $viewed = ProductViewed::model()->findAll(array('limit' => 8));
            } else {
                $recently = Products::model()->findAll(array('condition' => 'status = 1 and enquiry_sale = 0', 'limit' => 8));
                $viewed = ProductViewed::model()->findAll(array('limit' => 8));
            }
        }
        $model = new ProductEnquiry('create');
        if (isset($_POST['ProductEnquiry'])) {
            $model->attributes = $_POST['ProductEnquiry'];
            $product_details = Products::model()->findByPk($_POST['ProductEnquiry']['product_id']);
            $model->name = $_POST['ProductEnquiry']['name'];
            $model->color = $_POST['ProductEnquiry']['color'];
            $model->status = 1;
            $model->total_amount = $product_details->price;
            $model->balance_to_pay = $product_details->price;
            $model->doc = date('Y-m-d');
            $model->requirement = $_POST['ProductEnquiry']['requirement'];
            $model->delivery_date = date('Y-m-d', strtotime($_POST['ProductEnquiry']['delivery_date']));
            $model->browser_agent = $_SERVER['HTTP_USER_AGENT'];
            $model->enquiry_type = 1;
            if ($model->validate()) {
                if ($model->save()) {
                    $this->ProductEnquiryMail($model, $celib_history_update);
                    $model->unsetAttributes();
                }
            }
        }
        
        $product_reviews = UserReviews::model()->findAllByAttributes(['product_id' => $prduct->id,'approvel'=>1]);
        if (!empty($prduct)) {
            
            $this->render('detailed', array('product' => $prduct, 'product_reviews' => $product_reviews, 'model' => $model, 'viewed' => $viewed, 'recently' => $recently, 'related_products' => $related_products));
        } else {
            $this->redirect(array('Site/Error'));
        }
    }

    public function actionUpdateProduct($name, $cart) {
        $cart_det = Cart::model()->findByPk(base64_decode($cart));
        $product_view = new ProductViewed;
        $prduct = Products::model()->findByAttributes(array('canonical_name' => $name, 'status' => 1));
        $recently = '';
        $product_category = explode(",", $prduct->category_id);
        $related_condition = '';
        foreach ($product_category as $p_cat) {
            if ($p_cat != '') {
                $related_condition .= "FIND_IN_SET('$p_cat',`category_id`) OR ";
            }
        }
        $related_condition = rtrim($related_condition, ' OR');
        if ($related_condition != '') {
            $related_products = Products::model()->findAll(array('condition' => 'status = 1 AND (' . $related_condition . ') AND id !=' . $prduct->id . ' order by id desc', 'limit' => 8));
        }
        if (Yii::app()->session['user'] != '' && Yii::app()->session['user'] != NULL) {
            $user_id = Yii::app()->session['user']['id'];
        } else {
            if (!isset(Yii::app()->session['temp_user'])) {
                Yii::app()->session['temp_user'] = microtime(true);
            }
            $sessonid = Yii::app()->session['temp_user'];
        }

        $product_reviews = UserReviews::model()->findAllByAttributes(['product_id' => $prduct->id,'approvel'=>1]);
        if (!empty($prduct)) {
            $this->render('update_products', array('product' => $prduct, 'cart_det' => $cart_det, 'product_reviews' => $product_reviews, 'viewed' => $viewed, 'recently' => $recently, 'related_products' => $related_products));
        } else {
            $this->redirect(array('Site/Error'));
        }
    }

    public function actionAddreview() {
        if (Yii::app()->request->isAjaxRequest) {
            $name = $_REQUEST['name'];
            $email = $_REQUEST['email'];
            $comment = $_REQUEST['comment'];
            $star = $_REQUEST['star'];
            $review_product_id = $_REQUEST['review_product_id'];
            $review_exist = UserReviews::model()->findByAttributes(array('author' => $name, 'email' => $email));
            if (!empty($review_exist)) {
                echo 1;
            } else {
                $reviews = new UserReviews;
                if (isset(Yii::app()->session['user'])) {
                    $reviews->user_id = Yii::app()->session['user']['id'];
                }
                $reviews->author = $name;
                $reviews->email = $email;
                $reviews->review = $comment;
                $reviews->rating = $star;
                $reviews->product_id = $review_product_id;

                if ($reviews->save(false)) {
                    echo 2;
                } else {
                    echo 3;
                }
            }
        }
    }

    public function ProductEnquiryMail($model, $celib_history_update) {
        $pdts = Products::model()->findByPk($model->product_id);
        $optn = OptionCategory::model()->findByPk($model->color)->color_name;
        $to = $model->email;
        $admin = AdminUser::model()->findByPk(4)->email;


        $subject = 'Solesilove Enquiry' . " - $pdts->product_name";
        $subject1 = "$model->name - " . "$pdts->product_name - " . 'Make Payment Link Sent';
        $message = new YiiMailMessage;
        $message->view = "_product_enquiry_mail_client";  // view file name
        $params = array('model' => $model, 'pdts' => $pdts, 'celib_history_update' => $celib_history_update); // parameters
        $message->subject = $subject;
        $message->setBody($params, 'text/html');
        $message->addTo($to);
        $message->setFrom('no-reply@solesilove.com.au', 'Solesilove.com.au');
        Yii::app()->mail->send($message);



        $message1 = new YiiMailMessage;
        $message1->view = "_product_enquiry_mail_admin";  // view file name
        $params1 = array('model' => $model); // parameters
        $message1->subject = $subject1;
        $message1->setBody($params1, 'text/html');
        $message1->addTo($admin);
        $message1->setFrom('no-reply@solesilove.com.au', 'Solesilove.com.au');
        Yii::app()->mail->send($message1);
    }

    public function actionProductEnquiryMailAdmin($id) {

        $model = ProductEnquiry::model()->findByPk($id);

        $to = $model->email;
        $admin = AdminUser::model()->findByPk(4)->email;
        $subject = 'Product Enquiry';
        $message1 = $this->renderPartial('mail/_product_enquiry_mail_admin', array('model' => $model), true);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $subject1 = "$model->name " . "- $model->email " . '- I wish to proceed';
        // More headers
        $headers .= 'From: Solesilove.com.au <no-reply@solesilove.com.au>' . "\r\n";
        $this->render('product_enquiry_wish_to_admin', array('model' => $model));
        echo "Successfully sent your request";
    }

    public function ProductEnquiryMailAdmin($id) {

        $model = ProductEnquiry::model()->findByPk($id);
        $pdts = Products::model()->findByPk($model->product_id);
        $to = $model->email;
        $admin = AdminUser::model()->findByPk(4)->email;
        $subject = "$pdts->product_name - " . "$model->name - " . 'Product Enquiry';

        $message1 = $this->renderPartial('mail/_normal_product_enquiry_mail_admin', array('model' => $model), true);

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: Solesilove.com.au <support@solesilove.com.au>' . "\r\n";
        mail($admin, $subject, $message1, $headers);
    }

    public function actionWishlist($id) {
        if (Yii::app()->session['user'] != '' && Yii::app()->session['user'] != NULL) {
            $value = UserWishlist::model()->findByAttributes(array('user_id' => Yii::app()->session['user']['id'], 'prod_id' => $id));
            if ($value != "") {
                Yii::app()->user->setFlash('error', "This product has already been added to your wishlist.");
                $this->redirect(Yii::app()->request->urlReferrer);
            } else {
                $model = new UserWishlist;
                $model->user_id = Yii::app()->session['user']['id'];
                $model->session_id = NULL;
                $model->prod_id = $id;
                $model->date = date('Y-m-d');
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Added to Wish List");
                    $this->redirect(Yii::app()->request->urlReferrer);
                }
            }
        } else {

            if (!isset(Yii::app()->session['temp_user'])) {
                Yii::app()->session['temp_user'] = microtime(true);
            }
            $value = UserWishlist::model()->findByAttributes(array('session_id' => Yii::app()->session['temp_user'], 'prod_id' => $id));

            if ($value != "") {
                Yii::app()->user->setFlash('error', "This product has already been added to your wishlist.");
                $this->redirect(Yii::app()->request->urlReferrer);
            } else {
                $model = new UserWishlist;
                $model->session_id = Yii::app()->session['temp_user'];
                $model->prod_id = $id;
                $model->date = date('Y-m-d');
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Product added to your wishlist");
                    $this->redirect(Yii::app()->request->urlReferrer);
                }
            }
        }
    }

    public function actionProductNotify($id) {
        if ($id != "") {
            if (isset($_POST['email'])) {
                $email = $_POST['email'];
            } else {
                $email = Yii::app()->session['user']['email'];
            }
            $model = UserNotify::model()->findByAttributes(array('email_id' => $email, 'prod_id' => $id));
            if (!empty($model)) {
                Yii::app()->user->setFlash('error', "This product already registered for out of stock notification.");
                $this->redirect(Yii::app()->request->urlReferrer);
            } else {
                $notify = new UserNotify();
                $notify->email_id = $email;
                $notify->prod_id = $id;
                $notify->status = 1;
                $notify->date = date('Y-m-d');
                if ($notify->validate()) {
                    $notify->save();
                    Yii::app()->user->setFlash('success', " Thank you! We shall notify you as soon as this product is back in stock!");
                    $this->redirect(Yii::app()->request->urlReferrer);
                } else {
                    Yii::app()->user->setFlash('error', "Sorry! Message sending failed..");
                }
            }
        } else {
            $this->redirect('site/error');
        }
    }

    public function actionPriceRange() {
        if (Yii::app()->request->isAjaxRequest) {
            $min = $_REQUEST['min'];
            $max = $_REQUEST['max'];
            $cat = $_REQUEST['cat'];
            $size_type = $_REQUEST['size'];
            $data[0] = $min;
            $data[1] = $max;
            $data[3] = $cat;

            if ($cat != '' && $min != '' && $max != '') {
                $categry = ProductCategory::model()->findByPk($cat);
                if (!empty($categry)) {
                    Yii::app()->session['temp_product_filter'] = $data;
                }
            }
            if ($size_type != '' && $cat != '') {
                $sizes = OptionCategory::model()->findByAttributes(array('option_type_id' => 2, 'id' => $size_type));
                if (!empty($sizes)) {
                    $data[4] = $size_type;
                    Yii::app()->session['temp_product_filter'][4] = $size_type;
                    Yii::app()->session['temp_product_filter'][3] = $size_type;
                } else {
                    $size_type = '';
                }
            }
            if ($cat != '' && $min != '' && $max != '') {
                $parent = ProductCategory::model()->findByPk($cat);
                $cats = ProductCategory::model()->findAllByattributes(array('parent' => $parent->id), array('condition' => "id != $parent->id"));
                $dataProvider = Yii::app()->Menu->MenuCategories($cats, $parent, $categ = '', $min, $max, $size_type);
            } else {
                $parent = ProductCategory::model()->findByPk(Yii::app()->session['temp_product_filter'][3]);
                $cats = ProductCategory::model()->findAllByattributes(array('parent' => $parent->id), array('condition' => "id != $parent->id"));
                $dataProvider = Yii::app()->Menu->MenuCategories($cats, $parent, $categ = '', Yii::app()->session['temp_product_filter'][0], Yii::app()->session['temp_product_filter'][1], Yii::app()->session['temp_product_filter'][4]);
            }

            $this->renderPartial('_view1', array('dataProvider' => $dataProvider, 'parent' => $parent, 'name' => $cat));
        } else {

        }
    }

    public function actionmailfn() {
        $page_url = $_SERVER["HTTP_REFERER"];
        $from = $_POST['from'];
        $to = $_POST['email'];
        $subject = 'Solesilove Product Details from ' . $from;
        $message = $this->renderPartial('_product_info_mail', array('page_url' => $page_url, 'from' => $from), TRUE);
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: Solesilove.com.au <no-reply@solesilove.com.au>' . "\r\n";

        if (mail($to, $subject, $message, $headers)) {
            Yii::app()->user->setFlash('success', " your email sent successfully..");
            $this->redirect(Yii::app()->request->urlReferrer);
        }
    }

    public function actionSelectgallery() {
        if (Yii::app()->request->isAjaxRequest) {

            $product_id = $_REQUEST['product_id'];
            $color = $_REQUEST['color'];
            if ($product_id != "" && $color != "") {

                $model = OptionImages::model()->findByAttributes(array('product_id' => $product_id, 'color_id' => $color));
                if (!empty($model)) {
                    echo $this->renderPartial('_gallery', array('model' => $model), TRUE);
                } else {
                    echo "";
                }
            }
        }
    }

    public function actionGetstock() {
        if (Yii::app()->request->isAjaxRequest) {

            $product_id = $_REQUEST['product_id'];

            $color = $_REQUEST['color'];
            $size = $_REQUEST['size'];

            if ($product_id != "") {
                if ($_REQUEST['color']) {

                    $condition = 'color_id = ' . $_REQUEST['color'];
                }

                if ($_REQUEST['size']) {

                    $condition .= ' AND size_id = ' . $_REQUEST['size'];
                }
                $model = OptionDetails::model()->findByAttributes(array('product_id' => $product_id), array('condition' => $condition));

                if (!empty($model)) {
                    if (Yii::app()->session['user'] != '' && Yii::app()->session['user'] != NULL) {
                        $user_id = Yii::app()->session['user']['id'];
                    } else if (isset(Yii::app()->session['temp_user'])) {
                        $sessonid = Yii::app()->session['temp_user'];
                    }
                    if (isset($user_id)) {
                        $condition = "user_id= $user_id";
                    } else if (isset($sessonid)) {
                        $condition = "session_id= $sessonid";
                    }
                    $cart_contents = Cart::model()->findAllByAttributes(array('options' => $model->id), array('condition' => ($condition)));


                    if (!empty($cart_contents)) {

                        foreach ($cart_contents as $cart_content) {
                            $stock_cart += $cart_content->quantity;
                        }
                    } else {
                        $stock_cart = 0;
                    }

                    $stock_exist = $stock_cart;
                    $final_stock = $model->stock - $stock_exist;
                    echo $final_stock;
                }
            }
        }
    }

    public function actionOptions() {
        if (Yii::app()->request->isAjaxRequest) {

            $option = $_REQUEST['option'];
            $color = $_REQUEST['color'];
            if ($option != "" && $color != "") {
                $sizes = OptionCategory::model()->findAll(['condition' => 'option_type_id=2', 'order' => 'id asc']);
                ?>
                <label>Size : </label>
                <?php
                if (!empty($sizes)) {
                    foreach ($sizes as $size) {
                        $product_option = OptionDetails::model()->findByAttributes(array('color_id' => $color, 'master_option_id' => $option, 'size_id' => $size->id));
                        if ($product_option->stock > 0 && $product_option->status != 0) {
                            $c = 1;
                            if ($size->id == $product_option->size_id) {
                                $c = 2;
                                $disabled = '';
                            } else {
                                $c = 3;
                                $disabled = 'disabled';
                            }
                        } else {
                            $c = 4;
                            $disabled = 'disabled';
                        }
                        if (!empty($product_option)) {
                            ?>
                            <div class="side-size-cont">
                                    <input type="radio" <?php echo $disabled; ?> name="size_selector" value="<?php echo $size->id; ?>" id="size_selector_<?php echo $size->id; ?>">

                                    <label class="<?php echo $disabled; ?>" id="<?php echo $size->id; ?>" product="<?php echo $product_option->product_id; ?>" option_id="<?php echo $product_option->master_option_id; ?>"  size="<?php echo $size->id; ?>" for="size_selector_<?php echo $size->id; ?>"><?php echo $size->size; ?>
                                    </label>
                            </div>
                            <?php
                        }
                    }
                }
            }
        }
    }

    public function actionWidthoptions() {
        if (Yii::app()->request->isAjaxRequest) {

            $option = $_REQUEST['option'];
            $size = $_REQUEST['size'];
            $color = $_REQUEST['color']; 
            if ($option != "" && $color != "" && $size != "") {
                $product_op = OptionDetails::model()->findByAttributes(array('color_id' => $color, 'size_id' => $size, 'master_option_id' => $option));
                if($product_op->width_id != 0)    
                {
                $widths = WidthFitting::model()->findAll(['condition' => 'status=1', 'order' => 'id asc']);
                ?>
                <label>Width Fittings : </label>
                <?php
                if (!empty($widths)) {
                    foreach ($widths as $width) {
                        $product_option = OptionDetails::model()->findByAttributes(array('color_id' => $color, 'size_id' => $size, 'width_id' => $width->id, 'master_option_id' => $option));
                        //  echo $product_option->id . 'hahahah';
                        if ($product_option->stock > 0 && $product_option->status != 0) {
                            $c = 1;
                            if ($width->id == $product_option->width_id) {
                                $c = 2;
                                $disabled = '';
                            } else {
                                $c = 3;
                                $disabled = 'disabled';
                            }
                        } else {
                            $c = 4;
                            $disabled = 'disabled';
                        }
                        if (!empty($product_option)) {
                            ?>
                            <div class="side-size-cont" style="width: auto">
                                    <input <?php echo $disabled; ?> class="clrr" id="width_selector_<?php echo $width->id; ?>" type="radio" name="width_selector" value="<?php echo $width->id; ?>">

                                    <label class="<?php echo $disabled; ?>" id="<?php echo $width->id; ?>" for="width_selector_<?php echo $width->id; ?>"><?php echo $width->title; ?>
                                    </label>
                            </div>
                            <?php
                        }
                    }
                }
            }
        }
        }
    }

    public function actionPolicies() {
        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_policy');
        }
    }

    public function actionProductQuickView() {
        $product_id = $_POST['product_id'];
        $model = Products::model()->findByPk($product_id);
        $this->renderPartial('_quickview', array('data' => $model));
    }

    public function actionorder_giftoption() {
        $model = Products::model()->findAllByAttributes(array('gift_option' => 1, 'status' => 1));
        $this->render('order_giftoption', array('model' => $model));
    }

    public function encrypt_decrypt($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';

// hash
        $key = hash('sha256', $secret_key);

// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    public function siteURL() {
        $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
        $domainName = $_SERVER['HTTP_HOST'];
        return $protocol . $domainName;
    }

}
