<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$user = dirname(dirname(__FILE__));
Yii::setPathOfAlias('user', $user);
Yii::setPathOfAlias('captcha', dirname(__FILE__) . '/../extensions/captchaExtended-1.0.2');
return array(
    'timeZone' => 'Asia/Calcutta',
    'basePath' => dirname($user),
    'runtimePath' => $user . '/runtime',
    'controllerPath' => $user . '/controllers',
    'viewPath' => $user . '/views',
    'name' => 'Soles iLove',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'user.components.*',
        'user.controllers.*',
        'user.views.*',
        'user.extensions.captcha.CaptchaExtendedAction',
        'user.extensions.captcha.CaptchaExtendedValidator',
        'application.user.extensions.yii-mail.*',
         'application.extensions.eway.*',
    ),
    'modules' => array(
// uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'gii',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'Smtpmail' => array(
            'class' => 'application.components.smtpmail.PHPMailer',
            'Host' => "smtp.gmail.com",
            'Username' => 'dhanya@intersmart.in',
            'Password' => '123@dhanya',
            'Mailer' => 'smtp',
            'Port' => 465,
            'SMTPAuth' => true,
        ),
        'mail' => array(
            'class' => 'application.user.extensions.yii-mail.YiiMail',
            // 'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'mail.solesilove.com.au',
                'Username' => 'no-reply@solesilove.com.au',
                'Password' => 'L%^8lVLgqZcm',
                'port' => '25', // ssl port for gmail
            ),
            'viewPath' => 'application.user.template',
            'logging' => true,
            'dryRun' => false
        ),
        'clientScript' => array(
            'scriptMap' => array(
                'jquery.js' => false,
                'jquery.min.js' => false,
            ),
        /* 'packages' => array(
          'jquery' => array(
          'baseUrl' => '//ajax.googleapis.com/ajax/libs/jquery/1.11.3/',
          'js' => array('jquery.min.js'),
          )
          ), */
        // other clientScript config
        ),
        'widgetFactory' => array(
            'widgets' => array(
                'CLinkPager' => array(
                    'header' => '<div class="pagination pagination-centered">',
                    'footer' => '</div>',
                    'selectedPageCssClass' => 'active',
                    'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true" style="font-size: 15px;font-weight:bold;"></i>',
                    'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true" style="font-size: 15px;font-weight:bold;"></i>',
                    'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true" style="font-size: 15px;font-weight:bold;"></i>',
                    'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true" style="font-size: 15px;font-weight:bold;"></i>',
                    'hiddenPageCssClass' => 'disabled',
                    'htmlOptions' => array(
                        'class' => '',
                    )
                )
            )
        ),
        'Upload' => array('class' => 'UploadFile'),
        'Currency' => array('class' => 'Converter'),
        'Discount' => array('class' => 'DiscountPrice'),
        'Menu' => array('class' => 'MenuCategory'),
        'user' => array(
// enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(

            'urlFormat' => 'path',
            
            'showScriptName'=>false,
            'rules' => array(
                'Profile' => 'Myaccount/Profile',
                'OrderHistory' => 'Myaccount/Myordernew',
                'Wishlists' => 'Myaccount/Mywishlists',
                'Measurement' => 'Myaccount/SizeChartType',
                'CreditHistory' => 'MyWallet/CreditHistory',
                'AddCredit' => 'MyWallet/index',
                'new-user' => 'site/register',
                'search' => 'searching/SearchList',
                'category/<name:\w+(-\w+)*>' => 'products/category/<name:\w+(-\w+)*>',
                'product/<name:\w+(-\w+)*>' => 'products/detail/<name:\w+(-\w+)*>',
                'brand/<name:\w+(-\w+)*>' => 'brands/list/<name:\w+(-\w+)*>',
                'update/<name:\w+(-\w+)*>' => 'products/updateproduct/<name:\w+(-\w+)*>',
//                'update/<name:\w+(-\w+)*>/<cart:\w+(-\w+)*>' => 'products/updateproduct/<name:\w+(-\w+)*>/<cart:\w+(-\w+)*>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),

        ),
        // database settings are configured in database.php
        'db' => require(dirname(__FILE__) . '/database.php'),
//        'db' => array(
//            'connectionString' => 'mysql:host=localhost;dbname=vijaymasala',
//            'emulatePrepare' => true,
//            'username' => 'root',
//            'password' => 'mysql',
//            'charset' => 'utf8',
//        ),
        'errorHandler' => array(
// use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ), 
                // uncomment the following to show log messages on web pages
                array(
                    'class' => 'CWebLogRoute',
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
// using Yii::app()->params['paramName']
    'params' => array(
// this is used in contact page
        'adminEmail' => 'webmaster@example.com',
       
    ),
);

