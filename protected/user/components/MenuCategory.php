<?php

class MenuCategory extends CApplicationComponent {

    public function AllProducts($cats, $parent, $categ, $min_, $max_, $pagesize) {
        if (!empty($min_) && !empty($max_)) {
            $min = $this->currencychange($min_);
            $max = $this->currencychange($max_);
        }
        if (!empty($cats)) {
            $find_ids = $this->ids($cats, $parent, $categ);
        } else {
            $find_ids[0] = $parent->id;
        }

        if (!empty($categ)) {
            $srt = $this->sorting($categ);
        } else {
            $srt = 'id DESC';
        }
        if (!empty($find_ids) || $find_ids != '') {
            $find_in_set = '';
            foreach ($find_ids as $find_id) {
                if ($find_id != '') {
                    $find_in_set .= "FIND_IN_SET('$find_id',`category_id`) OR ";
                }
            }
        }

        $date = date('Y-m-d');
        $prod_ids = DealProducts::model()->findByAttributes(array('date' => $date))->deal_products;
        $ids = explode(',', $prod_ids);
        $nprod_ids = "";
        foreach ($ids as $id) {
            if ($id != '') {
                $optionexists = OptionDetails::model()->findAllByAttributes(array('product_id' => $id));
                foreach ($optionexists as $option_exist) {
                    $total_stock += $option_exist->stock;
                    $out_stock +=$option_exist->status;
                }
                if ($total_stock == 0 || $out_stock == 0) {

                } else {
                    $nprod_ids .= "$id" . ',';
                }
            }
        }
        $nprod_ids = rtrim($nprod_ids, ",");

        if ($nprod_ids != "") {
            $product_id = 'id not in(' . $nprod_ids . ') ';
        } else {

        }
        $find_in_set = rtrim($find_in_set, ' OR');
        $product_id = rtrim($product_id, ' OR');

        if (!empty($prod_ids)) {
            $condition = '(' . $product_id . ') AND (' . $find_in_set . ')';
        } else {
            $condition = '(' . $find_in_set . ')';
        }


        if ($find_in_set != '') {
            $product = Products::model()->findAll("(" . $condition . ") AND status = 1");
            return $product;
        } else {
            return $product;
        }
    }
 
    public function MenuCategories($cats, $srt, $parent, $categ, $min_, $max_, $pagesize) {
        if (!empty($min_) && !empty($max_)) {
            $min = $this->currencychange($min_);
            $max = $this->currencychange($max_);
        } 
        if (!empty($cats)) {
            $find_ids = $this->ids($cats, $parent, $categ);
        } else {
            $find_ids[0] = $parent->id;
        }

//        if (!empty($categ)) {
//            $srt = $this->sorting($categ);
//        } else {
//            $srt = 'id DESC';
//        }
        if (!empty($find_ids) || $find_ids != '') {
            $find_in_set = '';
            foreach ($find_ids as $find_id) {
                if ($find_id != '') {
                    $find_in_set .= "FIND_IN_SET('$find_id',`category_id`) OR ";
                }
            }
        }
        $date = date('Y-m-d');
        $prod_ids = DealProducts::model()->findByAttributes(array('date' => $date))->deal_products;
        $ids = explode(',', $prod_ids);
        $nprod_ids = "";
        foreach ($ids as $id) {
            if ($id != '') {
                $optionexists = OptionDetails::model()->findAllByAttributes(array('product_id' => $id));
                foreach ($optionexists as $option_exist) {
                    $total_stock += $option_exist->stock;
                    $out_stock +=$option_exist->status;
                }
                if ($total_stock == 0 || $out_stock == 0) {

                } else {
                    $nprod_ids .= "$id" . ',';
                }
            }
        }
        $nprod_ids = rtrim($nprod_ids, ",");

        if ($nprod_ids != "") {
            $product_id = 'id not in(' . $nprod_ids . ') ';
        } else {

        }
        $find_in_set = rtrim($find_in_set, ' OR');
        $product_id = rtrim($product_id, ' OR');
//AND (price > ' . $min . ' AND price <' . $max . ')
        if (!empty($prod_ids)) {
            $rty = 1;
            if (!empty($find_in_set) && !empty($prod_ids)) {
                $condition = '(' . $find_in_set . ') AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($prod_ids)) {
                $condition = '(' . $find_in_set . ')  AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($categ) && !empty($prod_ids)) {
                $condition = '(' . $find_in_set . ') AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($prod_ids)) {
                $condition = '(' . $find_in_set . ') AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($prod_ids)) {
                $condition = '(' . $product_id . ') AND (' . $find_in_set . ')';
                $order = 'RAND()';
            }
        } else {
            // echo $min;
            // exit;
            if (!empty($find_in_set) && !empty($size)) {
                $rty = 10;

                $condition = '(' . $find_in_set . ')';
                $order = '';
            } elseif (!empty($find_in_set)) {
                $rty = 20;

                $condition = '(' . $find_in_set . ') ';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($categ) && !empty($prod_ids)) {
                $rty = 30;

                $condition = '(' . $find_in_set . ') AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($size)) {
                $rty = 40;
                $condition = '(' . $find_in_set . ')';
                $order = '';
            } elseif (!empty($find_in_set)) {

                $rty = 50;
                $condition = $find_in_set;
                $order = 'id DESC';
            }
        }
        $add_condition = '';
        if (isset(Yii::app()->session['width_fitting'])) {
            $widt_fitting = rtrim(Yii::app()->session['width_fitting'], '|');
            $exp_width = explode(',', $widt_fitting);
            $width_condition = '';
            foreach ($exp_width as $width) {
                if ($width != '') {
                    $width_condition .= "FIND_IN_SET('$width',`width_fittings`) OR ";
                }
            }
            $width_condition = rtrim($width_condition, ' OR');
            if ($width_condition != '') {
                $add_condition.= " AND (" . $width_condition . ")";
            }
        }


        if (isset(Yii::app()->session['toy_style'])) {
            $toy_style = rtrim(Yii::app()->session['toy_style'], '|');
            $exp_toy = explode(',', $toy_style);
            $toy_condition = '';
            foreach ($exp_toy as $toy) {
                if ($toy != '') {
                    $toy_condition .= "FIND_IN_SET('$toy',`toy_style`) OR ";
                }
            }
            $toy_condition = rtrim($toy_condition, ' OR');
            if ($toy_condition != '') {
                $add_condition.= " AND (" . $toy_condition . ")";
            }
        }
        if (Yii::app()->session['min'] != 0 && Yii::app()->session['max'] != 0) {
            $min = Yii::app()->session['min'];
            $max = Yii::app()->session['max'];
            $price_condition = "price >= " . $min . "  AND price <= " . $max;
            if ($price_condition != '') {
                $add_condition.= " AND (" . $price_condition . ")";
            }
        }
        if (isset(Yii::app()->session['color'])) {
            $color = rtrim(Yii::app()->session['color'], '|');
            $exp_color = explode(',', $color);
            $exp_color = array_filter($exp_color);
            $color_condition = '';
            foreach ($exp_color as $cl) {
                $all_colors = OptionDetails::model()->findAllByAttributes(array('color_id' => $cl));
                foreach ($all_colors as $all_color) {
                    $color_products[] = $all_color->product_id;
                }
            }
            if (!empty($color_products)) {
                $color_products = array_unique($color_products);
                $imp_value = implode(',', $color_products);
                if ($color_products != NULl) {
                    $color_condition = "`id` IN (" . $imp_value . ")";
                }
                if ($color_condition != '') {
                    $add_condition.= " AND (" . $color_condition . ")";
                }
            }
        }

        if (isset(Yii::app()->session['size'])) {
            $size = rtrim(Yii::app()->session['size'], '|');
            $exp_size = explode(',', $size);
            $exp_size = array_filter($exp_size);
            $size_condition = '';
            foreach ($exp_size as $si) {
                if ($si != '') {
                    $all_sizes = OptionDetails::model()->findAllByAttributes(array('size_id' => $si));
                    foreach ($all_sizes as $all_size) {
                        $size_products[] = $all_size->product_id;
                    }
                }
            }
//            var_dump($exp_size);
            if (!empty($size_products)) {


                $size_products = array_unique($size_products);
                $imp_value_size = implode(',', $size_products);
                if ($size_products != NULl) {
                    $size_condition = "`id` IN (" . $imp_value_size . ")";
                }
                if ($size_condition != '') {
                    $add_condition.= " AND (" . $size_condition . ")";
                }
            }
        }

        if ($find_in_set != '') {
            $final_condition = "(" . $condition . ") AND status = 1" . $add_condition;
            return $final_condition;
        } else {
            return $final_condition = '';
        }
    }

   public function SearchByCategory($cats, $parent, $categ, $min_, $max_, $pagesize) {
        if (!empty($min_) && !empty($max_)) {
            $min = $this->currencychange($min_);
            $max = $this->currencychange($max_);
        }
       // return $cats.'--'.$parent.'--'. $categ;
        if (!empty($cats)) {
            $find_ids = $this->idso($cats, $parent, $categ);
        } else {
            $find_ids[0] = $parent->id;
        }
//return $find_ids;
        if (!empty($find_ids) || $find_ids != '') {
            $find_in_set = '';
            foreach ($find_ids as $find_id) {
                if ($find_id != '') {
                    $find_in_set .= "FIND_IN_SET('$find_id',`category_id`) OR ";
                }
            }
        }
        $date = date('Y-m-d');
        $prod_ids = DealProducts::model()->findByAttributes(array('date' => $date))->deal_products;
        $ids = explode(',', $prod_ids);
        $nprod_ids = "";
        foreach ($ids as $id) {
            if ($id != '') {
                $optionexists = OptionDetails::model()->findAllByAttributes(array('product_id' => $id));
                foreach ($optionexists as $option_exist) {
                    $total_stock += $option_exist->stock;
                    $out_stock +=$option_exist->status;
                }
                if ($total_stock == 0 || $out_stock == 0) {

                } else {
                    $nprod_ids .= "$id" . ',';
                }
            }
        }
        $nprod_ids = rtrim($nprod_ids, ",");

        if ($nprod_ids != "") {
            $product_id = 'id not in(' . $nprod_ids . ') ';
        } else {

        }
        $find_in_set = rtrim($find_in_set, ' OR');
        $product_id = rtrim($product_id, ' OR');
//AND (price > ' . $min . ' AND price <' . $max . ')
        if (!empty($prod_ids)) {
            $rty = 1;
            if (!empty($find_in_set) && !empty($prod_ids)) {
                $condition = '(' . $find_in_set . ') AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($prod_ids)) {
                $condition = '(' . $find_in_set . ')  AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($categ) && !empty($prod_ids)) {
                $condition = '(' . $find_in_set . ') AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($prod_ids)) {
                $condition = '(' . $find_in_set . ') AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($prod_ids)) {
                $condition = '(' . $product_id . ') AND (' . $find_in_set . ')';
                $order = 'RAND()';
            }
        } else {
            // echo $min;
            // exit;
            if (!empty($find_in_set) && !empty($size)) {
                $rty = 10;

                $condition = '(' . $find_in_set . ')';
                $order = '';
            } elseif (!empty($find_in_set)) {
                $rty = 20;

                $condition = '(' . $find_in_set . ') ';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($categ) && !empty($prod_ids)) {
                $rty = 30;

                $condition = '(' . $find_in_set . ') AND (' . $product_id . ')';
                $order = '';
            } elseif (!empty($find_in_set) && !empty($size)) {
                $rty = 40;
                $condition = '(' . $find_in_set . ')';
                $order = '';
            } elseif (!empty($find_in_set)) {

                $rty = 50;
                $condition = $find_in_set;
                $order = 'id DESC';
            }
        }
        $add_condition = '';
        if (isset(Yii::app()->session['width_fitting'])) {
            $widt_fitting = rtrim(Yii::app()->session['width_fitting'], '|');
            $exp_width = explode(',', $widt_fitting);
            $width_condition = '';
            foreach ($exp_width as $width) {
                if ($width != '') {
                    $width_condition .= "FIND_IN_SET('$width',`width_fittings`) OR ";
                }
            }
            $width_condition = rtrim($width_condition, ' OR');
            if ($width_condition != '') {
                $add_condition.= " AND (" . $width_condition . ")";
            }
        }


        if (isset(Yii::app()->session['toy_style'])) {
            $toy_style = rtrim(Yii::app()->session['toy_style'], '|');
            $exp_toy = explode(',', $toy_style);
            $toy_condition = '';
            foreach ($exp_toy as $toy) {
                if ($toy != '') {
                    $toy_condition .= "FIND_IN_SET('$toy',`toy_style`) OR ";
                }
            }
            $toy_condition = rtrim($toy_condition, ' OR');
            if ($toy_condition != '') {
                $add_condition.= " AND (" . $toy_condition . ")";
            }
        }
        if (Yii::app()->session['min'] != 0 && Yii::app()->session['max'] != 0) {
            $min = Yii::app()->session['min'];
            $max = Yii::app()->session['max'];
            $price_condition = "price >= " . $min . "  AND price <= " . $max;
            if ($price_condition != '') {
                $add_condition.= " AND (" . $price_condition . ")";
            }
        }
        if (isset(Yii::app()->session['color'])) {
            $color = rtrim(Yii::app()->session['color'], '|');
            $exp_color = explode(',', $color);
            $exp_color = array_filter($exp_color);
            $color_condition = '';
            foreach ($exp_color as $cl) {
                $all_colors = OptionDetails::model()->findAllByAttributes(array('color_id' => $cl));
                foreach ($all_colors as $all_color) {
                    $color_products[] = $all_color->product_id;
                }
            }
            if (!empty($color_products)) {
                $color_products = array_unique($color_products);
                $imp_value = implode(',', $color_products);
                if ($color_products != NULl) {
                    $color_condition = "`id` IN (" . $imp_value . ")";
                }
                if ($color_condition != '') {
                    $add_condition.= " AND (" . $color_condition . ")";
                }
            }
        }

        if (isset(Yii::app()->session['size'])) {
            $size = rtrim(Yii::app()->session['size'], '|');
            $exp_size = explode(',', $size);
            $exp_size = array_filter($exp_size);
            $size_condition = '';
            foreach ($exp_size as $si) {
                if ($si != '') {
                    $all_sizes = OptionDetails::model()->findAllByAttributes(array('size_id' => $si));
                    foreach ($all_sizes as $all_size) {
                        $size_products[] = $all_size->product_id;
                    }
                }
            }
//            var_dump($exp_size);
            if (!empty($size_products)) {


                $size_products = array_unique($size_products);
                $imp_value_size = implode(',', $size_products);
                if ($size_products != NULl) {
                    $size_condition = "`id` IN (" . $imp_value_size . ")";
                }
                if ($size_condition != '') {
                    $add_condition.= " AND (" . $size_condition . ")";
                }
            }
        }

        if ($find_in_set != '') {
            $final_condition = "(" . $condition . ") AND status = 1" . $add_condition;
            return $final_condition;
        } else {
            return $final_condition = '';
        }
    }

    public function SearchByProducts($products, $parent, $categ, $min_, $max_, $pagesize) {
        if (!empty($min_) && !empty($max_)) {
            $min = $this->currencychange($min_);
            $max = $this->currencychange($max_);
        }

        if (!empty($products) || $products != '') {
            $find_in_set = '';
            foreach ($products as $find_id) {
                if ($find_id != '') {
                    $find_in_set .= "FIND_IN_SET('$find_id',`id`) OR ";
                }
            }
        }
        $find_in_set = rtrim($find_in_set, ' OR');
        if ($find_in_set != '') {
            if (!empty($find_in_set)) {
                $condition = '(' . $find_in_set . ')';
            }
        }
        $add_condition = '';
        if (isset(Yii::app()->session['width_fitting'])) {
            $widt_fitting = rtrim(Yii::app()->session['width_fitting'], '|');
            $exp_width = explode(',', $widt_fitting);
            $width_condition = '';
            foreach ($exp_width as $width) {
                if ($width != '') {
                    $width_condition .= "FIND_IN_SET('$width',`width_fittings`) OR ";
                }
            }
            $width_condition = rtrim($width_condition, ' OR');
            if ($width_condition != '') {
                $add_condition.= " AND (" . $width_condition . ")";
            }
        }
        if (isset(Yii::app()->session['toy_style'])) {
            $toy_style = rtrim(Yii::app()->session['toy_style'], '|');
            $exp_toy = explode(',', $toy_style);
            $toy_condition = '';
            foreach ($exp_toy as $toy) {
                if ($toy != '') {
                    $toy_condition .= "FIND_IN_SET('$toy',`toy_style`) OR ";
                }
            }
            $toy_condition = rtrim($toy_condition, ' OR');
            if ($toy_condition != '') {
                $add_condition.= " AND (" . $toy_condition . ")";
            }
        }
        if (Yii::app()->session['min'] != 0 && Yii::app()->session['max'] != 0) {
            $min = Yii::app()->session['min'];
            $max = Yii::app()->session['max'];
            $price_condition = "price >= " . $min . "  AND price <= " . $max;
            if ($price_condition != '') {
                $add_condition.= " AND (" . $price_condition . ")";
            }
        }
        if (isset(Yii::app()->session['color'])) {
            $color = rtrim(Yii::app()->session['color'], '|');
            $exp_color = explode(',', $color);
            $exp_color = array_filter($exp_color);
            $color_condition = '';
            foreach ($exp_color as $cl) {
                $all_colors = OptionDetails::model()->findAllByAttributes(array('color_id' => $cl));
                foreach ($all_colors as $all_color) {
                    $color_products[] = $all_color->product_id;
                }
            }
            if (!empty($color_products)) {
                $color_products = array_unique($color_products);
                $imp_value = implode(',', $color_products);
                if ($color_products != NULl) {
                    $color_condition = "`id` IN (" . $imp_value . ")";
                }
                if ($color_condition != '') {
                    $add_condition.= " AND (" . $color_condition . ")";
                }
            }
        }

        if (isset(Yii::app()->session['size'])) {
            $size = rtrim(Yii::app()->session['size'], '|');
            $exp_size = explode(',', $size);
            $exp_size = array_filter($exp_size);
            $size_condition = '';
            foreach ($exp_size as $si) {
                if ($si != '') {
                    $all_sizes = OptionDetails::model()->findAllByAttributes(array('size_id' => $si));
                    foreach ($all_sizes as $all_size) {
                        $size_products[] = $all_size->product_id;
                    }
                }
            }
//            var_dump($exp_size);
            if (!empty($size_products)) {


                $size_products = array_unique($size_products);
                $imp_value_size = implode(',', $size_products);
                if ($size_products != NULl) {
                    $size_condition = "`id` IN (" . $imp_value_size . ")";
                }
                if ($size_condition != '') {
                    $add_condition.= " AND (" . $size_condition . ")";
                }
            }
        }

        if ($find_in_set != '') {
            $final_condition = "(" . $condition . ") AND status = 1" . $add_condition;
            return $final_condition;
        } else {
            return $final_condition = '';
        }
    }

    public function SearchByBrands($products, $parent, $categ, $min_, $max_, $pagesize) {
        if (!empty($min_) && !empty($max_)) {
            $min = $this->currencychange($min_);
            $max = $this->currencychange($max_);
        }

        if (!empty($products) || $products != '') {
            $find_in_set = '';
            foreach ($products as $find_id) {
                if ($find_id != '') {
                    $find_in_set .= "FIND_IN_SET('$find_id',`id`) OR ";
                }
            }
        }
        $find_in_set = rtrim($find_in_set, ' OR');
        if ($find_in_set != '') {
            if (!empty($find_in_set)) {
                $condition = '(' . $find_in_set . ')';
            }
        }
        $add_condition = '';
        if (isset(Yii::app()->session['width_fitting'])) {
            $widt_fitting = rtrim(Yii::app()->session['width_fitting'], '|');
            $exp_width = explode(',', $widt_fitting);
            $width_condition = '';
            foreach ($exp_width as $width) {
                if ($width != '') {
                    $width_condition .= "FIND_IN_SET('$width',`width_fittings`) OR ";
                }
            }
            $width_condition = rtrim($width_condition, ' OR');
            if ($width_condition != '') {
                $add_condition.= " AND (" . $width_condition . ")";
            }
        }
        if (isset(Yii::app()->session['toy_style'])) {
            $toy_style = rtrim(Yii::app()->session['toy_style'], '|');
            $exp_toy = explode(',', $toy_style);
            $toy_condition = '';
            foreach ($exp_toy as $toy) {
                if ($toy != '') {
                    $toy_condition .= "FIND_IN_SET('$toy',`toy_style`) OR ";
                }
            }
            $toy_condition = rtrim($toy_condition, ' OR');
            if ($toy_condition != '') {
                $add_condition.= " AND (" . $toy_condition . ")";
            }
        }
        if (Yii::app()->session['min'] != 0 && Yii::app()->session['max'] != 0) {
            $min = Yii::app()->session['min'];
            $max = Yii::app()->session['max'];
            $price_condition = "price >= " . $min . "  AND price <= " . $max;
            if ($price_condition != '') {
                $add_condition.= " AND (" . $price_condition . ")";
            }
        }
        if (isset(Yii::app()->session['color'])) {
            $color = rtrim(Yii::app()->session['color'], '|');
            $exp_color = explode(',', $color);
            $exp_color = array_filter($exp_color);
            $color_condition = '';
            foreach ($exp_color as $cl) {
                $all_colors = OptionDetails::model()->findAllByAttributes(array('color_id' => $cl));
                foreach ($all_colors as $all_color) {
                    $color_products[] = $all_color->product_id;
                }
            }
            if (!empty($color_products)) {
                $color_products = array_unique($color_products);
                $imp_value = implode(',', $color_products);
                if ($color_products != NULl) {
                    $color_condition = "`id` IN (" . $imp_value . ")";
                }
                if ($color_condition != '') {
                    $add_condition.= " AND (" . $color_condition . ")";
                }
            }
        }

        if (isset(Yii::app()->session['size'])) {
            $size = rtrim(Yii::app()->session['size'], '|');
            $exp_size = explode(',', $size);
            $exp_size = array_filter($exp_size);
            $size_condition = '';
            foreach ($exp_size as $si) {
                if ($si != '') {
                    $all_sizes = OptionDetails::model()->findAllByAttributes(array('size_id' => $si));
                    foreach ($all_sizes as $all_size) {
                        $size_products[] = $all_size->product_id;
                    }
                }
            }
//            var_dump($exp_size);
            if (!empty($size_products)) {


                $size_products = array_unique($size_products);
                $imp_value_size = implode(',', $size_products);
                if ($size_products != NULl) {
                    $size_condition = "`id` IN (" . $imp_value_size . ")";
                }
                if ($size_condition != '') {
                    $add_condition.= " AND (" . $size_condition . ")";
                }
            }
        }

        if ($find_in_set != '') {
            $final_condition = "(" . $condition . ") AND status = 1" . $add_condition;
            return $final_condition;
        } else {
            return $final_condition = '';
        }
    }

    public function ids($cats, $parent, $categ) {
        $ids = array();
        foreach ($cats as $cat) {

            $subcats = ProductCategory::model()->findAllByattributes(array('parent' => $parent->id));

            if (!empty($subcats) || $subcats != '') {

                foreach ($subcats as $subcat) {
                    //  $_SESSION['category']['0'] = '';
                    $vals = $this->selectCategory($subcat, $parent->id);
                    if (!empty($vals) || $vals != '') {
                        foreach ($vals as $val) {
                            if (!in_array($val, $ids)) {
                                array_push($ids, $val);
                            }
                        }
                    }

                    $find_ids = $ids;
                }
            }
        }

        $cat_details = ProductCategory::model()->findByPk($parent->id);
        $vals = $this->selectCategory($cat_details, $parent->id);
        if (!empty($vals) || $vals != '') {
            foreach ($vals as $val) {
                if (!in_array($val, $ids)) {
                    array_push($ids, $val);
                }
            }
        }
        return $ids;
    }
      public function idso($cats, $parent, $categ) {
        $ids = array();
        foreach ($cats as $cat) {

            $subcats = ProductCategory::model()->findAllByattributes(array('parent' => $parent->id));

            if (!empty($subcats) || $subcats != '') {
                $i=0;
                foreach ($subcats as $subcat) {
                    //  $_SESSION['category']['0'] = '';
                    // $vals = $this->selectCategory($subcat, $parent->id);
                    // if (!empty($vals) || $vals != '') {
                    //     foreach ($vals as $val) {
                    //         if (!in_array($val, $ids)) {
                    //             array_push($ids, $val);
                    //         }
                    //     }
                    // }

                    //$find_ids = $ids;
                    $ids[$i] = $subcat->id;
               $i++; }
            }
        }

        // $cat_details = ProductCategory::model()->findByPk($parent->id);
        // $vals = $this->selectCategory($cat_details, $parent->id);
        // if (!empty($vals) || $vals != '') {
        //     foreach ($vals as $val) {
        //         if (!in_array($val, $ids)) {
        //             array_push($ids, $val);
        //         }
        //     }
        // }
        return $ids;
    }

    public function sorting($categ) {
        if ($categ == 1) {
            $srt = 'id DESC';
        } elseif ($categ == 2) {
            $srt = 'price ASC';
        } elseif ($categ == 3) {
            $srt = 'price DESC';
        } elseif ($categ == 4) {
            $srt = 'product_name ASC';
        } elseif ($categ == 5) {
            $srt = 'product_name DESC';
        } elseif ($categ == 6) {
            $srt = 'id DESC';
        } else {
            $srt = 'id DESC';
        }
        return $srt;
    }

    public function selectCategory($data, $id) {
        $index = count($_SESSION['category_new']);
        
        if ($data->id == $id) {
            $_SESSION['category_new'][$index + 1] = $data->id;
        } else {
            $results = ProductCategory::model()->findByPk($data->parent);
            $_SESSION['category_new'][$index + 1] = $data->id;
            return $this->selectCategory($results, $id);
        }
        $return = array();
        $category_arr = array_reverse($_SESSION['category_new']);
        foreach ($category_arr as $cat) {
            array_push($return, $cat);
        }
        return $return;
    }

    public function findParent($parent) {
        $master = ProductCategory::model()->findByPk($parent);
        if ($master != '') {
            if ($master->id == $master->parent) {
                return $master->id;
            } else {
                return $this->findParent($master->parent);
            }
        } else {
            return $parent;
        }
    }

    public function GiftOptionFilter($min, $max, $size_type) {

        if (!empty($min) && !empty($max) && !empty($size_type)) {

            $condition = '(id  IN (SELECT product_id FROM option_details WHERE size_id = ' . $size_type . '))  AND (price > ' . $min . ' AND price <' . $max . ' AND gift_option = 1)';
            $order = '';
        } elseif (!empty($min) && !empty($max)) {
            $condition = '(price > ' . $min . ' AND price <' . $max . '  AND gift_option = 1)';
            $order = '';
        } else {

        }
        return $dataProvider = new CActiveDataProvider('Products', array(
            'criteria' => array(
                'condition' => $condition,
            ),
            'pagination' => array(
                'pageSize' => 20,
            ),
            'sort' => array(
            )
                )
        );
    }

    public function currencychange($price) {
        if (Yii::app()->session['currency'] != '') {
            if (Yii::app()->session['currency']['rate'] <= 1) {
                return $price / Yii::app()->session['currency']['rate'];
            } else {
                return $price * Yii::app()->session['currency']['rate'];
            }
        } else {
            return $price;
        }
    }

}
