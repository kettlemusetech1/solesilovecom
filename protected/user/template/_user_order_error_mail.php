<?php echo $this->renderPartial('//site/mail/_email_header'); ?>

<tr>
        <td valign="top">
                <h1 style="font-size:13px;font-weight:normal;line-height:22px;margin:13px 0 12px 9px;text-align:left;">Dear <span style="font-weight: bold;"><?php echo $userdetails->first_name; ?><?php echo $userdetails->last_name; ?> , <br/><br/>Greetings from Solesilove.com.au!</span>,
                        <span style="float: right;font-size: 13px;padding: 10px;font-weight: bold; padding-top: 0px;">Order ID #<?php echo $order->id; ?></span>
                </h1>
                <p style="font-size:13px;line-height:16px;margin: 0px 12px 8px 9px;text-align:left;">
                        We are unable to receive your payment at Solesilove.com.au due to Transaction Failure.
                </p>
                <p style="font-size:13px;line-height:16px;margin: 0px 12px 8px 9px;text-align:left;">
                        We invite you to visit https://solesilove.com.au once more to place this order, using credit card or online bank account or SolesiLove credit amount.
                </p>

        </td>
</tr>
<tr>
        <th  bgcolor="#d2d2d2" style="    font-family: 'Open Sans',arial, sans-serif;font-size:13px;padding:20px;line-height:1em">ORDER DETAILS</th>

</tr>
<tr>
        <td>
                <table cellspacing="0" cellpadding="0" border="0" width="610" style="    font-family: 'Open Sans',arial, sans-serif;font-size: 13px;">
                        <thead>
                                <tr>
                                        <th align="left" width="325" bgcolor="#EAEAEA" style="    font-family: 'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Billing Information:</th>
                                        <th width="10"></th>
                                        <th align="left" width="325" bgcolor="#EAEAEA" style="font-family:'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Payment Method:</th>
                                </tr>
                        </thead>
                        <tbody>
                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                <?php echo $bill_address->first_name; ?>   <?php echo $bill_address->last_name; ?>  <br>
                                               <?php echo $bill_address->address_1; ?> <br>
                                                <?php
                                                if ($bill_address->address_2) {
                                                        echo $bill_address->address_2;
                                                        ?> <br><?php } ?>
                                                <?php echo $bill_address->city; ?><br>
                                                <?php echo $bill_address->state; ?><br>
                                                <?php echo $bill_address->postcode; ?><br>
                                                <?php echo Countries::model()->findByPk($bill_address->country)->country_name; ?><br>
                                                +<?php echo Countries::model()->findByPk($bill_address->country)->phonecode . ' '; ?><?php echo $bill_address->contact_number; ?><br/>


                                        </td>
                                        <td>&nbsp;</td>
                                        <td valign="top" style="font-family: 'Open Sans',arial, sans-serif;font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                <p style="text-transform: uppercase;font-weight: bold;padding-top:20px;">pay via <?php
                                                        if ($order->payment_mode == 1) {
                                                                echo "SolesiLove Credit";
                                                        } elseif ($order->payment_mode == 2) {
                                                                echo "CREDIT/DEBIT CARD OR NET BANKING";
                                                        } elseif ($order->payment_mode == 3) {
                                                                echo "Paypal";
                                                        } elseif ($order->payment_mode == 4) {
                                                                $wallet_amt = $order->wallet;
                                                                if ($order->netbanking != '') {
                                                                        $payment_amt = $order->netbanking;
                                                                        $method = 'CREDIT/DEBIT CARD OR NET BANKING';
                                                                } else if ($order->paypal != '') {
                                                                        $payment_amt = $order->paypal;
                                                                        $method = 'Paypal';
                                                                }
                                                                echo "<br> Credit = " . $wallet_amt;
                                                                echo "<br>" . $method . " = " . $payment_amt;
                                                        }
                                                        ?></p>



                                        </td>
                                </tr>
                        </tbody>
                </table>
                <br>

                <table cellspacing="0" cellpadding="0" border="0" width="610" style="    font-family: 'Open Sans',arial, sans-serif;font-size: 13px;">
                        <thead>
                                <tr>
                                        <th align="left" width="364" bgcolor="#EAEAEA" style="font-family:'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Shipping Information:</th>
                                        <th width="10"></th>
                                        <th align="left" width="364" bgcolor="#EAEAEA" style="font-family:'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Shipping Method:</th>
                                </tr>
                        </thead>
                        <tbody>
                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                <?php echo $user_address->first_name; ?>   <?php echo $bill_address->last_name; ?><br>
                                                <?php echo $user_address->address_1; ?> <br>
                                                <?php
                                                if ($user_address->address_2) {
                                                        echo $user_address->address_2;
                                                        ?> <br><?php } ?>
                                                <?php echo $user_address->city; ?><br>
                                                <?php echo $user_address->state; ?> <br>
                                                <?php echo $user_address->postcode; ?><br>
                                                <?php echo Countries::model()->findByPk($user_address->country)->country_name; ?><br/>
                                                +<?php echo Countries::model()->findByPk($user_address->country)->phonecode . ' '; ?><?php echo $user_address->contact_number; ?><br/>
 &nbsp;
                                        </td>
                                        <td>&nbsp;</td>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                               <?php
                                                       echo "Delivered within 3-14 working days";
                                                
                                                ?>
                                                &nbsp;
                                        </td>
                                </tr>
                        </tbody>
                </table>
                <br>
                <table cellspacing="0" cellpadding="0" border="0" width="610" style="border:1px solid #eaeaea;font-family: 'Open Sans',arial, sans-serif;">
                        <thead>
                                <tr>
                                        <th align="left" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Item</th>
                                        <th align="left" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Sku</th>
                                        <th align="center" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Qty</th>
                                        <th align="right" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Subtotal</th>
                                </tr>
                        </thead>

                        <tbody bgcolor="#F6F6F6">
                                <?php
                                foreach ($order_details as $orders) {
                                        $product_names = Products::model()->findByAttributes(array('id' => $orders->product_id));
                                        $product_option = OptionDetails::model()->findByAttributes(array('id' => $orders->option_id));
                                        $color = OptionCategory::model()->findByPk($product_option->color_id);
                                        $size = OptionCategory::model()->findByPk($product_option->size_id);
                                        $width = WidthFitting::model()->findByPk($product_option->width_id);
                                        ?>
                                        <tr>
                                                <td align="left" valign="top" style="font-size:11px;padding:3px 9px;padding-top:10px; <?php if ($orders->gift_option == 0) { ?>padding-bottom:10px;border-bottom:1px dotted #cccccc;<?php } ?>">
                                                        <strong style="font-size:11px;text-transform: uppercase;"><?php echo $product_names->product_name; ?></strong>
                                                        <?php
                                                        if ($orders->option_id != '') {
                                                                ?>
                                                                <dl style="margin:0;padding:0">
                                                                        <?php if ($product_option->color_id != '' && $product_option->color_id != 0) {
                                                                                ?>
                                                                                <dt><strong>Color : </strong>
                                                                                <span style="margin:0;padding:0 0 0 9px"><?php echo $color->color_name; ?> </span></dt>
                                                                                <?php
                                                                        }
                                                                        if ($product_option->size_id != '' && $product_option->size_id != 0) {
                                                                                ?>
                                                                                <dt><strong>Size &nbsp;&nbsp;: </strong>
                                                                                <span style="margin:0;padding:0 0 0 9px"><?php echo $size->size; ?></span></dt>
                                                                        <?php }if ($product_option->width_id != '' && $product_option->width_id != 0) {
                                                                                ?>
                                                                                <dt><strong>Width &nbsp;&nbsp;: </strong>
                                                                                <span style="margin:0;padding:0 0 0 9px"><?php echo $width->title; ?></span></dt>
                                                                        <?php } ?>
                                                                        <?php if($orders->left_right != ''){?>
                                                                         <dt><strong>Left/Right &nbsp;&nbsp;: </strong>
                                                                                <span style="margin:0;padding:0 0 0 9px"><?php echo $orders->left_right; ?></span></dt>
                                                                        <?php } ?>
                                                                </dl>
                                                        <?php } ?>
                                                </td>
                                                <td align="left" valign="top" style="font-size:11px;padding:3px 9px;padding-top:10px; <?php if ($orders->gift_option == 0) { ?>padding-bottom:10px;border-bottom:1px dotted #cccccc;<?php } ?>"><?php echo $product_names->sku; ?></td>
                                                <td align="center" valign="top" style="font-size:11px;padding:3px 9px;padding-top:10px; <?php if ($orders->gift_option == 0) { ?>padding-bottom:10px;border-bottom:1px dotted #cccccc;<?php } ?>"><?php echo $orders->quantity; ?></td>
                                                <td align="right" valign="top" style="font-size:11px;padding:3px 9px;padding-top:10px; <?php if ($orders->gift_option == 0) { ?>padding-bottom:10px;border-bottom:1px dotted #cccccc;<?php } ?>">


                                                        <span><?php echo Yii::app()->Currency->convertCurrencyCode($orders->amount); ?></span>                                        </td>
                                        </tr>
                                        <?php if ($orders->gift_option == 1) { ?>
                                                <tr>
                                                        <td colspan="3" style="padding: 0px 0px 10px 10px;text-transform: uppercase;font-size: 10px;font-weight: bold;border-bottom:1px dotted #cccccc">Gift Packing</td>
                                                        <td align="right" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;"><?php echo Yii::app()->Currency->convertCurrencyCode($orders->rate); ?></td>
                                                </tr>

                                                <?php
                                        }
                                }
                                ?>

                               <tr>
                                        <?php
                                        foreach ($order_details as $total_order) {
                                                $totorder += $total_order->amount;
                                        }

                                        foreach ($order_details as $giftoption) {
                                                $totgift += $giftoption->rate;
                                        }
                                        $granttotal = $totgift + $totorder;
                                        $total = $granttotal + Order::model()->findByPk($order->id)->shipping_charge;
                                        ?>
                                        <td colspan="3" align="right" style="padding:13px 9px 0 0;font-size:13px;">
                                                Subtotal                    </td>
                                        <td align="right" style="padding:13px 9px 0 0;font-size:13px;">
                                                <span>$ <?php echo $granttotal; ?></span>                    </td>
                                </tr>
                                <tr>
                                        <td colspan="3" align="right" style="padding:3px 9px;font-size:13px;">
                                                Shipping &amp; Handling                    </td>
                                        <td align="right" style="padding:3px 9px;font-size:13px;">
                                                <span>$ <?php echo Order::model()->findByPk($order->id)->shipping_charge; ?></span>                    </td>
                                </tr>
                                
                                <tr>
                                        <td colspan="3" align="right" style="padding:3px 9px;font-size:13px;">
                                                GST <small style="font-size:11px">(<?php echo Settings::model()->findByAttributes(array('id'=>1))->tax_rate;?>)</small> </td>
                                        <td align="right" style="padding:3px 9px;font-size:13px;">
                                                <span>$ <?php echo $this->Tax($total); ?></span>                    </td>
                                </tr>
                                
                                <tr>
                                        <td colspan="3" align="right" style="padding:3px 9px 13px 0;font-size:13px;">
                                                <strong>Total Inc GST</strong>
                                        </td>
                                        <td align="right" style="padding:3px 9px 13px 0;font-size:13px;">
                                                <strong><span>AUD <?php echo $total; ?></span></strong>
                                        </td>
                                </tr>
                        </tbody>
                </table>

                <br>
                <p style="font-size:12px;margin:0 0 10px 0"></p>
        </td>
</tr>



<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>