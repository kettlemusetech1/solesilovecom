<?php echo $this->renderPartial('//site/mail/_email_header'); ?>
<tr>
        <td style="padding:9px 20px 6px; font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;">

                <p>Hi <?php echo $model->first_name; ?><span>      <?php echo $model->last_name; ?></span>,<br/><br/>
                        Welcome to Soles iLove! </p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;">We welcome you to solesilove.com.au</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;">We have received your enquiry and will contact you soon.</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#abaaaa;padding-left: 3px;">* This is an automatically generated email, please do not reply to this email.</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#abaaaa;padding-left: 3px;">* This email address was provided on our registration page. If you own the email and did not register on our site, please send an email to info@solesilove.com.au</p>
                <!-- <hr style="border-color:#404241;">-->
        </td>
</tr>

<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>