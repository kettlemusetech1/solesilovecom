<?php echo $this->renderPartial('//site/mail/_email_header'); ?>
<tr>
        <td align="center" style="padding: 0px 20px;">
                <!--Header Ends Here-->
                <p style="padding: 5px 9px 6px 9px;text-align:center; margin-bottom: 0px; padding-bottom: 0px;font-family:'Open Sans',arial, sans-serif; font-size: 13px;"><strong>Hello from Soles iLove!</span><br/></p>
                <p style="padding: 5px 9px 0px 9px;$clients_meta_fieldstext-align:center; margin-top: 0px; padding-top:0;">You are just one step away from accessing your Soles iLove account.</p>
                <p style="padding: 5px 9px 0px 9px;$clients_meta_fieldstext-align:center; margin-top: 0px; padding-top:0;">To complete the process of creating your account and to verify your email address, please return to our website and enter the following verification code:</p>
                <p style="padding: 5px 9px 0px 9px;$clients_meta_fieldstext-align:center; margin-top: 0px; padding-top:0;">Verification Code: <strong><?php echo $model->verify_code; ?></strong></p>
        </td>
</tr>
<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>