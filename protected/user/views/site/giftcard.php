<section class="gift-wrp">
        <div class="container">
                <ul class="breadcrumbs list-inline">
                        <li><a href="javascript:void(0)">Home</a></li>
                        <li class="active">Giftcard</li>
                </ul>
                <div class="gift-det">
                        <h4>Artstra GiFt Cards</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <ul class="gift-ul list-inline">
                                <li><a href="#" data-toggle="modal" data-target="#buyModal">HOW TO BUY?</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#redeemModal">HOW TO REDEEM GIFT CARD ?</a></li>
                                <!--<li><a href="#" data-toggle="modal" data-target="#faModal">Q & A</a></li>-->
                                <!--<li><a href="#" data-toggle="modal" data-target="#termsModal">TERMS AND CONDITIONS</a></li>-->
                                <li><a href="#" data-toggle="modal" data-target="#normalenquiry">ENQUIRE NOW</a></li>
                        </ul>
                </div>
        </div>
        <div class="modal fade" id="redeemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                        <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4>How to redeem Gift Card</h4><br/>
                                </div>
                                <div class="modal-body">


                                        You can redeem your gift card online and In-store. Here's how:
                                </div>

                        </div>
                </div>
        </div>
        <div class="modal fade" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                        <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4>How to buy Gift Card</h4><br/>
                                </div>
                                <div class="modal-body">


                                        You can redeem your gift card online and In-store. Here's how:
                                </div>

                        </div>
                </div>
        </div>

        <?php if (Yii::app()->user->hasFlash('enuirysuccess')): ?>

                <script>
                        $(document).ready(function() {
                                $('#normalenquirysuccess').modal('show');

                                $("#normalenquirysuccess .modal-body").html('<h4 style="line-height: 25px">We have recieved your enquiry and will respond to you soon on possible. For urgent enquiry please call us directly on +91 9142202222 (Mon to Sat  9.30AM-6.30PM(IST))</h4>');

                        });
                </script>
        <?php endif; ?>
        <div class="modal fade in" id="normalenquirysuccess" tabindex="-2" role="dialog">
                <div class="modal-dialog">
                        <div class="modal-content">
                                <div class="modal-header text-left">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <img class="wmimg1" src="<?php echo yii::app()->baseUrl; ?>/images/watermark-logo.png">
                                        <h4>Send Us A Message</h4>

                                </div>
                                <div class="modal-body">

                                        <h4> <?php echo Yii::app()->user->getFlash('enuirysuccess'); ?></h4>

                                </div>
                        </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->






        <div class="modal fade in" id="normalenquiry" tabindex="-2" role="dialog">
                <div class="modal-dialog">
                        <div class="modal-content">
                                <div class="modal-header text-left">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <img class="wmimg1" src="<?php echo yii::app()->baseUrl; ?>/images/watermark-logo.png">
                                        <h4>Send Us A Message</h4><br/>
                                        <h3>Please provide as much information as possible for us to help you with your enquiry.</h3>
                                        <h3 class="btombrn" style="font-size: 11px;"> Your personal information will be kept confidential and will not be shared with any other third party.</h3>

                                </div>
                                <div class="modal-body">




                                        <?php if (Yii::app()->user->hasFlash('enuirysuccess')): ?>
                                                <div class="alert alert-success">
                                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        <?php echo Yii::app()->user->getFlash('enuirysuccess'); ?>
                                                </div>
                                        <?php endif; ?>
                                        <div class="form">

                                                <?php
                                                $form = $this->beginWidget('CActiveForm', array(
                                                    'action' => Yii::app()->request->baseUrl . '/index.php/site/Giftcard',
                                                    'id' => 'product-enquiry-form',
                                                    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
                                                ));
                                                ?>
                                                <?php
                                                $usr_det = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                                                $model1->phone = $usr_det->phone_no_2;
                                                ?>
                                                <?php $model1->name = $usr_det->first_name; ?>
                                                <?php $model1->email = $usr_det->email; ?>
                                                <?php $model1->country = $usr_det->country; ?>

                                                <div class="row">
                                                        <div class="col-sm-6">

                                                                <?php echo $form->hiddenField($model1, 'product_id', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'value' => $product->id)); ?>
                                                                <?php echo $form->labelEx($model1, 'name'); ?>
                                                                <?php echo $form->textField($model1, 'name', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'placeholder' => 'Name')); ?>
                                                                <span style="color:red;"> <?php echo $form->error($model1, 'name'); ?></span>
                                                        </div>

                                                        <div class="col-sm-6">
                                                                <?php echo $form->labelEx($model1, 'country'); ?>
                                                                <?php echo CHtml::activeDropDownList($model1, 'country', CHtml::listData(Countries::model()->findAll(), 'id', 'country_name'), array('empty' => '--Select Country--', 'class' => 'form-control')); ?>

                                                                <span style="color:red;"> <?php echo $form->error($model1, 'country'); ?></span>
                                                        </div>
                                                </div>

                                                <div class="row">
                                                        <div class="col-sm-6">
                                                                <?php echo $form->labelEx($model1, 'email'); ?>
                                                                <?php echo $form->textField($model1, 'email', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control hello_form', 'placeholder' => 'Email')); ?>
                                                                <span style="color:red;">  <?php echo $form->error($model1, 'email'); ?></span>
                                                        </div>
                                                        <div class="col-sm-3">
                                                                <?php echo $form->labelEx($model1, 'Code'); ?>
                                                                <?php
                                                                $phonecode = Countries::model()->findByPk(Yii::app()->session['user']['country'])->phonecode;
                                                                $state_options = array();

                                                                $states = Countries::model()->findAll();
                                                                if (!empty($states)) {
                                                                        $state_options[""] = "--Select--";
                                                                        foreach ($states as $state) {
                                                                                $state_options[$state->phonecode] = $state->phonecode;
                                                                        }
                                                                } else {
                                                                        $state_options[""] = "--Code--";
                                                                        $state_options[0] = "Other";
                                                                }
                                                                ?>
                                                                <?php echo CHtml::activeDropDownList($model1, 'phonecode', $state_options, array('class' => 'form-control', 'options' => array($phonecode => array('selected' => 'selected')))); ?>
                                                                <?php echo $form->error($model1, 'phonecode'); ?>




                                                        </div>



                                                        <div class="col-sm-3">
                                                                <?php echo $form->labelEx($model1, 'phone'); ?>
                                                                <?php echo $form->textField($model1, 'phone', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'placeholder' => 'Phone Number')); ?>

                                                                <span style="color:red;">  <?php echo $form->error($model1, 'phone'); ?></span>
                                                        </div>



                                                </div>



                                                <div class="row">

                                                        <div class="col-sm-12">
                                                                <?php echo $form->labelEx($model1, 'Enquiry'); ?>
                                                                <?php echo $form->textArea($model1, 'requirement', array('maxlength' => 300, 'class' => 'form-control', 'placeholder' => 'Requirement')); ?>
                                                                <?php echo $form->error($model1, 'requirement'); ?>
                                                        </div>
                                                </div>


                                                <div class="row">
                                                        <div class="col-sm-6">
                                                                <?php echo $form->labelEx($model1, 'verifyCode'); ?><br /><div class="dsl">
                                                                <?php $this->widget("CCaptcha", array('buttonLabel' => '<i class="fa fa-refresh" aria-hidden="true"></i>', 'buttonOptions' => array('style' => 'padding-top: 18px;position: absolute; right: 13px;'))); ?>

                                                                        <?php echo $form->textField($model1, 'verifyCode', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control codez', 'placeholder' => 'Type the captcha shown above', 'autocomplete' => 'off')); ?>
                                                                </div>
                                                                <span style="color:red;">  <?php echo $form->error($model1, 'verifyCode'); ?></span>
                                                        </div>

                                                        <div class="col-sm-6" style="padding-top: 36px;text-align: center;">
                                                                <?php echo CHtml::submitButton($model1->isNewRecord ? 'SUBMIT' : 'Save', array('class' => 'btn btn-primary')); ?>

                                                        </div>
                                                </div>


                                                <?php $this->endWidget(); ?>
                                        </div><!-- form -->
                                </div>
                        </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
        </div>
        <div class="gift-images-main">
                <div class="container">
                        <div class="gift-img-slider">
                                <?php
                                foreach ($model as $gift) {
                                        ?>
                                        <div class="item">
                                                <div class="gift-img-box">
                                                        <div class="gift-img">
                                                                <img src="<?php echo Yii::app()->baseUrl ?>/uploads/gift/<?php echo $gift->id ?>.<?php echo $gift->image ?>">
                                                        </div>
                                                        <?php if (isset(Yii::app()->session['user'])) { ?>
                                                                <a class="butter" href = "<?= Yii::app()->baseUrl; ?>/index.php/Giftcard/index?card_id=<?= $gift->id ?>" > BUY NOW</a>
                                                        <?php } else { ?>
                                                                <a class="butter" data-toggle="modal" onclick="giftcard(id =<?= $gift->id ?>)"  data-target="#login">BUY NOW</a>
                                                        <?php } ?>
                                                </div>
                                        </div>
                                <?php } ?>
                        </div>
                </div>
        </div>
</section>
<script>
        function giftcard(id) {
                $('#login_form').append('<input type="hidden" name="gift_id" id="gift_card" value="' + id + '" />');
        }
</script>