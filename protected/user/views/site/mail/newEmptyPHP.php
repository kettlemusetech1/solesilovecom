<!Doctype html>
<html lang="en">
        <head>
                <title>RCSS</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" href="admission/css/bootstrap.min.css">
                <link rel="stylesheet" href="admission/css/style.css">
                <link rel="stylesheet" type="text/css" href="admission/css/slick.css"/>
                <link rel="stylesheet" type="text/css" href="admission/css/slick-theme.css"/>
                <link rel="stylesheet" type="text/css" href="admission/fonts/font-awesome.min.css">
                <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,200,500' rel='stylesheet' type='text/css'>
                <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700' rel='stylesheet' type='text/css'>
        </head>
        <body>
                <script>
                        (function(i, s, o, g, r, a, m) {
                                i['GoogleAnalyticsObject'] = r;
                                i[r] = i[r] || function() {
                                        (i[r].q = i[r].q || []).push(arguments)
                                }, i[r].l = 1 * new Date();
                                a = s.createElement(o),
                                        m = s.getElementsByTagName(o)[0];
                                a.async = 1;
                                a.src = g;
                                m.parentNode.insertBefore(a, m)
                        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                        ga('create', 'UA-67427887-1', 'auto');
                        ga('send', 'pageview');

                </script>

                <div class="results">
                        <style>
                                .resultsss {
                                        background-color: #fff;
                                        border: 1px solid #ff6666 !important;
                                        color: red;
                                        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
                                        font-size: 16px;
                                        font-weight: bold;
                                        -webkit-box-shadow: 0 10px 17px -6px #717070;
                                        -moz-box-shadow: 0 10px 17px -6px #717070;
                                        box-shadow: 0 10px 17px -6px #717070;
                                        border-radius: 20px 0px 0px 20px !important;
                                        border: 0;
                                        position: fixed;
                                        z-index: 100000;
                                        right: -1px;
                                        top: 40%;
                                }
                        </style>
                        <a class="btn resultsss" href="https://fedena.rajagiri.edu/guest_users/login#openModal" role="button" target="_blank">APPLY NOW</a>
                </div>
                <section>

                        <div class="headercl">
                                <div class="container">
                                        <div class="row">
                                                <div class="col-md-4"> <img src="admission/images/logo.png"> </div>
                                                <div class="col-md-8">
                                                        <h1>ADMISSIONS 2017</h1>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </section>
                <section>
                        <div class="ne">
                                <div class="container">
                                        <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">

                                                                <div><br><br></div>
                                                                <div class="col-md-4 col-xs-12">


                                                                </div>
                                                                <div class="col-md-4 col-xs-12">
                                                                        <a href="https://fedena.rajagiri.edu/guest_users/login#openModal" target="_blank"><img src="admission/images/rounded.png" class="img-responsive round"></a>
                                                                </div>
                                                                <!--<div class="col-md-4 col-xs-12">
                                                               <a href="https://fedena.rajagiri.edu/guest_users/login#openModal" target="_blank"><button type="button" class="btn btn-primary seen pluss">Eligibility Criteria </button></a>
                                                               </div>-->



                                                        </div>



                                                </div>
                                        </div>
                                        <div class="row">
                                                <div class="col-md-12">
                                                        <div class="newbox"> <img src="admission/images/yel.png" class="img-responsive yell">
                                                                <div class="apply">
                                                                        <div class="rajagiri">
                                                                                <div class="item">
                                                                                        <div class="main news"> <img src="admission/images/college.jpg" class="img-responsive frn">
                                                                                                <div class="clss">
                                                                                                        <h3>Rajagiri Immersive Learning Experience</h3>
                                                                                                        <h4>At RCSS, the curriculum is designed to enable students to deveop their competencies and
                                                                                                                capabilities so that they excel in every domain of life. The Rajagiri Immersive Learning Experience
                                                                                                                Methodology is a pedagogical innovation of Rajagiri College of Social Sciences.. Along with academics,
                                                                                                                students are exposed to programmes and activities which prepare them to manage and lead in challenging
                                                                                                                environments. The methodology is based on four dimensions: Conceptual Learning, Experiential Engagement,
                                                                                                                Executive Modeling and Corporate Competency. The various activities under these dimensions help transform
                                                                                                                the Rajagirians into professionally competent and socially sensitive individuals.</h4>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="item">
                                                                                        <div class="main news"> <img src="admission/images/friends2.jpg" class="img-responsive frn">
                                                                                                <div class="clss">
                                                                                                        <h3>Conceptual Learning</h3>
                                                                                                        <h4>Through case discussion, lectures and seminars students are introduced the concepts and
                                                                                                                theoretical framework of their respective subjects. Students are also introduced into the world
                                                                                                                of business through business update sessions and research projects.</h4>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="item">
                                                                                        <div class="main news"> <img src="admission/images/friends3.jpg" class="img-responsive frn">
                                                                                                <div class="clss">
                                                                                                        <h3>Experiential Engagement</h3>
                                                                                                        <h4>The experiential engagement activities help students get immersed into the practical realm
                                                                                                                of their subjects through activities like Rural Sensitization Camp , field work ,
                                                                                                                field projects etc, and students learn from doing experience.</h4>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="item">
                                                                                        <div class="main news"> <img src="admission/images/friends4.jpg" class="img-responsive frn">
                                                                                                <div class="clss">
                                                                                                        <h3>Executive Modeling</h3>
                                                                                                        <h4>RCSS ensures that the students develop the winning abilities and skills to create a
                                                                                                                great impression at work and advance in their professional life. Through various activities
                                                                                                                students are groomed to improve their communication skills, interpersonal skills, analytical
                                                                                                                skills and put on the winning attitude.</h4>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="item">
                                                                                        <div class="main news"> <img src="admission/images/friends5.jpg" class="img-responsive frn">
                                                                                                <div class="clss">
                                                                                                        <h3>Corporate Competency</h3>
                                                                                                        <h4>Students are made industry ready through continuous interaction with industry
                                                                                                                professionals. Summer internship, industry visits and industry interactions give
                                                                                                                students hands on experience on how organizations function and give them rare insights
                                                                                                                into real time managerial issues.</h4>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>

                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>



                                        <div class="another">
                                                <ul>
                                                        <li><a href="http://rcss.rajagiri.edu/index.php/Course/details/id/25" target="_blank">MCA</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/index.php/Course/details/id/4" target="_blank">M.Phil</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/index.php/Course/Details/id/16" target="_blank">MSW</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/Course/details/id/53" target="_blank">MSc(Psy)</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/index.php/Course/Details/id/2" target="_blank">BSW</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/Course/details/id/54" target="_blank">BSc(Psy)</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/index.php/Course/details/id/49" target="_blank">MLISc</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/index.php/Course/details/id/44" target="_blank">BLISc</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/index.php/Course/details/id/46" target="_blank">B.Com</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/index.php/Course/details/id/55" target="_blank">BBA</a></li>
                                                        <li><a href="http://rcss.rajagiri.edu/index.php/Course/details/id/5" target="_blank"> PGDCSW</a></li>
                                                </ul>

                                        </div>



                                </div>
                        </div>
                </section>
                <section>
                        <div class="container">
                                <div class="row">
                                        <div class="col-md-12">
                                                <div class="rcses">
                                                        <h1>General Instructions</h1>
                                                        <div class="rcss-college">
                                                                <ul>
                                                                        <li>RCSS has the right to reject an Application that is incomplete or incorrect.</li>
                                                                        <li>Please be aware that ignorance of the guidelines will not be accepted as an excuse.</li>
                                                                        <li>RCSS reserves the right to change/alter the dates or timing at any given time during the application process.</li>
                                                                        <li>Please have your mark lists ready before you start to proceed to the online application process.</li>
                                                                </ul>
                                                                <p>	You can pay the Non Refundable application fee for the preferred program through any of the following modes given below. </p>
                                                        </div>
                                                        <div class="neft">


                                                                <ol>
                                                                        <li>NEFT</li>

                                                                        <li>Online Payment Gateway option.</br>
                                                                                <ol>	<li>For NEFT Mode : NEFT details are provided in the
                                                                                                Application Fee Page. After the payment fill-in all required
                                                                                                fields provided in the Application Fee Page and also upload a scanned copy of the NEFT payment slip.</li>
                                                                                        <li>	For Online Payment gateway Mode : Candidates should have a valid Credit Card/Debit Card or
                                                                                                Net Banking facility to proceed further.</li>

                                                                        </li>

                                                                </ol>
                                                                <li>Candidate should send the print out of the application form at the earliest, along with the
                                                                        <ol>
                                                                                <li>	Photostat copies of the mark lists</li>
                                                                                <li>	Proof of application fee payment (Original DD / copy of NEFT details) [Not   mandatory for candidates who made online fee payment]</li>
                                                                        </ol>
                                                                </li>

                                                                </ol>
                                                                <p>	Candidates sending the above documents by Speed Post /Registered Post / Courier Service should address the same to:</p>
                                                                <div class="candit">

                                                                        <h6>Admissions Coordinator (Course Name),</h6>
                                                                        <h6>Rajagiri College of Social Sciences
                                                                                Rajagiri P.O,</h6>
                                                                        <h6>Kalamassery, Cochin - 683 104
                                                                                Kerala,
                                                                                India</h6>




                                                                        <p>	Candidates may submit the forms directly at:</p>

                                                                        <h6>RCSS Admissions Helpdesk,</h6>
                                                                        <h6>Rajagiri College of Social Sciences
                                                                                Rajagiri P.O,</h6>
                                                                        <h6>Kalamassery, Cochin - 683 104
                                                                                Kerala,
                                                                                India</h6>



                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                </section>

                <section>
                        <div class="chek">
                                <div class="container">
                                        <div class="row">
                                                <div class="col-md-12">
                                                        <h1>Alumni Testimonials</h1>
                                                        <div class="test"> <img src="admission/images/sample.png" > </div>
                                                </div>
                                        </div>
                                </div>
                                <div class="container">
                                        <div class="featured">
                                                <div class="item">
                                                        <div class="main news"> <img src="admission/images/fig1.jpg" class="img-responsive fn">
                                                                <div class="clss">
                                                                        <p>Rajagiri is an institution that continues to provide a wonderful but different learning and life
                                                                                experience to its students.For a large majority the time spent as student is truly transformational,
                                                                                at personal level and the way one looks at organizations beyond the bottom line.The uniqueness stems
                                                                                from the pedagogical approach that combines science of management with application orientation, allowing
                                                                                students space to question, think and innovate. The environment nurtured inclusiveness and taught students
                                                                                to be agile when they enter the workforce. The college has defenitely shaped the thinking of organizations
                                                                                across the globe, through alumni who occupy positions of influence and the academic extention activities it
                                                                                undertakes for various societal groups and organizations.The leadership and faculty team at Rajagiri continue
                                                                                to be source of inspiration to many who walked the corridors.</p>

                                                                        <p style="font-weight:bold;">Prof. Biju Varkey</p>
                                                                        <h6>Faculty, IIM Ahmedabad</h6>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="item">
                                                        <div class="main news"> <img src="admission/images/fig2.jpg" class="img-responsive fn">
                                                                <div class="clss">
                                                                        <p>Looking back I owe a lot to this fantastic institution Rajagiri for all the fundamental
                                                                                conceptual strength that rajagiri has helped me pick.I think the key differentiator for rajagiri
                                                                                has been the amount of focus on social networking, being able to build social awareness, the focus
                                                                                on co curricular activities and the amount of Industry exposure Rajagiri has given concurrently with
                                                                                the education. </p>

                                                                        <p style="font-weight:bold;">Narayanan Nair</p>
                                                                        <p>Sr. Vice President and Head - International HR, Mphasis</p>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="item">
                                                        <div class="main news"> <img src="admission/images/fig3.jpg" class="img-responsive fn">
                                                                <div class="clss">
                                                                        <p>When a lot of my friends chose to do their MA in Economics,
                                                                                History, and other Social Sciences, I decided to go for Management
                                                                                Programme at Rajagiri knowing that it is a road less travelled. The
                                                                                programme gave me a lot of confidence, an entirely different perspective
                                                                                to life.The is where i learnt my basics of handling people, Human Resource
                                                                                Management. Dedicated Faculty, excellent facilities, good student body,
                                                                                all that made my stay at Rajagiri truly memorable and enjoyable </p>

                                                                        <p style="font-weight:bold;">Ninan Thariyan</p>
                                                                        <p>CEO, Daily Thanthi</p>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="item">
                                                        <div class="main news"> <img src="admission/images/fig5.png" class="img-responsive fn">
                                                                <div class="clss">
                                                                        <p>"The uniqueness in imparting learning from real
                                                                                life situations will enable the students to contribute
                                                                                from day one of taking up a job. The course curriculam
                                                                                is very relavant to the requirements of the industry"
                                                                        </p>

                                                                        <p style="font-weight:bold;">Issac Varghese</p>
                                                                        <p>Leader- People & Organisation, EY</p>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="item">
                                                        <div class="main news"> <img src="admission/images/fig6.png" class="img-responsive fn">
                                                                <div class="clss">
                                                                        <p>Rajagiri has given me the most valuable skill that has helped me so
                                                                                far- It's called Confidence. Of Course the top Class facilities and Faculty
                                                                                deserves special mention. The High Quality Industry Expert Panel that the college
                                                                                arranges for the students helped immensely to gain a practical (real-world) exposure.
                                                                                No Wonder Rajagirians are successful across various industries and disciplines.
                                                                                Proud to be a Rajagirian.</p>
                                                                        <p style="font-weight:bold;">Shabeer Mohamed K (Rajagiri MBA 1996-1998)</p>
                                                                        <p>Country Manager - Software Subscription and Support, IBM India</p>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="item">
                                                        <div class="main news"> <img src="admission/images/fig10.png" class="img-responsive fn">
                                                                <div class="clss">
                                                                        <p>What stands out in the mind about the days in Rajagiri is the opportunity
                                                                                I got for balanced all round development. To top it the college gave immense
                                                                                opportunity to take up leadership opportunities to hone skills and develop empathy
                                                                                through volunteering opportunities.When I reflect back, the way we were organized
                                                                                those years really represented a microcosm of a very collaborative large team with
                                                                                mentors and guides available to us round the clock.Personally for me I was fortunate
                                                                                to get the opportunity to take up active role in the college union as the Chairman,
                                                                                Arts Club secretary etc. and these experiences helped me later in the organizations
                                                                                as I worked with or led diverse teams.The community life of the college is something
                                                                                which is distinctly unique and even after 25 years I feel I could fall back and speak
                                                                                to my professors and mentors anytime I need some help.</p>

                                                                        <p style="font-weight:bold;">BINU PHILIP</p>
                                                                        <p>Director - Human Resources at Schneider Electric</p>
                                                                </div>
                                                        </div>
                                                </div>

                                        </div>
                                </div>
                        </div>
                </section>
                <section>
                        <div class="container">
                                <div class="row">
                                        <div class="col-md-12">
                                                <div class="rec">
                                                        <h1>Our Recruiters</h1>
                                                </div>
                                                <div class="test"> <img src="admission/images/ddd.png" > </div>
                                                <div class="iconlog">
                                                        <div class="item">
                                                                <div class="main news"> <img src="admission/images/syn.jpg"> </div>
                                                        </div>
                                                        <div class="item">
                                                                <div class="main news"> <img src="admission/images/hcl.jpg"> </div>
                                                        </div>
                                                        <div class="item">
                                                                <div class="main news"> <img src="admission/images/tvs.jpg"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/idea.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/icon1.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/ibm.jpg">
                                                                <div class="main news"> </div>
                                                        </div>

                                                        <div class="item"> <img src="admission/images/01.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/02.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/03.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/04.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/05.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/06.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/07.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/08.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/09.jpg">
                                                                <div class="main news"> </div>
                                                        </div>

                                                        <div class="item"> <img src="admission/images/10.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/11.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/12.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/13.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/14.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/15.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/16.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/17.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/18.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/20.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/21.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/22.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/23.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/24.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/25.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/26.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/28.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/29.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/30.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/31.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/32.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/33.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/34.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/35.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/36.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/37.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/38.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/39.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/40.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/41.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/42.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/43.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/44.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/45.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/46.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/47.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/48.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/49.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/50.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/51.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/52.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/53.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/52.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/54.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/55.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/56.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/57.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/58.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/ne.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/60.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/61.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/62.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/63.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/64.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/65.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/66.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/67.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/68.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/69.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/70.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/71.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/72.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/73.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/74.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/75.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/76.jpg">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss1.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss2.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss3.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss5.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss6.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss6.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss7.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss7.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss11.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss12.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss13.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss14.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss15.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss16.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                        <div class="item"> <img src="admission/images/rcss17.png">
                                                                <div class="main news"> </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </section>
                <section>
                        <div class="international">
                                <div class="container">
                                        <div class="row">
                                                <div class="col-md-12">
                                                        <h1>International Collaborations</h1>
                                                        <div class="test"> <img src="admission/images/nation.jpg" class="img-responsive need" > </div>
                                                        <div class="os">
                                                                <div class="item">
                                                                        <div class="main news"> <img src="admission/images/1.png"> </div>
                                                                </div>
                                                                <div class="item">
                                                                        <div class="main news"> <img src="admission/images/2.png"> </div>
                                                                </div>
                                                                <div class="item">
                                                                        <div class="main news"> <img src="admission/images/3.png"> </div>
                                                                </div>
                                                                <div class="item"> <img src="admission/images/4.png">
                                                                        <div class="main news"> </div>
                                                                </div>
                                                                <div class="item"> <img src="admission/images/5.png">
                                                                        <div class="main news"> </div>
                                                                </div>
                                                                <div class="item"> <img src="admission/images/6.png">
                                                                        <div class="main news"> </div>
                                                                </div>
                                                                <div class="item"> <img src="admission/images/7.png">
                                                                        <div class="main news"> </div>
                                                                </div>
                                                                <div class="item"> <img src="admission/images/3.png">
                                                                        <div class="main news"> </div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </section>
                <section>
                        <div class="footr">
                                <div class="container">

                                        <div class="row">

                                                <div class="col-md-12">
                                                        <p>Rajagiri College of Social Sciences,  Rajagiri P.O, Kalamassery, Cochin - 683 104,Kerala,  India</p>

                                                        <p>Phone:    +91 484 - 2555564/2911111,+91 484 - 2532862, Email:  admin@rajagiri.edu</p>
                                                        <p>&#169; 2015 Rajagiri College of Social Sciences. All rights reserved. Please review our  <a class="policies" href="#">Privacy Policy</a>  |  <a class="policies" href="#">Site Map</a></p>

                                                        <div class="power">
                                                                <ul>
                                                                        <li>
                                                                                <h6>Powered by Web Design Company Kerala <a href="http://www.intersmartsolution.com/" target="_blank"><img src="admission/images/intersmart.png"/></a></h6>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <ul class="list-inline list-unstyled neww">
                                                                <li><a href="https://www.facebook.com/Rajagiri-College-of-Social-Sciences-Autonomous-1376623805998150/?fref=ts" target="_blank"><img src="admission/images/s1.png" class="img-responsive"></a></li>
                                                                <li><a href="https://plus.google.com/108128696793864326281/about" target="_blank"><img src="admission/images/s2.png" class="img-responsive"></a></li>
                                                                <li><a href="https://twitter.com/hashtag/rajagiri" target="_blank"><img src="admission/images/s3.png" class="img-responsive"></a></li>
                                                                <li><a href="https://www.youtube.com/watch?v=MGAbYTU4pYY" target="_blank"><img src="admission/images/s5.png" class="img-responsive"></a></li>
                                                                <li><a href="https://www.linkedin.com/company/rajagiri-school-of-management?trk=hb_tab_compy_id_445227" target="_blank"><img src="admission/images/s6.png" class="img-responsive"></a></li>

                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </section>
                <script src="admission/js/jquery.min.js"></script>
                <script src="admission/js/bootstrap.min.js"></script>
                <script type="text/javascript" src="admission/js/slick.min.js"></script>
                <script>

                         $(document).ready(function() {

                                 $('.featured').slick({
                                         slidesToShow: 2,
                                         dots: true,
                                         autoplay: true,
                                         autoplaySpeed: 2000,
                                         slidesToScroll: 1,
                                         pauseOnHover: true,
                                         responsive: [
                                                 {
                                                         breakpoint: 1000,
                                                         settings: {
                                                                 centerMode: false,
                                                                 slidesToShow: 1
                                                         }
                                                 },
                                                 {
                                                         breakpoint: 900,
                                                         settings: {
                                                                 centerMode: false,
                                                                 slidesToShow: 1
                                                         }
                                                 },
                                                 {
                                                         breakpoint: 480,
                                                         settings: {
                                                                 centerMode: false,
                                                                 slidesToShow: 1
                                                         }
                                                 }
                                         ]
                                 });

                         });

                </script>
                <script>

                        $(document).ready(function() {

                                $('.iconlog').slick({
                                        slidesToShow: 6,
                                        dots: false,
                                        autoplay: true,
                                        autoplaySpeed: 2000,
                                        slidesToScroll: 1,
                                        pauseOnHover: true,
                                        prevArrow: '<i id="prev_slide_3" class="fa fa-chevron-left"></i>',
                                        nextArrow: '<i id="next_slide_3" class="fa fa-chevron-right"></i>',
                                        responsive: [
                                                {
                                                        breakpoint: 1000,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 2
                                                        }
                                                },
                                                {
                                                        breakpoint: 900,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                },
                                                {
                                                        breakpoint: 480,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                }
                                        ]
                                });

                        });

                </script>
                <script>

                        $(document).ready(function() {

                                $('.os').slick({
                                        slidesToShow: 7,
                                        dots: false,
                                        autoplay: true,
                                        autoplaySpeed: 2000,
                                        slidesToScroll: 1,
                                        pauseOnHover: true,
                                        responsive: [
                                                {
                                                        breakpoint: 1000,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                },
                                                {
                                                        breakpoint: 900,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 2
                                                        }
                                                },
                                                {
                                                        breakpoint: 480,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                }
                                        ]
                                });

                        });

                </script>
                <script>

                        $(document).ready(function() {

                                $('.nam').slick({
                                        slidesToShow: 2,
                                        dots: false,
                                        autoplay: true,
                                        autoplaySpeed: 2000,
                                        slidesToScroll: 1,
                                        pauseOnHover: true,
                                        responsive: [
                                                {
                                                        breakpoint: 1000,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                },
                                                {
                                                        breakpoint: 900,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                },
                                                {
                                                        breakpoint: 480,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                }
                                        ]
                                });

                        });

                </script>
                <script>

                        $(document).ready(function() {

                                $('.rajagiri').slick({
                                        slidesToShow: 1,
                                        dots: true,
                                        autoplay: true,
                                        autoplaySpeed: 2000,
                                        slidesToScroll: 1,
                                        pauseOnHover: true,
                                        responsive: [
                                                {
                                                        breakpoint: 1000,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                },
                                                {
                                                        breakpoint: 900,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                },
                                                {
                                                        breakpoint: 480,
                                                        settings: {
                                                                centerMode: false,
                                                                slidesToShow: 1
                                                        }
                                                }
                                        ]
                                });

                        });

                </script>
        </body>
</html>