<?php echo $this->renderPartial('//site/mail/_email_header'); ?>
<tr>
        <td style="padding:40px 20px; font-family:'Open Sans',arial, sans-serif; font-size:13px"><p>Hi Admin,<br/><br/>There is a new appointment booking.</p>
                <table id="Table_01"  border="0" cellpadding="0" cellspacing="0" align="left" style="padding:13px 0px; font-family:'Open Sans',arial, sans-serif; font-size:13px">
                        <tr>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;">Customer Name</p></td>
                                <td>:</td>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;"><?php echo $model->name; ?></p></td>
                        </tr>
                        <tr>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;">Email ID</p></td>
                                <td>:</td>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;"><?php echo $model->email; ?></p></td>
                        </tr>
                        <tr>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;">Occasion</p></td>
                                <td>:</td>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;"><?php
                                                if ($model->occasion == 1) {
                                                        echo 'WEDDING';
                                                } else if ($model->occasion == 2) {
                                                        echo 'SPECIAL OCCASIONS';
                                                } else if ($model->occasion == 3) {
                                                        echo 'OTHERS';
                                                } else {
                                                        echo 'Not Mentioned';
                                                }
                                                ?></p></td>
                        </tr>
                        <tr>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;">Phone Number</p></td>
                                <td>:</td>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;"><?php echo $model->phone; ?></p></td>
                        </tr>
                        <tr>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;">Country</p></td>
                                <td>:</td>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;"><?php echo $model->country0->country_name; ?></p></td>
                        </tr>
                        <tr>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;">Requirement</p></td>
                                <td>:</td>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;"><?php echo $model->notes; ?></p></td>
                        </tr>
                        <tr>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;">Date</p></td>
                                <td>:</td>
                                <td><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;padding:10px;"><?php echo $model->date ?></p>
                                </td>
                        </tr>




                </table>


        </td>
</tr>

<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>
