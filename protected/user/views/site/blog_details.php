<?php
$url=Yii::app()->request->url;
$ur=explode("blog/",$url);
$category_name = $ur[1];
$get_cat_name = Blog::model()->findByPk($category_name);
$this->setPageTitle($get_cat_name->meta_title);
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_keywords, 'keywords');
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_description, 'description');
?>
<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/blog">Blog</a>  /  Blog title Contents
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Cart</h2>
                <div class="row">
                        <div class="col-sm-3">
                                <div class="profile_menu blog_menu">
                                        <h3>Recent Blogs</h3>
                                        <ul class="text-normal">
                                                <?php
                                                $model = Blog::model()->findAllByAttributes(array('status' => 1));
                                                foreach ($model as $models) {
                                                        ?>
                                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/blogdetails/blog/<?= $models->id; ?>"><?= $models->heading; ?></a></li>
                                                        <?php
                                                }
                                                ?>
                                        </ul>
                                </div>
                        </div>
                        <div class="col-sm-9 static_page_content_area">
                                <h1><?= $modelz->heading; ?></h1>
                                <p><?php if(!empty($modelz->accredited)){ ?>Accredited Blog by <?= $modelz->accredited; ?><?php } ?></p>
                                <!--                                <div class="blog_meta">
                                                                        Mar 10 / By admin
                                                                </div>-->

                                <?= $modelz->big_content; ?>
                                 <?php
                                if($modelz->id == 2)
                                {
                                    ?>
                                   
                                <video controls="controls" autoplay="true" width="800" height="600" name="Video Name" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/blog/<?= $modelz->id; ?>/blog2.mov"></video>
                                <?php
                                }
                                ?>
                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/blog/<?= $modelz->id; ?>/big.<?= $modelz->big_image; ?>" alt=""/>
                               <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/blog/<?= $modelz->id; ?>/small.<?= $modelz->small_image; ?>" alt=""/>
                                <hr>
                        </div>
                </div>

        </div>
</section>