<section class="about-wrp">
        <div class="container">
                <div class="row">
                        <div class="col-sm-3">
                                <div class="side-menu-main">
                                        <div class="side-menu">
                                                <h3>Category</h3>
                                                <ul class="side-menu-ul">
                                                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/aboutus">About Us</a></li>
                                                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/careers">Careers</a></li>
                                                        <li class="active"><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/faq">FAQ</a></li>
                                                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/policy">Return Policy</a></li>
                                                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/terms">Terms & Conditions</a></li>
                                                </ul>
                                        </div>
                                </div>
                        </div>
                        <div class="col-sm-9">
                                <ul class="breadcrumbs list-inline">
                                        <li><a href="javascript:void(0)">Home</a></li>
                                        <li class="active">FAQ</li>
                                </ul>
                                <div class="aboutus-det faq-det">
                                        <div class="about-head clearfix">
                                                <h1>FAQ</h1>
                                        </div>
                                        <?php
                                        $general = StaticPage::model()->findByPk(3);
                                        $enq = StaticPage::model()->findByPk(4);
                                        $gift = StaticPage::model()->findByPk(5);
                                        ?>
                                        <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#faq-det01" aria-controls="faq-det01" role="tab" data-toggle="tab">GENERAL</a></li>
                                                <li role="presentation"><a href="#faq-det02" aria-controls="faq-det02" role="tab" data-toggle="tab">ENQUIRY</a></li>
                                                <li role="presentation"><a href="#faq-det03" aria-controls="faq-det03" role="tab" data-toggle="tab">GIFTCARD</a></li>
                                        </ul>
                                        <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="faq-det01">
                                                        <?= $general->big_content; ?> </div>
                                                <div role="tabpanel" class="tab-pane" id="faq-det02"> <?= $enq->big_content; ?></div>
                                                <div role="tabpanel" class="tab-pane" id="faq-det03"> <?= $gift->big_content; ?></div>

                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section>