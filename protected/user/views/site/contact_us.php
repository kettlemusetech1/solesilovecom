<section class="contact-wrp">
        <div class="container">
                <ul class="breadcrumbs list-inline">
                        <li><a href="javascript:void(0)">Home</a></li>
                        <li class="active">Contact Us</li>
                </ul>
                <div class="contact-main">
                        <div class="row">
                                <div class="col-sm-4">
                                        <div class="contact-address clearfix">
                                                <h4>Contact Us</h4>
                                                <?php
                                                $add = StaticPage::model()->findByPk(12);
                                                $eml = StaticPage::model()->findByPk(13);
                                                $phn = StaticPage::model()->findByPk(14);
                                                ?>
                                                <div class="contact-add-box">
                                                        <div class="contact-icon"><i class="fa fa-map-marker"></i></div>
                                                        <div class="contact-content">
                                                                <?= $add->big_content; ?>
                                                        </div>
                                                </div>
                                                <div class="contact-add-box">
                                                        <div class="contact-icon"><i class="fa fa-phone"></i></div>
                                                        <div class="contact-content">
                                                                <?= $phn->big_content; ?>

                                                        </div>
                                                </div>
                                                <div class="contact-add-box">
                                                        <div class="contact-icon"><i class="fa fa-envelope-o"></i></div>
                                                        <div class="contact-content">
                                                                <?= $eml->big_content; ?>
                                                        </div>
                                                </div>
                                        </div>

                                </div>
                                <div class="col-sm-8">
                                        <?php
                                        if (Yii::app()->session['user']) {

                                                $user_id = Yii::app()->session['user']['id'];
                                                $user_det = UserDetails::model()->findByPk($user_id);
                                                $name = $user_det->first_name;
                                                $email = $user_det->email;
                                                $mobile = $user_det->phone_no_2;
                                        }
                                        $form = $this->beginWidget('CActiveForm', array(
                                            'id' => 'contact-us-contact-form',
                                            'action' => Yii::app()->baseUrl . '/site/contactUs/',
                                            'enableAjaxValidation' => false,
                                        ));
                                        ?>
                                        <div class="login-form-inputs material-slide-line clearfix">
                                                <h3>ENquire Now</h3>
                                                <span class="input input--akira input-half">
                                                        <?php echo $form->textField($model, 'email', array('size' => 60, 'class' => 'input__field input__field--akira', 'value' => $email, 'id' => 'input2')); ?>
                                                        <label class="input__label input__label--akira" for="input-2">
                                                                <span class="input__label-content input__label-content--akira">Email*</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <?php echo $form->textField($model, 'name', array('size' => 60, 'class' => 'input__field input__field--akira', 'value' => $name, 'id' => 'input1')); ?>
                                                        <label class="input__label input__label--akira" for="input-1">
                                                                <span class="input__label-content input__label-content--akira">Name *</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <?php echo $form->textField($model, 'comment', array('size' => 60, 'class' => 'input__field input__field--akira', 'value' => $comment, 'id' => 'input1')); ?>
                                                        <label class="input__label input__label--akira" for="input-1">
                                                                <span class="input__label-content input__label-content--akira">Message *</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <?php echo $form->textField($model, 'phone', array('size' => 60, 'class' => 'input__field input__field--akira', 'value' => $mobile, 'id' => 'input2')); ?>
                                                        <label class="input__label input__label--akira" for="input-2">
                                                                <span class="input__label-content input__label-content--akira">Mobile *</span>
                                                        </label>
                                                </span>
                                                <button class="butter login-btn">Submit</button>
                                        </div>
                                        <?php $this->endWidget(); ?>
                                </div>
                        </div>
                </div>
        </div>
</section>