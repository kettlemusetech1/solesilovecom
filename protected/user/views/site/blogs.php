<?php
$get_cat_name = StaticPage::model()->findByPk(9);
$this->setPageTitle($get_cat_name->meta_title);
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_keywords, 'keywords');
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_description, 'description');
?>
<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
        <div class="content-body listings_page about_page">

                <div class="page_banner">
                        <div class="breadcrumb">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>">Home</a> / <a href="">Blog</a>
                        </div>
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/blog_main_img.JPG" alt=""/>
                        <h1>BLOG</h1>
                </div>
                <div class="container from-blog">
                        <div class="row">
                                <?php
                                foreach ($model as $blogs) {
                                        ?>
                                        <div class="blog_lists col-sm-4 col-xs-6 col-iphone-12">
                                                <div class="blog_item">
                                                        <div class="blog_image">
                                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/blog/<?= $blogs->id; ?>/small.<?= $blogs->small_image; ?>" alt=""/>
                                                        </div>
                                                        <h3><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/blogdetails/blog/<?= $blogs->id; ?>"><?= $blogs->heading; ?></a></h3>
                                                        <!--                                                        <div class="blog_meta">
                                                                                                                        Mar 10 / By <a href="#">admin</a> / 3  comments
                                                                                                                </div>-->
                                                        <p><?= $blogs->small_content; ?></p>
                                                </div>
                                        </div>

                                        <?php
                                }
                                ?>

                        </div>
                </div>

        </div>
</section>