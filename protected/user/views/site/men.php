<section class="banner-wrp">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                        <?php
                        $i = 1;
                        foreach ($slider as $sliders) {
                                if ($i == 1)
                                        $act = "active";
                                else
                                        $act = "";
                                ?>
                                <div class="item img-wrapper <?= $act; ?>">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/sliders/<?php echo $sliders->id; ?>.<?php echo $sliders->image_extension; ?>" alt="<?php echo $sliders->id; ?>">
                                        <div class="container">
                                                <div class="carousel-caption-div">
                                                        <h2><?php echo $sliders->content; ?></h2>
                                                        <a href="javascript:void(0)" class="butter shop-btn">Shop Now</a>
                                                </div>
                                        </div>
                                </div>
                                <?php
                                $i++;
                        }
                        ?>
                </div>
                <div class="carousel-caption-main"></div>
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="sr-only">Next</span>
                </a>
        </div>
</section>
<?php
$designer = Banner::model()->findByPk(1);
$kurtas = Banner::model()->findByPk(2);
$shirt = Banner::model()->findByPk(3);
$blazers = Banner::model()->findByPk(4);
$mix = Banner::model()->findByPk(5);
$dhoties = Banner::model()->findByPk(6);
$gifts = Banner::model()->findByPk(7);
?>
<section class="quick-links">
        <div class="container">
                <div class="quick-links-head">
                        <ul class="quicklinks-ul list-inline">
                                <li><span>We Ship Worldwide</span></li>
                                <li><span>Sign up for emails</span></li>
                                <li><span>Free Shipping In India</span></li>
                        </ul>
                </div>
                <div class="quick-link-top">
                        <div class="quick-link-box large">
                                <div class="quick-link-img img-wrapper">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $designer->id; ?>.<?= $designer->image; ?>">
                                </div>
                                <div class="quick-link-det">
                                        <h4><?= $designer->heading; ?></h4>
                                        <p><?= $designer->description; ?></p>
                                        <a href="javascript:void(0)" class="butter shop-btn">Shop Now</a>
                                </div>
                        </div>
                        <div class="quick-link-box mid">
                                <a href="javascript:void(0)" class="quick-link-box-inner">
                                        <div class="quick-link-img img-wrapper">
                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $kurtas->id; ?>.<?= $kurtas->image; ?>">
                                        </div>
                                        <div class="quick-link-det">
                                                <h4><?= $kurtas->heading; ?></h4>
                                                <p><span><?= $kurtas->description; ?></span> <i class="fa fa-long-arrow-right"></i></p>
                                        </div>
                                </a>
                                <a href="javascript:void(0)" class="quick-link-box-inner">
                                        <div class="quick-link-img img-wrapper">
                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $shirt->id; ?>.<?= $shirt->image; ?>">
                                        </div>
                                        <div class="quick-link-det">
                                                <h4><?= $shirt->heading; ?></h4>
                                                <p><span><?= $shirt->description; ?></span> <i class="fa fa-long-arrow-right"></i></p>
                                        </div>
                                </a>
                        </div>
                        <a href="javascript:void(0)" class="quick-link-box mid">
                                <div class="quick-link-img img-wrapper">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $blazers->id; ?>.<?= $blazers->image; ?>">
                                </div>
                                <div class="quick-link-det">
                                        <h4><?= $blazers->heading; ?></h4>
                                        <p><span><?= $blazers->description; ?></span> <i class="fa fa-long-arrow-right"></i></p>
                                </div>
                        </a>
                </div>
                <div class="quick-link-top">
                        <div class="row">
                                <div class="col-sm-6">
                                        <div class="match-cole-box">
                                                <div class="match-cole-img img-wrapper">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $mix->id; ?>.<?= $mix->image; ?>">
                                                </div>
                                                <div class="match-cole-det">
                                                        <h4><?= $mix->heading; ?></h4>
                                                        <p><?= $mix->heading; ?></p>
                                                        <a href="javascript:void(0)" class="butter shop-btn">Shop Now</a>
                                                </div>
                                        </div>
                                </div>
                                <div class="col-sm-6">
                                        <div class="match-cole-box">
                                                <div class="match-cole-img img-wrapper">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $dhoties->id; ?>.<?= $dhoties->image; ?>">
                                                </div>
                                                <div class="match-cole-det">
                                                        <h4><?= $dhoties->heading; ?></h4>
                                                        <p><?= $dhoties->heading; ?></p>
                                                        <a href="javascript:void(0)" class="butter shop-btn">Shop Now</a>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section>
<section class="featured-products-wrp">
        <div class="container">
                <h3><span>Featured Products</span></h3>
                <div class="fea-pro-slider">
                        <?php
                        foreach ($feat as $featured) {
                                ?>
                                <div class="item">
                                        <div class="fea-pro-box">
                                                <div class="fea-pro-img img-wrapper">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php echo Yii::app()->Upload->folderName(0, 1000, $featured->id) ?>/<?php echo $featured->id; ?>/medium.<?php echo $featured->main_image; ?>" alt="<?php echo $featured->product_name; ?>">
                                                </div>
                                                <div class="fea-pro-det">
                                                        <h4><?php echo $featured->product_name; ?></h4>
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Products/Detail/name/<?php echo $featured->canonical_name; ?>" class="butter shop-btn">Shop Now</a>
                                                </div>
                                        </div>
                                </div>
                                <?php
                        }
                        ?>
                </div>
        </div>
</section>
<section class="client-say-wrp">
        <div class="container">
                <div class="client-main">
                        <div class="client-bg"></div>
                        <div class="client-content">
                                <div class="client-img-slider">
                                        <div class="client-img img-wrapper">
                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/client-say.jpg">
                                        </div>
                                        <div class="client-img img-wrapper">
                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/client-say.jpg">
                                        </div>
                                        <div class="client-img img-wrapper">
                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/client-say.jpg">
                                        </div>
                                </div>
                                <div class="client-det-slider">
                                        <?php
                                        foreach ($model as $models) {
                                                ?>
                                                <div class="client-det">
                                                        <h4><?= $models->title; ?></h4>
                                                        <?= $models->content; ?>
                                                        <h6><?= $models->name; ?>,<?= $models->position; ?></h6>
                                                </div>
                                                <?php
                                        }
                                        ?>
                                </div>
                        </div>
                </div>
        </div>
</section>