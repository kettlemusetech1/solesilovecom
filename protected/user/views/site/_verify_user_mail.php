<?php echo $this->renderPartial('//site/mail/_email_header'); ?>



<tr>
        <td style="padding:40px 20px; font-family:'Open Sans',arial, sans-serif; font-size:13px"><p>Hi <?php echo $model->first_name; ?><span>      <?php echo $model->last_name; ?></span>,</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Greetings from Artstra.com!</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Your OTP: <?php echo $model->verify_code; ?></p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Treat this as confidential. Sharing this with anyone gives them full access to your artstra account.</p>

                <br/>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#abaaaa;">* This is an automatically generated email, please do not reply to this email.</p>
        </td>
</tr>





<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>