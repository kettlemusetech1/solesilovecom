<?php
$get_cat_name = StaticPage::model()->findByPk(7);
$this->setPageTitle($get_cat_name->meta_title);
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_keywords, 'keywords');
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_description, 'description');
?>
<?php echo $this->renderPartial('mainmodal'); ?>
<div>
        <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="alert alert-success mesage">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="alert alert-danger mesage msg2">
                    <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
</div>
<div class="home_slider" >
        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-pause="hover" data-interval="5000">
                <div class="carousel-inner" role="listbox">
                        <?php
                        $i = 1;
                        foreach ($slider as $sliders) {
                            if ($i == 1) {
                                $act = "active";
                            } else {
                                $act = " ";
                            }
                            ?>
                            <div class="item <?= $act; ?>"> <img class="first-slide" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/sliders/<?= $sliders->id; ?>.<?= $sliders->image_extension; ?>" alt="slider<?= $sliders->id; ?>">
                                    <div class="container">
                                            <div class="carousel-caption ">
                                                    <div class="to_up animdelay">
                                                    </div> 
                                                    <div class="button_group animated fadeInDown animdelay">
                                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/<?= $sliders->content; ?>" class="button">Know More</a>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                            <?php
                            $i++;
                        }
                        ?>
                </div>
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
        </div>
</div>
<section class="delivery">
        <div class="container">
                <div class="row">
                        <div class="col-sm-6">
                                <div class="row">
                                        <div class="col-xs-4 col-md-6 col-lg-4 smart_half">
											<div class="fast_delivery"><a href="https://www.solesilove.com.au/index.php/site/Shipping/">Fast<br>Delivery</a></div>
                                        </div>
                                        <div class="col-xs-4 col-md-6 col-lg-4 smart_half">
                                                <div class="return"><a href="https://www.solesilove.com.au/index.php/site/Return/">30 day change of mind policy</a></div>
                                        </div>
                                        <div class="col-xs-4 hidden-md hidden-sm hidden_smart">
                                                <div class="quick_enquirys"><a href="#contact-enquiry">Contact and enquiries</a></div>
                                        </div>
                                </div>
                        </div>
                        <div class="col-sm-6 hidden-xs">
                                <div class="row newsletter_row">
                                        <div class="col-lg-3 col-md-4 col-sm-4">
                                                <h4>Subscribe</h4>
                                                <p>Our Newsletter</p>
                                        </div>
                                        <div class="col-lg-9 col-md-8 col-sm-8">
                                                <div class="input-group">
                                                        <input name="email" type="text" class="form-control newsletter_email" id="newsletter" placeholder="Email Address">
                                                        <div class="input-group-addon"><button class="button subscribe_btn" type="button">SUBMIT</button></div>
                                                </div>
                                                <div class="newsletter_msg" style="color: red;"></div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section>
<section class="featured">
        <div class="container">
                <div class="featured_grid">
                        <?php
                        $baner1 = Banner::model()->findByPk(1);
                        ?>
                        <div class="grid-3 ad_banner" style="background-image: url(<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $baner1->id; ?>.<?= $baner1->image; ?>);">
                                <div class="add_claption">
                                        <h3><?= $baner1->name; ?></h3>
                                        <h5><?= $baner1->heading; ?></h5>
                                        <p><?= $baner1->description; ?></p>
                                        <a href="<?= $baner1->link; ?>" class="button"><strong>SHOP NOW</strong></a>
                                </div>
                        </div>
                        <div class="grid-9">
                                <h1>Featured Products<span>From Soles iLove</span></h1>
                                <div class="product_list_home">
                                        <div class="row home_product_slider">
                                                <?php
                                                $feat = ProductsFeatured::model()->findAllByAttributes(array('status' => 1, 'featured' => 1));
                                                foreach ($feat as $featz) {
                                                    $pdct = Products::model()->findByPk($featz->product_id);
                                                    ?>
                                                    <div class="col-xs-4 item">
                                                            <div class="item-wrap">
                                                                    <?php
                                                                    if ($pdct->quantity == 0) {
                                                                        ?>
                                                                        <div class="sold-badge"></div>

                                                                        <?php
                                                                    } else if ($pdct->discount_rate != 0) {
                                                                        ?>
                                                                        <div class="sale-badge"></div>
                                                                    <?php } else if ($featz->new == 1) {
                                                                        ?>
                                                                        <div class="new-badge"></div>
                                                                        <?php
                                                                    }
                                                                    ?> 
                                                                    <div class="prdt_img img-wrapper"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?= $pdct->canonical_name; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/1000/<?= $pdct->id ?>/<?= $pdct->id ?>.<?= $pdct->main_image ?>" alt="<?= $pdct->canonical_name; ?>"/></a>
                                                                            <div class="product_hover">
                                                                                <?php if (Yii::app()->session['user']) { ?>
                                                                                    <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $pdct->id ?>" class="button">ADD TO WISHLIST</a>
                                                                                     <?php } else
                                                                                     {
                                                                                         ?>
                                                                                         <a href="javascript: void(0)" class="button userlogin">ADD TO WISHLIST</a>
                                                                                         <?php
                                                                                     }?>
                                                                                    <a href="javascript:void(0)" class="quick button button_2 quick_data" product_id="<?php echo $pdct->id; ?>">QUICK VIEW</a>
                                                                            </div>
                                                                    </div>
                                                                    <div class="product_title"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?= $pdct->canonical_name; ?>">
                                                                    <?php
                                                                                        $len = strlen($pdct->product_name);
                                                                                        if ($len > 15) {
                                                                                                echo substr($pdct->product_name, 0, 15)."...";
                                                                                        } else {
                                                                                                echo $pdct->product_name;
                                                                                        }
                                                                                        ?>
                                                                                        </a></div>
                                                                    <div class="product_price">
                                                                            <div class="product_srar"><a href="#" class="star1 active"><i class="fa fa-star-o"></i></a><a href="#" class="star2"><i class="fa fa-star-o"></i></a><a href="#" class="star3"><i class="fa fa-star-o"></i></a><a href="#" class="star4"><i class="fa fa-star-o"></i></a><a href="#" class="star5"><i class="fa fa-star-o"></i></a></div>
                                                                            <?php
                                                                            if ($pdct->discount_rate != 0) {
                                                                                ?>
                                                                                <del class="text-danger"><?php echo Yii::app()->Currency->convert($pdct->price); ?></del> <?php echo Yii::app()->Discount->Discount($pdct); ?>
                                                                            <?php } else { ?>
                                                                                <?php echo Yii::app()->Discount->Discount($pdct); ?>
                                                                            <?php } ?>
                                                                    </div>

                                                            </div>

                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section>
<section class="ad_sections">
        <div class="container">
                <?php
                $baner2 = Banner::model()->findByPk(2);
                $baner3 = Banner::model()->findByPk(3);
                $baner4 = Banner::model()->findByPk(4);
                ?>
                <div class="row adrow">
                        <div class="col-sm-4 col-xs-6">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/<?= $baner2->link; ?>"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $baner2->id; ?>.<?= $baner2->image; ?>" alt="banner<?= $baner2->id; ?>"/></a>
                        </div>
                        <div class="col-sm-4 col-xs-6 animdelay">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/<?= $baner3->link; ?>"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $baner3->id; ?>.<?= $baner3->image; ?>" alt="banner<?= $baner3->id; ?>"/></a>
                        </div>
                        <div class="col-sm-4 hidden-xs animdelay1">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/<?= $baner4->link; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $baner4->id; ?>.<?= $baner4->image; ?>" alt="banner<?= $baner4->id; ?>"/></a>
                        </div>
                </div>
        </div>
</section>
<section class="latest_products">
        <div class="container">
                <h2>NEW ARRIVALS</h2>
                <h5>From Soles iLove</h5>
                <div class="product_list_home">
                        <div class="row new-arrival-slider">
                                <?php
                                $me = ProductsFeatured::model()->findAllByAttributes(array('status' => 1, 'new' => 1));
                                foreach ($me as $new) {
                                    $pdt = Products::model()->findByPk($new->product_id);
                                    ?>
                                    <div class="col-xs-3 item">
                                            <div class="item-wrap">
                                                    <?php
                                                    if ($pdt->quantity == 0) {
                                                        ?>
                                                        <div class="sold-badge"></div>

                                                        <?php
                                                    } else if ($pdt->discount_rate != 0) {
                                                        ?>
                                                        <div class="sale-badge"></div>
                                                    <?php } else {
                                                        ?>
                                                        <div class="new-badge"></div>
                                                    <?php } ?>
                                                    <div class="prdt_img img-wrapper"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?= $pdt->canonical_name; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/1000/<?= $pdt->id ?>/<?= $pdt->id ?>.<?= $pdt->main_image ?>" alt="<?= $pdt->canonical_name; ?>"/></a>
                                                            <div class="product_hover">
                                                                   <?php if (Yii::app()->session['user']) { ?>
                                                                                     <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $pdt->id ?>" class="button">ADD TO WISHLIST</a>
                                                                                     <?php } else
                                                                                     {
                                                                                         ?>
                                                                                         <a href="javascript: void(0)" class="button userlogin">ADD TO WISHLIST</a>
                                                                                         <?php
                                                                                     }?>
                                                                    <a href="javascript:void(0)" class="quick button button_2 quick_data" product_id="<?php echo $pdt->id; ?>">QUICK VIEW</a>
                                                            </div>
                                                    </div>
                                                    <div class="product_title"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?= $pdt->canonical_name; ?>">
                                                        <?php
                                                                                        $len = strlen($pdt->product_name);
                                                                                        if ($len > 15) {
                                                                                                echo substr($pdt->product_name, 0, 15)."...";
                                                                                        } else {
                                                                                                echo $pdt->product_name;
                                                                                        }
                                                                                        ?>
                                                        </a></div>
                                                    <div class="product_price">
                                                            <div class="product_srar"><a href="#" class="star1 active"><i class="fa fa-star-o"></i></a><a href="#" class="star2"><i class="fa fa-star-o"></i></a><a href="#" class="star3"><i class="fa fa-star-o"></i></a><a href="#" class="star4"><i class="fa fa-star-o"></i></a><a href="#" class="star5"><i class="fa fa-star-o"></i></a></div>
                                                            <?php
                                                            if ($pdt->discount_rate != 0) {
                                                                ?>
                                                                <del class="text-danger"><?php echo Yii::app()->Currency->convert($pdt->price); ?></del> <?php echo Yii::app()->Discount->Discount($pdt); ?>
                                                            <?php } else { ?>
                                                                <?php echo Yii::app()->Discount->Discount($pdt); ?>
                                                            <?php } ?>
                                                    </div>
                                            </div>

                                    </div>
                                    <?php
                                }
                                ?>
                        </div>
                </div>
        </div>
</section>
<section class="shop_by_brand">
        <div class="container">
                <div class="row">
                        <div class="col-sm-7 brand-col">
                                <h3>SHOP BY BRAND</h3>
                                <div class="row brand_row">
                                        <?php
                                        $brand = Brands::model()->findAllByAttributes(array('status' => 1), array('limit' => 4));
                                        foreach ($brand as $brands) {
                                            ?>
                                            <div class="col-xs-6">
                                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/brand/<?= $brands->canonical_name; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/brands/<?= $brands->id; ?>.<?= $brands->image; ?>" alt="brand<?= $brands->id; ?>"/></a>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                </div>
                        </div>
                        <?php
                        $baner5 = Banner::model()->findByPk(5);
                        ?>
                        <div class="col-sm-5 orthic-col hidden-xs">
                                <h3><?= $baner5->name; ?></h3>
                                <div class="orthotic_support">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/banner/<?= $baner5->id; ?>.<?= $baner5->image; ?>" alt="banner<?= $baner5->id; ?>"/>
                                        <div class="orth-body">
                                                <h4><?= $baner5->heading; ?></h4>
                                                <a href="<?= $baner5->link; ?>" class="button button_3">SHOP NOW</a>
                                        </div>

                                </div>
                        </div>
                </div>
        </div>
</section>
<section class="different">
        <div class="container">
                <?php
                $stat = StaticPage::model()->findByPk(1);
                $pdts = Products::model()->findAllByAttributes(array('status' => 1));
                $cnt = count($pdts);
                $cust = UserDetails::model()->findAllByAttributes(array('status' => 1));
                $custs = count($cust);
                ?>
                <h3><?= $stat->title; ?></h3>
                <div class="row">
                        <div class="col-sm-6 border-rights">
                                <p><?= $stat->big_content; ?></p>
                        </div>
                        <div class="col-sm-6">
                                <div class="row counters">
                                        <div class="col-xs-6">
                                                <h4 class="counter_1"><?= $cnt; ?>+</h4>
                                                <h5>Live Products</h5>
                                        </div>
                                        <div class="col-xs-6">
                                                <h4 class="counter_2"><?= $custs; ?>+</h4>
                                                <h5>Happy Customers</h5>
                                        </div>
                                </div>
                                <div class="row counters">
                                        <div class="col-xs-6">
                                                <h4 class="counter_3">100%</h4>
                                                <h5>Quality Of Products</h5>
                                        </div>
                                        <div class="col-xs-6">
                                                <h4 class="counter_4">100%</h4>
                                                <h5>Fit For Your Legs</h5>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section>
<section class="from-blog">
        <div class="container">
                <h2>From The Blog</h2>
                <div class="row blog_slider">
                        <?php
                        $blog = Blog::model()->findAllByAttributes(array('status' => 1));
                        foreach ($blog as $blogs) {
                            ?>
                            <div class="col-xs-4">

                                    <div class="blog_item">
                                            <div class="blog_image">
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/blog/<?= $blogs->id; ?>/small.<?= $blogs->small_image; ?>" alt="blog<?= $blogs->id; ?>"/>
                                            </div>
                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/blogdetails/blog/<?= $blogs->id; ?>" > <h3><?= $blogs->heading; ?></h3></a>

                                            <p><?= $blogs->small_content; ?></p>
                                    </div>
                            </div>
                            <?php
                        }
                        ?>
                </div>
        </div>
</section>
<section class="testimoni_section">
        <div class="container">
                <h2>What Our Clients Say</h2>
                <div class="test_slider">
                        <?php
                        foreach ($testi as $testim) {
                            ?>
                            <div class="item">
                                    <div class="testi-image">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/testimonial/<?= $testim->id; ?>.<?= $testim->image; ?>" alt="testimonial<?= $testim->id; ?>"/>
                                    </div>
                                    <div class="testi-content">
                                            <p><?= $testim->content; ?></p>
                                            <h4><?= $testim->name; ?></h4>
                                    </div>

                                    <div class="clearfix"></div>
                            </div>
                            <?php
                        }
                        ?>
                </div>
        </div>
</section>
<section class="newsletter_section">
        <div class="container ">
                <div class="row">
                        <div class="col-sm-6">
                                <h4>Subscribe</h4>
                                <h3>NEWSLETTER</h3>
                                <p>“Stay updated with our Sneak Previews of our Latest Styles arriving and Seasonal Promotions by subscribing to our Newsletter.”</p>
                                <div class="email_form">
                                        <div class="row">
                                                <div class="col-xs-8">
                                                        <input name="email" placeholder="Email Address" type="text" class="form-control newsletter_email2">
                                                </div>
                                                <div class="col-xs-4">
                                                        <button class="button subscribe_btn2" type="button">Submit</button>
                                                       
                                                </div>
                                                 <div class="newsletter_msg2" style="color: red;"></div>
                                        </div>
                                </div>
                        </div>
                        <div class="col-sm-6 facebook_col">
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsolesilove&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="480px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                        </div>
                </div>
        </div>
</section>
<div class="fadeIn animated quick_view modal" id="quick_view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog zoomIn animated" role="document">
                <div class="modal-content quick_view_content">
                </div>
        </div>
</div>