<?php $controller = Yii::app()->controller->id; ?>
<div class="modal  login_modal" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog " role="document">
                <div class="modal-content">
                        <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <div class="login_form register register_pop">
                                        <form name="frmreg" class="formreg" id="reset_form" action="" method="POST">
                                                <h1>REGISTER</h1>
                                                <div id="regresponse" style="color: red;"></div>
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-sm-12">
                                                                        <input type="email" name="emailid" id="emailid" class="form-control" placeholder="Email Address">
                                                                        <input type="hidden" name="guest_checkout" value="1" id="guest_checkout" class="">
                                                                </div>
                                                        </div>
                                                </div>
                                                <!--                                                <div class="form-group">
                                                                                                        <div class="row">
                                                                                                                <div class="col-sm-12">
                                                <?php if ($controller == 'cart') { ?>
                                                                                                                                                        <label style="font-weight: 600;
                                                                                                                                                               font-size: 12px;
                                                                                                                                                               color: #314451;
                                                                                                                                                               font-family: inherit;"><input type="checkbox" name="guest_checkout" value="1" id="guest_checkout" class=""> Continue as Guest </label>
                                                <?php } ?>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                </div>-->
                                                <div id="guest_name" style="display:none;">
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-sm-12">
                                                                                <input type="text" name="name" id="g_name" class="form-control" placeholder="Name">
                                                                        </div>

                                                                </div>
                                                        </div>
                                                </div>
                                                <div id="regpart" style="display:none;">
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-sm-6">
                                                                                <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                                <input type="number" name="mobile" id="mobile" class="form-control" placeholder="Phone/Mobile">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-sm-6">
                                                                                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                                <input type="password" name="password1" id="password1" class="form-control" placeholder="Confirm Password">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <!--<div class="form-group">-->
                                                        <!--        <div class="row">-->
                                                        <!--                <div class="col-sm-12">-->
                                                        <!--                        <input type="text" name="otp" id="otp" class="form-control" placeholder="Enter Verification Code:">-->
                                                        <!--                </div>-->
                                                        <!--        </div>-->

                                                        <!--</div>-->
                                                </div>

                                                <div class="form-group">  <input type="hidden" name="enq" class="enq" value="0">
                                                        <div class="col-xs-12 text-right">
                                                                <span class="logins"> Login</span> <i class="fa fa-arrow-circle-right"></i>
                                                        </div>
                                                        <button class="button"  id="submitreg">SUBMIT</button>
                                                </div>
                                        </form>
                                </div>

                        </div>

                </div>
                <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>

<div class="modal login_modal" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <div class="login_form">
                                        <form name="frm" class="form" action="" method="POST">
                                                <h4>LOGIN</h4>
                                                <div id="response" style="color: red;"></div>
                                                <div class="form-group">
                                                        <input type="text" name="email" id="email" class="form-control" placeholder="Email Address" type="email">
                                                </div>
                                                <div class="form-group">
                                                        <input type="password" name="passwords" id="login-password" class="form-control" placeholder="Password">
                                                </div>
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-xs-6">
                                                                        <span class="forgot">Forgot your password?</span>
                                                                </div>
                                                                <div class="col-xs-6 text-right">
                                                                        <span class="newuser"> New User</span> <i class="fa fa-arrow-circle-right"></i>
                                                                </div>
                                                        </div>
                                                </div>

                                                <div class="form-group">
                                                        <button class="button"  id="submit">SUBMIT</button>
                                                </div>
                                        </form>
                                </div>

                        </div>

                </div>
                <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>

<div class="modal login_modal" id="forgotpw" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <div class="login_form">
                                        <form name="fpfrm" class="fpfrm" action="" method="POST">
                                                <h4>FORGOT PASSWORD</h4>
                                                <div id="fgtresponse" style="color: red;"></div>
                                                <div class="form-group">
                                                        <input type="text" name="emailad" id="emailad" class="form-control" placeholder="Email Address" type="email">
                                                </div><input type="hidden" name="flg" class="flg" value="0">
                                                <div id="fpotp" style="display:none;">
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-sm-12">
                                                                                <input type="text" name="otp" id="otpc" class="form-control" placeholder="Enter Verification Code:">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <input type="password" name="paswd" id="paswd" class="form-control" placeholder="New Password">
                                                        </div>
                                                        <a href="#" class="rotp">Resend Verification Code</a>
                                                </div>
                                                <div class="form-group">
                                                        <button class="button" id="submit">SUBMIT</button>
                                                </div>
                                        </form>
                                </div>

                        </div>

                </div>
                <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>
<script>
    $(document).ready(function () {
        $('#password1').keyup(function () {
            if (Check() == true) {
                return true;
            } else {
                return false;
            }
        });
        $('#password').keyup(function () {
            if (Check() == true) {
                return true;
            } else {
                return false;
            }
        });
        $('#reset_form').submit(function () {
            if (Check() == true) {
                return true;
            } else {
                return false;
            }
        });
    });
    function Check() {
        var pass = $('#password').val();
        var confirm = $('#password1').val();
        if (pass != '' && confirm != '') {
            if (pass != confirm) {
                $('#regresponse').html(" Password doesn't match");
                return false;
            } else {
                $('#regresponse').html("");
                return true;

            }
        } else {
            $('#regresponse').html("");
            return true;
        }
    }
</script>
<script>
    $(document).ready(function () {
        $('.rotp').click(function (e) {
            var email = $("#emailad").val();
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: baseurl + 'Site/reotp',
                data: {email: email},
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    if (obj.status == 200) {
                        $('#fgtresponse').html(obj.message);
                        $('#fpotp').show();
                    }
                },
            });
        });
        $('.logins').click(function () {
            $("#login").modal('show');
            $("#register").modal('hide');
            $("#forgotpw").modal('hide');
        });
        $('.userlogin').click(function () {
            $("#login").modal('show');
            $("#register").modal('hide');
            $("#forgotpw").modal('hide');
        });
        $('.newuser').click(function () {
            $("#register #guest_checkout").val(1);
            $('#guest_name').hide();
            $("#register h1").html('REGISTER');
            $("#login").modal('hide');
            $("#forgotpw").modal('hide');
            $("#register").modal('show');
        });
        $('.guestcheckout').click(function () {
            $("#login").modal('hide');
            $("#register #guest_checkout").val(2);
            $('#guest_name').show();
            $("#register h1").html('GUEST CHECKOUT');
            $("#forgotpw").modal('hide');
            $("#register").modal('show');
        });
        $('.forgot').click(function () {
            $("#login").modal('hide');
            $("#register").modal('hide');
            $("#forgotpw").modal('show');
        });
        $('.fpfrm').on('submit', function (e) {
            var email = $("#emailad").val();
            var paswd = $("#paswd").val();
            var otp = $("#otpc").val();
            var flg = $('.flg').val();
            if (flg == 0) {
                if (email == '') {
                    $('#fgtresponse').html('Please enter email');
                    e.preventDefault();
                }
                else {
                    e.preventDefault();
                    $.ajax({
                        type: 'post',
                        url: baseurl + 'Site/fgtpwd',
                        data: {email: email},
                        success: function (data) {
                            var obj = jQuery.parseJSON(data);
                            if (obj.status == 200) {
                                $('#fgtresponse').html(obj.message);
                                $('#fpotp').show();
                                $('.flg').val(1);
                            }
                            if (obj.status == 300) {
                                $('#fgtresponse').html(obj.message);
                                $('#fpotp').show();
                                $('.flg').val(1);
                            }
                            if (obj.status == 303) {
                                $('#regresponse').html(obj.message);
                                $("#register").modal('show');
                                $("#forgotpw").modal('hide');
                            }
                        },
                    });
                }
            } else {
                if (email == '') {
                    $('#fgtresponse').html('Please enter email');
                    e.preventDefault();
                }
                else if (paswd == '') {
                    $('#fgtresponse').html('Please enter password');
                    e.preventDefault();
                } 
                //else if (otp == '') {
               //     $('#fgtresponse').html('Please enter Verification Code');
                //    e.preventDefault();
               // }
                else {
                    e.preventDefault();
                    $.ajax({
                        type: 'post',
                        url: baseurl + 'Site/updpwd',
                        data: {email: email, otp: otp, password: paswd},
                        success: function (data) {
                            var obj = jQuery.parseJSON(data);
                            if (obj.status == 305) {
                                $('#fgtresponse').html(obj.message);
                            } else if (obj.status == 304) {
                                $('#fgtresponse').html(obj.message);
                            } else if (obj.status == 1) {
                                $('#fgtresponse').html('');
                                location.reload();
                            }
                        },
                    });
                }
            }
        });
        $('.form').on('submit', function (e) {
            var email = $("#email").val();
            var password = $("#login-password").val();
            if (email == '') {
                $('#response').html('Please enter emailid');
                e.preventDefault();
            } else if (password == '') {
                $('#response').html('Please enter password');
                e.preventDefault();
            }
            else {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: baseurl + 'Site/loginmod', 
                    data: {email: email, password: password},
                    success: function (data) {
                        var obj = jQuery.parseJSON(data);
                        if (obj.status == 200) {
                            location.reload();
                        } else if (obj.status == 300) {
                            $('#response').html(obj.message);
                        } else if (obj.status == 301) {
                            $('#response').html(obj.message);
                        }
                    },
                });
            }
        });
//        $('#guest_checkout').on('click', function (e) {
//            if ($(this).is(":checked"))
//            {
//                $('#guest_name').show();
//            } else {
//                $('#guest_name').hide();
//            }
//        });
        $('.formreg').on('submit', function (e) {
            var emailid = $("#emailid").val();
            var name = $("#name").val();
            var mobile = $("#mobile").val();
            var password = $("#password").val();
            var password1 = $("#password1").val();
            var otp = $("#otp").val();
            var enq = $('.enq').val();
            var guest_checkout = $('#guest_checkout').val();

            if (guest_checkout == 2)
            {
                var account_type = 2;
                $('.guest_name').show();
            } else {
                $('.guest_name').hide();
                var account_type = 1;
            }
            if (account_type == 2) {
                var g_name = $("#g_name").val();
                if (emailid == '') {
                    $('#regresponse').html('Please enter email');
                    e.preventDefault();
                }
                if (g_name == '') {
                    $('#regresponse').html('Please enter Guest Name');
                    e.preventDefault();
                }
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: baseurl + 'Site/guestregistr',
                    data: {emailid: emailid, name: g_name, account_type: account_type},
                    success: function (data) {
                        var obj = jQuery.parseJSON(data);
                        
                        if (obj.status == 305) {
                            $('#regresponse').html(obj.message);
                        } else if (obj.status == 301) {
                            $('#regresponse').html(obj.message);
                        } else if (obj.status == 1) {
                            $('#regresponse').html('');
                            location.reload();
                        }
                    },
                });


            } else {

                if (enq == 0) {
                    if (emailid == '') {
                        $('#regresponse').html('Please enter email');
                        e.preventDefault();
                    }
                    else {
                        e.preventDefault();
                        $.ajax({
                            type: 'post',
                            url: baseurl + 'Site/regmod',
                            data: {emailid: emailid},
                            success: function (data) {
                                var obj = jQuery.parseJSON(data);
                                if (obj.status == 200) {
                                    $('#regresponse').html(obj.message);
                                    $('#regpart').show();
                                    $('.enq').val(1);
                                }
                                if (obj.status == 300) {
                                    $('#regresponse').html(obj.message);
                                    $('#regpart').show();
                                    $('.enq').val(1);
                                }
                                if (obj.status == 301) {
                                    $('#regresponse').html(obj.message);
                                    $('#regpart').hide();
                                    $('.enq').val(0);
                                }
                            },
                        });
                    }
                } else {
                    if (emailid == '') {
                        $('#regresponse').html('Please enter email');
                        e.preventDefault();
                    }
                    else if (name == '') {
                        $('#regresponse').html('Please enter name');
                        e.preventDefault();
                    }
                    else if (mobile == '') {
                        $('#regresponse').html('Please enter mobile number');
                        e.preventDefault();
                    } else if (password == '') {
                        $('#regresponse').html('Please enter password');
                        e.preventDefault();
                    } else if (password1 == '') {
                        $('#regresponse').html('Please enter confirm password');
                        e.preventDefault();
                    } else if (password1 != password) {
                        $('#regresponse').html("Passwords doesn't match");
                        e.preventDefault();
                    } 
                    //else if (otp == '') {
                     //   $('#regresponse').html('Please enter Verification Code');
                    //    e.preventDefault();
                   // }
                    else {
                        e.preventDefault();
                        $.ajax({
                            type: 'post',
                            url: baseurl + 'Site/registr',
                            data: {emailid: emailid,mobile:mobile, name: name, password: password, password1: password1, otp: otp},
                            success: function (data) {
                                var obj = jQuery.parseJSON(data);
                                if (obj.status == 305) {
                                    $('#regresponse').html(obj.message);
                                } else if (obj.status == 304) {
                                    $('#regresponse').html(obj.message);
                                } else if (obj.status == 1) {
                                    $('#regresponse').html('');
                                    location.reload();
                                }
                            },
                        });
                    }
                }
            }
        });
    });
    function Login() {
        var result;
        showLoader();
        $.ajax({
            type: "POST",
            cache: 'false',
            async: false, url: baseurl + 'Site/login',
            data: {res: res}
        }).done(function (data) {
            result = data;
            hideLoader();
        });
        return result;
    }
</script>