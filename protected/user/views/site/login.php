<?php echo $this->renderPartial('mainmodal'); ?>
<section class="login_page">
        <div class="container content-body listings_page">
                <div class="breadcrumb">
                        <a href="">Home</a> / Login
                </div>
                <div class="login_form_container">
                        <div class="login_form">
                                <form name="frmlogins" class="formloginnew" action="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/login" method="POST">
                                        <h1>LOGIN</h1>
                                        <?php if (Yii::app()->user->hasFlash('login_list')): ?>
                                                <div class="alert alert-danger">
                                                    <?php echo Yii::app()->user->getFlash('login_list'); ?>
                                                </div>
                                        <?php endif; ?>
                                        <div class="form-group">
                                                <input type="text" name="email" id="" class="form-control" placeholder="Email Address">
                                        </div>
                                        <div class="form-group">
                                                <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                                <div class="row">
                                                        <div class="col-xs-6">
                                                                <span class="forgot">Forgot your password?</span>
                                                        </div>
                                                        <div class="col-xs-6 text-right">
                                                                <span class="newuser sign-up">New User <i class="fa fa-arrow-circle-right"></i></span>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <button class="button" name="Submit">SUBMIT</button>
                                        </div>
                                </form>
                        </div>

                </div>


        </div>
</section>
<script>
  $(document).ready(function () {
     $('.forgot').click(function () {
          $("#forgotpw").modal('show');
            $("#login").modal('hide');
            $("#register").modal('hide');
           
        });
  });
</script>