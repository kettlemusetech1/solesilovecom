<?php echo $this->renderPartial('//site/mail/_email_header'); ?>



<tr>
        <td style="padding:40px 20px; font-family:'Open Sans',arial, sans-serif; font-size:13px"><p>Hi <?php echo $model->first_name; ?>,<br/>Greetings from Artstra.com!</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">You have added Your Email ID to Artstra's newsletter</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">You will get our news updates.</p>

                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Thank You,</p>

                <br/>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#abaaaa;">* This is an automatically generated email, please do not reply to this email.</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#abaaaa;">* This email address was provided on our registration page. If you own the email and did not register on our site, please send an email to support@artstra.com</p>
        </td>
</tr>






<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>