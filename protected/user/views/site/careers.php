<section class="about-wrp">
        <div class="container">
                <div class="row">
                        <div class="col-sm-3">
                                <div class="side-menu-main">
                                        <div class="side-menu">
                                                <h3>Category</h3>
                                                <ul class="side-menu-ul">
                                                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/aboutus">About Us</a></li>
                                                        <li class="active"><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/careers">Careers</a></li>
                                                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/faq">FAQ</a></li>
                                                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/policy">Return Policy</a></li>
                                                        <li><a href="<?php echo Yii::app()->baseUrl; ?>/index.php/site/terms">Terms & Conditions</a></li>
                                                </ul>
                                        </div>
                                </div>
                        </div>

                        <div class="col-sm-9">
                                <ul class="breadcrumbs list-inline">
                                        <li><a href="javascript:void(0)">Home</a></li>
                                        <li class="active"><?= $model->title; ?></li>
                                </ul>
                                <div class="aboutus-det">
                                        <div class="about-head clearfix">
                                                <h1><?= $model->title; ?></h1>
                                        </div>
                                        <?= $model->big_content; ?>
                                </div>
                        </div>

                </div>
        </div>
</section>