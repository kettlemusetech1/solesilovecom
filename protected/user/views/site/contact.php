<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section class="contact-wrp">
        <div class="container">
                <ul class="breadcrumbs list-inline">
                        <li><a href="javascript:void(0)">Home</a></li>
                        <li class="active">Contact Us</li>
                </ul>
                <div class="contact-main">
                        <div class="row">
                                <div class="col-sm-4">
                                        <div class="contact-address clearfix">
                                                <h4>Contact Us</h4>
                                                <div class="contact-add-box">
                                                        <div class="contact-icon"><i class="fa fa-map-marker"></i></div>
                                                        <div class="contact-content">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                                                        </div>
                                                </div>
                                                <div class="contact-add-box">
                                                        <div class="contact-icon"><i class="fa fa-phone"></i></div>
                                                        <div class="contact-content">
                                                                <p>+91123 456 7890 (India)</p>
                                                                <p>+91123 456 7890 (India)</p>
                                                        </div>
                                                </div>
                                                <div class="contact-add-box">
                                                        <div class="contact-icon"><i class="fa fa-envelope-o"></i></div>
                                                        <div class="contact-content">
                                                                <p>info@solesilove.com.au</p>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="contact-address clearfix">
                                                <h4>Subscribe to Our <br>newsletter</h4>
                                                <div class="login-form-inputs material-slide-line">
                                                        <span class="input input--akira">
                                                                <input class="input__field input__field--akira" type="text" id="input-2" />
                                                                <label class="input__label input__label--akira" for="input-2">
                                                                        <span class="input__label-content input__label-content--akira">Email Address</span>
                                                                </label>
                                                        </span>
                                                        <button class="butter login-btn">Subscribe</button>
                                                </div>
                                        </div>
                                </div>
                                <div class="col-sm-8">
                                        <div class="login-form-inputs material-slide-line clearfix">
                                                <h3>ENquire Now</h3>
                                                <span class="input input--akira input-half">
                                                        <input class="input__field input__field--akira" type="text" id="input-1" />
                                                        <label class="input__label input__label--akira" for="input-1">
                                                                <span class="input__label-content input__label-content--akira">First Name *</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <input class="input__field input__field--akira" type="text" id="input-2" />
                                                        <label class="input__label input__label--akira" for="input-2">
                                                                <span class="input__label-content input__label-content--akira">Last Name *</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <label for="sel1" class="input__label input__label--akira select__label">
                                                                <select class="select__option" id="sel1">
                                                                        <option value="" disabled selected>Country *</option>
                                                                        <option>India</option>
                                                                        <option>Siriya</option>
                                                                        <option>Somalia</option>
                                                                </select>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <input class="input__field input__field--akira" type="text" id="input-2" />
                                                        <label class="input__label input__label--akira" for="input-2">
                                                                <span class="input__label-content input__label-content--akira">Email *</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <input class="input__field input__field--akira" type="text" id="input-2" />
                                                        <label class="input__label input__label--akira" for="input-2">
                                                                <span class="input__label-content input__label-content--akira">Phone</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <input class="input__field input__field--akira" type="text" id="input-1" />
                                                        <label class="input__label input__label--akira" for="input-1">
                                                                <span class="input__label-content input__label-content--akira">Mobile *</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <input class="input__field input__field--akira" type="text" id="input-1" />
                                                        <label class="input__label input__label--akira" for="input-1">
                                                                <span class="input__label-content input__label-content--akira">Password *</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira input-half">
                                                        <input class="input__field input__field--akira" type="text" id="input-1" />
                                                        <label class="input__label input__label--akira" for="input-1">
                                                                <span class="input__label-content input__label-content--akira">Confirm Password *</span>
                                                        </label>
                                                </span>
                                                <button class="butter login-btn">Submit</button>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section>





























<?php
/* @var $this ContactUsController */
/* @var $model ContactUs */
/* @var $form CActiveForm */
?>
<div class="container main_container inner_pages centerd_page">
        <!--<div class="breadcrumbs"> <a href="#">HOME</a> <span>/</span> SHOPPING BAG </div>-->
        <h1>Contact Us</h1>



        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'contact-us-contact-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation' => false,
        ));
        ?>

        <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success normal">
                        <strong>Success!</strong> <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
        <?php endif; ?>
        <?php if (Yii::app()->user->hasFlash('error')): ?>
                <div class="alert alert-danger">
                        <strong>Danger!</strong> <?php echo Yii::app()->user->getFlash('error'); ?>
                </div>
        <?php endif; ?>
        <div class="registration_form">
                <div class="row">
                        <div class="col-sm-6">
                                <?php echo $form->labelEx($model, 'name'); ?>
                                <?php echo $form->textField($model, 'name', array('size' => 60, 'class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'name', array('style' => 'color:red')); ?>
                        </div>

                        <div class="col-sm-6">
                                <?php echo $form->labelEx($model, 'email'); ?>
                                <?php echo $form->textField($model, 'email', array('size' => 60, 'class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'email', array('style' => 'color:red')); ?>
                        </div>
                </div>
                <div class="row">
                        <div class="col-sm-6">
                                <?php echo $form->labelEx($model, 'phone'); ?>
                                <?php echo $form->textField($model, 'phone', array('size' => 60, 'class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'phone', array('style' => 'color:red')); ?>
                        </div>
                        <div class="col-sm-6">
                                <?php echo $form->labelEx($model, 'comment'); ?>
                                <?php echo $form->textArea($model, 'comment', array('rows' => 5, 'cols' => 60, 'class' => 'form-control')); ?>
                                <?php echo $form->error($model, 'comment', array('style' => 'color:red')); ?>
                        </div>

                </div>
                <div class="text-center form_button">
                        <?php echo CHtml::submitButton('Submit', array('class' => 'btn-primary')); ?>
                </div>

                <?php $this->endWidget(); ?>
        </div>