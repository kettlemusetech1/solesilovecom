<section class="contact-wrp">
        <div class="container">
                <ul class="breadcrumbs list-inline">
                        <li><a href="javascript:void(0)">Home</a></li>
                        <li class="active">Location</li>
                </ul>
                <div class="contact-main">
                        <div class="row">
                                <div class="col-sm-4">
                                        <div class="contact-address clearfix">
                                                <h4>Contact Us</h4>
                                                <?php
                                                $add = StaticPage::model()->findByPk(12);
                                                $eml = StaticPage::model()->findByPk(13);
                                                $phn = StaticPage::model()->findByPk(14);
                                                ?>
                                                <div class="contact-add-box">
                                                        <div class="contact-icon"><i class="fa fa-map-marker"></i></div>
                                                        <div class="contact-content">
                                                                <?= $add->big_content; ?>
                                                        </div>
                                                </div>
                                                <div class="contact-add-box">
                                                        <div class="contact-icon"><i class="fa fa-phone"></i></div>
                                                        <div class="contact-content">
                                                                <?= $phn->big_content; ?>

                                                        </div>
                                                </div>
                                                <div class="contact-add-box">
                                                        <div class="contact-icon"><i class="fa fa-envelope-o"></i></div>
                                                        <div class="contact-content">
                                                                <?= $eml->big_content; ?>
                                                        </div>
                                                </div>
                                        </div>

                                </div>
                                <div class="col-sm-8">
                                        <div class="login-form-inputs material-slide-line clearfix">
                                                <h3>Location</h3>
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3929.1540767033207!2d76.30691031479427!3d10.004128992848289!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b080f2639561fd3%3A0xe12d7c568bd2d7b8!2sInter+Smart+Solution+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1497264368397" width="756" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                        </div>

                                </div>
                        </div>
                </div>
        </div>
</section>