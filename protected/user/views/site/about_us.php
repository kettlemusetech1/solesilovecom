<?php
$get_cat_name = StaticPage::model()->findByPk(6);
$this->setPageTitle($get_cat_name->meta_title);
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_keywords, 'keywords');
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_description, 'description');
?>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / <a href="#">Privacy Policy</a>
                </div>
                <!--<h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Cart</h2>-->
                <div class="row">
                        <div class="col-sm-3">
                                <div class="profile_menu">
                                       <h3><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Return/">Returns and Exchanges</a></h3>
                                        <h3><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Policy/">Privacy Policy</a></h3>
                                        <h3><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Shipping/">Shipping and Delivery</a></h3>
                                        <h3><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/About/">About Us</a></h3>
                                </div>
                        </div>
                        <div class="col-sm-9 static_page_content_area">
                           
                                <h1>About Us</h1>
                                  <div class="abn">Company ABN: 55619543994</div>
                                <?php echo StaticPage::model()->findByPk(6)->big_content; ?></div>
                </div>

        </div>
</section>
<!--Start Footer-->