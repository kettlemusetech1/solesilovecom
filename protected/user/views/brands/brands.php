<?php
$get_cat_name = StaticPage::model()->findByPk(8);
$this->setPageTitle($get_cat_name->meta_title);
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_keywords, 'keywords');
Yii::app()->clientScript->registerMetaTag($get_cat_name->meta_description, 'description');
?>
<section>
        <div class="container content-body listings_page brands-page">
                <div class="breadcrumb">
                        <a href="<?= Yii::app()->baseUrl; ?>">Home</a> / Brands
                </div>
                <h1>BRANDS</h1>
                <div class="row brand_row">
                        <?php
                        foreach ($brands as $brand) {
                            ?>
                            <div class="col-sm-3 col-xs-6">
                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/brand/<?php echo $brand->canonical_name; ?> "> <img src="<?= Yii::app()->baseUrl; ?>/uploads/brands/<?= $brand->id; ?>.<?= $brand->image; ?>" alt=""/></a>
                            </div>
                            <?php
                        }
                        ?>
                </div>

        </div>
</section>