<?php
$category_name = Yii::app()->request->getParam('name');
if ($category_name != "") {
        $get_cat_name = ProductCategory::model()->findByAttributes(array('canonical_name' => $category_name));
}
$url = $_SERVER[REQUEST_URI];
$exp = explode("name=", $url);
$val = $exp[1];
if (!empty($getproducts)) {
        foreach ($getproducts as $get_p) {
                $pid[] = $get_p->id;
        }
}
if (!empty($pid)) {
        $pid = array_unique($pid);

        $imp_value = implode(',', $pid);
}
if (!empty($imp_value)) {
        $get_all_option = OptionDetails::model()->findAllByAttributes(array('status' => 1), array('condition' => ' `product_id` IN (' . $imp_value . ')'));
}
?>
<!-- End Of Header Section-->
<section>
        <div class="container content-body listings_page">
                <?php echo $this->renderPartial('//products/_bread_crumb', array('category_name' => $category_name)); ?>
                <div class="row">
                        <?php echo $this->renderPartial('//products/left_menu', array('category' => $category, 'parent' => $parent, 'getproducts' => $getproducts, 'get_all_option' => $get_all_option, 'brand' => $brand)); ?>

                        <div class="col-sm-9 main-content ">
                                <h1><?php echo $brand->brand_name; ?></h1>
                                <h5 class = "sub_head">From Soles iLove</h5>
                                <div class = "sorter-row">
                                        <div class = "row">
                                                <?php
                                                $form = $this->beginWidget('CActiveForm', array(
                                                    'id' => 'form_id',
                                                    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
                                                    // Please note: When you enable ajax validation, make sure the corresponding
                                                    // controller action is handling ajax validation correctly.
                                                    // There is a call to performAjaxValidation() commented in generated controller code.
                                                    // See class documentation of CActiveForm for details on this.
                                                    'enableAjaxValidation' => false,
                                                ));
                                                ?>
                                                <div class="col-sm-8 grid-selector">
                                                        <a href="#" class="active grid-switch grid-view-btn"><i class="fa fa-th"></i> Grid</a>
                                                        <a href="#" class="grid-switch list-view-btn"><i class="fa fa-list-ul"></i> List</a>
                                                        <a href="#" class="filter hidden visible-xs cat_btn"><i class="fa fa-sitemap"></i> Category</a>
                                                        <a href="#" class="filter hidden visible-xs filt_btn"><i class="fa fa-filter"></i> Filter</a>
                                                        <div class="sort">
                                                                <label for="" class="hidden-xs">Sort By</label>
                                                                <i class="fa fa-sort-alpha-asc hidden visible-xs pull-left"></i>
                                                                <select name="sorting" id="" onchange="products();">
                                                                        <option <?= Yii::app()->session['sort_id'] == 1 ? 'selected' : ''; ?> value="1"> <?= Yii::app()->session['sort_id'] == 1 ? '&#10004;' : ''; ?> Newest</option>
                                                                        <option <?= Yii::app()->session['sort_id'] == 2 ? 'selected' : ''; ?> value="2"><?= Yii::app()->session['sort_id'] == 2 ? '&#10004;' : ''; ?> Price: Low To High</option>
                                                                        <option <?= Yii::app()->session['sort_id'] == 3 ? 'selected' : ''; ?> value="3"><?= Yii::app()->session['sort_id'] == 3 ? '&#10004;' : ''; ?> Price: High To Low</option>
                                                                        <option <?= Yii::app()->session['sort_id'] == 4 ? 'selected' : ''; ?> value="4"><?= Yii::app()->session['sort_id'] == 4 ? '&#10004;' : ''; ?> Name: A-Z</option>
                                                                        <option <?= Yii::app()->session['sort_id'] == 5 ? 'selected' : ''; ?> value="5"><?= Yii::app()->session['sort_id'] == 5 ? '&#10004;' : ''; ?> Name: Z-A</option>
                                                                </select>
                                                                <i class="fa fa-long-arrow-up hidden-xs"></i>
                                                                <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                </div>
                                                <div class="col-sm-4 pager">
                                                        <div class="perpage">
                                                                <label for="">Show</label>
                                                                <select name="pagesize" id="" onchange="products();">
                                                                        <option  <?= Yii::app()->session['pagesize'] == 15 ? 'selected' : ''; ?> value="15">15</option>
                                                                        <option <?= Yii::app()->session['pagesize'] == 30 ? 'selected' : ''; ?> value="30">30</option>
                                                                        <option <?= Yii::app()->session['pagesize'] == 60 ? 'selected' : ''; ?> value="60">60</option>
                                                                        <option <?= Yii::app()->session['pagesize'] == 120 ? 'selected' : ''; ?> value="120">120</option>
                                                                        <option <?= Yii::app()->session['pagesize'] == '' ? 'selected' : ''; ?> value="">All Products</option>
                                                                </select>
                                                                <label for="">per page</label>
                                                                <div class="clearfix"></div>
                                                        </div>
                                                </div>
                                                <?php $this->endWidget(); ?>
                                        </div>
                                </div>
                                <div class="product_list_main">
                                        <div class="row " style="position: relative">
                                                <div class="product_loader"><img style="width: 150px;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/product_loader.gif"/></div>
                                                <div>
                                                        <?php if (Yii::app()->user->hasFlash('success')): ?>
                                                                <div class="alert alert-success mesage">
                                                                        <?php echo Yii::app()->user->getFlash('success'); ?>
                                                                </div>
                                                        <?php endif; ?>
                                                        <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                                <div class="alert alert-danger mesage msg2">
                                                                        <?php echo Yii::app()->user->getFlash('error'); ?>
                                                                </div>
                                                        <?php endif; ?>
                                                </div>
                                                <?php
                                                if (!empty($dataprovider) || $dataProvider != '') {
                                                        $this->widget('zii.widgets.CListView', array(
                                                            'dataProvider' => $dataProvider,
                                                            'itemView' => '//products/_view',
                                                            'template' => "{items}\n{pager}",
                                                            'id' => 'product-grid',
                                                            'ajaxUpdate' => 'true',
                                                            'emptyText' => 'No Product found.',
                                                            'enablePagination' => true,
                                                        ));
                                                } else {
                                                      ?>
                                                    <div style="margin-left:15px;padding-top: 36px;">
                                                        <?php
                                                        echo 'No Product Found';
                                                        ?>
                                                        </div>
                                                        <?php
                                                }
                                                ?>



                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</section>
<!--Start Footer-->
<div class="fadeIn animated quick_view modal" id="quick_view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog zoomIn animated" role="document">
                <div class="modal-content quick_view_content">



                </div>
                <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>

<script>
        function products() {
                document.getElementById("form_id").submit();
        }
        // Size Selector
        $(".size_selector input").change(function() {
                $('#selected_size').val(this.id);
                productFilter();

        });
</script>