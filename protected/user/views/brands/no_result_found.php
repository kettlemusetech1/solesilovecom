<div class="container">
        <div class="row">
                <div class="col-xs-12 col-sm-12 text-center">
                        <img class="no_result_image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/no_result_found.png"/>
                        <h3>Sorry, no products found in this brand!</h3>
                        <!--<h4>Please check the spelling or try searching for something else</h4>-->
                </div>
        </div>
</div>