<?php
session_start();
 Yii::import('application.user.extensions.eway.*');          
 include 'lib/eWAY/RapidAPI.php';

$request = new eWAY\CreateAccessCodeRequest();

        if (!empty($_POST['txtTokenCustomerID'])) {
                $request->Customer->TokenCustomerID = $_POST['txtTokenCustomerID'];
        }

        $request->Customer->Reference = $_POST['txtCustomerRef'];
       // $request->Customer->Title = $_POST['ddlTitle'];
        $request->Customer->FirstName = $_POST['txtFirstName'];
        $request->Customer->LastName = $_POST['txtLastName'];
      //  $request->Customer->CompanyName = $_POST['txtCompanyName'];
      //  $request->Customer->JobDescription = $_POST['txtJobDescription'];
        $request->Customer->Street1 = $_POST['txtStreet'];
        $request->Customer->City = $_POST['txtCity'];
        $request->Customer->State = $_POST['txtState'];
        $request->Customer->PostalCode = $_POST['txtPostalcode'];
        $request->Customer->Country = $_POST['txtCountry'];
        $request->Customer->Email = $_POST['txtEmail'];
      //  $request->Customer->Phone = $_POST['txtPhone'];
        $request->Customer->Mobile = $_POST['txtMobile'];
        $request->Customer->Comments = $_POST['txtComments'];
        $request->Customer->Fax = $_POST['txtFax'];
        $request->Customer->Url = $_POST['txtUrl'];

        // Populate values for ShippingAddress Object.
        // This values can be taken from a Form POST as well. Now is just some dummy data.
        $request->ShippingAddress->FirstName =  $_POST['delivery_firstname'];
        $request->ShippingAddress->LastName = $_POST['delivery_lastname'];
        $request->ShippingAddress->Street1 = $_POST['delivery_address'];
      //  $request->ShippingAddress->Street2 = " Square";
        $request->ShippingAddress->City = $_POST['delivery_city'];
        $request->ShippingAddress->State = $_POST['delivery_state'];
        $request->ShippingAddress->Country = $_POST['delivery_country'];
        $request->ShippingAddress->PostalCode = $_POST['delivery_zip'];
        $request->ShippingAddress->Email = $_POST['txtEmail'];
        $request->ShippingAddress->Phone = $_POST['delivery_tel'];
        // ShippingMethod, e.g. "LowCost", "International", "Military". Check the spec for available values.
        $request->ShippingAddress->ShippingMethod = "LowCost";

        if ($_POST['ddlMethod'] == 'ProcessPayment' || $_POST['ddlMethod'] == 'Authorise' || $_POST['ddlMethod'] == 'TokenPayment') {
                // Populate values for LineItems
                $item1 = new eWAY\LineItem();
                $item1->SKU = "SKU1";
                $item1->Description = "Description1";
                $item2 = new eWAY\LineItem();
                $item2->SKU = "SKU2";
                $item2->Description = "Description2";
                $request->Items->LineItem[0] = $item1;
                $request->Items->LineItem[1] = $item2;

                // Populate values for Payment Object
                $request->Payment->TotalAmount = $_POST['txtAmount'];
                $request->Payment->InvoiceNumber = $_POST['txtCustomerRef'];
                $request->Payment->InvoiceDescription = $_POST['txtInvoiceDescription'];
                $request->Payment->InvoiceReference = $_POST['txtInvoiceReference'];
                $request->Payment->CurrencyCode = $_POST['txtCurrencyCode'];
        }

        // Populate values for Options (not needed since it's in one script)
        $opt1 = new eWAY\Option();
        $opt1->Value = $_POST['txtOption1'];
        $opt2 = new eWAY\Option();
        $opt2->Value = $_POST['txtOption2'];
        $opt3 = new eWAY\Option();
        $opt3->Value = $_POST['txtOption3'];

        $request->Options->Option[0] = $opt1;
        $request->Options->Option[1] = $opt2;
        $request->Options->Option[2] = $opt3;

        // Build redirect URL
        $self_url = 'http';
        if (!empty($_SERVER['HTTPS'])) {
                $self_url .= "s";
        }
        $self_url .= "://" . $_SERVER["SERVER_NAME"];
        if ($_SERVER["SERVER_PORT"] != "80") {
                $self_url .= ":" . $_SERVER["SERVER_PORT"];
        }
        $self_url .= $_SERVER["REQUEST_URI"];

        $request->RedirectUrl = $self_url;
        $request->Method = $_POST['ddlMethod'];
        $request->TransactionType = $_POST['ddlTransactionType'];

        // Call RapidAPI
        $eway_params = array();
        if ($_POST['ddlSandbox'])
                $eway_params['sandbox'] = true;
        $service = new eWAY\RapidAPI($_POST['APIKey'], $_POST['APIPassword'], $eway_params);
        $result = $service->CreateAccessCode($request);

        // Check if any error returns
        if (isset($result->Errors)) {
                // Get Error Messages from Error Code.
                $ErrorArray = explode(",", $result->Errors);
                $lblError = "";
                foreach ($ErrorArray as $error) {
                        $error = $service->getMessage($error);
                        $lblError .= $error . "<br />\n";
                }
        } else {
                $_SESSION['eWAY_key'] = $_POST['APIKey'];
                $_SESSION['eWAY_password'] = $_POST['APIPassword'];
                $_SESSION['eWAY_sandbox'] = $_POST['ddlSandbox'];

                $in_page = 'payment_page';
        }
        
        ?>
        
         <div id="maincontent">
                                                                                        <div class="response">
                                                                                                <div class="fields">
                                                                                                        <label for="lblAuthorisationCode">
                                                                                                                Authorisation Code</label>
                                                                                                        <label id="lblAuthorisationCode"><?php echo isset($result->AuthorisationCode) ? $result->AuthorisationCode : ""; ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblInvoiceNumber">
                                                                                                                Invoice Number</label>
                                                                                                        <label id="lblInvoiceNumber"><?php echo $result->InvoiceNumber; ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblInvoiceReference">
                                                                                                                Invoice Reference</label>
                                                                                                        <label id="lblInvoiceReference"><?php echo $result->InvoiceReference; ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblOption1">
                                                                                                                Option1</label>
                                                                                                        <label id="lblOption1"><?php echo isset($result->Options[0]->Value) ? $result->Options[0]->Value : ""; ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblOption2">
                                                                                                                Option2</label>
                                                                                                        <label id="lblOption2"><?php echo isset($result->Options[1]->Value) ? $result->Options[1]->Value : ""; ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblOption3">
                                                                                                                Option3</label>
                                                                                                        <label id="lblOption3"><?php echo isset($result->Options[2]->Value) ? $result->Options[2]->Value : ""; ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblResponseCode">
                                                                                                                Response Code</label>
                                                                                                        <label id="lblResponseCode"><?php echo $result->ResponseCode; ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblResponseMessage">
                                                                                                                Response Message</label>
                                                                                                        <label id="lblResponseMessage">
                                                                                                                <?php
                                                                                                                if (isset($result->ResponseMessage)) {
                                                                                                                        //Get Error Messages from Error Code.
                                                                                                                        $ResponseMessageArray = explode(",", $result->ResponseMessage);
                                                                                                                        $responseMessage = "";
                                                                                                                        foreach ($ResponseMessageArray as $message) {
                                                                                                                                $real_message = $service->getMessage($message);
                                                                                                                                if ($message != $real_message)
                                                                                                                                        $responseMessage .= $message . " " . $real_message . "<br>";
                                                                                                                                else
                                                                                                                                        $responseMessage .= $message;
                                                                                                                        }
                                                                                                                        echo $responseMessage;
                                                                                                                }
                                                                                                                ?>
                                                                                                        </label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblTokenCustomerID">
                                                                                                                TokenCustomerID
                                                                                                        </label>
                                                                                                        <label id="lblTokenCustomerID"><?php
                                                                                                                if (isset($result->TokenCustomerID)) {
                                                                                                                        echo $result->TokenCustomerID;
                                                                                                                }
                                                                                                                ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblTotalAmount">
                                                                                                                Total Amount</label>
                                                                                                        <label id="lblTotalAmount"><?php
                                                                                                                if (isset($result->TotalAmount)) {
                                                                                                                        $amount = $result->TotalAmount;
                                                                                                                        echo '$' . number_format($amount / 100, 2);
                                                                                                                }
                                                                                                                ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblTransactionID">
                                                                                                                TransactionID</label>
                                                                                                        <label id="lblTransactionID"><?php
                                                                                                                if (isset($result->TransactionID)) {
                                                                                                                        echo $result->TransactionID;
                                                                                                                }
                                                                                                                ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblTransactionStatus">
                                                                                                                Transaction Status</label>
                                                                                                        <label id="lblTransactionStatus"><?php
                                                                                                                if (isset($result->TransactionStatus) && $result->TransactionStatus && (is_bool($result->TransactionStatus) || $result->TransactionStatus != "false")) {
                                                                                                                        echo 'True';
                                                                                                                } else {
                                                                                                                        echo 'False';
                                                                                                                }
                                                                                                                ?></label>
                                                                                                </div>
                                                                                                <div class="fields">
                                                                                                        <label for="lblBeagleScore">
                                                                                                                Beagle Score</label>
                                                                                                        <label id="lblBeagleScore"><?php
                                                                                                                if (isset($result->BeagleScore)) {
                                                                                                                        echo $result->BeagleScore;
                                                                                                                }
                                                                                                                ?></label>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>

                                                                      

                                                                        <br />
                                                                        <br />
                                                                        <a href="index.php">[Start Over]</a><br />
                                                                        <br />

                                                                        <a href="#" id="showraw">[Show raw request & response]</a>

                                                                        <div id="raw">
                                                                                <div style="width: 45%; margin-right: 2em; background: #f3f3f3; float:left; overflow: scroll; white-space: nowrap;">
                                                                                        <?php echo $service->getLastUrl(); ?><br>
                                                                                                <pre id="request_dump"></pre>
                                                                                </div>
                                                                                <div style="width: 45%; margin-right: 2em; background: #f3f3f3; float:left; overflow: scroll; white-space: nowrap;"><pre id="response_dump"></pre></div>
                                                                        </div>
                                                                        <script>
                                                                                jQuery('#raw').hide();
                                                                                // no body for GetAccessCodeResult
                                                                                var response_dump = JSON.stringify(JSON.parse('<?php echo $service->getLastResponse(); ?>'), null, 4);
                                                                                jQuery('#response_dump').html(response_dump);

                                                                                jQuery("#showraw").click(function() {
                                                                                        if (jQuery('#raw:visible').length)
                                                                                                jQuery('#raw').hide();
                                                                                        else
                                                                                                jQuery('#raw').show();
                                                                                });
                                                                        </script>

                                                                        <div id="maincontentbottom">
                                                                        </div>