<?php echo $this->renderPartial('//site/mainmodal'); ?>
<?php $default_billing = UserAddress::model()->findByAttributes(array('default_billing_address' => 1)); ?>
<?php $default_shipping = UserAddress::model()->findByAttributes(array('default_shipping_address' => 1)); ?>
<section>
        <div class="container content-body listings_page cart-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / Checkout
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Checkout</h2>
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'user-address-form',
                    'htmlOptions' => array('class' => 'form-group'),
                    'action' => Yii::app()->baseUrl . '/CheckOut/CheckOut/',
                    'enableAjaxValidation' => false,
                ));
                ?>
                <?php if (Yii::app()->user->hasFlash('success')):
                    ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if (Yii::app()->user->hasFlash('checkout_error')): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo Yii::app()->user->getFlash('checkout_error'); ?>
                    </div>
                <?php endif; ?>


                <?php if (Yii::app()->user->hasFlash('shipp_availability')): ?>
                    <div class="alert1 alert-danger1">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>
                            <?php echo Yii::app()->user->getFlash('shipp_availability'); ?>
                    </div>
                <?php endif; ?>
                
                <div class="row cart_details">
                        <div class="col-sm-9">
                                <div class="panel panel-default cart_panel">
                                        <div class="panel-heading checkout_panel_head">
                                                <h1>BILLING ADDRESS</h1>
                                        </div>
                                        <div class="panel-body checkout_panel_body">
                                                <div class="billing-form">
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">
                                                                                <select  name="bill_address" class="select_bill_exist select__option form-control" id="bill_exist">
                                                                                        <option selected=""  value="0">New Address</option>
                                                                                        <?php
                                                                                        foreach ($addresss as $address) {
                                                                                            ?>
                                                                                            <?php if ($address->address_for == '') { ?>
                                                                                                <option <?php
                                                                                                if ($address->default_billing_address == 1) {
                                                                                                    echo 'selected';
                                                                                                }
                                                                                                ?>  value="<?php echo $address->id; ?>"><?php echo $address->first_name; ?> <?php echo $address->last_name; ?></option>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        ?>
                                                                                                <option <?php
                                                                                                if ($address->default_billing_address == 1) {
                                                                                                    echo 'selected';
                                                                                                }
                                                                                                ?>  value="<?php echo $address->id; ?>"><?php echo $address->address_for; ?></option>
                                                                                                    <?php } ?>
                                                                                                    <?php
                                                                                                    if (isset($_GET['box'])) {
                                                                                                        echo "Success!";
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                </select>
                                                                        </div>

                                                                </div>
                                                        </div> <!--/formgroup-->
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($billing, '[bill]first_name', array('class' => 'form-control ', 'placeholder' => 'First Name')); ?>
                                                                                <?php echo $form->error($billing, '[bill]first_name'); ?>
                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($billing, '[bill]last_name', array('class' => 'form-control ', 'placeholder' => 'Last Name')); ?>
                                                                                <?php echo $form->error($billing, '[bill]last_name'); ?>
                                                                        </div>
                                                                </div>
                                                        </div> <!--/formgroup-->
                                                        <div class="form-group">
                                                                <?php echo $form->textField($billing, '[bill]address_1', array('class' => 'form-control ', 'placeholder' => 'Address Line 1')); ?>
                                                                <?php echo $form->error($billing, '[bill]address_1'); ?>
                                                        </div><!--/formgroup-->
                                                        <div class="form-group">

                                                                <?php echo $form->textField($billing, '[bill]address_2', array('class' => 'form-control ', 'placeholder' => 'Address Line 2')); ?>
                                                                <?php echo $form->error($billing, '[bill]address_2'); ?>
                                                        </div><!--/formgroup-->
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">

                                                                                <?php echo CHtml::activeDropDownList($billing, '[bill]country', CHtml::listData(Countries::model()->findAll(), 'id', 'country_name'), array('empty' => '--Country--', 'class' => 'form-control select__option billing_country', 'autocomplete' => 'off')); ?>
                                                                                <?php echo $form->error($billing, '[bill]country'); ?>

                                                                        </div>
                                                                        <div class="col-xs-6">

                                                                                <?php
                                                                                $state_options = array();
                                                                                if ($billing->country != 0) {
                                                                                    $states = States::model()->findAllByAttributes(array('country_id' => $billing->country));
                                                                                    if (!empty($states)) {
                                                                                        $state_options[""] = "--Select--";
                                                                                        foreach ($states as $state) {
                                                                                            $state_options[$state->state_name] = $state->state_name;
                                                                                        }
                                                                                    } else {
                                                                                        $state_options[""] = "--Select State--";
                                                                                        $state_options[0] = "Other";
                                                                                    }
                                                                                } else {
                                                                                    $state_options[""] = '--select--';
                                                                                }
                                                                                ?>
                                                                                <span class="cn_ext" style="display: block; <?php
//                                                                                if ($model->isNewRecord) {
//                                                                                    echo 'display: block !important';
//                                                                                } else if ($states != NULL) {
//                                                                                    echo 'display: block !important';
//                                                                                } else {
//                                                                                    echo 'display: none !important';
//                                                                                }
                                                                                ?>">
                                                                                              <?php echo CHtml::activeDropDownList($billing, '[bill]state', $state_options, array('class' => 'select__option form-control', 'autocomplete' => 'off', 'options' => array('state' => array('selected' => 'selected')))); ?>


                                                                                        <?php echo $form->error($billing, '[bill]state'); ?>
                                                                                </span>

                                                                                <span class=" not_ext" style="display: none;   <?php
//                                                                                if ($model->isNewRecord) {
//                                                                                    echo 2500;
//                                                                                    echo 'display: none ';
//                                                                                } else if ($states == NULL) {
//                                                                                    echo 2501;
//                                                                                    echo 'display: block !important';
//                                                                                } else {
//                                                                                    echo 'display: none ';
//                                                                                }
                                                                                ?>">
                                                                                        <input class="v_state b_state1 form-control" id="bill_type_1" name="UserAddress[bill][state1]" placeholder="State" value="<?php echo $billing->state; ?>"  type="text" maxlength="111">

                                                                                </span>


                                                                        </div>
                                                                </div>
                                                        </div> <!--/formgroup-->
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($billing, '[bill]city', array('class' => 'form-control', 'placeholder' => 'City')); ?>

                                                                                <?php echo $form->error($billing, '[bill]city'); ?>

                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($billing, '[bill]postcode', array('class' => 'form-control', 'placeholder' => 'POST CODE')); ?>
                                                                                <?php echo $form->error($billing, '[bill]postcode'); ?>
                                                                        </div>
                                                                </div>
                                                        </div> <!--/formgroup-->
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($billing, '[bill]phonecode', array('class' => 'form-control', 'placeholder' => 'PHONE CODE')); ?>

                                                                                <?php echo $form->error($billing, '[bill]phonecode'); ?>

                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($billing, '[bill]contact_number', array('class' => 'form-control', 'placeholder' => 'CONTACT NUMBER')); ?>
                                                                                <?php echo $form->error($billing, '[bill]contact_number'); ?>
                                                                        </div>
                                                                </div>
                                                        </div> <!--/formgroup-->
                                                </div>
                                        </div>

                                </div>
                                <div class="panel panel-default cart_panel">
                                        <div class="panel-heading checkout_panel_head">
                                                <h2>SHIPPING ADDRESS <span><label for="same-shipping"> <input id="profedit_color01" name="billing_same" type="checkbox" > Same as billing Address.</label></span></h2>
                                        </div>
                                        <div class="panel-body checkout_panel_body">
                                                <div class="billing-form">
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">
                                                                                <select  name="ship_address" class="select_ship_exist select__option form-control" id="ship_exist">
                                                                                        <option  value="0">New Address</option>
                                                                                        <?php
                                                                                        foreach ($addresss as $address) {
                                                                                            ?>
                                                                                            <?php if ($address->address_for == '') { ?>
                                                                                                <option <?php
                                                                                                if ($address->default_shipping_address == 1) {
                                                                                                    echo 'selected';
                                                                                                }
                                                                                                ?>   value="<?php echo $address->id; ?>"><?php echo $address->first_name; ?> <?php echo $address->last_name; ?> </option>
                                                                                                        <?php
                                                                                                    } else {
                                                                                                        ?>
                                                                                                <option <?php
                                                                                                if ($address->default_shipping_address == 1) {
                                                                                                    echo 'selected';
                                                                                                }
                                                                                                ?>   value="<?php echo $address->id; ?>"><?php echo $address->address_for; ?></option>
                                                                                                    <?php } ?>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                </select>
                                                                        </div>

                                                                </div>
                                                        </div> <!--/formgroup-->
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($shipping, '[ship]first_name', array('class' => 'form-control', 'placeholder' => 'First Name')); ?>
                                                                                <?php echo $form->error($shipping, '[ship]first_name'); ?>
<!--                                                                                <input type="text" name="" id="" class="form-control error" placeholder="First Name">-->
                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($shipping, '[ship]last_name', array('class' => 'form-control', 'placeholder' => 'Last Name')); ?>
                                                                                <?php echo $form->error($shipping, '[ship]last_name'); ?>
                                                                        </div>
                                                                </div>
                                                        </div> <!--/formgroup-->
                                                        <div class="form-group">
                                                                <?php echo $form->textField($shipping, '[ship]address_1', array('class' => 'form-control', 'placeholder' => 'Address Line 1')); ?>
                                                                <?php echo $form->error($shipping, '[ship]address_1'); ?>
                                                        </div><!--/formgroup-->
                                                        <div class="form-group">
                                                                <?php echo $form->textField($shipping, '[ship]address_2', array('class' => 'form-control', 'placeholder' => 'Address Line 2')); ?>
                                                                <?php echo $form->error($shipping, '[ship]address_2'); ?>
                                                        </div><!--/formgroup-->
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">
                                                                                <?php echo CHtml::activeDropDownList($shipping, '[ship]country', CHtml::listData(Countries::model()->findAllByAttributes([], ['condition' => 'id = 13 OR id = 153']), 'id', 'country_name'), array('empty' => '--Country--', 'class' => 'select__option shipping_country form-control', 'autocomplete' => 'off')); ?>
                                                                                <?php echo $form->error($shipping, '[ship]country'); ?>
                                                                        </div>
                                                                        <div class="col-xs-6">

                                                                                <?php
                                                                                $state_options1 = array();
                                                                                if ($shipping->country != 0) {
                                                                                    $states1 = States::model()->findAllByAttributes(array('country_id' => $shipping->country));
                                                                                    if (!empty($states1)) {
                                                                                        $state_options1[""] = "--Select--";
                                                                                        foreach ($states1 as $state) {
                                                                                            $state_options1[$state->state_name] = $state->state_name;
                                                                                        }
                                                                                    } else {
                                                                                        $state_options1[""] = "--Select State--";
                                                                                        $state_options1[0] = "Other";
                                                                                    }
                                                                                } else {
                                                                                    $state_options1[""] = '--select--';
                                                                                }
                                                                                ?>

                                                                                <span class=" ship_cn_ext" style="display: block;  <?php
//                                                                                if ($model->isNewRecord) {
//                                                                                    echo 'display: block !important';
//                                                                                } else if ($states1 != NULL) {
//                                                                                    echo 'display: block !important';
//                                                                                } else {
//                                                                                    echo 'display: none !important';
//                                                                                }
                                                                                ?>">

                                                                                        <?php echo CHtml::activeDropDownList($shipping, '[ship]state', $state_options1, array('class' => 'select__option form-control', 'autocomplete' => 'off', 'options' => array('state' => array('selected' => 'selected')))); ?>




                                                                                        <?php echo $form->error($shipping, '[ship]state'); ?>
                                                                                </span>

                                                                                <span class=" ship_not_ext" style="display: none; <?php
//                                                                                if ($model->isNewRecord) {
//                                                                                    echo 'display: none !important';
//                                                                                } else if ($states1 == NULL) {
//                                                                                    echo 'display: block !important';
//                                                                                } else {
//                                                                                    echo 'display: none ';
//                                                                                }
                                                                                ?>">
                                                                                        <input  class="v_state s_state1 form-control" id="bill_type_2" name="UserAddress[ship][state1]" value="<?php echo $shipping->state; ?>" placeholder="State"  type="text" maxlength="111">

                                                                                        <?php echo $form->error($shipping, '[ship]state'); ?>
                                                                                </span>


                                                                        </div>
                                                                </div>
                                                        </div> <!--/formgroup-->
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($shipping, '[ship]city', array('class' => 'form-control', 'placeholder' => 'City')); ?>
                                                                                <?php echo $form->error($shipping, '[ship]city'); ?>

                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($shipping, '[ship]postcode', array('class' => 'form-control', 'placeholder' => 'POST CODE')); ?>
                                                                                <?php echo $form->error($shipping, '[ship]postcode'); ?>
                                                                        </div>
                                                                </div>
                                                        </div> <!--/formgroup-->
                                                        <div class="form-group">
                                                                <div class="row">
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($shipping, '[ship]phonecode', array('class' => 'form-control', 'placeholder' => 'Phone Code')); ?>
                                                                                <?php echo $form->error($shipping, '[ship]phonecode'); ?>

                                                                        </div>
                                                                        <div class="col-xs-6">
                                                                                <?php echo $form->textField($shipping, '[ship]contact_number', array('class' => 'form-control', 'placeholder' => 'CONTACT NUMBER')); ?>
                                                                                <?php echo $form->error($shipping, '[ship]contact_number'); ?>
                                                                        </div>
                                                                </div>
                                                        </div> <!--/formgroup-->
                                                </div>
                                        </div>

                                </div>

                        </div>

                        <div class="col-sm-3 summary_col">
                                <div class="cart_summary">
                                        <?php
                                        foreach ($carts as $cart) {
                                            $prod_details = Products::model()->findByPk($cart->product_id);
                                            ?>
                                            <?php $producttotal = Yii::app()->Discount->DiscountAmount($prod_details) * $cart->quantity; ?>
                                            <?php $product_price += $producttotal; ?>
                                        <?php } ?>
                                        <?php $subtotal = $product_price; ?>
                                        <h2>Summary</h2>


                                        <?php
                                        foreach ($carts as $cart) {
                                            $prod_details = Products::model()->findByPk($cart->product_id);
                                            $folder = Yii::app()->Upload->folderName(0, 1000, $prod_details->id);
                                            $colors = OptionDetails::model()->findByPk($cart->options);
                                            $sizes = OptionDetails::model()->findByPk($cart->options);
                                            if (!empty($colors)) {
                                                $color_name = OptionCategory::model()->findByPk($colors->color_id);
                                            }
                                            if (!empty($sizes)) {
                                                $size_name = OptionCategory::model()->findByPk($sizes->size_id);
                                            }
                                            if(($sizes->width_id != 0)){
                                                $widthset= WidthFitting::model()->findByPk($sizes->width_id)->title;
                                            }
                                            ?>
                                            <?php $producttotal = Yii::app()->Discount->DiscountAmount($prod_details) * $cart->quantity; ?>
                                            <?php $product_price += $producttotal; ?>
                                            <div class="summary_row product_item">
                                                    <div class="col-left">
                                                            <h3><a style="color:#fff;" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $prod_details->canonical_name; ?>"><?php echo substr($prod_details->product_name, 0, 15) . '..'; ?></a></h3>
                                                            <div class="product_srar summary disabled"><a href="#" class="star1 active"><i class="fa fa-star"></i></a><a href="#" class="star2"><i class="fa fa-star"></i></a><a href="#" class="star3"><i class="fa fa-star"></i></a><a href="#" class="star4"><i class="fa fa-star"></i></a><a href="#" class="star5"><i class="fa fa-star"></i></a></div>
                                                            <?php if (!empty($size_name)) { ?> <div><label for="">Size: </label> <?php echo $size_name->size; ?></div><?php } ?>
                                                            <?php if (!empty($color_name)) { ?> <div><label>Color: </label> <span style="width: 35px; height: 35px;background-image:url('<?php echo Yii::app()->request->baseUrl; ?>/uploads/colors/<?php echo $color_name->id; ?>.<?php echo $color_name->image; ?>'); background-size: cover;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
                                                            <?php } if (($sizes->width_id != 0)){ ?> <div><label for="">Width: </label> <span style="margin-right: 20px;"><?php echo $widthset; ?></span></div><?php } ?>
                                                            <div><label for="">Quantity: </label> <?php echo $cart->quantity; ?></div>
                                                    </div>
                                                    <dov class="col-right">
                                                            <?php
                                                            if (isset(Yii::app()->session['currency'])) {
                                                                $prod_price = round(Yii::app()->Discount->DiscountAmount($prod_details), 2) * $cart->quantity;
                                                                echo Yii::app()->Currency->convert($prod_price);
                                                            } else {
                                                                $prod_price = Yii::app()->Discount->DiscountAmount($prod_details) * $cart->quantity;
                                                                echo Yii::app()->Currency->convert($prod_price);
                                                            }
                                                            ?>
                                                    </dov>
                                            </div>
                                        <?php } ?>

                                        <div class="summary_row sub_total">
                                                <div class="col-left">
                                                        <h4><strong>SUB TOTAL</strong> <a href=""  data-toggle="tooltip" data-placement="bottom" title="Total product amount"><i class="fa fa-question-circle"></i></a></h4>

                                                </div>
                                                <dov class="col-right">
                                                        <strong><?php echo Yii::app()->Currency->convert($subtotal); ?></strong>
                                                </dov>
                                        </div>
                                        <div class="summary_row shipping">
                                                <div class="col-left">
                                                        <label for="">Shipping </label>
                                                </div>
                                                <div class="col-right">
                                                        <?php
                                                        $user = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                                                        if ($user->first_purchase == 0 && $user->account_type ==1) {
                                                            
                                                            echo Yii::app()->Currency->convert(0);
                                                        } else {
                                                            
                                                            echo Yii::app()->Currency->convert($this->shipping());
                                                        }
                                                        ?>
                                                        <!--<span id="shipping_charge"></span>-->
                                                </div>
                                                <div class="clearfix"></div>

                                                <!--                                                <div class="col-left">
                                                                                                        <label for="">Tax </label>
                                                                                                </div>
                                                                                                <dov class="col-right">
                                                                                                        $AUD 000.00
                                                                                                </dov>-->
                                        </div>
                                        <div class="summary_row sub_total">
                                                <div class="col-left">
                                                        <h4><strong>Order Total</strong></h4>

                                                </div>
                                                <dov class="col-right">
                                                        <!--<strong><span id="grant_total"></span></strong>-->
                                                        <strong><?php echo Yii::app()->Currency->convert($subtotal + $this->shipping()); ?></strong>
                                                </dov>
                                        </div>

                                        
                                       <!-- <div class="summary_row shipping">
                                                <div class="col-left">
                                                        <label for="">GST <small style="    font-size: 10px;
    font-style: italic;">(<?php echo Settings::model()->findByAttributes(array('id'=>1))->tax_rate;?>%) </small> </label>
                                                </div>
                                                <dov class="col-right">
                                                        <strong><span id="total_tax"></span></strong>
                                                        
                                                </dov>
                                        </div>-->
                                        <?php if (UserDetails::model()->findByPk(Yii::app()->session['user']['id'])->wallet_amt != 0) { ?>
                                            <div class="summary_row shipping">
                                                    <div class="myCredit">
                                                            <label for="">MY CREDIT</label>
                                                            <input type="text" name="wallet_amount" id="" class="form-control wallet_amount" checkout_amount="" placeholder="Amount From Credit">
                                                            <p>Available  Credit  <?php echo Yii::app()->Currency->convert(UserDetails::model()->findByPk(Yii::app()->session['user']['id'])->wallet_amt); ?></p>
                                                    </div>

                                            </div>
                                        <?php } else { ?>
                                            <input type="hidden" class="wallet_amount" checkout_amount="" name="wallet_amount" value="0"  />
                                        <?php } ?>
                                        <div class="summary_row shipping">
                                                <div class="col-left">
                                                        <label for="">Total Amount To Pay </label>
                                                </div>
                                                <dov class="col-right">
                                                        <strong><span id="total_pay"></span></strong>
                                                        <input type="hidden" name="total_pay" class="total_pay" />
                                                </dov>
                                        </div>
                                        <div class="summary_row shipping">
                                                <div class="profedit-color-box">
                                                        <input id="profedit_color02" class="payment_agree"  type="checkbox" name="payment_agree">
                                                        <label for="profedit_color02">Accept our terms & policies</label>
                                                        <div id="agrees" required="required" style="display: none; color:red">Please accept our Terms and Policies.</div>
                                                </div>
                                                <div class="check_out_btn">
                                                        <button type="submit" class="btn btn-primary no-arrow butter login-btn" name="payment_method" value="2" id="order_payment"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/paypal-btn.png" alt=""/></button>
                                                        <!--<a href="#" class="btn btn-primary no-arrow"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/paypal-btn.png" alt=""/></a>-->
                                                </div>
      
					<div class="check_out_btn">
						<button type="submit" class="btn btn-primary no-arrow butter login-btn" name="payment_method" value="3" id="order_payment"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/paypal-btn-paypal.png" alt=""/></button>
						</div>
                                                <div class="text-centre">
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/" class="continue">CONTINUE SHOPPING <i class="fa fa-angle-right"></i> </a>
                                                </div>
                                        </div>
                                </div>
                        </div>

                </div>
                <?php $this->endWidget(); ?>

        </div>
</section>
<div class="clearfix"></div>
<style>
        .not_ext{
                display: none;
        }
        .ship_not_ext{
                display: none;
        }
        .time{
                font-size:12px;
                font-weight:none;
        }


        .form-control.error {
                border-color: rgba(205,32,35,1.00);
        }

</style>

<script>
    $(document).ready(function () {
            var country = $("#UserAddress_bill_country").val();
            if (country == '') {
                    country = 99;
            }
            $.ajax({
                    type: "POST",
                    url: baseurl + "ajax/selectState",
                    data: {country: country}
            }).done(function (data) {
//                        if (data == 0) {
//                                $('.cn_ext').hide();
//                                $('.not_ext').show();
//                        } else {
//                                $('.not_ext').hide();
//                                $('.cn_ext').show();
//                        }
            });


    });

    $(document).ready(function () {
<?php if ($shipping->hasErrors()) { ?>
                $('.select_ship_exist').val(0);
                $('.ship_form').show();
<?php } ?>
<?php if ($billing->hasErrors()) { ?>
                $('.select_bill_exist').val(0);
                $('.bill_form').show();
<?php } ?>
            totalcalculate();
            $(".wallet_amount").blur(function () {

                    var wallet = $(this).val();
                    var checkout_amount = $(this).attr('checkout_amount');
                    var userwallet = <?php echo UserDetails::model()->findByPk(Yii::app()->session['user']['id'])->wallet_amt; ?>;
                    if (wallet != '') {
                            
                            if (wallet >= checkout_amount)
                            {
                               $(".wallet_amount").val(checkout_amount);
                                 
                            } 
                            if (wallet < 0) {
                                    alert('wallet Amount is Invalid');
                                    $(".wallet_amount").val('');
                                    return false;
                            }
                            if (wallet > userwallet) {
                                    alert('wallet Amount is should not exeed your Available credit.');
                                    $(".wallet_amount").val('');
                                    return false;
                            }
                            totalcalculate();
                    }

            });
            $("#order_payment").click(function () {


                    if ($('.payment_agree').is(":checked"))
                    {
                            $('#agrees').hide();
                    } else {
                            $('#agrees').show();
                            return false;
                    }
            });

            $('#profedit_color01').on('click', function (e) {
                    if ($('#profedit_color01').is(':checked')) {
                            $('#UserAddress-ship-id').prop('disabled', true);
                            var first_name = $('#UserAddress_bill_first_name').val();
                            var last_name = $('#UserAddress_bill_last_name').val();
                            var address = $('#UserAddress_bill_address_1').val();
                            var address2 = $('#UserAddress_bill_address_2').val();
                            var country = $('#UserAddress_bill_country').val();
                            var state = $('#UserAddress_bill_state').val();
                            var state1 = $('#bill_type_1').val();
                            var city = $('#UserAddress_bill_city').val();
                            var post_code = $('#UserAddress_bill_postcode').val();
                            var phone_code = $('#UserAddress_bill_phonecode').val();
                            var contact_number = $('#UserAddress_bill_contact_number').val();



                           
                            if (country == 13 || country == 153 ) {
                                    getshipstate(country, state);
                            }else{
                              $('#UserAddress_ship_state').html('<option value="" selected="selected">--select--</option>');
                                
                            }
                            $('#UserAddress_ship_first_name').val(first_name).prop('readonly', true);
                            $('#UserAddress_ship_last_name').val(last_name).prop('readonly', true);
                            $('#UserAddress_ship_address_1').val(address).prop('readonly', true);
                            $('#UserAddress_ship_address_2').val(address2).prop('readonly', true);
                            $('#ship_exist').prop('disabled', true);
                            $('#UserAddress_ship_country option[value= ' + country + ']').prop('selected', true).prop('readonly', true);
                            $('#UserAddress_ship_city').val(city).prop('readonly', true);
                            $('#UserAddress_ship_postcode').val(post_code).prop('readonly', true);
                            $('#UserAddress_ship_phonecode').val(phone_code).prop('readonly', true);
                            $('#UserAddress_ship_contact_number').val(contact_number).prop('readonly', true);

                    } else {
                            $('#ship_exist').prop('disabled', false);
                            $('#ship_exist option:first').prop('selected', true).prop('disabled', false);
                            $('#UserAddress_ship_id').prop('disabled', false).prop('readonly', false);
                            $('#UserAddress_ship_first_name').val('').prop('readonly', false);
                            $('#UserAddress_ship_last_name').val('').prop('readonly', false);
                            $('#UserAddress_ship_address_1').val('').prop('readonly', false);
                            $('#UserAddress_ship_address_2').val('').prop('readonly', false);
                            $('#UserAddress_ship_state option:first').prop('selected', true).prop('readonly', false);
                            $('#UserAddress_ship_country option:first').prop('selected', true).prop('readonly', false);
                            $('#UserAddress_ship_city').val('').prop('readonly', false);
                            $('#UserAddress_ship_postcode').val('').prop('readonly', false);
                            $('#UserAddress_ship_phonecode').val('').prop('readonly', false);
                            $('#UserAddress_ship_contact_number').val('').prop('readonly', false);
                            $('#bill_type_2').val('');


                    }
            });
            function getshipstate(country, state) {
                    $.ajax({
                            url: baseurl + 'ajax/SelectShipState',
                            type: 'POST',
                            data: {'country': country, 'state': state},
                            success: function (data) {
                                    if (data == 0) {
                                            $('.s_state1').val(state);

                                            $('.ship_cn_ext').hide();
                                            $('.ship_not_ext').show();
                                    } else {
                                            $('.s_state1').val('');
                                            $('.ship_not_ext').hide();
                                            $('.ship_cn_ext').show();
                                            $('#UserAddress_ship_state').html(data).prop('readonly', true);
                                    }
                                    $('#UserAddress_ship_state').html(data).prop('readonly', true);
                            }
                    });
            }

            function getbillstate(country, state) {
                    $.ajax({
                            url: baseurl + '/ajax/SelectShipState',
                            type: 'POST',
                            data: {'country': country, 'state': state},
                            success: function (data) {
                                    if (data == 0) {
                                            $('.v_state').val(state);

                                            $('.cn_ext').hide();
                                            $('.not_ext').show();
                                    } else {
                                            $('.not_ext').hide();
                                            $('.cn_ext').show();

                                            $('#UserAddress_bill_state').html(data);
//                                            $('.s_state1').val('');
//                                            $('.ship_not_ext').hide();
//                                            $('.ship_cn_ext').show();
//                                            $('#UserAddress_ship_state').html(data).prop('readonly', true);
                                    }

                            }
                    });
            }
            $('#bill_exist').on('change', function (e) {
                    var aid = $(this).val();
                    if (aid == 0) {

                            $('#UserAddress_bill_first_name').val('').prop('readonly', false);
                            $('#UserAddress_bill_last_name').val('').prop('readonly', false);
                            $('#UserAddress_bill_address_1').val('').prop('readonly', false);
                            $('#UserAddress_bill_address_2').val('').prop('readonly', false);
                            $('#UserAddress_bill_state option:first').prop('selected', true).prop('readonly', false);
                            $('#UserAddress_bill_country option:first').prop('selected', true).prop('readonly', false);
                            $('#UserAddress_bill_city').val('').prop('readonly', false);
                            $('#UserAddress_bill_postcode').val('').prop('readonly', false);
                            $('#UserAddress_bill_phonecode').val('').prop('readonly', false);
                            $('#UserAddress_bill_contact_number').val('').prop('readonly', false);
                    } else {
                            $.ajax({
                                    url: baseurl + 'ajax/SelectAddress',
                                    type: 'POST',
                                    data: {'aid': aid},
                                    success: function (data) {
                                            var obj = jQuery.parseJSON(data);
                                            country = obj.address.country;
                                            state = obj.address.state;
                                            getbillstate(country, state);


                                            $('#UserAddress_bill_first_name').val(obj.address.first_name).prop('readonly', true);
                                            $('#UserAddress_bill_last_name').val(obj.address.last_name).prop('readonly', true);
                                            $('#UserAddress_bill_address_1').val(obj.address.address_1).prop('readonly', true);
                                            $('#UserAddress_bill_address_2').val(obj.address.address_2).prop('readonly', true);
                                            $('#UserAddress_bill_country option[value= ' + obj.address.country + ']').prop('selected', true).prop('readonly', true);
                                            $('#UserAddress_bill_city').val(obj.address.city).prop('readonly', true);
                                            $('#UserAddress_bill_postcode').val(obj.address.postcode).prop('readonly', true);
                                            $('#UserAddress_bill_phonecode').val(obj.address.phonecode).prop('readonly', true);
                                            $('#UserAddress_bill_contact_number').val(obj.address.contact_number).prop('readonly', true);

                                    }
                            });
                    }
            });
            $('#ship_exist').on('change', function (e) {
                    var aid = $(this).val();
                    if (aid == 0) {

                            $('#UserAddress_ship_first_name').val('').prop('readonly', false);
                            $('#UserAddress_shipl_last_name').val('').prop('readonly', false);
                            $('#UserAddress_ship_address_1').val('').prop('readonly', false);
                            $('#UserAddress_ship_address_2').val('').prop('readonly', false);
                            $('#UserAddress_ship_state option:first').prop('selected', true).prop('readonly', false);
                            $('#UserAddress_ship_country option:first').prop('selected', true).prop('readonly', false);
                            $('#UserAddress_ship_city').val('').prop('readonly', false);
                            $('#UserAddress_ship_postcode').val('').prop('readonly', false);
                            $('#UserAddress_ship_phonecode').val('').prop('readonly', false);
                            $('#UserAddress_ship_contact_number').val('').prop('readonly', false);
                    } else {
                            $.ajax({
                                    url: baseurl + 'ajax/SelectAddress',
                                    type: 'POST',
                                    data: {'aid': aid},
                                    success: function (data) {
                                            var obj = jQuery.parseJSON(data);
                                            country = obj.address.country;
                                            state = obj.address.state;
                                            getshipstate(country, state);


                                            $('#UserAddress_ship_first_name').val(obj.address.first_name).prop('readonly', true);
                                            $('#UserAddress_ship_last_name').val(obj.address.last_name).prop('readonly', true);
                                            $('#UserAddress_ship_address_1').val(obj.address.address_1).prop('readonly', true);
                                            $('#UserAddress_ship_address_2').val(obj.address.address_2).prop('readonly', true);
                                            $('#UserAddress_ship_country option[value= ' + obj.address.country + ']').prop('selected', true).prop('readonly', true);
                                            $('#UserAddress_ship_city').val(obj.address.city).prop('readonly', true);
                                            $('#UserAddress_ship_postcode').val(obj.address.postcode).prop('readonly', true);
                                            $('#UserAddress_ship_phonecode').val(obj.address.phonecode).prop('readonly', true);
                                            $('#UserAddress_ship_contact_number').val(obj.address.contact_number).prop('readonly', true);

                                    }
                            });
                    }
            });
            $('#UserAddress_ship_country').on('change', function (e) {
                    totalcalculate();
            });
            function totalcalculate() {
                    var wallet = $(".wallet_amount").val();
                    var country = $("#UserAddress_ship_country").val();
                    // showLoader();
                    $.ajax({
                            type: "POST",
                            cache: 'false',
                            async: false,
                            url: baseurl + 'CheckOut/totalcalculate',
                            data: {wallet: wallet, country: country}
                    }).done(function (data) {
                            var obj = jQuery.parseJSON(data);

                            $("#wallet_total").html(obj.wallet_balance);
                            if (obj.total == 0) {
                                    $(".wallet_amount").val(obj.wallet);
                                    $("#total_pay").html(obj.totalamounttopay);
                                     $("#total_tax").html(obj.tax);
                                    $(".total_pay").val(obj.total);
                                    $('.total_to_pay').hide();
                                    $('#laksyah_order_payment').val('CONFIRM ORDER');
                                    $('.wallet_amount').attr('checkout_amount',obj.checkout_amount);
                            } else {
                                    $(".wallet_amount").val(obj.wallet);
                                     $("#total_tax").html(obj.tax);
                                    $('.total_to_pay').show();
                                    $("#total_pay").html(obj.totalamounttopay);
                                    $(".total_pay").val(obj.total);
                                    $('#laksyah_order_payment').val('PAY SECURELY NOW');
                                    $('.wallet_amount').attr('checkout_amount',obj.checkout_amount);
                                    
                            }
                            //hideLoader();
                    });
            }
    }
    );
</script>