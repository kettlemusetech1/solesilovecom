<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section class="login-wrp content-body">
        <div class="container">
                <div>
                        <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#loginreg01" aria-controls="loginreg01" role="tab" data-toggle="tab">Order Success</a></li>
                        </ul>
                        <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="loginreg01">
                                        <div class="login-form-inputs login-input-main material-slide-line">

                                                <h4>Dear Customer Your Order Has been Successfully Created . Please Check your Order History</h4>
                                                <h4><a style="text-decoration: none; color: orange" href="<?php echo Yii::app()->request->baseUrl; ?>/">Continue Shopping</a></h4>
                                                </form>
                                        </div>
                                </div>

                        </div>

                </div>
        </div>
</section>

