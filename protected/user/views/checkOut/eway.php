<?php
session_start();
Yii::import('application.user.extensions.eway.*');
include 'lib/eWAY/RapidAPI.php';
 
//require($path);
$in_page = 'before_submit';
 
 
if (isset($_POST['btnSubmit'])) {
 
    $request = new eWAY\CreateAccessCodeRequest();
 
    if (!empty($_POST['txtTokenCustomerID'])) {
        $request->Customer->TokenCustomerID = $_POST['txtTokenCustomerID'];
    }
 
    $request->Customer->Reference = $_POST['txtCustomerRef'];
    // $request->Customer->Title = $_POST['ddlTitle'];
    $request->Customer->FirstName = $_POST['txtFirstName'];
    $request->Customer->LastName = $_POST['txtLastName'];
    //  $request->Customer->CompanyName = $_POST['txtCompanyName'];
    //  $request->Customer->JobDescription = $_POST['txtJobDescription'];
    $request->Customer->Street1 = $_POST['txtStreet'];
    $request->Customer->City = $_POST['txtCity'];
    $request->Customer->State = $_POST['txtState'];
    $request->Customer->PostalCode = $_POST['txtPostalcode'];
    $request->Customer->Country = $_POST['txtCountry'];
    $request->Customer->Email = $_POST['txtEmail'];
    //  $request->Customer->Phone = $_POST['txtPhone'];
    $request->Customer->Mobile = $_POST['txtMobile'];
    $request->Customer->Comments = $_POST['txtComments'];
    $request->Customer->Fax = $_POST['txtFax'];
    $request->Customer->Url = $_POST['txtUrl'];
 
    // Populate values for ShippingAddress Object.
    // This values can be taken from a Form POST as well. Now is just some dummy data.
    $request->ShippingAddress->FirstName = $_POST['delivery_firstname'];
    $request->ShippingAddress->LastName = $_POST['delivery_lastname'];
    $request->ShippingAddress->Street1 = $_POST['delivery_address'];
    //  $request->ShippingAddress->Street2 = " Square";
    $request->ShippingAddress->City = $_POST['delivery_city'];
    $request->ShippingAddress->State = $_POST['delivery_state'];
    $request->ShippingAddress->Country = $_POST['delivery_country'];
    $request->ShippingAddress->PostalCode = $_POST['delivery_zip'];
    $request->ShippingAddress->Email = $_POST['txtEmail'];
    $request->ShippingAddress->Phone = $_POST['delivery_tel'];
    // ShippingMethod, e.g. "LowCost", "International", "Military". Check the spec for available values.
    $request->ShippingAddress->ShippingMethod = "LowCost";
 
    if ($_POST['ddlMethod'] == 'ProcessPayment' || $_POST['ddlMethod'] == 'Authorise' || $_POST['ddlMethod'] == 'TokenPayment') {
        // Populate values for LineItems
        $item1 = new eWAY\LineItem();
        $item1->SKU = "SKU1";
        $item1->Description = "Description1";
        $item2 = new eWAY\LineItem();
        $item2->SKU = "SKU2";
        $item2->Description = "Description2";
        $request->Items->LineItem[0] = $item1;
        $request->Items->LineItem[1] = $item2;
 
        // Populate values for Payment Object
        $request->Payment->TotalAmount = $_POST['txtAmount'];
        $request->Payment->InvoiceNumber = $_POST['txtCustomerRef'];
        $request->Payment->InvoiceDescription = $_POST['txtInvoiceDescription'];
        $request->Payment->InvoiceReference = $_POST['txtInvoiceReference'];
        $request->Payment->CurrencyCode = $_POST['txtCurrencyCode'];
    }
 
    // Populate values for Options (not needed since it's in one script)
    $opt1 = new eWAY\Option();
    $opt1->Value = $_POST['txtOption1'];
    $opt2 = new eWAY\Option();
    $opt2->Value = $_POST['txtOption2'];
    $opt3 = new eWAY\Option();
    $opt3->Value = $_POST['txtOption3'];
 
    $request->Options->Option[0] = $opt1;
    $request->Options->Option[1] = $opt2;
    $request->Options->Option[2] = $opt3;
 
    // Build redirect URL
    $self_url = 'http';
    if (!empty($_SERVER['HTTPS'])) {
        $self_url .= "s";
    }
    $self_url .= "://" . $_SERVER["SERVER_NAME"];
    if ($_SERVER["SERVER_PORT"] != "80") {
        $self_url .= ":" . $_SERVER["SERVER_PORT"];
    }
    $self_url .= $_SERVER["REQUEST_URI"];
 
    $request->RedirectUrl = $self_url;
    $request->Method = $_POST['ddlMethod'];
    $request->TransactionType = $_POST['ddlTransactionType'];
 
    // Call RapidAPI
    $eway_params = array();
    if ($_POST['ddlSandbox'])
        $eway_params['sandbox'] = false;
    $service = new eWAY\RapidAPI($_POST['APIKey'], $_POST['APIPassword'], $eway_params);
    $result = $service->CreateAccessCode($request);
 
    // Check if any error returns
    if (isset($result->Errors)) {
        // Get Error Messages from Error Code.
        $ErrorArray = explode(",", $result->Errors);
        $lblError = "";
        foreach ($ErrorArray as $error) {
            $error = $service->getMessage($error);
            $lblError .= $error . "<br />\n";
        }
    } else {
        $_SESSION['eWAY_key'] = $_POST['APIKey'];
        $_SESSION['eWAY_password'] = $_POST['APIPassword'];
        $_SESSION['eWAY_sandbox'] = $_POST['ddlSandbox'];
 
        $in_page = 'payment_page';
    }
}
 
if (isset($_GET['AccessCode'])) {
    $AccessCode = $_GET['AccessCode'];
 
    // should be somewhere from config instead of SESSION
    if ($_SESSION['eWAY_key'] && $_SESSION['eWAY_password']) {
        // Call RapidAPI
        $eway_params = array();
        if ($_SESSION['eWAY_sandbox'])
            $eway_params['sandbox'] = false;
        $service = new eWAY\RapidAPI($_SESSION['eWAY_key'], $_SESSION['eWAY_password'], $eway_params);
 
        $request = new eWAY\GetAccessCodeResultRequest();
        $request->AccessCode = $AccessCode;
        $result = $service->GetAccessCodeResult($request);
 
        $in_page = 'view_result';
        if (isset($result->Errors)) {
            $ErrorArray = explode(",", $result->Errors);
            $lblError = "";
            foreach ($ErrorArray as $error) {
                $error = $service->getMessage($error);
                $lblError .= $error . "<br />\n";
            }
        }
    }
}
?>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
        <head>
                <title>eWAY Rapid Transparent Redirect</title>
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Styles/Site.css" rel="stylesheet" type="text/css" />
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Styles/jquery-ui-1.8.11.custom.css" rel="stylesheet" type="text/css" />
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Scripts/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Scripts/jquery.ui.datepicker-en-GB.js" type="text/javascript"></script>
                <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/checkOut/eway/assets/Scripts/tooltip.js"></script>
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet" type="text/css" />
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/developer.css" rel="stylesheet" type="text/css" />
                
                <style>
                    input#btnSubmit {
    background-color: #383838;
    padding: 10px 15px;
    border: none;
    color: #fff;
    font-size: 15px;
        cursor: pointer;
}
.login_form {
    width: 400px;
    max-width: 100%;
    margin: auto;
    padding: 22px;
    background-color: rgb(236, 236, 236);
}
input.button {
    background-color: #383838;
    padding: 10px 15px;
    border: none;
    color: #fff;
    font-size: 15px;
    cursor: pointer;
}
.header {
    font-size: 20px;
    font-family: sans-serif;
    font-weight: bold;
}
label {
    width: auto;
    float: left;
    margin: 5px 0px;
    margin-right: 25px;
}
.what_is_this{
    line-height: 32px;
    color:#666666;
    cursor: help;
}
.cvv_helper{
    padding: 5px;
    background-color: #fff;
    border: solid 1px #ccc;
    position: absolute;
    z-index: 2;
    width: 164px;
    display: none;
	top: 0px;
	left: 60px;
}
@media screen and (max-width:768px){
    .cvv_helper{
        left: auto;
        right:0px;
        bottom: 0px;
		top: auto;
    }
}
                </style>
        </head>
        <body>
                <center>
                        <div id="outer">
                                <div id="toplinks">
                                        <img alt="eWAY Logo" class="logo" src="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Images/companylogo.gif" width="960px" height="65px" />
                                </div>
                                <div id="main">
 
                                        <?php
                                        if ($in_page === 'view_result') {
                                            ?>
 
                                            <div id="titlearea">
                                                    <h2>Response</h2>
                                                    <br>
 
                                                            <br>
                                                                    </div>
 
                                                                    <?php
                                                                    if (isset($lblError)) {
                                                                        ?>
                                                                        <div id="error">
                                                                                <label style="color:red"><?php echo $lblError ?></label>
                                                                        </div>
                                                                    <?php } else { ?>
 
                                                                        <div id="maincontent">
                                                                                <div class="response">
                                                                                        <div class="fields">
                                                                                                <label for="lblAuthorisationCode">
                                                                                                        Authorisation Code</label>
                                                                                                <label id="lblAuthorisationCode"><?php echo isset($result->AuthorisationCode) ? $result->AuthorisationCode : ""; ?></label>
                                                                                        </div>
                                                                                        <div class="fields">
                                                                                                <label for="lblInvoiceNumber">
                                                                                                        Invoice Number</label>
                                                                                                <label id="lblInvoiceNumber"><?php echo $result->InvoiceNumber; ?></label>
                                                                                        </div>
                                                                                        <div class="fields">
                                                                                                <label for="lblInvoiceReference">
                                                                                                        Invoice Reference</label>
                                                                                                <label id="lblInvoiceReference"><?php echo $result->InvoiceReference; ?></label>
                                                                                        </div>
                                                                                        
                                                                                        <div class="fields">
                                                                                                <label for="lblResponseCode">
                                                                                                        Response Code</label>
                                                                                                <label id="lblResponseCode"><?php echo $result->ResponseCode; ?></label>
                                                                                        </div>
                                                                                        <div class="fields">
                                                                                                <label for="lblResponseMessage">
                                                                                                        Response Message</label>
                                                                                                <label id="lblResponseMessage">
                                                                                                        <?php
                                                                                                        if (isset($result->ResponseMessage)) {
                                                                                                            //Get Error Messages from Error Code.
                                                                                                            $ResponseMessageArray = explode(",", $result->ResponseMessage);
                                                                                                            $responseMessage = "";
                                                                                                            foreach ($ResponseMessageArray as $message) {
                                                                                                                $real_message = $service->getMessage($message);
                                                                                                                if ($message != $real_message)
                                                                                                                    $responseMessage .= $message . " " . $real_message . "<br>";
                                                                                                                else
                                                                                                                    $responseMessage .= $message;
                                                                                                            }
                                                                                                            echo $responseMessage;
                                                                                                        }
                                                                                                        ?>
                                                                                                </label>
                                                                                        </div>
                                                                                       
                                                                                        <div class="fields">
                                                                                                <label for="lblTotalAmount">
                                                                                                        Total Amount</label>
                                                                                                <label id="lblTotalAmount"><?php
                                                                                                        if (isset($result->TotalAmount)) {
                                                                                                            $amount = $result->TotalAmount;
                                                                                                            echo '$' . number_format($amount / 100, 2);
                                                                                                        }
                                                                                                        ?></label>
                                                                                        </div>
                                                                                        <div class="fields">
                                                                                                <label for="lblTransactionID">
                                                                                                        TransactionID</label>
                                                                                                <label id="lblTransactionID"><?php
                                                                                                        if (isset($result->TransactionID)) {
                                                                                                            echo $result->TransactionID;
                                                                                                        }
                                                                                                        ?></label>
                                                                                        </div>
                                                                                        <div class="fields">
                                                                                                <label for="lblTransactionStatus">
                                                                                                        Transaction Status</label>
                                                                                                <label id="lblTransactionStatus"><?php
                                                                                                        if (isset($result->TransactionStatus) && $result->TransactionStatus && (is_bool($result->TransactionStatus) || $result->TransactionStatus != "false")) {
                                                                                                            echo 'True';
                                                                                                                header("location:http://solesilove.com.au/index.php/CheckOut/OrderSuccess/payid/" . $result->TransactionID . "/tranid/" . $result->TransactionID . "/amt/" . number_format($amount / 100, 2));
                                                                                                           } else {
                                                                                                            echo 'False';
                                                                                                                 header("location:http://solesilove.com.au/index.php/CheckOut/OrderFailed/payid/" . $result->TransactionID . "/tranid/" . $result->TransactionID . "/amt/" . number_format($amount / 100, 2));
                                                                                                           
                                                                                                        }
                                                                                                        ?></label>
                                                                                        </div>
                                                                                       
                                                                                </div>
                                                                        </div>
 
                                                                    <?php } ?>
 
                                                                    <br />
                                                                    <br />
                                                                     <br />
 
                                                                    <div id="raw">
                                                                            <div style="width: 45%; margin-right: 2em; background: #f3f3f3; float:left; overflow: scroll; white-space: nowrap;">
                                                                                    <?php echo $service->getLastUrl(); ?><br>
                                                                                            <pre id="request_dump"></pre>
                                                                            </div>
                                                                            <div style="width: 45%; margin-right: 2em; background: #f3f3f3; float:left; overflow: scroll; white-space: nowrap;"><pre id="response_dump"></pre></div>
                                                                    </div>
                                                                    <script>
                                        jQuery('#raw').hide();
                                        // no body for GetAccessCodeResult
                                        var response_dump = JSON.stringify(JSON.parse('<?php echo $service->getLastResponse(); ?>'), null, 4);
                                        jQuery('#response_dump').html(response_dump);
 
                                        jQuery("#showraw").click(function () {
                                            if (jQuery('#raw:visible').length)
                                                jQuery('#raw').hide();
                                            else
                                                jQuery('#raw').show();
                                        });
                                                                    </script>
 
                                                                    <div id="maincontentbottom">
                                                                    </div>
 
                                                                    <?php
                                                                } else if ($in_page == 'payment_page') { // else if ($in_page === 'view_result') {
                                                                    ?>
 
                                                                    <div id="titlearea">
 
                                                                    </div>
                                                                    <div id="maincontent">
                                                                            <form method="POST" action="<?php echo $result->FormActionURL ?>" id="form1">
                                                                                    <input type='hidden' name='EWAY_ACCESSCODE' value="<?php echo $result->AccessCode ?>" />
 
                                                                                    <style>
                                                                                            .options li { display: inline-block; padding:10px 0; clear: both; }
                                                                                            .options img { margin-left:10px; top:10px; }
                                                                                    </style>
                                                                                    <div id="paymentoption">
                                                                                            <div class="transactioncustomer">
                                                                                                    <div class="header">Select Payment Option</div>
                                                                                                    <ul class="options">
                                                                                                            <li>
                                                                                                                    <label for="payment_option_creditcard">
                                                                                                                            <input id="payment_option_creditcard" value="creditcard" name="EWAY_PAYMENTTYPE" type="radio">
                                                                                                                                    <img alt="creditcards" src="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Images/creditcard_master.png">
                                                                                                                                            <img alt="creditcards" src="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Images/creditcard_visa.png">
                                                                                                                                                    </label>
                                                                                                                                                    </li>
                                                                                                                                                   <!-- <li>
                                                                                                                                                            <label for="payment_option_paypal">
                                                                                                                                                                    <input id="payment_option_paypal" value="paypal" name="EWAY_PAYMENTTYPE" type="radio">
                                                                                                                                                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Images/paypal.png"></label>
                                                                                                                                                                                    </li>-->
                                                                                                                                                                                    <!--<li>-->
                                                                                                                                                                                    <!--        <label for="payment_option_masterpass">-->
                                                                                                                                                                                    <!--                <input id="payment_option_masterpass" value="masterpass" name="EWAY_PAYMENTTYPE" type="radio">-->
                                                                                                                                                                                    <!--                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Images/masterpass.png"></label>-->
                                                                                                                                                                                    <!--                        </li>-->
                                                                                                                                                                                                            </ul>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div>
                                                                                                                                                                                                            <br />
                                                                                                                                                                                                            <br />
                                                                                                                                                                                                            <input class="button" type="button" value="Continue" onclick="ChoosePaymentOption();" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
 
                                                                                                                                                                                                            <script>
                                                                                                            function ChoosePaymentOption() {
                                                                                                                if (jQuery("input[name='EWAY_PAYMENTTYPE']:checked").val() != 'creditcard') {
                                                                                                                    jQuery('#form1').submit();
                                                                                                                } else {
                                                                                                                    jQuery('#paymentoption').hide();
                                                                                                                    jQuery('#payment').show();
                                                                                                                }
                                                                                                            }
                                                                                                                                                                                                            </script>
 
                                                                                                                                                                                                            <div id="payment" style="display:none">
                                                                                                                                                                                                            <div class="transactioncustomer">
                                                                                                                                                                                                                 <div class="login_form">
                                                                                                                                                                                                            <div class="header">
                                                                                                                                                                                                            Customer Card Details
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                            <div class="row">
                                                                                                                                                                                                                <div class="col-xs-3">
                                                                                                                                                                                                                <label for="EWAY_CARDNAME">Card Holder</label>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                <div class="col-xs-9">
                                                                                                                                                                                                                    <input type='text' class="form-control" name='EWAY_CARDNAME' id='EWAY_CARDNAME' value="" />
                                                                                                                                                                                                                </div>                                                                                                                                                                                                                
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                            <div class="row">
                                                                                                                                                                                                                <div class="col-xs-3">
                                                                                                                                                                                                                    <label for="EWAY_CARDNUMBER">
                                                                                                                                                                                                            Card Number</label>
                                                                                                                                                                                                                 </div>
                                                                                                                                                                                                                <div class="col-xs-9">
                                                                                                                                                                                                                    <input type='text' class="form-control" name='EWAY_CARDNUMBER' id='EWAY_CARDNUMBER' value="" />
                                                                                                                                                                                                                </div>                                                                                                                                                                                                                
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            
                                                                                                                                                                                                            
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                            <div class="row">
                                                                                                                                                                                                                <div class="col-xs-3">
                                                                                                                                                                                                                    <label for="EWAY_CARDEXPIRYMONTH">
                                                                                                                                                                                                            Expiry Date</label>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                <div class="col-xs-9">
                                                                                                                                                                                                                    <select class="frm-ctrl" required="required" ID="EWAY_CARDEXPIRYMONTH" name="EWAY_CARDEXPIRYMONTH">
                                                                                                                                                                                                                        <option value="">Select Month</option>
                                                                                                                                                                                                                                                    <?php
                                                                                                                                                                                                                                                    $expiry_month = date('m');
                                                                                                                                                                                                                                                    for ($i = 1; $i <= 12; $i++) {
                                                                                                                                                                                                                                                        $s = sprintf('%02d', $i);
                                                                                                                                                                                                                                                        echo "<option value='$s'";
                                                                                                                                                                                                                                                       
                                                                                                                                                                                                                                                        echo ">$s</option>\n";
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                    ?>
                                                                                                                                                                                                            </select>
                                                                                                                                                                                                            
                                                                                                                                                                                                            <select class="frm-ctrl" required="required" ID="EWAY_CARDEXPIRYYEAR" name="EWAY_CARDEXPIRYYEAR">
                                                                                                                                                                                                               <option value="">Select Year</option>
                                                                                                                                                                                                                                                    <?php
                                                                                                                                                                                                                                                    $i = date("y");
                                                                                                                                                                                                                                                    $j = $i + 25;
                                                                                                                                                                                                                                                    for ($i; $i <= $j; $i++) {
                                                                                                                                                                                                                                                        echo "<option value='$i'>$i</option>\n";
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                    ?>
                                                                                                                                                                                                            </select>
                                                                                                                                                                                                                </div>                                                                                                                                                                                                                
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            
                                                                                                                                                                                                            
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                           <!-- <div class="form-group">
                                                                                                                                                                                                            <label for="EWAY_CARDSTARTMONTH">
                                                                                                                                                                                                            Valid From Date</label>
                                                                                                                                                                                                            <select class="frm-ctrl" ID="EWAY_CARDSTARTMONTH" name="EWAY_CARDSTARTMONTH">
                                                                                                                                                                                                                                                    <?php
                                                                                                                                                                                                                                                    $expiry_month = ""; //date('m');
                                                                                                                                                                                                                                                    echo "<option></option>";
 
                                                                                                                                                                                                                                                    for ($i = 1; $i <= 12; $i++) {
                                                                                                                                                                                                                                                        $s = sprintf('%02d', $i);
                                                                                                                                                                                                                                                        echo "<option value='$s'";
                                                                                                                                                                                                                                                       echo ">$s</option>\n";
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                    ?>
                                                                                                                                                                                                            </select>
                                                                                                                                                                                                            /
                                                                                                                                                                                                            <select class="frm-ctrl" ID="EWAY_CARDSTARTYEAR" name="EWAY_CARDSTARTYEAR">
                                                                                                                                                                                                                                                    <?php
                                                                                                                                                                                                                                                    $i = date("y");
                                                                                                                                                                                                                                                    $j = $i - 11;
                                                                                                                                                                                                                                                    echo "<option></option>";
                                                                                                                                                                                                                                                    for ($i; $i >= $j; $i--) {
                                                                                                                                                                                                                                                        $year = sprintf('%02d', $i);
                                                                                                                                                                                                                                                        echo "<option value='$year'>$year</option>\n";
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                    ?>
                                                                                                                                                                                                            </select>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                            <label for="EWAY_CARDISSUENUMBER">
                                                                                                                                                                                                            Issue Number</label>
                                                                                                                                                                                                            <input type='text' class="form-control" name='EWAY_CARDISSUENUMBER' id='EWAY_CARDISSUENUMBER' value="" maxlength="2" style="width:40px;"/>
                                                                                                                                                                                                            </div>-->
                                                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                                            <div class="row">
                                                                                                                                                                                                                <div class="col-xs-3">
                                                                                                                                                                                                                <label for="EWAY_CARDCVN">
                                                                                                                                                                                                            CVN</label>
                                                                                                                                                                                                                 </div>
                                                                                                                                                                                                                <div class="col-xs-5">
                                                                                                                                                                                                                <input type='text' class="form-control" name='EWAY_CARDCVN' id='EWAY_CARDCVN' value="" maxlength="4" style="width:20px;"/> <!-- This field is optional but highly recommended -->
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                <div class="col-xs-4 text-left" style="padding-left: 0px; position: relative">
                                                                                                                                                                                                        <em><a href="#" class="what_is_this">What is this?</a></em> <!-- This field is optional but highly recommended -->
                                                                                                                                                                                                        <div class="cvv_helper"><img src="https://www.solesilove.com.au/images/cvv.png"></div>
                                                                                                                                                                                                        <script>
                                                                                                                                                                                                        $(document).ready(function(){
                                                                                                                                                                                                            $('.what_is_this').click(function(){
                                                                                                                                                                                                                $('.cvv_helper').toggle();
                                                                                                                                                                                                            });
                                                                                                                                                                                                            $('.cvv_helper').click(function(){
                                                                                                                                                                                                                $(this).toggle();
                                                                                                                                                                                                            });
                                                                                                                                                                                                        });
                                                                                                                                                                                                        </script>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            
                                                                                                                                                                                                            
                                                                                                                                                                                                            
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div>
                                                                                                                                                                                                            <br />
                                                                                                                                                                                                            <br />
                                                                                                                                                                                                            <input class="button" type="submit" id="btnSubmit" name="btnSubmit" value="Submit" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
 </div>
                                                                                                                                                                                                            </form>
                                                                                                                                                                                                            </div>
 
                                                                                                                                                                                                            <br />
                                                                                                                                                                                                            <br />
 
                                                                                                                                                                                                            
 
                                                                                                                                                                                                            <div id="raw">
                                                                                                                                                                                                            <div style="width: 45%; margin-right: 2em; background: #f3f3f3; float:left; overflow: scroll; white-space: nowrap;">
                                                                                                                                                                                                                                    <?php echo $service->getLastUrl(); ?><br>
                                                                                                                                                                                                            <pre id="request_dump"></pre>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div style="width: 45%; margin-right: 2em; background: #f3f3f3; float:left; overflow: scroll; white-space: nowrap;"><pre id="response_dump"></pre></div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <script>
                                                                                                            jQuery('#raw').hide();
                                                                                                            var request_dump = JSON.stringify(JSON.parse('<?php echo $service->getLastRequest(); ?>'), null, 4);
                                                                                                            jQuery('#request_dump').html(request_dump);
                                                                                                            var response_dump = JSON.stringify(JSON.parse('<?php echo $service->getLastResponse(); ?>'), null, 4);
                                                                                                            jQuery('#response_dump').html(response_dump);
 
                                                                                                            jQuery("#showraw").click(function () {
                                                                                                                if (jQuery('#raw:visible').length)
                                                                                                                    jQuery('#raw').hide();
                                                                                                                else
                                                                                                                    jQuery('#raw').show();
                                                                                                            });
                                                                                                                                                                                                            </script>
 
                                                                                                                                                                                                            <div id="maincontentbottom">
                                                                                                                                                                                                            </div>
 
 
                                                                                                                                                                                                            <script>
                                                                                                            $(document).ready(function () {
                                                                                                                $("#frms").submit();
                                                                                                            });
                                                                                                                                                                                                            </script>
 
                                                                                                                                                                                                                    <?php
                                                                                                                                                                                                                } else { // for if ($in_page === 'view_result') {
                                                                                                                                                                                                                    ?>
 
                                                                                                                                                                                                            <div id="titlearea">
 
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                    <?php
                                                                                                                                                                                                                    if (isset($lblError)) {
                                                                                                                                                                                                                        ?>
                                                                                                                                                                                                                <div id="error">
                                                                                                                                                                                                                <label style="color:red"><?php echo $lblError ?></label>
                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                    <?php } ?>
                                                                                                                                                                                                            
                                                                                                                                                                                                            <div id="maincontent">
                                                                                                                                                                                                            <div class="transactioncustomer">
                                                                                                                                                                                                            <form name="frms" method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/CheckOut/payment">
                                                                                                                                                                                                            <div style="display:none;">
                                                                                                                                                                                                            <div class="header first">
                                                                                                                                                                                                            Request Options
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="APIKey">API Key</label>
                                                                                                                                                                                                            <input id="APIKey" name="APIKey" type="text" value="F9802APZzQ3c3fEhBC2jcoA3p945VdelIz2q4pUiatEBPuV/ZjYUctJtqS5vf+6yo9/yCP" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="APIPassword">API Password</label>
                                                                                                                                                                                                            <input id="APIPassword" name="APIPassword" value="siI4DdRO" type="password" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="ddlSandbox">API Sandbox</label>
                                                                                                                                                                                                            <select id="ddlSandbox" name="ddlSandbox">
                                                                                                                                                                                                            <option value="0">No</option>
                                                                                                                                                                                                            </select>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="ddlMethod">Payment Method</label>
                                                                                                                                                                                                            <select id="ddlMethod" name="ddlMethod" style="width: 140px" onchange="onMethodChange(this.options[this.options.selectedIndex].value)">
                                                                                                                                                                                                            <option value="ProcessPayment">ProcessPayment</option>
                                                                                                                                                                                                            <option value="TokenPayment">TokenPayment</option>
                                                                                                                                                                                                            <option value="CreateTokenCustomer">CreateTokenCustomer</option>
                                                                                                                                                                                                            <option value="UpdateTokenCustomer">UpdateTokenCustomer</option>
                                                                                                                                                                                                            <option value="Authorise">Authorise</option>
                                                                                                                                                                                                            </select>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <script>
                                                                                                            function onMethodChange(v) {
                                                                                                                if (v == 'ProcessPayment' || v == 'Authorise' || v == 'TokenPayment') {
                                                                                                                    jQuery('#payment_details').show();
                                                                                                                } else {
                                                                                                                    jQuery('#payment_details').hide();
                                                                                                                }
                                                                                                            }
                                                                                                                                                                                                            </script>
 
                                                                                                                                                                                                            <div id='payment_details'>
                                                                                                                                                                                                            <div class="header">
                                                                                                                                                                                                            Payment Details
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtAmount">Amount &nbsp;<img src="<?php echo Yii::app()->request->baseUrl; ?>/eway/assets/Images/question.gif" alt="Find out more" id="amountTipOpener" border="0" /></label>
                                                                                                                                                                                                            <input id="txtAmount" name="txtAmount" type="text" value="<?= $hdfc_details['totaltopay']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtCurrencyCode">Currency Code </label>
                                                                                                                                                                                                            <input id="txtCurrencyCode" name="txtCurrencyCode" type="text" value="AUD" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtInvoiceNumber">Invoice Number</label>
                                                                                                                                                                                                            <input id="txtInvoiceNumber" name="txtInvoiceNumber" type="text" value="<?= $hdfc_details['order']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtInvoiceReference">Invoice Reference</label>
                                                                                                                                                                                                            <input id="txtInvoiceReference" name="txtInvoiceReference" type="text" value="<?= $hdfc_details['order']; ?>" />
                                                                                                                                                                                                            </div>
 
                                                                                                                                                                                                            </div> <!-- end for <div id='payment_details'> -->
 
 
                                                                                                                                                                                                            <div class="transactioncard">
                                                                                                                                                                                                            <div class="header first">
                                                                                                                                                                                                            Customer Details
                                                                                                                                                                                                            </div>
 
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="ddlTitle">Title</label>
                                                                                                                                                                                                            <select id="ddlTitle" name="ddlTitle">
                                                                                                                                                                                                            <option></option>
                                                                                                                                                                                                            <option value="Mr/Mrs" selected="selected">Mr/Mrs</option>
                                                                                                                                                                                                            </select>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtCustomerRef">Customer Reference</label>
                                                                                                                                                                                                            <input id="txtCustomerRef" name="txtCustomerRef" type="text" value="<?= $hdfc_details['order']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtFirstName">First Name</label>
                                                                                                                                                                                                            <input id="txtFirstName" name="txtFirstName" type="text" value="<?= $hdfc_details['bill_firstname']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtLastName">Last Name</label>
                                                                                                                                                                                                            <input id="txtLastName" name="txtLastName" type="text" value="<?= $hdfc_details['bill_lastname']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="header">
                                                                                                                                                                                                            Customer Address
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtStreet">Street</label>
                                                                                                                                                                                                            <input id="txtStreet" name="txtStreet" type="text" value="<?= $hdfc_details['bill_address']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtCity">City</label>
                                                                                                                                                                                                            <input id="txtCity" name="txtCity" type="text" value="<?= $hdfc_details['bill_city']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtState">State</label>
                                                                                                                                                                                                            <input id="txtState" name="txtState" type="text" value="<?= $hdfc_details['bill_state']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtPostalcode">Post Code</label>
                                                                                                                                                                                                            <input id="txtPostalcode" name="txtPostalcode" type="text" value="<?= $hdfc_details['bill_postal_code']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtCountry">Country</label>
                                                                                                                                                                                                            <input id="txtCountry" name="txtCountry" type="text" value="au" maxlength="2" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtEmail">Email</label>
                                                                                                                                                                                                            <input id="txtEmail" name="txtEmail" type="text" value="<?= $hdfc_details['bill_email']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="txtMobile">Mobile</label>
                                                                                                                                                                                                            <input id="txtMobile" name="txtMobile" type="text" value="<?= $hdfc_details['bill_phone_number']; ?>" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="header">
                                                                                                                                                                                                            Others
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div class="fields">
                                                                                                                                                                                                            <label for="ddlTransactionType">Transaction Type</label>
                                                                                                                                                                                                            <select id="ddlTransactionType" name="ddlTransactionType" style="width:140px;">
                                                                                                                                                                                                            <option value="Purchase">Ecommerce</option>
                                                                                                                                                                                                            </select>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            <div>
                                                                                                                                                                                                            <br />
                                                                                                                                                                                                            <br />
                                                                                                                                                                                                            <input class="button" type="submit" id="btnSubmit" name="btnSubmit" value="Proceed to pay" />
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                            </form>
                                                                                                                                                                                                           
                                                                                                                                                                                                            </div>
                                                                                                                                                                                                                            <?php
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                        ?>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        <div id="footer"></div>
                                                                                                                                                                                                        </div>
                                                                                                                                                                                                        </center>
 
                                                                                                                                                                                                        </body>
                                                                                                                                                                                                        </html>
<style>
#outer {
   width: 100%;
   text-align: center;
padding-top:50px;
}
#toplinks {
   width: 100%;
}
#main {
   background: none;
width: 1170px;
   padding: 20px;
   max-width: 100%;
   margin: auto;
}
#maincontent {
   float: none;
   width: 100%;
}
.transactioncustomer {
   float: none;
   width: 100%;
   text-align: center;
   background: none;
}
.button:hover {
   color: #000;
   text-decoration: none;
   text-shadow: none;
   box-shadow: none;
   background: none;
}
div.button {
   clear: both;
   text-align: right;
   margin-right: 10px;
   color: #000;
   margin-right: 37px;
   float: none !important;
   display: inline-block;
   background: none;
}
.button:hover:before {
   background: none;
}
#footer {
   width: 1170px;
margin:auto;
   max-width: 100%;
}
label {
    width: 100%;
    float: left;
    margin: 10px 0px;
    margin-right: 20px;
    text-align: left;
}
.login_form .form-control {
    padding: 4px 10px;
    height: 35px;
    min-width: 100%;
}
.transactioncustomer .header {
   width: 100%;
   margin-top: 5px;
   clear: both;
   font-weight: normal;
   text-align: center;
   margin-bottom: 20px;
   font-size: 18px;
   font-family: font89189, "Arial Rounded MT Bold", "Open Sans", "sans-serif";
   color: #37454f;
}
.frm-ctrl {
    height: 35px;
    width: calc(50% - 10px);
    float: left;
    margin-right: 10px;
}

</style>