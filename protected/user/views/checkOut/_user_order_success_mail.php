<?php
echo $this->renderPartial('//site/mail/_email_header');

$shiping_charge = ShippingCharges::model()->findByAttributes(array('country' => $user_address->country));
?>
<tr><td style="padding:10px 10px 10px 10px; font-style:italic;">

                Hi <?php echo $bill_address->first_name; ?> <?php echo $bill_address->last_name; ?>,<br/><br/>Welcome to Solesilove.com.au!<br/><br/>

                Thank you for your order from Solesilove.com.au. You can check the status of your order by logging into your account.<br/><br/>

               If you have any questions about your order please contact us at info@solesilove.com.au or call us at 1300045683 Monday-Saturday, 9.30am-6.30pm IST.<br/><br/>

                Your order confirmation is below. Thanks again for your Sole Support ! <br/><br/>

        </td></tr>
<tr><td> <span style="color:#f57a12;font-size: 15px;padding-left:10px;">Your Invoice #SLIN<?php echo $order->id + 2000; ?> for Order #SLOR<?php echo $order->id; ?></span>


        </td></tr>
<br/>

<tr>
        <td>
                <table cellspacing="0" cellpadding="0" border="0" width="" style="max-width:610px ;   font-family: 'Open Sans',arial, sans-serif;font-size: 13px;">
                        <thead>
                                <tr>
                                        <th align="left" width="364" bgcolor="#EAEAEA" style="    font-family: 'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Billing Information:</th>
                                        <th width="10"></th>
                                        <th align="left" width="364" bgcolor="#EAEAEA" style="font-family:'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Payment Method:</th>
                                </tr>
                        </thead>
                        <tbody>
                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                <?php echo $bill_address->first_name; ?>   <?php echo $bill_address->last_name; ?>  <br>

                                                <?php echo $bill_address->address_1; ?> <br>
                                                <?php
                                                if ($bill_address->address_2) {
                                                        echo $bill_address->address_2;
                                                        ?> <br><?php } ?>
                                                <?php echo $bill_address->city; ?><br>
                                                <?php echo $bill_address->state; ?><br>
                                                <?php echo $bill_address->postcode; ?><br>
                                                <?php echo Countries::model()->findByPk($bill_address->country)->country_name; ?><br>
                                                +<?php echo Countries::model()->findByPk($bill_address->country)->phonecode . ' '; ?><?php echo $bill_address->contact_number; ?><br/>


                                        </td>
                                        <td>&nbsp;</td>
                                        <td valign="top" style="font-family: 'Open Sans',arial, sans-serif;font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                <p style="text-transform: uppercase;font-weight: bold;padding-top:0px;"><?php
                                                        if ($order->payment_mode == 1) {
                                                                echo "MY CREDIT";
                                                        } elseif ($order->payment_mode == 2) {
                                                                echo "CREDIT/DEBIT CARD OR NET BANKING";
                                                        } elseif ($order->payment_mode == 3) {
                                                                echo "PAYPAL";
                                                        } elseif ($order->payment_mode == 4) {
                                                                $wallet_amt = $order->wallet;
                                                                if ($order->netbanking != '') {
                                                                        $payment_amt = $order->netbanking;
                                                                        $method = 'CREDIT/DEBIT CARD OR NET BANKING';
                                                                } else if ($order->paypal != '') {
                                                                        $payment_amt = $order->paypal;
                                                                        $method = 'PAYPAL';
                                                                }
                                                                echo "<br>My Credit Amount = " . $wallet_amt;
                                                                echo "<br>" . $method . " = " . $payment_amt;
                                                        }
                                                        ?></p>

                                                <?php if (($order->transaction_id != 0)) { ?>
                                                        <p style="color:#f57a12;font-size: 15px;">Transaction ID:<?php echo $order->transaction_id; ?> </p>
                                                <?php } ?>

                                        </td>
                                </tr>
                        </tbody>
                </table>
                <br>

                <table cellspacing="0" cellpadding="0" border="0" width="" style=" max-width:610px;   font-family: 'Open Sans',arial, sans-serif;font-size: 13px;">
                        <thead>
                                <tr>
                                        <th align="left" width="364" bgcolor="#EAEAEA" style="font-family:'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Shipping Information:</th>
                                        <th width="10"></th>
                                        <th align="left" width="364" bgcolor="#EAEAEA" style="font-family:'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Shipping Method:</th>
                                </tr>
                        </thead>
                        <tbody>
                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                <?php echo $user_address->first_name; ?>   <?php echo $bill_address->last_name; ?><br>

                                                <?php echo $user_address->address_1; ?> <br>
                                                <?php
                                                if ($user_address->address_2) {
                                                        echo $user_address->address_2;
                                                        ?> <br><?php } ?>
                                                <?php echo $user_address->city; ?><br>
                                                <?php echo $user_address->state; ?> <br>
                                                <?php echo $user_address->postcode; ?><br>
                                                <?php echo Countries::model()->findByPk($user_address->country)->country_name; ?><br/>
                                                +<?php echo Countries::model()->findByPk($user_address->country)->phonecode . ' '; ?><?php echo $user_address->contact_number; ?><br/>

                                                &nbsp;
                                        </td>
                                        <td>&nbsp;</td>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                <?php
                                                       echo "Delivered within 3-14 working days";
                                                
                                                ?>
                                                &nbsp;
                                        </td>
                                </tr>
                        </tbody>
                </table>
                <br>
                <table cellspacing="0" cellpadding="0" border="0" width="610" style="border:1px solid #eaeaea;font-family: 'Open Sans',arial, sans-serif;">
                        <thead>
                                <tr>
                                        <th align="left" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Item</th>
                                        <th align="left" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Product Code</th>
                                        <th align="center" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Qty</th>
                                        <th align="right" bgcolor="#EAEAEA" style="font-size:13px;padding:3px 9px">Subtotal</th>
                                </tr>
                        </thead>

                        <tbody bgcolor="#F6F6F6">
                                <?php
                                foreach ($order_details as $orders) {
                                        $product_names = Products::model()->findByAttributes(array('id' => $orders->product_id));
                                        $product_option = OptionDetails::model()->findByAttributes(array('id' => $orders->option_id));
                                        $color = OptionCategory::model()->findByPk($product_option->color_id);
                                        $size = OptionCategory::model()->findByPk($product_option->size_id);
                                        $width = WidthFitting::model()->findByPk($product_option->width_id);
                                        ?>
                                        <tr>
                                                <td align="left" valign="top" style="font-size:11px;padding:3px 9px;padding-top:10px; <?php if ($orders->gift_option == 0) { ?>padding-bottom:10px;border-bottom:1px dotted #cccccc;<?php } ?>">
                                                        <strong style="font-size:11px;text-transform: uppercase;"><?php echo $product_names->product_name; ?></strong>
                                                        <?php
                                                        if ($orders->option_id != '') {
                                                                ?>
                                                                <dl style="margin:0;padding:0">
                                                                        <?php if ($product_option->color_id != '' && $product_option->color_id != 0) {
                                                                                ?>
                                                                                <dt><strong>Color : </strong>
                                                                                <span style="margin:0;padding:0 0 0 9px"><?php echo $color->color_name; ?> </span></dt>
                                                                                <?php
                                                                        }
                                                                        if ($product_option->size_id != '' && $product_option->size_id != 0) {
                                                                                ?>
                                                                                <dt><strong>Size &nbsp;&nbsp;: </strong>
                                                                                <span style="margin:0;padding:0 0 0 9px"><?php echo $size->size; ?></span></dt>
                                                                        <?php }if ($product_option->width_id != '' && $product_option->width_id != 0) {
                                                                                ?>
                                                                                <dt><strong>Width &nbsp;&nbsp;: </strong>
                                                                                <span style="margin:0;padding:0 0 0 9px"><?php echo $width->title; ?></span></dt>
                                                                        <?php } ?>
                                                                        <?php if($orders->left_right != ''){?>
                                                                         <dt><strong>Left/Right &nbsp;&nbsp;: </strong>
                                                                                <span style="margin:0;padding:0 0 0 9px"><?php echo $orders->left_right; ?></span></dt>
                                                                        <?php } ?>
                                                                </dl>
                                                        <?php } ?>
                                                </td>
                                                <td align="left" valign="top" style="font-size:11px;padding:3px 9px;padding-top:10px; <?php if ($orders->gift_option == 0) { ?>padding-bottom:10px;border-bottom:1px dotted #cccccc;<?php } ?>"><?php echo $product_names->product_code; ?></td>
                                                <td align="center" valign="top" style="font-size:11px;padding:3px 9px;padding-top:10px; <?php if ($orders->gift_option == 0) { ?>padding-bottom:10px;border-bottom:1px dotted #cccccc;<?php } ?>"><?php echo $orders->quantity; ?></td>
                                                <td align="right" valign="top" style="font-size:11px;padding:3px 9px;padding-top:10px; <?php if ($orders->gift_option == 0) { ?>padding-bottom:10px;border-bottom:1px dotted #cccccc;<?php } ?>">


                                                        <span><?php echo Yii::app()->Currency->convertCurrencyCode($orders->amount); ?></span>                                        </td>
                                        </tr>
                                        <?php if ($orders->gift_option == 1) { ?>
                                                <tr>
                                                        <td colspan="3" style="padding: 0px 0px 10px 10px;text-transform: uppercase;font-size: 10px;font-weight: bold;border-bottom:1px dotted #cccccc">Gift Packing</td>
                                                        <td align="right" valign="top" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;"><?php echo Yii::app()->Currency->convertCurrencyCode($orders->rate); ?></td>
                                                </tr>

                                                <?php
                                        }
                                }
                                ?>

                                <tr>
                                        <?php
                                        foreach ($order_details as $total_order) {
                                                $totorder += $total_order->amount;
                                        }

                                        foreach ($order_details as $giftoption) {
                                                $totgift += $giftoption->rate;
                                        }
                                        $granttotal = $totgift + $totorder;
                                        $total = $granttotal + Order::model()->findByPk($order->id)->shipping_charge;
                                        ?>
                                        <td colspan="3" align="right" style="padding:13px 9px 0 0;font-size:13px;">
                                                Subtotal                    </td>
                                        <td align="right" style="padding:13px 9px 0 0;font-size:13px;">
                                                <span>$ <?php echo $granttotal; ?></span>                    </td>
                                </tr>
                                <tr>
                                        <td colspan="3" align="right" style="padding:3px 9px;font-size:13px;">
                                                Shipping &amp; Handling                    </td>
                                        <td align="right" style="padding:3px 9px;font-size:13px;">
                                                <span>$ <?php echo Order::model()->findByPk($order->id)->shipping_charge; ?></span>                    </td>
                                </tr>
                                
                                <tr>
                                        <td colspan="3" align="right" style="padding:3px 9px;font-size:13px;">
                                                GST <small style="font-size:11px">(<?php echo Settings::model()->findByAttributes(array('id'=>1))->tax_rate;?>)</small> </td>
                                        <td align="right" style="padding:3px 9px;font-size:13px;">
                                                <span>$ <?php echo $this->Tax($total); ?></span>                    </td>
                                </tr>
                                
                                <tr>
                                        <td colspan="3" align="right" style="padding:3px 9px 13px 0;font-size:13px;">
                                                <strong>Total Inc GST</strong>
                                        </td>
                                        <td align="right" style="padding:3px 9px 13px 0;font-size:13px;">
                                                <strong><span>AUD <?php echo $total; ?></span></strong>
                                        </td>
                                </tr>
                        </tbody>
                </table>
             
             

        </td>
</tr>


<br/>
<p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#acacb1; text-align: center">* This is an automatically generated email, please do not reply to this email.</p>

<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>