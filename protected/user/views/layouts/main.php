<!DOCTYPE html>
<html lang="en">
        <head>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="author" content="">
                <link rel="icon" href="<?php echo Yii::app()->baseUrl; ?>/images/fav.png" type="image/x-icon" />
                <title><?php echo CHtml::encode($this->pageTitle); ?></title> 
                
                <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700" rel="stylesheet">
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.css" rel="stylesheet">
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/slick.css" rel="stylesheet">
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/slick-theme.css" rel="stylesheet">
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.css" rel="stylesheet">
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.min.css" rel="stylesheet">
                <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/slick-lightbox.css">
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet">
                <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/developer.css" rel="stylesheet">
                <script type="text/javascript">
            var baseurl = "<?php print Yii::app()->request->baseUrl; ?>/";
            var basepath = "<?php print Yii::app()->basePath; ?>";
                </script>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.min.js"></script>
                <meta name="google-site-verification" content="vN69L6DZCPKRKupGuVNJgN4uFYMF6KSrXHnv5m-HtUU" />
                
                
                <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114106479-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114106479-1');
</script>


        </head>
        <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "https://www.solesilove.com.au/",
  "name": "Soles ILove",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "1300-045-683",
    "contactType": "Customer service"
  }
}
</script>
        <body>
                <?php
                if (isset(Yii::app()->session['user'])) {
                    if (Yii::app()->session['user'] != '' && Yii::app()->session['user'] != NULL) {
                        $user_id = Yii::app()->session['user']['id'];
                        Cart::model()->deleteAllByAttributes(array(), array('condition' => 'date < subdate(now(), 1) and user_id != ' . Yii::app()->session['user']['id'])); }
                } else {
                    if (!isset(Yii::app()->session['temp_user'])) {
                        Yii::app()->session['temp_user'] = microtime(true); }
                    Cart::model()->deleteAllByAttributes(array(), array('condition' => 'date < subdate(now(), 1)'));
                    $sessonid = Yii::app()->session['temp_user']; }
                if (isset($user_id)) {
                    $condition = "user_id= $user_id";
                } else if (isset($sessonid)) {
                    $condition = "session_id= $sessonid"; }
                $cart_contents = Cart::model()->findAllByAttributes(array(), array('condition' => ($condition)));
                $wish_contents = UserWishlist::model()->findAllByAttributes(array(), array('condition' => ($condition)));
                ?>
                <div class="menu_overlay"></div>
                <div class="side_menu" style="opacity: 0;">
                        <div class="menu_container">
                                <div class="header">
                                        <div class="nav_btn_group">
                                                <div class="nav_btn"> <span></span> <span></span> <span></span> </div>
                                        </div>
                                </div>
                                <ul class="main-menu">
                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/">Home</a></li>
                                        <?php 
                                        $cat = ProductCategory::model()->findAllByAttributes(array('status' => 1), array('condition' => 'header_visibility = 1 and id != 11 and id != 15', 'order' => 'sort_order ASC'));
                                        foreach ($cat as $cats) {
                                            ?>
                                            <li class="has-child"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/<?= $cats->canonical_name; ?>"><?= $cats->category_name; ?></a>

                                                    <?php
                                                    $catrem = ProductCategory::model()->findAllByAttributes(array('parent' => $cats->id), array('condition' => 'id!=parent'));
                                                    $cnt = count($catrem);
                                                    if ($cnt > 0) {
                                                        ?>
                                                        <ul>
                                                                <?php
                                                                foreach ($catrem as $catrems) {
                                                                    ?>
                                                                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/<?= $catrems->canonical_name; ?>"><?php echo $catrems->category_name; ?></a></li>
                                                                    <?php } ?>
                                                        </ul>
                                                        <?php  }  }  ?>
                                        </li>
                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/compression-11">Compression</a></li>
                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/orthotics-insoles-15">Orthotics & Insoles</a></li>
                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/brands">Brands</a></li>
                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/blog">Foot Health blog</a>
                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/products/sale">Sale</a></li>
                                </ul>
                        </div>
                </div>
                <header class="animated">
                        <div class="top-bar">
                                <div class="container">
                                        <?php
                                        $cont = ContactDetails::model()->findByPk(1);
                                        ?>
                                        <div class="row">
                                                <div class="col-md-5 col-sm-3 col-xs-4 smart-2">
                                                        <div class="phone_number">
                                                                <a href="tel:<?= $cont->contactno_1; ?>"><i class="fa fa-phone"></i> <span><?= $cont->contactno_1; ?></span></a>
                                                        </div>
                                                        <div class="time">
                                                                <i class="fa fa-clock-o"></i> <?= $cont->working_hrs; ?>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-7 col-sm-9 col-xs-8 smart-1">
                                                        <ul class="shop_menu_top">
                                                                <li class="hidden-xs">
                                                                        <a href="mailto:<?php echo ContactDetails::model()->findByPk(1)->email; ?>"><i class="fa fa-envelope-o"></i> <?php echo ContactDetails::model()->findByPk(1)->email; ?></a></li>
                                                                <?php
                                                                if (Yii::app()->session['user']) {
                                                                    $user_data = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                                                                    ?>
                                                                    <li class = "myaccount visible-mobile has_c_dropdown">
                                                                            <a href = "#"><i class = "fa fa-user"></i> <i class = "fa fa-angle-down"></i> </a>
                                                                            <ul class = "c_dropdown y_account animated fadeInUp">
                                                                                    <?php if ($user_data->account_type == 2) { ?>
                                                                                        <li class="myaccount"><a href = "javascript:void(0)">Guest!</a></li>
                                                                                    <?php } else { ?>
                                                                                        <li><a href = "<?php echo Yii::app()->baseUrl; ?>/index.php/myaccount">My Account</a></li>

                                                                                    <?php } ?>
                                                                                    <li><a href = "<?php echo Yii::app()->baseUrl; ?>/index.php/site/logout">Logout</li>
                                                                            </ul>
                                                                    </li>
                                                                    <?php } ?>
                                                                <li class="has_c_dropdown">
                                                                        <?php if (isset(Yii::app()->session['currency'])) { ?>
                                                                            <a href="#currency" class="current_currency"><span><?php echo Yii::app()->session['currency']['currency_code']; ?></span></a>
                                                                        <?php } else { ?>
                                                                            <a href="#currency" class="current_currency"><span>AUD</span></a>

                                                                        <?php } ?>
                                                                </li>
                                                                <li class = "search_box"><a href = "#search" class = "search_link"><i class = "fa fa-search"></i></a>
                                                                        <?php
                                                                        $form = $this->beginWidget('CActiveForm', array(
                                                                            'id' => 'search-form',
                                                                            'action' => Yii::app()->request->baseUrl . '/search',
                                                                            'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
                                                                            'enableAjaxValidation' => false,
                                                                        ));
                                                                        ?>
                                                                        <div class = "s_dropdown animated fadeInUp">
                                                                                <div class = "row">

                                                                                        <div class = "col-xs-9"><input name="keyword" type = "text"></div>
                                                                                        <div class = "col-xs-3"><button type="submit" class = "button"><i class = "fa fa-search"></i></button></div>
                                                                                </div>
                                                                        </div>
                                                                        <?php $this->endWidget(); ?>
                                                                </li>
                                                                <?php
                                                                if (Yii::app()->session['user']) {
                                                                    $user_data = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                                                                    ?>
                                                                    <li class="myaccount hidden_phone" style="cursor:pointer">
                                                                            <?php if ($user_data->account_type == 2) { ?>
                                                                        <li class="myaccount"><a href = "javascript:void(0)">Guest!</a></li>
                                                                    <?php } else { ?>
                                                                        <li><a href = "<?php echo Yii::app()->baseUrl; ?>/index.php/myaccount">My Account</a></li>
                                                                    <?php } ?>
                                                                    </li>
                                                                    <li class="hidden_phone myaccount" style="cursor:pointer">
                                                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/logout">  Logout</a>
                                                                    </li>
                                                                    <li class="my_credit hidden_phone">
                                                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/wallet-icon.png" alt="wallet-icon"/>
                                                                            <div class="text">
                                                                                    <a href="#"><span>MY CREDIT</span>
                                                                                            <strong><?php
                                                                                                    $user_data = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                                                                                                    echo $user_data->wallet_amt;
                                                                                                    ?></strong></a>
                                                                            </div>
                                                                    </li>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <li class="userlogin hidden_phone" style="cursor:pointer"> <i class="fa fa-user-circle-o"></i> Login </li>
                                                                    <?php  } ?>
                                                                <li class="cart_items">
                                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/cart/mycart"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/cart-icon.png" alt="cart-icon"/>
                                                                                <span class="cart_count"><?php echo count($cart_contents); ?></span></a> </li>
                                                                 <li class="cart_items">
                                                                      <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Mywishlists"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/heart-outline.png" alt="heart-outline"/><span class="wish_count"><?php echo count($wish_contents); ?></span></a>
                                                                </li>
                                                        </ul>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="container">
                                <div class="logo">
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" alt="logo"/></a>
                                        <div class="nav_btn_group">
                                                <div class="nav_btn"> <span></span> <span></span> <span></span> </div>
                                        </div>
                                </div>
                        </div>
                        <div class="main_menu hidden-sm hidden-xs">
                                <div class="container">
                                        <nav class="navbar">
                                                <div class="navbar-header">
                                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                                                </div>
                                                <div id="navbar" class="collapse navbar-collapse"><ul class="nav navbar-nav">
                                                                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/"><i class="fa fa-home"></i></a></li>
                                                                <?php
                                                                $cat = ProductCategory::model()->findAllByAttributes(array('status' => 1), array('condition' => 'header_visibility = 1', 'order' => 'sort_order ASC'));
                                                                foreach ($cat as $cats) {
                                                                    ?>
                                                                    <li class="dropdown has_menu_dropdown"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/<?= $cats->canonical_name; ?>" data-link="drop_menu_1"><?= $cats->category_name; ?></a>
                                                                            <?php
                                                                            $catrem = ProductCategory::model()->findAllByAttributes(array('parent' => $cats->id), array('condition' => 'id!=parent'));
                                                                            $cnt = count($catrem);
                                                                            if ($cnt > 0) {
                                                                                ?>
                                                                                <div class="menu_dropdown">
                                                                                        <div class="row">
                                                                                                <div class="col-md-6">
                                                                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/category/<?= $cats->id; ?>.<?= $cats->image; ?>" alt="category<?= $cats->id; ?>"/>
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                        <h3><?= $cats->category_name; ?>'s Categories</h3>
                                                                                                        <ul>
                                                                                                                <?php
                                                                                                                foreach ($catrem as $catrems) {
                                                                                                                    ?>
                                                                                                                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/<?= $catrems->canonical_name; ?>"><?= $catrems->category_name; ?></a></li>
                                                                                                                    <?php } ?>
                                                                                                        </ul>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                                <?php  } } ?>
                                                                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/brands">Brands</a></li>
                                                                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/blog">Foot Health blog</a></li>
                                                                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/products/sale">Sale</a></li>
                                                        </ul><div class="clearfix"></div>
                                                </div>
                                        </nav>
                                </div>
                        </div>
                </header>
                <?php echo $content; ?>
                <section class="footer_section">
                        <div class="container border-bottom">
                                <div class="row">
                                        <div class="col-md-9">
                                                <div class="row footer_links">
                                                        <div class="col-sm-3 col-xs-6">
                                                                <h4>Soles iLove</h4>
                                                                <ul>
                                                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/">Home</a></li>
                                                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/about">About Us</a></li>
                                                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/brands">Brands</a></li>
                                                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/blog">Foot Health blog</a></li>
                                                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Policy/">Privacy Policy</a></li>
                                                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Return/">Returns and Exchanges</a></li>
                                                                        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/Shipping/">Shipping and Delivery</a></li>
                                                                </ul>
                                                        </div>
                                                        <div class="col-sm-3 col-xs-6">
                                                                <h4>Women</h4>
                                                                <ul>
                                                                        <?php
                                                                        $sel = ProductCategory::model()->findByAttributes(array('category_name' => 'Women'));
                                                                        $wsub = ProductCategory::model()->findAllByAttributes(array('parent' => $sel->id), array('condition' => 'id!=parent'));
                                                                        foreach ($wsub as $wsubs) {
                                                                            ?>
                                                                            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/<?= $wsubs->canonical_name; ?>"><?= $wsubs->category_name; ?></a></li>
                                                                            <?php } ?>
                                                                </ul>
                                                        </div>
                                                        <div class="clearfix hidden-sm hidden-lg hidden-md"></div>
                                                        <div class="col-sm-3 col-xs-6">
                                                                <h4>Men</h4>
                                                                <ul>
                                                                        <?php
                                                                        $selm = ProductCategory::model()->findByAttributes(array('category_name' => 'Men'));
                                                                        $msub = ProductCategory::model()->findAllByAttributes(array('parent' => $selm->id), array('condition' => 'id!=parent'));
                                                                        foreach ($msub as $msubs) {
                                                                            ?>
                                                                            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/<?= $msubs->canonical_name; ?>"><?= $msubs->category_name; ?></a></li>
                                                                            <?php  }  ?>
                                                                </ul>
                                                        </div>
                                                     
                                                </div>
                                        </div>
                                        <div class="col-md-3">
                                                <div class="footer-contact" id="contact-enquiry">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/footer-logo.png" alt="footer-logo"/>
                                                        <p> <p><?= $cont->address; ?></p>
													<div class="fa fa-phone"><a href="tel:<?= $cont->contactno_1; ?>"><?= $cont->contactno_1; ?></a></div>
													<div class="fa fa-envelope"><a href="mailto:<?= $cont->email; ?>"><?= $cont->email; ?></a></div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="container copyright">
                                <div class="row">
									<div class="col-md-9 tab-center">Soles iLove  © 2017 All Rights Reserved. <a href="http://www.intersmartsolution.com" target="_blank">Developed by Intersmart</a> <span class="socials"><a href="https://www.facebook.com/solesilove/" target="_blank"><i class="fa fa-facebook"></i></a><a href="https://www.instagram.com/soles_ilove/" target="_blank"><i class="fa fa-instagram"></i></a></span>
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo1.png" width="60" height="50" alt="logo1"/>
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo2.png" width="60" height="50" alt="logo2"/>
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo3.png" width="60" height="50" alt="logo3"/>
                                            </div>
                                        <div class="col-md-3 text-right tab-center">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/payment-icons.png" alt="payment-icons"/></div>
                                </div>
                        </div>
                </section>
                <div class="modal fadeIn animated" id="newsletter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog zoomIn animated" role="document">
                                <div class="modal-content">
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Subscribe Newsletter</h4>
                                        </div>
                                        <div class="modal-body">
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-sm-6">
                                                                        <input type="text" placeholder="Your Name:" class="form-control">
                                                                </div>
                                                                <div class="col-sm-6">
                                                                        <input type="text" placeholder="Your Name:" class="form-control">
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                </div>
                        </div>
                </div>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/slick.min.js"></script>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.animateNumber.js"></script>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.elevateZoom-3.0.8.min.js"></script>
                <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/slick-lightbox.js"></script>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/site.js"></script>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/developer.js"></script>
                <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script>
        </body>
</html>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59a4f4f94fe3a1168eada2e3/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>