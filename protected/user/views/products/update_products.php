<?php echo $this->renderPartial('//site/mainmodal'); ?>

<?php
$value = rtrim($product->category_id, ',');
$ids = explode(',', $value);
foreach ($ids as $id) {
        $cat_name = ProductCategory::model()->findByPk($id)->category_name;
}
?>
<?php
$folder = Yii::app()->Upload->folderName(0, 1000, $product->id);
?>
<style>

        .side-size-cont input:disabled + label {
                //   background-image:url('<?php echo Yii::app()->request->baseUrl; ?>/images/red-cross.png');
                //       background-size: contain;
        }
</style>
<section style="position: relative">
        <div class="product_loader" style=""><img style="width: 150px;" src="/solesilove/images/product_loader.gif"></div>

        <div class="container content-body listings_page product-details-page">
                <?php echo $this->renderPartial('_bread_crumb', array('category_name' => $category_name, 'product' => $product)); ?>
                <h2 class="hidden-sm hidden-md hidden-lg"><?php echo $product->product_name; ?></h1>
                        <div class="row product-details">
                                <div class="col-sm-6">

                                        <?php
                                        $big = Yii::app()->basePath . '/../uploads/products/' . $folder . '/' . $product->id . '/gallery/big';
                                        $bigg = Yii::app()->request->baseUrl . '/uploads/products/' . $folder . '/' . $product->id . '/gallery/big/';
                                        $thu = Yii::app()->basePath . '/../uploads/products/' . $folder . '/' . $product->id . '/gallery/small';
                                        $thumbs = Yii::app()->request->baseUrl . '/uploads/products/' . $folder . '/' . $product->id . '/gallery/small/';
                                        $zoo = Yii::app()->basePath . '/../uploads/products/' . $folder . '/' . $product->id . '/gallery/zoom';
                                        $zoom = Yii::app()->request->baseUrl . '/uploads/products/' . $folder . '/' . $product->id . '/gallery/zoom/';
                                        $file_display = array('jpg', 'jpeg', 'png', 'gif');
                                        ?>

                                        <div class="product-gallery">

                                                <div class="big_image_slider2">
                                                        <?php
                                                        if (file_exists($big) == false) {

                                                        } else {
                                                                $dir_contents = scandir($big);
                                                                $i = 0;

                                                                foreach ($dir_contents as $file) {
                                                                        $file_type = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                                                                        if ($file !== '.' && $file !== '..' && in_array($file_type, $file_display) == true) {
                                                                                ?>
                                                                                <?php if ($i == 1) { ?>
                                                                                        <div class="item">
                                                                                                <img class="zoom_image" data-zoom-image="<?php echo $zoom . $file; ?>" src="<?php echo $bigg . $file; ?>" alt=""/>
                                                                                        </div>
                                                                                <?php } ?>
                                                                                <?php
                                                                                $i++;
                                                                        }
                                                                }
                                                        }
                                                        ?>
                                                </div>
                                                <div class="thumbnail_image_slider gal1" id="gal1">
                                                        <?php
                                                        if (file_exists($big) == false) {

                                                        } else {
                                                                $dir_contents = scandir($big);
                                                                $i = 0;

                                                                foreach ($dir_contents as $file) {
                                                                        $file_type = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                                                                        if ($file !== '.' && $file !== '..' && in_array($file_type, $file_display) == true) {
                                                                                ?>
                                                                                <div class="item">
                                                                                        <a href="#gall" data-image="<?php echo $bigg . $file; ?>" data-zoom-image="<?php echo $zoom . $file; ?>"> <img src="<?php echo $thumbs . $file; ?>" alt=""/></a>
                                                                                </div>
                                                                                <?php
                                                                                $i++;
                                                                        }
                                                                }
                                                        }
                                                        ?>

                                                </div>
                                        </div>
                                </div>
                                <div class="col-sm-6">
                                        <div>
                                                <?php if (Yii::app()->user->hasFlash('success')): ?>
                                                        <div class="alert alert-success mesage">
                                                                <?php echo Yii::app()->user->getFlash('success'); ?>
                                                        </div>
                                                <?php endif; ?>
                                                <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                        <div class="alert alert-danger mesage msg2">
                                                                <?php echo Yii::app()->user->getFlash('error'); ?>
                                                        </div>
                                                <?php endif; ?>
                                        </div>
                                        <div class="product-meta">
                                                <h1><?php echo $product->product_name; ?></h1>
                                                <div class="stars">
                                                        <ul class="list-unstyled list-inline">
                                                                <?php
                                                                $cn = count($product_reviews);
                                                                foreach ($product_reviews as $product_review) {
                                                                        $total_r += $product_review->rating;
                                                                }
                                                                if ($cn > 0) {
                                                                        $cn = $cn;
                                                                } else {
                                                                        $cn = 1;
                                                                }
                                                                $total_rating = ceil($total_r / $cn);
                                                                $j = $total_rating;
                                                                $k = 5 - $j;
                                                                ?>
                                                                <?php
                                                                for ($i = 1; $i <= $j; $i++) {
                                                                        ?>
                                                                        <li><i class="fa stars fa-star"></i></li>
                                                                <?php } ?>
                                                                <?php
                                                                for ($i = 1; $i <= $k; $i++) {
                                                                        ?>
                                                                        <li><i class="fa stars fa-star blank"></i></li>
                                                                <?php }
                                                                ?>
                                                                <?php
                                                                if ($total_rating == 0) {
                                                                        echo '(Not Yet Rated)';
                                                                }
                                                                ?>

                                                        </ul>
                                                </div>
                                                <?php
                                                if ($product->discount_rate != 0) {
                                                        ?>
                                                        <div class="product-price" >
                                                                <del class="text-danger"><?php echo Yii::app()->Currency->convert($product->price); ?></del> <?php echo Yii::app()->Discount->Discount($product); ?>

                                                        </div>

                                                        <?php
                                                } else {
                                                        ?>
                                                        <div class="product-price">
                                                                <?php echo Yii::app()->Discount->Discount($product); ?>
                                                        </div>
                                                        <?php
                                                }
                                                ?>
                                                <div class="product-des">
                                                        <p><?php echo substr($product->product_details, 0, 232); ?></p>
                                                </div>
                                                <div class="other-metas">
                                                        <div class="availability item">

                                                                <?php
                                                                $option_exists = OptionDetails::model()->findAllByAttributes(array('product_id' => $product->id));
                                                                if (empty($option_exists)) {
                                                                        if ($product->quantity == 0) {
                                                                                ?>
                                                                                <label for="">Availability:</label> <div style="color:red"><strong>Out Of Stock</strong></div>
                                                                        <?php } else if ($product->quantity <= 2 && $product->quantity != 0) {
                                                                                ?>
                                                                                <label for="">Availability:</label> <div style="color:#f78b17"><strong>Almost Gone</strong></div>
                                                                        <?php } else {
                                                                                ?>
                                                                                <label for="">Availability:</label> <div style="color:green !important"><strong>In Stock</strong></div>

                                                                                <?php
                                                                        }
                                                                } else {
                                                                        foreach ($option_exists as $option_exist) {
                                                                                $total_stock += $option_exist->stock;
                                                                                $out_stock +=$option_exist->status;
                                                                        }
                                                                        if ($total_stock == 0 || $out_stock == 0) {
                                                                                $out_stock = 1;
                                                                                ?>
                                                                                <label for="">Availability:</label> <div style="color:red"><strong>Out Of Stock</strong></div>


                                                                        <?php } else if ($total_stock <= 2 && $total_stock != 0) {
                                                                                ?>
                                                                                <label for="">Availability:</label> <div style="color:#f78b17"><strong>Almost Gone</strong></div>

                                                                                <?php
                                                                        } else {
                                                                                ?>
                                                                                <label for="">Availability:</label> <div style="color:green !important"><strong>In Stock</strong></div>
                                                                                <?php
                                                                        }
                                                                }
                                                                ?>
                                                        </div>

                                                        <?php
                                                        $product_option = MasterOptions::model()->findByAttributes(['product_id' => $product->id]);
                                                        if (!empty($product_option)) {
                                                                ?>
                                                                <div class="detail-zoom-color" id="gal1">

                                                                        <div class="option_errors"> </div>
                                                                        <input type="hidden" value="<?php echo $product_option->id; ?>" name="master_option" id="master_option"/>
                                                                        <input type="hidden" value="<?php echo $product_option->option_type_id; ?>" name="option_type" id="option_type"/>

                                                                        <?php
                                                                        if ($product_option->option_type_id == 1 || $product_option->option_type_id == 3) {
                                                                                $colors = OptionDetails::model()->findAllByAttributes(['master_option_id' => $product_option->id], ['group' => 'color_id', 'order' => 'color_id']);
                                                                                ?>
                                                                                <input type="hidden" value="" name="option_color" id="option_color"/>
                                                                                <h4 class="color_error" style="color:red"></h4>
                                                                                <label>Color :</label>
                                                                                <?php
                                                                                if (!empty($colors)) {
                                                                                        ?>
                                                                                        <?php
                                                                                        $total_stock_count = 0;
                                                                                        foreach ($colors as $colorz) {
                                                                                                $color_name = OptionCategory::model()->findByPk($colorz->color_id);
                                                                                                $color_countings = OptionDetails::model()->findAllByAttributes(array('color_id' => $colorz->color_id, 'product_id' => $product->id));

                                                                                                foreach ($color_countings as $color_countingz) {
                                                                                                        $total_stock_count += $color_countingz->stock;
                                                                                                }
                                                                                                if ($total_stock_count > 0) {
                                                                                                        $disabled1 = '';
                                                                                                        $dis = "";
                                                                                                } else {
                                                                                                        $disabled1 = 'disabled-input';
                                                                                                        $dis = "disabled";
                                                                                                }
                                                                                                ?>
                                                                                                <?php $g_color = OptionCategory::model()->findByPk($colorz->id); ?>
                                                                                                <?php
                                                                                                $check_color_image_exist = OptionImages::model()->findByAttributes(['color_id' => $colorz->color_id, 'product_id' => $product->id]);
                                                                                                $folder = Yii::app()->Upload->folderName(0, 1000, $product->id);
                                                                                                ?>
                                                                                                <?php
                                                                                                if ($total_stock_count > 0) {
                                                                                                        if (!empty($check_color_image_exist)) {
                                                                                                                ?>
                                                                                                                <a href = "javascript:void(0)" class="color_picker" style = "text-decoration: none;" data-image = "<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php echo $folder; ?>/<?php echo $product->id; ?>/option/<?php echo $colorz->color_id; ?>/zoom.<?php echo $check_color_image_exist->image; ?>" data-zoom-image = "<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php echo $folder; ?>/<?php echo $product->id; ?>/option/<?php echo $colorz->color_id; ?>/zoom.<?php echo $check_color_image_exist->image; ?>">
                                                                                                                <?php } else { ?>
                                                                                                                        <a href = "javascript:void(0)" class="color_picker" style = "text-decoration: none;" >
                                                                                                                        <?php } ?>
                                                                                                                        <div class = "detail-color-box  product_colors">
                                                                                                                                <input <?= $dis;
                                                                                                                        ?> class="<?= $disabled1; ?>" id="detail_color<?php echo $colorz->id; ?>" type="radio" name="detail-radio" product="<?php echo $product->id; ?>" option_id="<?php echo $product_option->id; ?>"  color="<?php echo $colorz->color_id; ?>">
                                                                                                                                <label for="detail_color<?php echo $colorz->id; ?>" style="">
                                                                <!--                                                                                                <span class="detail-svg"><svg preserveAspectRatio="xMidYMid" width="30.031" height="30" viewBox="0 0 30.031 30">
                                                                                                                                                <defs>
                                                                                                                                                <style>
                                                                                                                                                        .detail-color-box input:checked + label[for=detail_color<?= $colorz->id; ?>] .detail-path {
                                                                                                                                                                fill: #ccc;
                                                                                                                                                        }
                                                                                                                                                </style>
                                                                                                                                                </defs>
                                                                                                                                                <path d="M15.031,-0.000 C23.315,-0.000 30.031,6.716 30.031,15.000 C30.031,23.284 23.315,30.000 15.031,30.000 C-1.440,30.000 0.031,27.488 0.031,15.000 C0.031,6.716 6.747,-0.000 15.031,-0.000 Z" class="detail-path"/>
                                                                                                                                                </svg></span>-->
                                                                                                                                        <span class="cirle" style="background-color:<?php echo $color_name->color_code; ?>; background-image: url('<?php echo Yii::app()->request->baseUrl . "/uploads/colors/" . $color_name->id . "." . $color_name->image; ?>'); background-size: cover;"></span>
                                                                                                                                </label>
                                                                                                                        </div>
                                                                                                                </a>
                                                                                                                <?php
                                                                                                        }
                                                                                                        $total_stock_count = 0;
                                                                                                }
                                                                                        }
                                                                                }
                                                                                ?>
                                                                </div>
                                                                <input type="hidden" value="" name="option_size" id="option_size"/>

                                                                <div class="side-size-box size_selector">
                                                                        <?php
                                                                        if ($product_option->option_type_id == 2 || $product_option->option_type_id == 3) {
                                                                                $sizes = OptionCategory::model()->findAll(['condition' => 'option_type_id=2', 'order' => 'id ASC']);
                                                                                ?>
                                                                                <label>Size : </label>
                                                                                <?php
                                                                                if (!empty($sizes)) {
                                                                                        ?>

                                                                                        <?php
                                                                                        foreach ($sizes as $size) {
                                                                                                $productoption = OptionDetails::model()->findByAttributes(array('size_id' => $size->id, 'product_id' => $product->id, 'master_option_id' => $product_option->id));
                                                                                                if ($product_option->option_type_id == 3) {
                                                                                                        $disabled = 'disabled';
                                                                                                } else {
                                                                                                        $color_countings = OptionDetails::model()->findAllByAttributes(array('size_id' => $size->id, 'product_id' => $product->id, 'master_option_id' => $product_option->id));
                                                                                                        foreach ($color_countings as $color_counting) {
                                                                                                                $total_stock_count += $color_counting->stock;
                                                                                                        }
                                                                                                        if ($total_stock_count > 0) {
                                                                                                                $disabled = '';
                                                                                                        } else {
                                                                                                                $disabled1 = 'disabled-input';
                                                                                                        }
                                                                                                }
                                                                                                if (!empty($productoption)) {
                                                                                                        ?>
                                                                                                        <div class="side-size-cont" >
                                                                                                                <input <?php echo $disabled; ?> class="clrr" id="size_selector_<?php echo $size->id; ?>" type="radio" name="size_selector" value="<?php echo $size->id; ?>">

                                                                                                                <label class="<?php echo $disabled; ?>" product="<?php echo $product->id; ?>" option_id="<?php echo $productoption->master_option_id; ?>" id="<?php echo $size->id; ?>"  size="<?php echo $size->id; ?>" for="size_selector_<?php echo $size->id; ?>"><?php echo $size->size; ?>
                                                                                                                </label>
                                                                                                        </div>
                                                                                                        <?php
                                                                                                }
                                                                                        }
                                                                                        ?>

                                                                                        <?php
                                                                                }
                                                                        }
                                                                        ?>
                                                                </div>



                                                                <input type="hidden" value="" name="option_width" id="option_width"/>

                                                                <div class="side-width-box width_selector">
                                                                        <?php
                                                                        if ($product_option->option_type_id == 2 || $product_option->option_type_id == 3) {
                                                                                $widths = WidthFitting::model()->findAll(['condition' => 'status = 1', 'order' => 'id ASC']);
                                                                                $productop = OptionDetails::model()->findByAttributes(array('product_id' => $product->id));
                                                                                if ($productop->width_id != 0) {
                                                                                        ?>
                                                                                        <input type="hidden" value="0" name="option_width_hide" id="option_width_hide"/>
                                                                                        <label>Width Fittings : </label>
                                                                                        <?php
                                                                                        if (!empty($widths)) {
                                                                                                ?>

                                                                                                <?php
                                                                                                foreach ($widths as $width) {
                                                                                                        $productoption_width = OptionDetails::model()->findByAttributes(array('width_id' => $width->id, 'product_id' => $product->id, 'master_option_id' => $product_option->id));
                                                                                                        if ($product_option->option_type_id == 3) {
                                                                                                                $disabled = 'disabled';
                                                                                                        } else {
                                                                                                                $color_countings = OptionDetails::model()->findAllByAttributes(array('width_id' => $width->id, 'product_id' => $product->id, 'master_option_id' => $product_option->id));
                                                                                                                foreach ($color_countings as $color_counting) {
                                                                                                                        $total_stock_count += $color_counting->stock;
                                                                                                                }
                                                                                                                if ($total_stock_count > 0) {
                                                                                                                        $disabled = '';
                                                                                                                } else {
                                                                                                                        $disabled1 = 'disabled-input';
                                                                                                                }
                                                                                                        }
                                                                                                        if (!empty($productoption_width)) {
                                                                                                                ?>
                                                                                                                <div class="side-size-cont" style="width: auto">
                                                                                                                        <input <?php echo $disabled; ?> class="clrr" id="width_selector_<?php echo $width->id; ?>" type="radio" name="width_selector" value="<?php echo $width->id; ?>">

                                                                                                                        <label class="<?php echo $disabled; ?>" for="width_selector_<?php echo $width->id; ?>"><?php echo $width->title; ?>
                                                                                                                        </label>
                                                                                                                </div>
                                                                                                                <?php
                                                                                                        }
                                                                                                }
                                                                                                ?>

                                                                                                <?php
                                                                                        }
                                                                                } else {
                                                                                        ?>
                                                                                        <input type="hidden" value="1" name="option_width_hide" id="option_width_hide"/>
                                                                                        <?php
                                                                                }
                                                                        }
                                                                        ?>
                                                                </div>
                                                                <div class="side-width-box leftright_selector">
                                                                         <?php
                                                                         if($product->left_right == 1){
                                                                         ?>
                                                                          <label  for="left_selector"><input class="clrr lrradio" type="radio" id="leftright_selector" name="leftright_selector" value="L"> <?php echo LEFT; ?></label>
                                                                         
                                                                         <label  for="right_selector"><input class="clrr lrradio" type="radio" id="leftright_selector" name="leftright_selector" value="R"> <?php echo RIGHT; ?></label>
                                                                        <?php
                                                                         }
                                                                         ?>
                                                                     </div>
                                                                <?php
                                                        }
                                                        ?>
                                                </div>
                                                <div class="product_button_group">
                                                        <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $product->id ?>" class="button button-lined"><i class="fa fa-heart-o"></i> ADD TO WISHLIST</a>
                                                        <!--<a href="javascript:void(0)"  class="button add_to_cart" id="<?php echo $product->id; ?>">ADD TO CART</a>-->
                                                        <a href="javascript:void(0)"  class="button update_cart" id="<?php echo $product->id; ?>" cart_id="<?php echo $cart_det->id; ?>">Update Your Cart</a>

                                                         <!--<a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $product->id ?>" class="butter shop-btn detwish-btn add_to_wishlist">Add Wish list</a>-->
                                                        <input type = "hidden" id = "opt_id" name = "opt">
                                                        <input type = "hidden" value = "<?= $product->canonical_name; ?>" id="cano_name_<?= $product->id; ?>" name="cano_name">
                                                </div>
                                                <div class="product-share">
                                                        <div class="head">SHARE THIS PRODUCT:</div>
                                                        <a onclick="popWindow('https://www.facebook.com/sharer/sharer.php?u=<?= Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $product->canonical_name; ?>', 'facebook', 'width=1000,height=200,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');
                                                                        return false;"><i class="fa fa-facebook" ></i></a>
                                                        <a onclick="popWindow('http://twitter.com/share?ur=<?= Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $product->canonical_name; ?>', 'twitter', '');"><i class="fa fa-twitter" ></i></a>
                                                        <a onclick="popWindow('https://pinterest.com/pin/create/button/?url=<?= Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $product->canonical_name; ?>', 'pinterest', '');"><i class="fa fa-pinterest-p" ></i></a>
                                                      <!--  <a href="#"><i class="fa fa-pinterest"></i></a>
                                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                                        <a href="#"><i class="fa fa-linkedin"></i></a>-->
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="product-details-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">PRODUCT DESCRIPTION</a></li>
                                        <li role="presentation"><a href="#features" aria-controls="features" role="tab" data-toggle="tab">Features</a></li>
                                        <li role="presentation"><a href="#size" aria-controls="size" role="tab" data-toggle="tab">Sizing and Fitting</a></li>
                                        <li role="presentation"><a href="#delivery" aria-controls="delivery" role="tab" data-toggle="tab">Delivery</a></li>
                                        <li role="presentation"><a href="#style" aria-controls="style" role="tab" data-toggle="tab">Styling</a></li>
                                        <li role="presentation"><a href="#review" aria-controls="review" role="tab" data-toggle="tab">Reviews  <?php if (!empty($product_reviews)) { ?>(<?php
                                                                echo count($product_reviews) . ')';
                                                        }
                                                        ?></a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="description">

                                                <?php echo $product->product_details; ?>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="features">
                                                <?php echo $product->product_details1; ?>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="size">
                                                <?php echo $product->size_fitting; ?>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="delivery">
                                                <?php echo $product->delivery; ?>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="style">
                                                <?php echo $product->styling; ?>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="review">

                                                <?php
                                                if (!empty($product_reviews)) {
                                                        foreach ($product_reviews as $product_review) {
                                                                if ($product_review->user_id != 0) {
                                                                        $author = UserDetails::model()->findByPk($product_review->user_id)->first_name;
                                                                } else {
                                                                        $author = $product_review->author;
                                                                }
                                                                ?>
                                                                <div class="review_content">

                                                                        <h5><strong> <?php echo $author; ?></strong> On <?php echo date('d M Y', strtotime($product_review->date)); ?></h5>

                                                                        <ul class="list-inline">
                                                                                <?php
                                                                                $j = $total_rating;
                                                                                $j = $product_review->rating;
                                                                                $k = 5 - $j;
                                                                                ?>
                                                                                <?php
                                                                                for ($i = 1; $i <= $j; $i++) {
                                                                                        ?>
                                                                                        <li><i class="fa stars fa-star"></i></li>
                                                                                <?php } ?>
                                                                                <?php
                                                                                for ($i = 1; $i <= $k; $i++) {
                                                                                        ?>
                                                                                        <li><i class="fa stars fa-star blank"></i></li>
                                                                                <?php } ?>
                                                                        </ul><p><?php echo $product_review->review; ?></p>
                                                                </div>
                                                        <?php } ?>
                                                <?php } else { ?>
                                                        <p>No Review Found</p>
                                                <?php } ?>
                                                <input type="hidden" id="review_product_id" name="" value="<?php echo $product->id; ?>" />

                                                <?php if (Yii::app()->session['user'] != '' && Yii::app()->session['user'] != NULL) {
                                                        ?>

                                                        <form id="reviewform"  role="form">
                                                                <div class="login_form_container">
                                                                        <div class="login_form">
                                                                                <h1>Write Your Review</h1>
                                                                                <div class="form-group">
                                                                                        <input type="text" class="form-review form-control" id="name" value="<?php echo Yii::app()->session['user']['first_name']; ?>" placeholder="Name">

                                                                                </div>
                                                                                <div class="form-group">
                                                                                        <input type="text" class="form-review form-control" id="email" value="<?php echo Yii::app()->session['user']['email']; ?>" placeholder="Email">

                                                                                </div>
                                                                                <div class="form-group">
                                                                                        <textarea class="form-comment form-control" rows="5" id="comment" name="review_comment" placeholder="Comment"></textarea>

                                                                                </div>

                                                                                <div class="form-group">
                                                                                        <div class="stars">
                                                                                                <input type="hidden" id="review_star" name="review_star" />
                                                                                                <input class="star str star-5" id="star-5-2" type="radio" value="5" name="star"/>
                                                                                                <label class="star  star-5" for="star-5-2"></label>
                                                                                                <input class="star str star-4" id="star-4-2" type="radio" value="4" name="star"/>
                                                                                                <label class="star star-4" for="star-4-2"></label>
                                                                                                <input class="star str star-3" id="star-3-2" type="radio" value="3" name="star"/>
                                                                                                <label class="star star-3" for="star-3-2"></label>
                                                                                                <input class="star str star-2" id="star-2-2" type="radio" value="2" name="star"/>
                                                                                                <label class="star star-2" for="star-2-2"></label>
                                                                                                <input class="star str star-1" id="star-1-2" type="radio" value="1" name="star"/>
                                                                                                <label class="star star-1" for="star-1-2"></label>

                                                                                        </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                        <button class="button reviews review_submit" type="button">Post Your Review</button>
                                                                                </div>
                                                                                <div class="review_message"></div>

                                                                        </div>

                                                                </div>


                                                        </form>
                                                <?php } else { ?>
                                                        <form id="reviewform"  role="form">
                                                                <div class="login_form_container">
                                                                        <div class="login_form">
                                                                                <h1>Write Your Review</h1>
                                                                                <div class="form-group">
                                                                                        <input type="text" class="form-review form-control" id="names" value="<?php echo Yii::app()->session['user']['first_name']; ?>" placeholder="Name">

                                                                                </div>
                                                                                <div class="form-group">
                                                                                        <input type="text" class="form-review form-control" id="emails" value="<?php echo Yii::app()->session['user']['email']; ?>" placeholder="Email">

                                                                                </div>
                                                                                <div class="form-group">
                                                                                        <textarea class="form-comment form-control" rows="5" id="comment" name="review_comment" placeholder="Comment"></textarea>

                                                                                </div>

                                                                                <div class="form-group">
                                                                                        <div class="stars">
                                                                                                <input type="hidden" id="review_star" name="review_star" />
                                                                                                <input class="star str star-5" id="star-5-2" type="radio" value="5" name="star"/>
                                                                                                <label class="star  star-5" for="star-5-2"></label>
                                                                                                <input class="star str star-4" id="star-4-2" type="radio" value="4" name="star"/>
                                                                                                <label class="star star-4" for="star-4-2"></label>
                                                                                                <input class="star str star-3" id="star-3-2" type="radio" value="3" name="star"/>
                                                                                                <label class="star star-3" for="star-3-2"></label>
                                                                                                <input class="star str star-2" id="star-2-2" type="radio" value="2" name="star"/>
                                                                                                <label class="star star-2" for="star-2-2"></label>
                                                                                                <input class="star str star-1" id="star-1-2" type="radio" value="1" name="star"/>
                                                                                                <label class="star star-1" for="star-1-2"></label>

                                                                                        </div>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                        <button style="padding: 10px;margin-top: 0px;" type="button" class="button reviews review_submit">Post Your Review</button>
                                                                                </div>
                                                                                <div class="review_message"></div>

                                                                        </div>

                                                                </div>


                                                        </form>


                                                <?php } ?>
                                        </div>
                                </div>
                        </div>
                        <div class="related-products">
                                <?php if (!empty($related_products)) { ?>
                                        <h2>Related Products</h2>
                                        <div class="product_list_main">
                                                <div class="row ">

                                                        <?php foreach ($related_products as $data) { ?>
                                                                <?php echo $this->renderPartial('_view', array('data' => $data, 'width' => 3)); ?>
                                                        <?php } ?>


                                                </div>
                                        </div>
                                <?php } ?>
                        </div>
        </div>
</section>






<div class="modal fadeIn animated" id="success_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog zoomIn animated" role="document">
                <div class="modal-content">

                        <div class="modal-body verify_otp success_message">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title"><i class="fa fa-check-circle success"></i> SUCCESS</h4>
                                <p>Success: You have added <?php echo $product->product_name; ?> to your Shopping cart</p>

                                <div class="text-center centre-btn">
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>"><button type="button" class="button button_4" >Continue Shopping</button></a>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/cart/Mycart/"><button type="submit" class="button">PROCEED TO CHECKOUT</button></a>
                                </div>

                        </div>

                </div>
                <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="enquire_now" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php if (Yii::app()->user->hasFlash('enuirysuccess')) { ?>
                                        <h4 class="modal-title" id="myModalLabel">Thank You For Contacting Artstra</h4>
                                        <?php
                                }
                                ?>
                        </div>

                </div>
        </div>
</div>
<script>
        $(document).ready(function() {
                $("#review_star").val(0);
                $(".str").click(function() {
                        var values = $(this).val();
                        $("#review_star").val(values);
                });
        });
        $(document).ready(function() {
                $(".review_submit").click(function() {
                        var name = $("#names").val();
                        if (name == "") {
                                alert('Name Must be filled out');
                                return false;
                        }
                        var email = $("#emails").val();
                        if (email == "") {
                                alert('Email Must be filled out');
                                return false;
                        }
                        var comment = $("#comment").val();
                        var star = $("#review_star").val();
                        if (star == 0) {
                                alert('Star Must be Choosen');
                                return false;
                        }
                        var review_product_id = $("#review_product_id").val();
                        $('.review_submit').prop("disabled", true);
                        $.ajax({
                                type: "POST",
                                cache: 'false',
                                async: false,
                                url: baseurl + 'Products/Addreview',
                                data: {name: name, email: email, comment: comment, star: star, review_product_id: review_product_id}
                        }).done(function(data) {
                                $('.review_submit').prop("disabled", false);
                                $("#reviewform")[0].reset();
                                if (data == 2) {
                                        $(".review_message").html("<h5 style='color:green;'>Your Review Successfully Sent</h4>");
                                } else if (data == 1) {
                                        $(".review_message").html("<h5 style='color:red;'>You Already reviewed this Product</h4>");
                                }
                                //alert(data);

                        }).error(function(data) {
                                $('.review_submit').prop("disabled", false);
                        });
                });
        });</script>
<script>
        $(document).ready(function() {
                $(".update_cart").click(function() {
                        var id = $(this).attr('id');
                        var cart_id = $(this).attr('cart_id');
                        optionValidation(id, cart_id);
                });
                $(".add_to_cart").click(function() {
                        var id = $(this).attr('id');
                        optionValidation(id);
                });
                
                $('.size_selector').delegate('label', 'click', function () {
            var product_id = <?php echo $product->id; ?>;
            var option = $(this).attr('option_id');
            var color = $('.product_colors input.active').attr('color');
            var size = $(this).attr('size');
            $('#option_size').val('');
            if ($(this).hasClass("disabled")) {
//                            alert(1);
    $('.size_selector label').removeClass('active');
            } else {
                width_color_size(option, color, size);
                $('.size_selector label').removeClass('active');
                $(this).addClass('active');
                var size_opt = $('.size_selector .active').attr('id');
                $('#option_size').val(size_opt);
                var id = $(this).closest('.product_metas').find('.add_to_cart').attr('id');
                optionValError(id);
            }
            getstock(product_id);
            return false;
        });
        
        
//                 $('.size_selector label').removeClass('active');
//                 var product_id = <?php echo $product->id; ?>;
//                 var option = $('.size_selector').attr('option_id');
//                 var color = $('.product_colors input.active').attr('color');
//                 var size = $('.size_selector').attr('size');
//                 $('#option_size').val('');
//                 if ($(this).hasClass("disabled")) {
// //                            alert(1);
//                         $('.size_selector label').removeClass('active');
//                 } else {
//                         width_color_size(option, color, size);
//                         $('.size_selector label').removeClass('active');
//                         $(this).addClass('active');
//                         var size_opt = $('.size_selector .active').attr('id');
//                         $('#option_size').val(size_opt);
//                         var id = $(this).closest('.product_metas').find('.add_to_cart').attr('id');
//                         optionValError(id);
//                 }
//                 getstock(product_id);
//                 return false;
        });
        $('.width_selector').delegate('label', 'click', function() {
                var product_id = <?php echo $product->id; ?>;
//            var option = $(this).attr('option_id');
//            var color = $('.product_colors input.active').attr('color');
//            var size = $(this).attr('size');
                $('#option_width').val('');
                if ($(this).hasClass("disabled")) {
//                            alert(1);
                        $('.width_selector label').removeClass('active');
                } else {

                        $('.width_selector label').removeClass('active');
                        $(this).addClass('active');
                        var width_opt = $('.width_selector .active').attr('id');
                        $('#option_width').val(width_opt);
                        var id = $(this).closest('.product_metas').find('.add_to_cart').attr('id');
                        optionValError(id);
                }
                getstock(product_id);
                return false;
        });
        $('.color_picker').click(function() {
                $('.product_loader').show();
                $('.color_picker input').attr('checked', false);
                $(this).find('input').attr('checked', true);
                var option_type = $('#option_type').val();
                var color = $(this).find('input').attr('color');
                var option = $(this).find('input').attr('option_id');
                var product_id = $(this).find('input').attr('product');
                var id = $(this).find('input').closest('.product_metas').find('.add_to_cart').attr('id');
                $('.enq_color').val(color);
//                    selectgallery(color, product_id);
                $(".qntyerror").hide();
                if (option_type == 3) {

                        if ($(this).find('input').hasClass("disabled-input")) {
                                $('.product_loader').hide();
                        } else {
                                color_size(option, color);
                                $('.product_loader').hide();
                        }
                }
                if ($(this).find('input').hasClass("disabled")) {
                        $('.product_loader').hide();
                } else {

                        $('.color_picker input').removeClass('active');
                        $(this).find('input').addClass('active');
                        $('#option_color').val(color);
                        getstock(product_id);
                        optionValError(id);
                        $('.product_loader').hide();
                }
        });
//            $('.color_picker input').click(function () {
//
//                    var option_type = $('#option_type').val();
//                    var color = $(this).attr('color');
//                    var option = $(this).attr('option_id');
//                    var product_id = $(this).attr('product');
//
//                    var id = $(this).closest('.product_metas').find('.add_to_cart').attr('id');
//                    $('.enq_color').val(color);
//                    selectgallery(color, product_id);
//                    $(".qntyerror").hide();
//                    if (option_type == 3) {
//
//                            if ($(this).hasClass("disabled-input")) {
//                            } else {
//                                    color_size(option, color);
//                            }
//                    }
//                    if ($(this).hasClass("disabled")) {
//
//                    } else {
//
//                            $('.color_picker input').removeClass('active');
//                            $(this).addClass('active');
//                            $('#option_color').val(color);
//                            getstock(product_id);
//                            optionValError(id);
//                    }
//            });
//        });
        function getstock(product_id) {
                var color = $(".color_picker input.active").attr("color");
                var size = $(".size_selector label.active").attr("id");
                // showLoader();
                $.ajax({
                        type: "POST",
                        url: baseurl + 'products/Getstock',
                        data: {color: color, size: size, product_id: product_id}
                }).done(function(data) {
                        if (data > 0) {
                                var text = "";
                                for (i = 1; i <= data; i++) {
                                        text += "<option value=" + i + ">" + i + "</option>";
                                }
                                $(".qntyerror").html("");
                                $(".qntyerror").hide();
                                $(".qty").html(text);
                                $(".qty").show();
                        } else if (data <= 0) {
                        }
                        // hideLoader();
                });
        }


        function color_size(option, color) {
                $.ajax({
                        type: "POST",
                        url: baseurl + 'products/options',
                        data: {option: option, color: color}
                }).done(function(data) {
                        if (data != "") {
                                $('.size_selector').html(data);
                                //  hideLoader();
                        }
                });
        }
        function width_color_size(option, color, size) {
                $.ajax({
                        type: "POST",
                        url: baseurl + 'products/widthoptions',
                        data: {option: option, color: color, size: size}
                }).done(function(data) {
                        if (data != "") {
                                $('#option_width').val('');
                                $('.width_selector').html(data);
                                //  hideLoader();
                        }
                });
        }

        function selectgallery(color, product_id) {
                //  showLoader();
                $.ajax({
                        type: "POST",
                        url: baseurl + 'products/selectgallery',
                        data: {color: color, product_id: product_id}
                }).done(function(data) {
                        if (data != "") {
                                $(".zoomContainer").remove();
                                $('.image_gallery').html(data);
                        }
                        //  hideLoader();
                });
        }

        function optionValidation(id, cart_id) {
                var canname = $("#cano_name_" + id).val();
//            var qty = $(".qty").val();
                var qty = 1;
                var option_color = $('#option_color').val();
                var option_size = $('#option_size').val();
                var option_width = $('#option_width').val();
                var option_type = $('#option_type').val();
                var master_option = $('#master_option').val();
                var option_width_hide = $('#option_width_hide').val();
                var product_id = <?php echo $product->id; ?>;
                var eq_id = <?php echo $product->enquiry_sale; ?>;
                var extqty = getstockqnty(product_id);
                if (option_type == 3) {
                        if (option_color.length == 0 && option_size.length == 0 && option_width.length == 0) {
                                $('.option_errors').html('<p>Please select color and your Size to add this item to your shopping bag.</p>').show();
                                return false;
                        } else if (option_color.length == 0) {
                                $('.option_errors').html('<p>Please select color to add this item to your shopping bag.</p>').show();
                                return false;
                        } else if (option_size.length == 0) {
                                $('.option_errors').html('<p>Please select your size to add this item to your shopping bag.</p>').show();
                                return false;
                        } else if (option_width.length == 0) {
                                if (option_width_hide == 0) {
                                        $('.option_errors').html('<p>Please select your width fittings to add this item to your shopping bag.</p>').show();
                                        return false;
                                } else {
                                        $('.option_errors').html("").hide();
                                        updatecarts(canname, qty, option_color, option_size, master_option, cart_id, option_width);
                                }
                                //  $('.option_errors').html('<p>Please select your width fittings to add this item to your shopping bag.</p>').show();
                                //  return false;
                        } else if (extqty <= 0) {
                                $('.option_errors').html("").hide();
                                $(".qntyerror").show();
                                $(".option_errors").html("The selected options exceeds quantity available in stock.").show();
                                return false;
                        } else {
                                $('.option_errors').html("").hide();
                                updatecarts(canname, qty, option_color, option_size, master_option, cart_id, option_width);
                        }
                } else if (option_type == 1) {
                        if (option_color.length == 0) {
                                if (eq_id == 0) {
                                        $('.option_errors').html('<p>Please select Color</p>').show();
                                        return false;
                                }
                                else {
                                        $('.option_errors').html('<p>Please select color to add this item to your shopping bag.</p>').show();
                                        return false;
                                }
                        } else if (option_width.length == 0) {
                                if (option_width_hide == 0) {
                                        $('.option_errors').html('<p>Please select your width fittings to add this item to your shopping bag.</p>').show();
                                        return false;
                                } else {
                                        $('.option_errors').html("").hide();
                                        updatecarts(canname, qty, option_color, option_size, master_option, cart_id, option_width);
                                }

                        } else if (extqty <= 0) {
                                $('.option_errors').html("").hide();
                                $(".qntyerror").show();
                                $(".option_errors").html("The selected options exceeds quantity available in stock.").show();
                                return false;
                        } else {
                                $('.option_errors').html("").hide();
                                updatecarts(canname, qty, option_color, option_size, master_option, cart_id, option_width);
                        }
                } else if (option_type == 2) {
                        if (option_size.length == 0) {
                                $('.option_errors').html('<p>Please select your size to add this item to your shopping bag.</p>').show();
                                return false;
                        } else if (option_width.length == 0) {
                                if (option_width_hide == 0) {
                                        $('.option_errors').html('<p>Please select your width fittings to add this item to your shopping bag.</p>').show();
                                        return false;
                                } else {
                                        $('.option_errors').html("").hide();
                                        updatecarts(canname, qty, option_color, option_size, master_option, cart_id, option_width);
                                }

                        } else if (extqty <= 0) {
                                $('.option_errors').html("").hide();
                                $(".qntyerror").show();
                                $(".option_errors").html("The selected options exceeds quantity available in stock.").show();
                                return false;
                        } else {
                                $('.option_errors').html("").hide();
                                updatecarts(canname, qty, option_color, option_size, master_option, cart_id, option_width);
                        }
                }
                else {
                        $('.option_errors').html("").hide();
                        updatecarts(canname, qty, option_color = null, option_size = null, master_option = null, cart_id, option_width);
                }
        }
        function optionValError(id) {

                var canname = $("#cano_name_" + id).val();
                var qty = $(".qty").val();
                var option_color = $('#option_color').val();
                var option_size = $('#option_size').val();
                var option_width = $('#option_width').val();
                var option_type = $('#option_type').val();
                var master_option = $('#master_option').val();
                var option_width_hide = $('#option_width_hide').val();
                var product_id = <?php echo $product->id; ?>;
                //  checkquantity(canname);
                var extqty = getstockqnty(product_id);
                if (option_type == 3) {

                        if (option_color.length == 0 && option_size.length == 0 && option_width.length == 0) {
                                $('.option_errors').html('<p>Please select color and your size to add this item to your shopping bag.</p>').show();
                                return false;
                        } else if (option_color.length == 0) {
                                $('.option_errors').html('<p>Please select color to add this item to your shopping bag.</p>').show();
                                return false;
                        } else if (option_size.length == 0) {
                                $('.option_errors').html('<p>Please select your size to add this item to your shopping bag.</p>').show();
                                return false;
                        } else if (option_width.length == 0) {
                                if (option_width_hide == 0) {
                                        $('.option_errors').html('<p>Please select your width fittings to add this item to your shopping bag.</p>').show();
                                        return false;
                                } else {
                                        $('.option_errors').html("").hide();
                                        return true;
                                }
                        } else if (extqty <= 0) {
                                $('.option_errors').html("").hide();
                                $(".qntyerror").show();
                                $(".option_errors").html("The selected options exceeds quantity available in stock.").show();
                                return false;
                        } else {
                                $('.option_errors').html("").hide();
                        }
                } else if (option_type == 1) {

                        if (option_color.length == 0) {
                                $('.option_errors').html('<p>Please select color to add this item to your shopping bag.</p>').show();
                                return false;
                        } else if (option_width.length == 0) {
                                if (option_width_hide == 0) {
                                        $('.option_errors').html('<p>Please select your width fittings to add this item to your shopping bag.</p>').show();
                                        return false;
                                } else {
                                        $('.option_errors').html("").hide();
                                        return true;
                                }

                        } else if (extqty <= 0) {
                                $('.option_errors').html("").hide();
                                $(".qntyerror").show();
                                $(".option_errors").html("The selected options exceeds quantity available in stock.").show();
                                return false;
                        } else {
                                $('.option_errors').html("").hide();
                        }
                } else if (option_type == 2) {
                        if (option_size.length == 0) {
                                $('.option_errors').html('<p>Please select your size to add this item to your shopping bag.</p>').show();
                                return false;
                        } else if (option_width.length == 0) {
                                if (option_width_hide == 0) {
                                        $('.option_errors').html('<p>Please select your width fittings to add this item to your shopping bag.</p>').show();
                                        return false;
                                } else {
                                        $('.option_errors').html("").hide();
                                        return true;
                                }

                        } else if (extqty <= 0) {
                                $('.option_errors').html("").hide();
                                $(".qntyerror").show();
                                $(".option_errors").html("The selected options exceeds quantity available in stock.").show();
                                return false;
                        } else {
                                $('.option_errors').html("").hide();
                        }
                }
                else {
                        $('.option_errors').html("").hide();
                }
        }
        function getstockqnty(product_id) {
                var color = $(".product_colors input.active").attr("color");
                var size = $(".size_selector label.active").attr("id");
                var qty;
                $.ajax({
                        async: false,
                        type: "POST",
                        url: baseurl + 'products/Getstock',
                        data: {color: color, size: size, product_id: product_id}
                }).done(function(data) {
                        qty = data;
                });
                return qty;
        }
        function updatecarts(canname, qty, option_color, option_size, master_option, cart_id, option_width) {
                var position = $('input[name=leftright_selector]:checked').val();
                if (option_color === undefined) {
                        option_color = null;
                }
                if (option_size === undefined) {
                        option_size = null;
                }
                if (master_option === undefined) {
                        master_option = null;
                }
                $.ajax({
                        type: "POST",
                        url: baseurl + 'cart/UpdateProduct',
                        data: {cano_name: canname, qty: qty, option_color: option_color, option_size: option_size, master_option: master_option, cart_id: cart_id, option_width: option_width, position: position}
                }).done(function(data) {
                        var obj = jQuery.parseJSON(data);
                        if (obj.html == '9') {
                                $('.option_errors').html('<p>Invalid Product.Please try again</p>').show();
                        } else {
                                //$('.option_errors').html("").hide();
                                $("#cart_box").html(obj.html);
                                $(".cart_count").html(obj.cart_count);
                                //                            $(".cart_items").html(obj.cart_count);
                                //                            $('#success_modal .success_product_name').html(obj.product);
                                $('#success_modal p').html('Your product updated succesfully');
                                $('#success_modal').modal('show');
                        }
                        // hideLoader();
                });
        }
        function addtocarts(canname, qty, option_color, option_size, master_option, option_width) {
                if (option_color === undefined) {
                        option_color = null;
                }
                if (option_size === undefined) {
                        option_size = null;
                }
                if (master_option === undefined) {
                        master_option = null;
                }
                $.ajax({
                        type: "POST",
                        url: baseurl + 'cart/Buynow',
                        data: {cano_name: canname, qty: qty, option_color: option_color, option_size: option_size, master_option: master_option, option_width: option_width}
                }).done(function(data) {
                        var obj = jQuery.parseJSON(data);
                        if (obj.html == '9') {
                                $('.option_errors').html('<p>Invalid Product.Please try again</p>').show();
                        } else {
                                //$('.option_errors').html("").hide();
                                $("#cart_box").html(obj.html);
                                $(".cart_count").html(obj.cart_count);
//                            $('#success_modal .success_product_name').html(obj.product);
                                $('#success_modal').modal('show');
                        }
                        // hideLoader();
                });
        }
        $(".enq_button").click(function() {
                var enqcolor = $('.enq_color').val();
                if (enqcolor == '') {
                        return false;
                }
        });
        function showLoader() {
                $('.over-lay').show();
        }
        function hideLoader() {
                $('.over-lay').hide();
        }
</script>
<style>
        .option_errors{
                color:red;
        }
</style>
<script type="text/javascript">
        function popWindow(url) {
                var newWindow = window.open(url, "", "width=300, height=200");
        }
</script>
<script>
        (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                        return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.6";
                fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
</script>
<style>
    .lrradio{
        float: left;   
        margin-top: 8px !important; 
        margin-right: 7px !important;
        
    }
</style>