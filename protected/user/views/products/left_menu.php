<?php
//echo 32;
//exit;

$cid = Yii::app()->session['category_typ'];

$category_name = Yii::app()->request->getParam('name');
$parent = ProductCategory::model()->findByAttributes(array('canonical_name' => $category_name));
$parent_parent = ProductCategory::model()->findByAttributes(array('id' => $parent->parent));
$main_cats = ProductCategory::model()->findAllByAttributes(array('parent' => $parent->parent));

$max_value = $getproducts[0]->price;
$min_value = $getproducts[0]->price;
if (!empty($getproducts)) {
    foreach ($getproducts as $get) {
        $pid[] = $get->id;
        $width .= $get->width_fittings . ',';
        $toy .= $get->toy_style . ',';
        if ($get->price > $max_value) {
            $max_value = $get->price;
        }
        if ($get->price < $min_value) {
            $min_value = $get->price;
        }
    }
}
if (Yii::app()->session['min_val'] != '') {
    $current_min = Yii::app()->session['min_val'];
} else {
    $current_min = $min_value;
}
if (Yii::app()->session['max_val'] != '') {
    $current_max = Yii::app()->session['max_val'];
} else {
    $current_max = $max_value;
}
$width = rtrim($width, ',');
$toy = rtrim($toy, ',');

$width_fittings = explode(',', $width);
$toy_styles = explode(',', $toy);
$width_fittings = array_unique($width_fittings);
$width_fittings = array_filter($width_fittings);
$toy_styles = array_unique($toy_styles);
$toy_styles = array_filter($toy_styles);

if (!empty($get_all_option)) {
    foreach ($get_all_option as $color_option) {
        $clr[] = $color_option->color_id;
        $siz[] = $color_option->size_id;
    }
}
if (!empty($clr)) {
    $clr = array_unique($clr);
    $clr = array_filter($clr);
}
if (!empty($siz)) {
    $siz = array_unique($siz);
    $siz = array_filter($siz);
}
?>
<div class="col-sm-3 sidebar ">
        <h3 class="hidden-xs"><i class="fa fa-align-left"></i> Shopping Options</h3>
        <div class="listings_sidebar">
                <div class="category_selector">
                        <div class="close_bt hidden visible-xs">X</div>
                        <div class="close_bt hidden visible-xs apply_filter">APPLY</div>
                        <h4 class="fadeIn animated animdelay1"><?php echo $parent_parent->category_name; ?></h4>
                        <div class="filter-section">
                                <div class="link-items">
                                        <ul>
                                                <?php
                                                foreach ($main_cats as $main_cat) {
                                                    $subcats = ProductCategory::model()->findAllByAttributes(array('parent' => $main_cat->id), array('condition' => 'id !=' . $main_cat->id));
                                                    if (!empty($subcats)) {
                                                        $main_menus = '#';
                                                    } else {
                                                        $main_menus = Yii::app()->request->baseUrl . "/index.php/products/category/name/" . $main_cat->canonical_name;
                                                    }
                                                    ?>
                                                    <?php if ($parent_parent->id != $main_cat->id) { ?>
                                                        <li><a class="<?php
                                                                if ($main_cat->id == $parent->id) {
                                                                    echo 'sel_cat';
                                                                }
                                                                ?>" href="<?php echo $main_menus; ?>"><?= $main_cat->category_name; ?></a></li>
                                                                <?php
                                                            }
                                                            ?>
                                                        <?php } ?>
                                                        <?php
                                                        if (!empty($brand)) {
                                                            $get_all_brand = Brands::model()->findAllByAttributes(array('status' => 1));
                                                            ?>

                                                    <?php
                                                    if (!empty($get_all_brand)) {
                                                        foreach ($get_all_brand as $all_brand) {
                                                            ?>
                                                            <li><a class="<?php
                                                                    if ($all_brand->id == $brand->id) {
                                                                        echo 'sel_cat';
                                                                    }
                                                                    ?>" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/brand/<?php echo $all_brand->canonical_name; ?> "><?= $all_brand->brand_name; ?></a></li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        <?php } ?>
                                        </ul>
                                </div>
                        </div>
                </div>
                <?php if (!empty($getproducts)) { ?>
                    <?php
//                $form = $this->beginWidget('CActiveForm', array(
//                    'id' => 'left_form',
//                    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
//                    // Please note: When you enable ajax validation, make sure the corresponding
//                    // controller action is handling ajax validation correctly.
//                    // There is a call to performAjaxValidation() commented in generated controller code.
//                    // See class documentation of CActiveForm for details on this.
//                    'enableAjaxValidation' => false,
//                ));
                    ?>
                    <div class="filter_selector">
                            <div class="close_bt hidden visible-xs">X</div>
                            <div class="close_bt hidden visible-xs apply_filter">APPLY</div>
                            <div class="leftprice-filtr">
                            <h4>PRICE <span class="reset_filter"><a  href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/<?= $category_name; ?>">Reset All Filter</a></span></h4>
                            <div class="filter-section">
                                    <div class="price_filter">
                                            <div class="filter_box">
                                                    <p>
                                                            <input type="hidden" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                                                    </p>
                                                    <div id="slider-range"></div>
                                                    <div class="slider_values"> <span class="min_value"></span>  <span class="max_value"></span>
                                                            <input class="min_val" name="min_val" value="" type="hidden" >
                                                            <input class="max_val" name="max_val" value="" type="hidden" >
                                                            <div class="clearfix"></div>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                            </div>
                            <?php if (!empty($get_all_option)) { ?>
                                <?php if (!empty($clr)) { ?>
                                <div class="left-filtr">
                                    <h4 class="fadeIn animated animdelay1">COLOR </h4>
                                    <div class="filter-section">
                                            <div class="checkbox-items">
                                                    <?php
                                                    foreach ($clr as $color) {
                                                        $color_data = OptionCategory::model()->findByPk($color);
                                                        if ($color != 0) {
                                                            ?>
                                                            <div class="item color_data">
                                                                    <input type="checkbox" name="color[]" class="color_val" value="<?= $color; ?>">
                                                                    <label ><?= $color_data->color_name; ?></label>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </div>
                                    </div>
                                    </div>
                                <?php } ?>
                                <?php if (!empty($siz)) { ?>
                                <div class="left-filtr">
                                    <h4 class="fadeIn animated animdelay1">SIZE </h4>
                                    <div class="filter-section">
                                            <div class="checkbox-items size-checkbox">
                                                    <?php
//                                        $sizes = OptionCategory::model()->findAllByAttributes(array('option_type_id' => 2), array('order' => 'field1 asc'));
                                                    foreach ($siz as $size) {
                                                        if ($size != 0) {
                                                            $size_data = OptionCategory::model()->findByPk($size);
                                                            ?>
                                                            <div class="item size_data">
                                                                    <input type="checkbox" value="<?= $size ?>" name="size[]" >
                                                                    <label ><?= $size_data->size ?></label>
                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>


                                                    <div class="clearfix"></div>
                                            </div>
                                    </div>
                                    </div>
                                <?php } ?>

                            <?php } ?>
                            <div class="left-filtr">
                            <h4>WIDTH FITTING</h4>
                            <div class="filter-section">
                                    <div class="checkbox-items">
                                            <?php foreach ($width_fittings as $wd) { ?>
                                                <?php $width_fitting = WidthFitting::model()->findByPk($wd); ?>
                                                <div class="item width_data">
                                                        <input type="checkbox"  name="width[]" value="<?php echo $width_fitting->id; ?>" id="color_1">
                                                        <label><?php echo $width_fitting->title; ?></label>
                                                </div>
                                            <?php } ?>

                                    </div>
                            </div>
                            </div>
                            <div class="left-filtr">
                            <h4>TOE STYLE</h4>
                            <div class="filter-section">
                                    <div class="checkbox-items">
                                            <?php foreach ($toy_styles as $to) { ?>
                                                <?php $toy_style = ToyStyle::model()->findByPk($to); ?>
                                                <div class="item toy_data">
                                                        <input type="checkbox" name="width[]" value="<?php echo $toy_style->id; ?>" id="color_1">
                                                        <label><?php echo $toy_style->title; ?></label>
                                                </div>
                                            <?php } ?>
                                    </div>
                            </div>
                            </div>
                            <!--                        <div class="filter_search">
                                                            <button type="submit" class="button">Search</button>
                                                    </div>-->
                    </div>
                    <?php //$this->endWidget();   ?>
                <?php } ?>
        </div>
</div>
<?php
//Yii::app()->clientScript->registerScript('site', "$('.width_data').on('click',function(e){
//
//                            $.ajax({
//                                type: 'POST',
//                                url: baseurl + 'ajax/setsession',
//    //                            data: fdata,
//                                data: {'session_name': 'width_fitings', 'session_value': '2'},
//                                success: function (data) {
//
//                                       $.fn.yiiListView.update('product-grid');
//                                },
//                                errror: function () {
//
//                                }
//                            });
//                         });
//            ")
?>
<script>
    //Price Slider
    jQuery("#slider-range").slider({
            range: true,
            min: <?= $min_value; ?>,
            max: <?= $max_value; ?>,
            values: [<?= $current_min; ?>, <?= $current_max; ?>],
            slide: function (event, ui) {
                    jQuery("#amount").val("$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ]);
                    jQuery(".min_value").html(ui.values[ 0 ]);
                    jQuery(".max_value").html(ui.values[ 1 ]);
                    jQuery(".min_val").val(ui.values[ 0 ]);
                    jQuery(".max_val").val(ui.values[ 1 ]);
            }
    });
    jQuery("#amount").val("$" + jQuery("#slider-range").slider("values", 0) +
            " - $" + jQuery("#slider-range").slider("values", 1));
    jQuery(".min_value").html(jQuery("#slider-range").slider("values", 0));
    jQuery(".max_value").html(jQuery("#slider-range").slider("values", 1));
    ///// Mobile Category

    $("#slider-range").on("slidestop", function (event, ui) {
            var min_value = jQuery(".min_val").val();
            var max_value = jQuery(".max_val").val();
            var min_name = 'min';
            var max_name = 'max';

            setpricesession(min_name, min_value, max_name, max_value);
    });
    function setsession(session_name, session_value) {
            $('.product_loader').show();
            $.ajax({
                    type: 'POST',
                    url: baseurl + 'ajax/setsession',
                    //                            data: fdata,
                    data: {'session_name': session_name, 'session_value': session_value},
                    success: function (data) {
                            $('.product_loader').hide();
                            $.fn.yiiListView.update('product-grid');
                    },
                    errror: function () {
                            $('.product_loader').hide();

                    }
            });
    }
    function setpricesession(min_name, min_value, max_name, max_value) {
            $('.product_loader').show();
            $.ajax({
                    type: 'POST',
                    url: baseurl + 'ajax/setpricesession',
                    //                            data: fdata,
                    data: {min_name: min_name, min_value: min_value, max_name: max_name, max_value: max_value},
                    success: function (data) {
                            $('.product_loader').hide();
                            $.fn.yiiListView.update('product-grid');
                    },
                    errror: function () {
                            $('.product_loader').hide();

                    }
            });
    }

    $('document').ready(function () {
            $('.width_data').click(function () {
                    var color_value = '';
                    $(".checkbox-items .width_data input").each(function () {
                            var ischecked = $(this).is(":checked");
                            if (ischecked) {
                                    color_value += $(this).val() + "|";
                            }
                    });
                    var session_value = color_value;
                    var session_name = 'width_fitting';
                    setsession(session_name, session_value);
            });
            $('.toy_data').click(function () {
                    var color_value = '';
                    $(".checkbox-items .toy_data input").each(function () {
                            var ischecked = $(this).is(":checked");
                            if (ischecked) {
                                    color_value += $(this).val() + "|";
                            }
                    });
                    var session_value = color_value;
                    var session_name = 'toy_style';
                    setsession(session_name, session_value);
            });
            $('.color_data').click(function () {
                    var color_value = '';
                    $(".checkbox-items .color_data input").each(function () {
                            var ischecked = $(this).is(":checked");
                            if (ischecked) {
                                    color_value += $(this).val() + "|";
                            }
                    });
                    var session_value = color_value;
                    var session_name = 'color';
                    setsession(session_name, session_value);
            });
            $('.size_data').click(function () {
                    var color_value = '';
                    $(".checkbox-items .size_data input").each(function () {
                            var ischecked = $(this).is(":checked");
                            if (ischecked) {
                                    color_value += $(this).val() + "|";
                            }
                    });
                    var session_value = color_value;
                    var session_name = 'size';
                    setsession(session_name, session_value);
            });

    });


</script>