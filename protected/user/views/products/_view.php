<?php
$option_exists = OptionDetails::model()->findAllByAttributes(array('product_id' => $data->id));
if (empty($option_exists)) {
    if ($data->stock_availability == 0 || $data->quantity <= 0) {

        $cl = " list-out";
    }
} else {

    foreach ($option_exists as $option_exist) {
        $total_stock += $option_exist->stock;
        $out_stock +=$option_exist->status;
    }
    if ($total_stock == 0 || $out_stock == 0) {
        $cl = " list-out";
    }
}
?>
<div class="col-xs-<?php
if (isset($width)) {
    echo $width;
} else {
    echo '4';
}
?> item">
        <div class="item-wrap">
                <div class="prdt_img">
                        <?php
                        $feat = ProductsFeatured::model()->findAllByAttributes(array('product_id' => $data->id));
                        $pct = count($feat);
                        if ($data->quantity == 0) {
                            ?>
                            <div class="sold-badge"></div>

                        <?php } else if ($data->discount_rate != 0) {
                            ?>
                            <div class="sale-badge"></div>
                        <?php } else if ($pct > 0) {
                            ?>

                            <div class="new-badge"></div>
                            <?php
                        }
                        ?>
                        <div class="product_data img-wrapper">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $data->canonical_name; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php
                                        echo Yii::app()->Upload->folderName(0, 1000, $data->id)
                                        ?>/<?php echo $data->id; ?>/medium.<?php echo $data->main_image; ?>" alt=""/></a>
                        </div>
                        <div class="product_hover">
                                
                                   <?php if (Yii::app()->session['user']) { ?>
                                                                                  <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $data->id ?>" class="button">ADD TO WISHLIST</a>
                                                                                     <?php } else
                                                                                     {
                                                                                         ?>
                                                                                         <a href="javascript: void(0)" class="button userlogin">ADD TO WISHLIST</a>
                                                                                         <?php
                                                                                     }?>
                                
                                <a href="javascript:void(0)" class="quick button button_2 quick_data" product_id="<?php echo $data->id; ?>" > QUICK VIEW</a>
                        </div>
                </div>
                <?php $product_reviews = UserReviews::model()->findAllByAttributes(['product_id' => $data->id]); ?> 
                <div class="product_detail_grid">
                        <div class="product_title"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $data->canonical_name; ?>">
                            <?php
                                                                                        $len = strlen($data->product_name);
                                                                                        if ($len > 15) {
                                                                                                echo substr($data->product_name, 0, 15)."...";
                                                                                        } else {
                                                                                                echo $data->product_name;
                                                                                        }
                                                                                        ?>
                            </a></div>
                        <?php
                        if ($data->discount_rate != 0) {
                            ?>
                            <div class="product_price" >
                                    
                                    <div class="product_srar">
                                            <ul class="list-unstyled list-inline">
                                                    <?php
                                                    $cn = count($product_reviews);
                                                    foreach ($product_reviews as $product_review) {
                                                        $total_r += $product_review->rating;
                                                    }
                                                    if ($cn > 0) {
                                                        $cn = $cn;
                                                    } else {
                                                        $cn = 1;
                                                    }
                                                    $total_rating = ceil($total_r / $cn);
                                                    $j = $total_rating;
                                                    $k = 5 - $j;
                                                    ?>
                                                    <?php
                                                    for ($i = 1; $i <= $j; $i++) {
                                                        ?>
                                                        <li><i class="fa stars fa-star"></i></li>
                                                    <?php } ?>
                                                    <?php
                                                    for ($i = 1; $i <= $k; $i++) {
                                                        ?>
                                                        <li><i class="fa stars fa-star blank"></i></li>
                                                    <?php }
                                                    ?>
                                                    <?php
                                                    if ($total_rating == 0) {
//                                                        echo '(Not Yet Rated)';
                                                    }
                                                    ?>

                                            </ul>
                                    </div>
                                    <del class="text-danger"><?php echo Yii::app()->Currency->convert($data->price); ?></del> <?php echo Yii::app()->Discount->Discount($data); ?>
                            </div>

                            <?php
                        } else {
                            ?>
                            <div style="padding-left: 10px;">
                                    <div class="product_srar">
                                            <ul class="list-unstyled list-inline">
                                                    <?php
                                                    $cn = count($product_reviews);
                                                    foreach ($product_reviews as $product_review) {
                                                        $total_r += $product_review->rating;
                                                    }
                                                    if ($cn > 0) {
                                                        $cn = $cn;
                                                    } else {
                                                        $cn = 1;
                                                    }
                                                    $total_rating = ceil($total_r / $cn);
                                                    $j = $total_rating;
                                                    $k = 5 - $j;
                                                    ?>
                                                    <?php
                                                    for ($i = 1; $i <= $j; $i++) {
                                                        ?>
                                                        <li><i class="fa stars fa-star"></i></li>
                                                    <?php } ?>
                                                    <?php
                                                    for ($i = 1; $i <= $k; $i++) {
                                                        ?>
                                                        <li><i class="fa stars fa-star blank"></i></li>
                                                    <?php }
                                                    ?>
                                                    <?php
                                                    if ($total_rating == 0) {
//                                                        echo '(Not Yet Rated)';
                                                    }
                                                    ?>

                                            </ul>
                                    </div>
                                    <?php echo Yii::app()->Discount->Discount($data); ?></div>
                            <?php
                        }
                        ?>

                        <!--<div class="product_price">
                                <div class="product_srar"><a href="#" class="star1 active"><i class="fa fa-star-o"></i></a><a href="#" class="star2"><i class="fa fa-star-o"></i></a><a href="#" class="star3"><i class="fa fa-star-o"></i></a><a href="#" class="star4"><i class="fa fa-star-o"></i></a><a href="#" class="star5"><i class="fa fa-star-o"></i></a></div>
                        <?php echo Yii::app()->Discount->Discount($data); ?></div>-->
                        <div class="product-des">
                                <p><?php echo $data->description; ?> </p>
                        </div>
                </div>
                <div class="sction_meta">
                        <div class="row list_no">
                                <div class="col-xs-9"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $data->canonical_name; ?>" class="button button">SHOP NOW</a></div>
                                <!--<div class="col-xs-3 nopad_left text-right"><a href="#" class="quick" data-toggle="modal" data-target="#quick_view"><i class="fa fa-search"></i></a><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $data->canonical_name; ?>" class="fav"><i class="fa fa-heart-o"></i></a></div>-->
                        </div>
                        <div class="hidden list_yes">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $data->canonical_name; ?>" class="button">SHOP NOW</a>
                                <a href="javascript:void(0)" class="quick button button_2 quick_data" product_id="<?php echo $data->id; ?>">QUICK VIEW</a>
                               
                                  <?php if (Yii::app()->session['user']) { ?>
                                                                                   <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $data->id ?>" class="button button_2 wish">WISHLIST <i class="fa fa-heart-o"></i></a>
                                                                                     <?php } else
                                                                                     {
                                                                                         ?>
                                                                                         <a href="javascript: void(0)" class="button button_2 wish userlogin">ADD TO WISHLIST</a>
                                                                                         <?php
                                                                                     }?>
                                
                                
                        </div>

                </div>
        </div>

</div>
