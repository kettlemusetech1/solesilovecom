
<html>
        <head>
                <title>emailer</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        </head>
        <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
                <div id="fb-root"></div>


                <div style="margin:auto; width:776px; border:solid 1px #c0c0c0; margin-top:40px; margin-bottom:40px;">
                        <table id="Table_01" width="776" border="0" cellpadding="0" cellspacing="0" align="center">
                                <tr>
                                        <td><a href="http://laksyah.com"><img src="<?php echo $this->siteURL(); ?>/images/emailer_01.jpg" width="776" height="102" alt=""></a></td>
                                </tr>
                                <table width="776" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                                <tr>
                                                        <td style="font-family: 'Lucida Grande', 'DejaVu Sans', Verdana, 'Lucida Sans Unicode', 'sans-serif'; font-size: 14px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                                <tr>
                                                                                        <td></td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td align="center" style="padding: 0px 170px;"><p style="padding: 5px 9px 6px 9px;text-align:center; margin-bottom: 0px; padding-bottom: 0px;"><strong>Hi <?php echo $model->name; ?>,</strong></p>





                                                                                                <p style="padding: 5px 9px 6px 9px;text-align:center; margin-top: 0px; padding-top:0;">We sincerely appreciate your time and interest in our product. Please find the price details below. </p>
                                                                                                <p style="padding: 5px 9px 6px 9px;text-align:center"> Product Name: <strong>Charm Queen - Saree with blouse </strong><br>
                                                                                                        Product code: <strong>LKAN06125</strong> </p>
                                                                                                <p style="padding: 5px 9px 6px 9px;text-align:center"> Price: <strong style="font-size:22px; font-weight: bold;">Rs. 18,500/-</strong><br>
                                                                                                        Expected to ship: 2 - 3 weeks from order date<br>
                                                                                                        <em style="font-size: 13px;">Express delivery is available at an additional charge </em> </p>
                                                                                                <p style="padding: 5px 9px 6px 9px;text-align:center"><strong style="font-size: 18px;">Payment terms: </strong></p></td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td align="center" style="padding: 0px 160px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                        <tbody>
                                                                                                                <tr>
                                                                                                                        <td align="center" width="45%" style="padding: 20px; border: solid 1px #ccc;">
                                                                                                                                OPTION 1:<br>
                                                                                                                                <strong style="font-size: 16px;">Pay full amount at one go </strong>
                                                                                                                        </td>
                                                                                                                        <td align="center" width="10%"><strong>OR</strong></td>
                                                                                                                        <td align="center"  width="45%"  style="padding: 15px; border: solid 1px #ccc;">OPTION 2:<br>
                                                                                                                                <strong style="font-size: 16px;">Pay 50% now & 50% on completion </strong></td>
                                                                                                                </tr>
                                                                                                        </tbody>
                                                                                                </table></td>
                                                                                </tr>



                                                                                <tr>
                                                                                        <td align="center" style="padding: 0px 170px;"><p style="font-size: 14px; text-align: center; color:#333; font-style: italic;">A lesser deposit may be accepted under certain conditions. Please contact us on <strong>+91 9656303333</strong> for more details.</p></td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td align="center" style="padding: 0px 170px;">
                                                                                                <a href="<?php echo $this->siteURL(); ?><?php echo $celib_history_update->link; ?>" name="yt0"  style="text-transform: uppercase;background-color: #ec9721;border-radius: 0;outline: none;font-weight: bold;height: 40px;line-height: 40px;padding: 0px 28px;display:block;
                                                                                                   border: solid 1px #ec9721;border-radius: 4px;text-align: center;text-decoration: none;color: #fff;">CLICK HERE TO MAKE PAYMENT</a> <br/>
                                                                                                <p style="font-size: 11px;text-align: center;color:#acacb1;">* If this email was not intended for you and was sent to you by mistake, please call us at +9142202222 or email us at support@laksyah.com immediately.</p>
                                                                                                <p style="font-size: 11px;text-align: center;color:#acacb1;">* This is an automatically generated email, please do not reply to this email.</p></td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td><br>

                                                                                                <p style="font-size:12px;margin:0 0 10px 0"></p></td>
                                                                                </tr>
                                                                        </tbody>
                                                                </table></td>
                                                </tr>
                                        </tbody>
                                </table>

                                <tr>
                                        <td style="background-color:#61625d"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>

                                                                <tr>
                                                                        <td width="250" align="center" style=""></td>
                                                                        <td align="center" >
                                                                                <img src="<?php echo $this->siteURL(); ?>/images/emailer_03.jpg" width="250" height="47" alt=""></td>
                                                                        <td width="250" align="center" style="text-align: right;"><a href="https://www.facebook.com/laksyah/" target="new"><img src="<?php echo $this->siteURL(); ?>/images/facebook.png" style="height: 29px; padding-right: 10px;" alt=""></a></td>
                                                                </tr>
                                                        </tbody>
                                                </table></td>

                                </tr>
                                <tr>
                                        <td style="background-color:#f7f4f1"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>

                                                                <tr>
                                                                        <td width="250" align="center" style="border-right:solid 1px #d7d7d7; padding-top: 0px;">
                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                                <tr>
                                                                                                        <td align="center"><h4 style=" font-family:'Open Sans',arial, sans-serif; font-size:16px; color:#414042; margin-bottom:10px; margin-top: 0px;">Follow Us </h4></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                        <td align="center" style="padding: 5px 0px;"><a href="https://laksyah.com" style="display: block; border: none;"><img src="<?php echo $this->siteURL(); ?>/images/laksyah-com.jpg" width="172" height="26" alt=""/></a></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                        <td align="center">
                                                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                        <tbody>
                                                                                                                                <tr>
                                                                                                                                        <td>&nbsp;</td>
                                                                                                                                        <td  width="31"><a href="https://instagram.com/laksyah/" style="display: block; border: none;"><img src="<?php echo $this->siteURL(); ?>/images/email_insta.jpg" width="31" height="28" alt=""/></a></td>
                                                                                                                                        <td  width="31"><a href="https://www.youtube.com/channel/UConEr0j4L-p-NgWg5b6P_vw" style="display: block; border: none;"><img src="<?php echo $this->siteURL(); ?>/images/email-youtube.jpg" width="31" height="28" alt=""/></a></td>
                                                                                                                                        <td  width="31"><a href="https://www.facebook.com/laksyah" style="display: block; border: none;"><img src="<?php echo $this->siteURL(); ?>/images/email-fb.jpg" width="31" height="28" alt=""/></a></td>
                                                                                                                                        <td  width="31"><a href="https://twitter.com/laksyah" style="display: block; border: none;"><img src="<?php echo $this->siteURL(); ?>/images/email-twitter.jpg" width="31" height="28" alt=""/></a></td>
                                                                                                                                        <td  width="31"><a href="https://www.pinterest.com/laksyah/" style="display: block; border: none;"><img src="<?php echo $this->siteURL(); ?>/images/email-pin.jpg" width="31" height="28" alt=""/></a></td>
                                                                                                                                        <td>&nbsp;</td>
                                                                                                                                </tr>
                                                                                                                        </tbody>
                                                                                                                </table>

                                                                                                        </td>
                                                                                                </tr>
                                                                                        </tbody>
                                                                                </table>

                                                                        </td>
                                                                        <td align="center" style="border-right:solid 1px #d7d7d7; padding-top: 20px; padding-bottom: 20px">
                                                                                <h4 style=" font-family:'Open Sans',arial, sans-serif; font-size:16px; color:#414042; margin-bottom:10px;">Contact Us </h4>
                                                                                <p style="font-family:'Open Sans',arial, sans-serif; font-size:13px;">Tel: +91 914 220 2222 <br>
                                                                                        <a href="mailto:support@laksyah.com" style="border:none; color:#414042;">support@laksyah.com</a> <br>
                                                                                        Mon to Sat 9.30am to 6.30pm IST<br><span style="display:inline-block;"><img src="<?php echo $this->siteURL(); ?>/images/wup.png" style="float: left;margin-right: 5px;">+91 9656303333</span></p></td>
                                                                        <td width="250" align="center" style=" padding-top: 20px; padding-bottom: 20px"><h4 style=" font-family:'Open Sans',arial, sans-serif; font-size:16px; color:#414042; margin-bottom:10px;">Visit Us</h4>
                                                                                <p style="font-family:'Open Sans',arial, sans-serif; font-size:13px;">The Design House,  C-3,<br>
                                                                                        GCDA House, Mavelipuram,<br>
                                                                                        Kakkanad, Kochi-682030<br>Kerala, India</p></td>
                                                                </tr>
                                                        </tbody>
                                                </table></td>
                                </tr>
                        </table></div>
        </body>
</html>
