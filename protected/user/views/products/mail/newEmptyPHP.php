<?php
$value = rtrim($product->category_id, ',');
$ids = explode(',', $value);
foreach ($ids as $id) {
        $cat_name = ProductCategory::model()->findByPk($id)->category_name;
}
?>
<?php
$folder = Yii::app()->Upload->folderName(0, 1000, $product->id);
?>
<style>
        .product_metas .modal-header h3  {
                font-weight: normal !important;
                line-height: 1.428571 !important;
                text-transform:none !important;
        }
        .terms_link a{
                color: #337ab7;
                text-decoration:none;
        }
        .qntyerror{
                /*border: 1px dashed #F97575;*/
                // padding: 10px;

                color: red;
                margin-bottom: 10px;
        }
        .time{
                font-size:12px;
                font-weight:none;
        }
        .deal_timer .timer {
                font-size: 14px;
                font-weight: bolder;
        }
        .modal-header .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
                background-color: #f5f2ee !important;
        }
</style>
<div class="container main_container">
        <div class="breadcrumbs">
                <?php
                //$category_name = Yii::app()->request->getParam('name');
                $url = Yii::app()->request->urlReferrer;
                $catname = explode("/", $url);
                $category_name = $catname[5];
                ?>
                <?php echo $this->renderPartial('_bread_crumb', array('category_name' => $category_name)); ?><span>/</span><?php echo $product->product_name; ?>
        </div>
        <div class="product_details">
                <div class="row">

                        <div class="col-sm-7 col-md-8">
                                <div class="sml">Hover your mouse over the image to zoom.</div>
                                <div class="image_gallery">
                                        <div class="product_thumb">
                                                <ul id="gal1">
                                                        <?php
                                                        //  $folder = Yii::app()->Upload->folderName(0, 1000, $product->id);
                                                        $big = Yii::app()->basePath . '/../uploads/products/' . $folder . '/' . $product->id . '/gallery/big';
                                                        $bigg = Yii::app()->request->baseUrl . '/uploads/products/' . $folder . '/' . $product->id . '/gallery/big/';
                                                        $thu = Yii::app()->basePath . '/../uploads/products/' . $folder . '/' . $product->id . '/gallery/small';
                                                        $thumbs = Yii::app()->request->baseUrl . '/uploads/products/' . $folder . '/' . $product->id . '/gallery/small/';
                                                        $zoo = Yii::app()->basePath . '/../uploads/products/' . $folder . '/' . $product->id . '/gallery/zoom';
                                                        $zoom = Yii::app()->request->baseUrl . '/uploads/products/' . $folder . '/' . $product->id . '/gallery/zoom/';
                                                        $file_display = array('jpg', 'jpeg', 'png', 'gif');
                                                        if (file_exists($big) == false) {

                                                        } else {
                                                                $dir_contents = scandir($big);
                                                                $i = 0;

                                                                foreach ($dir_contents as $file) {
                                                                        $file_type = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                                                                        if ($file !== '.' && $file !== '..' && in_array($file_type, $file_display) == true) {
                                                                                ?>

                                                                                <li class="<?php echo $i; ?>"><a href="#" data-image="<?php echo $bigg . $file; ?>" data-zoom-image="<?php echo $zoom . $file; ?>"> <img src="<?php echo $thumbs . $file; ?>" alt=""/> </a> </li>
                                                                                <?php
                                                                                $i++;
                                                                        }
                                                                        ?>



                                                                        <?php
                                                                }
                                                        }
                                                        ?>
<!--                                                    <li><a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/images/product_big2.jpg" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/images/product_lg.jpg"> <img src="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" alt=""/> </a></li>
                                                        <li><a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/images/product_big.jpg"> <img src="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" alt=""/> </a></li>
                                                        <li><a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/images/product_big2.jpg"> <img src="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" alt=""/> </a></li>
                                                        -->
                                                        <?php if (empty($dir_contents)) { ?>
                                                                <li><a href="#" data-image="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder ?>/<?= $product->id ?>/big.<?= $product->main_image ?>" data-zoom-image="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder ?>/<?= $product->id ?>/zoom.<?= $product->main_image ?>"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder ?>/<?= $product->id ?>/small.<?= $product->main_image ?>" alt=""/> </a></li>
                                                        <?php } ?>
                                                </ul>
                                        </div>
                                        <?php
                                        $folder = Yii::app()->Upload->folderName(0, 1000, $product->id);
                                        ?>

                                        <?php
                                        if (!empty($dir_contents)) {
                                                $km = 0;
                                                foreach ($dir_contents as $file1) {

                                                        if ($km == 2) {
                                                                $resultimage = $file1;
                                                        }

                                                        $km++;
                                                }
                                                ?>

                                                <div class="product_big_image"> <img src="<?php echo $bigg . $resultimage; ?>" id="laksyah_zoom" data-zoom-image="<?php echo $zoom . $resultimage; ?>" alt=""/>
                                                        <div class="product_social_shares"> <span>Share this look with your friends</span>
                                                                <a onclick="popWindow('https://www.facebook.com/sharer/sharer.php?u=https://laksyah.com/Products/Detail/name/<?php echo $product->canonical_name; ?>', 'facebook', 'width=1000,height=200,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');"><i class="fa fa-facebook" ></i></a>
                                                                <a onclick="popWindow('http://twitter.com/share?ur=https://laksyah.com/Products/Detail/name/<?php echo $product->canonical_name; ?>', 'twitter', '');"><i class="fa fa-twitter" ></i></a>
                                                                <a onclick="popWindow('https://pinterest.com/pin/create/button/?url=https://laksyah.com/Products/Detail/name/<?php echo $product->canonical_name; ?>', 'pinterest', '');"><i class="fa fa-pinterest-p" ></i></a>
                                                                <?php // }  ?>
                                                                <a href="#" data-toggle="modal" data-target="#enquirymail"><i class="fa fa-envelope-o"></i></a> </div>
                                                </div>
                                        <?php } else { ?>

                                                <div class="product_big_image"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder ?>/<?= $product->id ?>/big.<?= $product->main_image ?>" id="laksyah_zoom" data-zoom-image="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder ?>/<?= $product->id ?>/zoom.<?= $product->main_image ?>" alt=""/>
                                                        <div class="product_social_shares"> <span>Share this look with your friends</span><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-pinterest-p"></i></a><a href="#" data-toggle="modal" data-target="#enquirymail"><i class="fa fa-envelope-o"></i></a> </div>
                                                </div>
                                        <?php } ?>

                                </div>
                                <div class="mobile_slider">
                                        <div class="laksyah_slider">
                                                <?php if (file_exists($big) == false) { ?>
                                                        <div class = "item"> <img src = "<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder ?>/<?= $product->id ?>/big.<?= $product->main_image ?>" id = "laksyah_zoom" data-zoom-image = "<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder ?>/<?= $product->id ?>/big.<?= $product->main_image ?>"></div>
                                                        <?php
                                                } else {
                                                        $dir_contents = scandir($big);
                                                        $i = 0;
                                                        foreach ($dir_contents as $file) {
                                                                $file_type = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                                                                if ($file !== '.' && $file !== '..' && in_array($file_type, $file_display) == true) {
                                                                        ?>

                                                                        <div class="item"> <img src="<?php echo $bigg . $file; ?>"  id="laksyah_zoom" data-zoom-image="<?php echo $zoom . $file; ?>"></div>
                                                                        <?php
                                                                }
                                                                ?>



                                                                <?php
                                                        }
                                                        $i++;
                                                }
                                                ?>
                                        </div>
                                </div>
                                <div class="clearfix"></div>
                                <script type="text/javascript">
                                        function popWindow(url) {
                                                var newWindow = window.open(url, "", "width=300, height=200");
                                        }
                                </script>
                        </div>
                        <div class="col-sm-5 col-md-4 product_details_sidebar">
                                <div class="product_metas">
                                        <?php
                                        // if ($product->enquiry_sale == 1) {
                                        $option_exists = OptionDetails::model()->findAllByAttributes(array('product_id' => $product->id));


                                        if (empty($option_exists)) {

                                                if ($product->quantity == 0) {
                                                        ?>
                                                        <div class="out_of_stock_badge"></div>
                                                <?php } else if ($product->quantity <= 2 && $product->quantity != 0) { ?>
                                                        <div class="allmost_gone_badge"></div>
                                                        <?php
                                                }
                                        } else {

                                                foreach ($option_exists as $option_exist) {
                                                        $total_stock += $option_exist->stock;
                                                        $out_stock +=$option_exist->status;
                                                }
                                                if ($total_stock == 0 || $out_stock == 0) {
                                                        $out_stock = 1;
                                                        ?>
                                                        <div class="out_of_stock_badge"></div>
                                                <?php } else if ($total_stock <= 2 && $total_stock != 0) { ?>
                                                        <div class="allmost_gone_badge"></div>
                                                        <?php
                                                }
                                        }
                                        //  }
                                        ?>

                                        <h1><?php echo $product->product_name; ?></h1>
                                        <!--<h5><?php //echo $product->product_code;                                                                                                                                                                                                                                                                                                                          ?></h5>-->
                                        <div class="product_ID">Product Code: <?php echo $product->product_code; ?></div>
                                        <?php if ($product->enquiry_sale == 1) { ?>


                                                <?php
                                                $today_deal_products = DealProducts::model()->findByAttributes(array('date' => date('Y-m-d')));
                                                if (!empty($today_deal_products)) {



                                                        $HiddenProducts = explode(',', $today_deal_products->deal_products);
                                                        if (in_array($product->id, $HiddenProducts)) {
                                                                if ($product->discount_type == 1) {
                                                                        $discountRate = $product->price - $product->discount_rate;
                                                                } else {
                                                                        $discountRate = $product->price - ( $product->price * ($product->discount_rate / 100));
                                                                }
                                                                ?>


                                                                <p><span class="old_price"><?php echo Yii::app()->Currency->convert($product->price); ?></span> &nbsp; <span class="discounted"><?php echo Yii::app()->Discount->Discount($product); ?></span></p>

                                                                <div class="deal_timer">
                                                                        <div class="deal_title">Deal Ends in:</div>
                                                                        <div class="deal_time">
                                                                                <ul class="timer list-unstyled list-inline text-center" id="clock">

                                                                                        <li><h1>00 :</h1><span class="time">Hrs</span></li>
                                                                                        <li><h1>00 :</h1><span class="time">Min</span></li>
                                                                                        <li><h1>00 </h1><span class="time">Sec</span></li>
                                                                                </ul>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                </div>





                                                                <?php
                                                                date_default_timezone_set("Asia/Kolkata");
                                                                $date = date('Y-m-d 23:59:59');
                                                                $exp_date = strtotime($date);
                                                                $now = time();

                                                                if ($now < $exp_date) {
                                                                        ?>
                                                                        <script>
                                                                                // Count down milliseconds = server_end - server_now = client_end - client_now
                                                                                var server_end = <?php echo $exp_date; ?> * 1000;
                                                                                var server_now = <?php echo time(); ?> * 1000;
                                                                                var client_now = new Date().getTime();
                                                                                var end = server_end - server_now + client_now; // this is the real end time

                                                                                var _second = 1000;
                                                                                var _minute = _second * 60;
                                                                                var _hour = _minute * 60;
                                                                                var _day = _hour * 24
                                                                                var timer;

                                                                                function showRemaining()
                                                                                {
                                                                                        var now = new Date();
                                                                                        var distance = end - now;
                                                                                        if (distance < 0) {
                                                                                                clearInterval(timer);
                                                                                                location.reload();
                                                                                                // document.getElementById('countdown').innerHTML = 'EXPIRED!';

                                                                                                return;
                                                                                        }
                                                                                        var day = Math.floor(distance / _day);

                                                                                        var days = day > 9 ? "" + day : "0" + day;


                                                                                        var hour = Math.floor((distance % _day) / _hour);
                                                                                        var hours = hour > 9 ? "" + hour : "0" + hour;
                                                                                        var minute = Math.floor((distance % _hour) / _minute);
                                                                                        var minutes = minute > 9 ? "" + minute : "0" + minute;
                                                                                        var second = Math.floor((distance % _minute) / _second);
                                                                                        var seconds = second > 9 ? "" + second : "0" + second;

                                                                                        var countdown = document.getElementById('clock');
                                                                                        countdown.innerHTML = '';
                                                                                        //if (days) {
                                                                                        //countdown.innerHTML += '<li><h1>' + days + ' : </h1></li>';
                                                                                        //}
                                                                                        countdown.innerHTML += '<li><h1>' + hours + ' : </h1><span class="time">Hrs</span></li>';
                                                                                        countdown.innerHTML += '<li><h1>' + minutes + ' : </h1><span class="time">Min</span></li>';
                                                                                        countdown.innerHTML += '<li><h1>' + seconds + '  </h1><span class="time">Sec</span></li>';
                                                                                }

                                                                                timer = setInterval(showRemaining, 1000);
                                                                        </script>
                                                                        <?php
                                                                }
                                                                ?>
                                                                <div id="countdown"></div>




                                                        <?php } else {
                                                                ?>
                                                                <div class="product_price"><span><?php echo Yii::app()->Discount->Discount($product); ?></span></div>
                                                                <?php
                                                        }
                                                } else {
                                                        ?>
                                                        <div class="product_price"><span><?php echo Yii::app()->Discount->Discount($product); ?></span></div>
                                                        <p class="tax_info"><em>Inclusive of all local taxes</em></p>
                                                        <?php
                                                }
                                        }
                                        ?>
                                        <p class="tax_info"></p>


                                        <?php
                                        if ($product->enquiry_sale == 1) {
                                                //instock//

                                                if ($product->stock_availability == 1) {

                                                        if (empty($option_exists)) {

                                                                if ($product->quantity == 0) {
                                                                        ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--<p class="tax_info"><em>Inclusive of all local taxes</em></p>-->
                                                                        <form action = "<?= Yii::app()->baseUrl; ?>/index.php/products/ProductNotify/id/<?= $product->id; ?>" method = "post" name = "notify">
                                                                                <?php if (Yii::app()->user->hasFlash('success')): ?>
                                                                                        <div class="alert alert-success">
                                                                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                                                                <?php echo Yii::app()->user->getFlash('success'); ?>
                                                                                        </div>
                                                                                <?php endif; ?>
                                                                                <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                                                        <div class="alert alert-success">
                                                                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                                                                <?php echo Yii::app()->user->getFlash('error'); ?>
                                                                                        </div>
                                                                                <?php endif; ?>
                                                                                <div class="sold_out_notify">
                                                                                        <h4>Product Out of Stock Subscription</h4>
                                                                                        <div class="input-group">
                                                                                                <?php if (isset(Yii::app()->session['user'])) { ?>
                                                                                                        <input type="email" class="form-control" required="required"  id="email" required=""  name="email" value="<?= Yii::app()->session['user']['email'] ?>">
                                                                                                        <?php
                                                                                                } else {
                                                                                                        ?>
                                                                                                        <input type="email" class="form-control" required="required" id="email" name="email" required=""  placeholder="Enter Email Address">
                                                                                                        <?php
                                                                                                }
                                                                                                ?>
                                                                                                <div class="input-group-btn"><button type="submit" class="btn-primary btn">Notify Me</button></div>
                                                                                        </div>
                                                                                        <p>(Notify me when this product is back in stock)</p>
                                                                                </div>
                                                                        </form>
                                                                        <?php
                                                                } else {

                                                                }
                                                        } else {
                                                                if ($total_stock == 0 || $out_stock == 0) {
                                                                        ?>
                                                                        <form action = "<?= Yii::app()->baseUrl; ?>/index.php/products/ProductNotify/id/<?= $product->id; ?>" method = "post" name = "notify">

                                                                                <div class="sold_out_notify">
                                                                                        <h4>Product Out of Stock Subscription</h4>
                                                                                        <div class="input-group">
                                                                                                <?php if (isset(Yii::app()->session['user'])) { ?>
                                                                                                        <input type="email" required="required" class="form-control"  id="email"  name="email" value="<?= Yii::app()->session['user']['email'] ?>">
                                                                                                        <?php
                                                                                                } else {
                                                                                                        ?>
                                                                                                        <input type="email" required="required" class="form-control" id="email" name="email"  placeholder="Enter Email Address">
                                                                                                        <?php
                                                                                                }
                                                                                                ?>
                                                                                                <div class="input-group-btn"><button type="submit" class="btn-primary btn">Notify Me</button></div>
                                                                                        </div>
                                                                                        <p>(Notify me when this product is back in stock)</p>
                                                                                </div>
                                                                        </form>
                                                                        <?php
                                                                }
                                                        }
                                                }
                                        }
                                        ?>
                                        <?php if ($product->video != '') { ?>
                                                <div class="project_video">
                                                        <h3>Watch Video</h3>
                                                        <div class="video_thumb">

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--<video src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder ?>/<?= $product->id ?>/videos/video.<?= $product->video ?>" >-->
                                                                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/video_thumb.jpg" alt=""/>
                                                                <a class="video_link laksyah_video fancybox.iframe" href="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder ?>/<?= $product->id ?>/videos/video.<?= $product->video ?>"><i class="fa fa-play-circle-o"></i></a>
                                                        </div>
                                                </div>
                                        <?php }
                                        ?>
                                        <?php
                                        $product_option = MasterOptions::model()->findByAttributes(['product_id' => $product->id]);
                                        if (!empty($product_option)) {
                                                ?>
                                                <div class="option_errors">

                                                </div>
                                                <input type="hidden" value="<?php echo $product_option->id; ?>" name="master_option" id="master_option"/>
                                                <input type="hidden" value="<?php echo $product_option->option_type_id; ?>" name="option_type" id="option_type"/>
                                                <?php
                                                if ($product_option->option_type_id == 1 || $product_option->option_type_id == 3) {
                                                        $colors = OptionDetails::model()->findAllByAttributes(['master_option_id' => $product_option->id], ['group' => 'color_id', 'order' => 'color_id']);
                                                        ?>

                                                        <input type="hidden" value="" name="option_color" id="option_color"/>

                                                        <div class = "color_picker">
                                                                <?php if ($product->enquiry_sale == 1) { ?>
                                                                        <h3>Select Color</h3>
                                                                        <?php
                                                                } else {
                                                                        ?>
                                                                        <h3>Select Color</h3>
                                                                        <?php
                                                                }
                                                                ?>
                                                                <?php
                                                                if (!empty($colors)) {
                                                                        ?>
                                                                        <ul class = "product_colors">
                                                                                <?php
                                                                                $total_stock_count = 0;
                                                                                foreach ($colors as $color) {

                                                                                        $color_name = OptionCategory::model()->findByPk($color->color_id);
                                                                                        $color_countings = OptionDetails::model()->findAllByAttributes(array('color_id' => $color->color_id, 'product_id' => $product->id));

                                                                                        foreach ($color_countings as $color_counting) {
                                                                                                $total_stock_count = $total_stock_count + $color_counting->stock;
                                                                                        }
//echo $total_stock;
//                                                                                        if ($color->stock > 0 && $color->status != 0) {
//                                                                                        echo $product->id;
//                                                                                        echo $color->color_id;
// echo $total_stock;
                                                                                        if ($total_stock_count > 0) {
                                                                                                $disabled1 = '';
                                                                                        } else {
                                                                                                $disabled1 = 'disabled';
                                                                                        }
                                                                                        ?>
                                                                                        <li class = "<?php echo $disabled1; ?>" option_id="<?php echo $product_option->id; ?>" product="<?php echo $product->id; ?>" color="<?php echo $color->color_id; ?>"> <a class = "#" style = "<?php
                                                                                                if ($color_name->color_code == 'PRINT') {
                                                                                                        echo 'background-image: url(https://laksyah.com/images/print-color.jpg)';
                                                                                                } else if ($color_name->color_code == 'BLUEORANGE') {
                                                                                                        echo 'background-image: url(https://laksyah.com/images/blue-orange.jpg)';
                                                                                                } else if ($color_name->color_code == 'BLUEBLACK') {
                                                                                                        echo 'background-image: url(https://laksyah.com/images/blue-black.jpg)';
                                                                                                } else {
                                                                                                        echo 'background-color:' . $color_name->color_code;
                                                                                                }
                                                                                                ?>;" title="<?php echo $color_name->color_name; ?>"></a> </li>

                                                                                        <?php
                                                                                        $total_stock_count = 0;
                                                                                }
                                                                                ?> </ul>
                                                                <?php }
                                                                ?>
                                                        </div>
                                                <?php } ?>
                                                <!--/ Color_picker-->

                                                <?php
                                                if ($product_option->option_type_id == 2 || $product_option->option_type_id == 3) {
                                                        $sizes = OptionCategory::model()->findAll(['condition' => 'option_type_id=2', 'order' => 'field1 asc']);
                                                        ?>
                                                        <input type="hidden" value="" name="option_size" id="option_size"/>
                                                        <div class = "product_size size_filter">
                                                                <h3>Select Size&nbsp;&nbsp;<span><a href = "#" data-toggle = "modal" data-target = "#sizechartModal" class="sgde" style="color: #f47721 ; font-size: 12px;">VIEW SIZE GUIDE</a>&nbsp; <?php
                                                                                if ($product->sizechartforwhat == 'Blouse') {
                                                                                        echo "(" . $product->sizechartforwhat . ")";
                                                                                }
                                                                                ?> </span></h3>
                                                                <?php
                                                                if (!empty($sizes)) {
                                                                        ?>
                                                                        <div class = "size_selector">
                                                                                <?php
                                                                                foreach ($sizes as $size) {

                                                                                        $productoption = OptionDetails::model()->findByAttributes(array('size_id' => $size->id, 'master_option_id' => $product_option->id));

                                                                                        if ($product_option->option_type_id == 3) {

                                                                                                $disabled = 'disabled';
                                                                                        } else {

                                                                                                $color_countings = OptionDetails::model()->findAllByAttributes(array('size_id' => $size->id, 'product_id' => $product->id, 'master_option_id' => $product_option->id));
                                                                                                foreach ($color_countings as $color_counting) {
                                                                                                        $total_stock_count += $color_counting->stock;
                                                                                                }
//                                                                                        if ($color->stock > 0 && $color->status != 0) {
                                                                                                if ($total_stock_count > 0) {
                                                                                                        $disabled = '';
//                                                                                                        if ($size->id == $productoption->size_id) {
//                                                                                                                $disabled = '';
//                                                                                                        } else {
//                                                                                                                $disabled = 'disabled';
//                                                                                                        }
                                                                                                } else {
                                                                                                        $disabled1 = 'disabled';
                                                                                                }
                                                                                        }
                                                                                        ?>
                                                                                        <label class="<?php echo $disabled; ?>" id="<?php echo $size->id; ?>"><?php echo $size->size; ?>
                                                                                                <input type = "radio" name = "size_selector_<?php echo $size->id; ?>" value = "<?php echo $size->id; ?>" id = "size_selector_<?php echo $size->id; ?>">
                                                                                        </label>
                                                                                        <?php
                                                                                }
                                                                                ?>

                                                                        </div>
                                                                        <?php
                                                                }
                                                                ?>
                                                        </div>
                                                        <?php
                                                }
                                                ?>

                                                <?php
                                        }
                                        ?>

                                        <!--/ Size Chart-->

                                        <!--/ Shipping_ifo-->



                                        <script>
                                                $(document).ready(function() {
<?php
if ($model->hasErrors()) {
        if ($model->enquiry_type == 1) {
                ?>
                                                                        $("#myModal").modal('show');
        <?php } else { ?>
                                                                        $("#normalenquiry").modal('show');
                <?php
        }
}
?>
                                                });</script>

                                        <script>
                                                $(document).ready(function() {
<?php if (Yii::app()->user->hasFlash('enuirysuccess')) { ?>
                                                                $("#myModal").modal('show');
                                                                $("#myModal .modal-body").html('<h4 style="line-height: 25px;text-align:center;">We have recieved your enquiry and will respond to you <br/>via email as soon as possible.</h4><h4 style="line-height: 25px;text-align:center;"> For urgent enquiry please call us directly on +91 9142202222 <br/>(Mon to Sat  9.30AM-6.30PM(IST))</h4><h5 style="font-size:11px;line-height: 25px;color: #e4702d;text-align:center;text-transform:uppercase;border:1px solid #e4702d;">IN CASE IF YOU DID NOT RECEIVE OUR EMAIL, KINDLY CHECK YOUR JUNK/SPAM FOLDER.</h5>');
<?php } ?>



<?php if (Yii::app()->user->hasFlash('enuirysuccess1')) { ?>
                                                                $("#normalenquiry").modal('show');
                                                                $("#normalenquiry .modal-body").html('<h4 style="line-height: 25px;text-align:center;">We have recieved your enquiry and will respond to you <br/>via email as soon as possible.</h4><h4 style="line-height: 25px;text-align:center;"> For urgent enquiry please call us directly on +91 9142202222 <br/>(Mon to Sat  9.30AM-6.30PM(IST))</h4><h5 style="font-size:11px;line-height: 25px;color: #e4702d;text-align:center;text-transform:uppercase;border:1px solid #e4702d;">IN CASE IF YOU DID NOT RECEIVE OUR EMAIL, KINDLY CHECK YOUR JUNK/SPAM FOLDER.</h5>');
<?php } ?>




                                                });</script>
                                        <div class="modal fade" id="sizechartModal" tabindex="-1" role="dialog">
                                                <div class="modal-dialog">
                                                        <div class="modal-content">
                                                                <div class="modal-header text-center">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        <!-- <h2 class="modal-title">Size Chart</h2>-->
                                                                </div>
                                                                <div class="modal-body text-center">

                                                                        <a href="" data-toggle = "modal" data-target = "#sizechartModal"><img src="<?= Yii::app()->request->baseUrl; ?>/images/sample.jpg" alt=""/> </a>     </div>
                                                        </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->


                                        <div class="modal fade"  id="enquirymail" tabindex="-1" role="dialog">
                                                <div class="modal-dialog">
                                                        <div class="modal-content">
                                                                <div class="modal-header text-center">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        <h4 class="modal-title">Email to a Friend</h4>
                                                                </div>
                                                                <br />
                                                                <div class="modal-body text-center">
                                                                        <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Products/mailfn">
                                                                                <div class="row">
                                                                                        <div class="col-sm-4 validating">
                                                                                                <input placeholder="From" required="required" size="60" maxlength="225" class="form-control"  name="from" id="ProductEnquiry_product_id" type="text" >
                                                                                        </div>
                                                                                        <div class="col-sm-4 validating">
                                                                                                <input placeholder="Recipient Email" required="required" size="60" maxlength="225" class="form-control"  name="email" id="ProductEnquiry_product_id" type="text" >
                                                                                        </div>
                                                                                        <div class="col-sm-4 validating">
                                                                                                <button type="submit" class="btn btn-primary">SEND MAIL</button>
                                                                                        </div>
                                                                                </div>
                                                                        </form>
                                                                </div>

                                                        </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                        <!-- Return Policy Modal starts-->
                                        <div class="modal fade"  id="returns" tabindex="-1" role="dialog">
                                                <div class="modal-dialog">
                                                        <div class="modal-content" id="return_policy">

                                                        </div>
                                                </div>
                                        </div>
                                        <!-- Return Policy Modal ends-->
                                        <?php
                                        if (isset(Yii::app()->session['user'])) {
                                                $user_info = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                                                $model->name = $user_info->first_name . ' ' . $user_info->last_name;
                                                $model->country = $user_info->country;
                                                $model->phonecode = $user_info->phonecode;
                                                $model->email = $user_info->email;
                                                $model->phone = $user_info->phone_no_2;
                                                // echo $user_info->first_name;
                                        }
                                        ?>





                                        <div class="modal fade" id="myModal" tabindex="-2" role="dialog">
                                                <div class="modal-dialog">
                                                        <div class="modal-content">
                                                                <div class="modal-header text-left">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        <img class="wmimg1" src="<?php echo yii::app()->baseUrl; ?>/images/watermark-logo.png">

                                                                        <?php if ((Yii::app()->user->hasFlash('enuirysuccess')) || ($product->enquiry_sale != 1)) { ?>
                                                                                <h4>Thank You For Your Interest In This Product!</h4><br/>

                                                                                <?php
                                                                        }
                                                                        ?>


                                                                </div>
                                                                <div class="modal-body">
                                                                        <h3>Please fill out the form below and we will get back to you with price, availability and estimate of when we would be able to deliver it to you.</h3>
                                                                        <h3 class="btombrn" style="font-size: 11px;">Your personal information will be kept confidential and will not be shared with any other third party.</h3>

                                                                        <?php if (Yii::app()->user->hasFlash('enuirysuccess')): ?>
                                                                                <div class="alert alert-success">
                                                                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                                                        <?php echo Yii::app()->user->getFlash('enuirysuccess'); ?>
                                                                                </div>
                                                                        <?php endif; ?>
                                                                        <div class="form">

                                                                                <?php
                                                                                $form = $this->beginWidget('CActiveForm', array(
                                                                                    'action' => Yii::app()->request->baseUrl . '/index.php/Products/Detail/name/' . $product->canonical_name,
                                                                                    'id' => 'product-enquiry-form',
                                                                                    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
                                                                                        // Please note: When you enable ajax validation, make sure the corresponding
                                                                                        // controller action is handling ajax validation correctly.
                                                                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                                                                        // See class documentation of CActiveForm for details on this.
                                                                                        // 'enableAjaxValidation' => true,
                                                                                ));
                                                                                ?>


                                                                                <div class="row">
                                                                                        <div class="col-xs-12"><h4 class="color_error" style="color:red"></h4></div>
                                                                                        <div class="col-sm-5 padd_ing_r0">

                                                                                                <?php echo $form->hiddenField($model, 'product_id', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'value' => $product->id)); ?>
                                                                                                <?php echo $form->hiddenField($model, 'color', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control enq_color')); ?>

                                                                                                <?php echo $form->labelEx($model, 'name'); ?>
                                                                                                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'placeholder' => 'Name')); ?>
                                                                                                <span style="color:red;"> <?php echo $form->error($model, 'name'); ?></span>
                                                                                        </div>
                                                                                        <div class="col-sm-7">
                                                                                                <?php echo $form->labelEx($model, 'country'); ?>
                                                                                                <?php echo CHtml::activeDropDownList($model, 'country', CHtml::listData(Countries::model()->findAll(), 'id', 'country_name'), array('empty' => '--Select Country--', 'class' => 'form-control')); ?>

                                                                                                <span style="color:red;"> <?php echo $form->error($model, 'country'); ?></span></div>
                                                                                </div>

                                                                                <div class="row">
                                                                                        <div class="col-sm-5 padd_ing_r0">
                                                                                                <?php echo $form->labelEx($model, 'email'); ?>
                                                                                                <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control hello_form', 'placeholder' => 'Email')); ?>
                                                                                                <span style="color:red;">  <?php echo $form->error($model, 'email'); ?></span>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                                <?php echo $form->labelEx($model, 'Code'); ?>
                                                                                                <?php //echo Yii::app()->session['user']['country'];   ?>                                                                        <?php
                                                                                                $phonecode = Countries::model()->findByPk(Yii::app()->session['user']['country'])->phonecode;
                                                                                                $state_options = array();

                                                                                                $states = Countries::model()->findAll();
                                                                                                if (!empty($states)) {
                                                                                                        $state_options[""] = "--Select--";
                                                                                                        foreach ($states as $state) {
                                                                                                                $state_options[$state->phonecode] = '+' . $state->phonecode;
                                                                                                        }
                                                                                                } else {
                                                                                                        $state_options[""] = "--Code--";
                                                                                                        $state_options[0] = "Other";
                                                                                                }
                                                                                                ?>
                                                                                                <?php echo CHtml::activeDropDownList($model, 'phonecode', $state_options, array('class' => 'form-control', 'options' => array($phonecode => array('selected' => 'selected')))); ?>
                                                                                                <?php echo $form->error($model, 'phonecode'); ?>




                                                                                        </div>



                                                                                        <div class="col-sm-4">
                                                                                                <?php echo $form->labelEx($model, 'phone'); ?>
                                                                                                <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'placeholder' => 'Phone Number')); ?>

                                                                                                <span style="color:red;">  <?php echo $form->error($model, 'phone'); ?></span>
                                                                                        </div>

                                                                                </div>


                                                                                <div class="row">
                                                                                        <!--  <div class="col-sm-6">
                                                                                        <?php echo $form->labelEx($model, 'size'); ?>
                                                                                        <?php echo CHtml::activeDropDownList($model, 'size', CHtml::listData(MasterSize::model()->findAll(), 'id', 'size'), array('empty' => '--Select Size--', 'class' => 'form-control')); ?>
                                                                                        <?php echo $form->error($model, 'size'); ?>
                                                                                          </div>-->
                                                                                        <div class="col-sm-6">
                                                                                                <?php echo $form->labelEx($model, 'verifyCode *'); ?><br /><div class="dsl">
                                                                                                <?php $this->widget("CCaptcha", array('buttonLabel' => '<i class="fa fa-refresh" aria-hidden="true"></i>', 'buttonOptions' => array('style' => 'padding-top: 18px;position: absolute; right: 13px;'))); ?>

                                                                                                        <?php echo $form->textField($model, 'verifyCode', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control codez', 'placeholder' => 'Type the captcha shown above', 'autocomplete' => 'off')); ?>
                                                                                                </div>

                                                                                                <span style="color:red;">  <?php echo $form->error($model, 'verifyCode'); ?></span>
                                                                                        </div>


                                                                                        <div class="col-sm-6">
                                                                                                <div class="" style="text-align: center;margin-top:35px;">
                                                                                                        <?php echo CHtml::submitButton($model->isNewRecord ? 'GET QUOTE' : 'Save', array('class' => 'btn btn-primary enq_submit_btn', 'style' => 'width: 70%;')); ?>
                                                                                                        <?php //echo CHtml::resetButton($model->isNewRecord ? 'Reset' : 'Save', array('class' => 'btn btn-default'));             ?>

                                                                                                </div>


                                                                                                <!-- <div class="col-sm-6">
                                                                                                <?php echo $form->labelEx($model, 'Delivery Date'); ?>

                                                                                                <?php
                                                                                                $from = date('Y');
                                                                                                $to = date('Y') + 2;
                                                                                                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                                                    'model' => $model,
                                                                                                    'attribute' => 'delivery_date',
                                                                                                    'value' => 'delivery_date',
                                                                                                    'options' => array(
                                                                                                        'dateFormat' => 'dd-mm-yy',
                                                                                                        'changeYear' => true, // can change year
                                                                                                        'changeMonth' => true, // can change month
                                                                                                        'yearRange' => $from . ':' . $to, // range of year
                                                                                                        'showButtonPanel' => true, // show button panel
                                                                                                        'minDate' => '0',
                                                                                                    ),
                                                                                                    'htmlOptions' => array(
                                                                                                        'size' => '10', // textField size
                                                                                                        'maxlength' => '10', // textField maxlength
                                                                                                        'class' => 'form-control',
                                                                                                        'placeholder' => 'Delivery Date',
                                                                                                    ),
                                                                                                ));
                                                                                                ?>


                                                                                               </div>-->

                                                                                        </div
                                                                                        <br/>
                                                                                        <!--                                                                                <div class="row">

                                                                                                                                                                                <div class="col-sm-12">
                                                                                        <?php //echo $form->labelEx($model, 'requirement');          ?>
                                                                                        <?php //echo $form->textArea($model, 'requirement', array('maxlength' => 300, 'class' => 'form-control', 'placeholder' => 'Requirement'));    ?>
                                                                                        <?php //echo $form->error($model, 'requirement');      ?></div>
                                                                                                                                                                        </div>-->

                                                                                        <?php
//echo $this->widget('application.user.extensions.captcha.CaptchaExtendedAction', array(
//'model' => $model,
// 'attribute' => 'description',
// ));
                                                                                        ?>
                                                                                        <div class="row">
                                                                                                <!--  <div class="col-sm-6">
                                                                                                <?php echo $form->labelEx($model, 'verifyCode'); ?><br />
                                                                                                <?php $this->widget("CCaptcha", array('buttonLabel' => 'change one for me', 'buttonOptions' => array('style' => 'margin-bottom:20px;'))); ?>

                                                                                                <?php echo $form->textField($model, 'verifyCode', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'placeholder' => 'Type the captcha shown above', 'autocomplete' => 'off')); ?>

                                                                                                          <span style="color:red;">  <?php echo $form->error($model, 'verifyCode'); ?></span>
                                                                                                  </div>-->
                                                                                        </div>
                                                                                        <!--  <div class="row">
                                                                                                  <div class="col-sm-12">
                                                                                                          <div class="modal-footer" style="text-align: center;">
                                                                                        <?php echo CHtml::submitButton($model->isNewRecord ? 'GET QUOTE' : 'Save', array('class' => 'btn btn-primary ', 'style' => 'width: 70%;')); ?>
                                                                                        <?php //echo CHtml::resetButton($model->isNewRecord ? 'Reset' : 'Save', array('class' => 'btn btn-default'));             ?>

                                                                                                          </div>
                                                                                                  </div>-->
                                                                                        <p class="terms_link" style="margin-top:54px;">View Laksyah.com's <?php echo CHtml::link('Terms', array('site/Terms')); ?> &amp; <?php echo CHtml::link('Policies', array('site/PrivacyPolicy')); ?></p>
                                                                                </div>
                                                                                <?php $this->endWidget(); ?>
                                                                        </div><!-- form -->

                                                                </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                        </div>

                                        <div class="modal fade" id="normalenquiry" tabindex="-2" role="dialog">
                                                <div class="modal-dialog">
                                                        <div class="modal-content">
                                                                <div class="modal-header text-left">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        <img class="wmimg1" src="<?php echo yii::app()->baseUrl; ?>/images/watermark-logo.png">
                                                                        <!--<h4>Thank You For Your Interest In This Product!</h4><br/>-->

                                                                </div>
                                                                <div class="modal-body">
                                                                        <h3>Please provide as much information as possible for us to help you with your enquiry.</h3>
                                                                        <h3 class="btombrn" style="font-size: 11px;">Your personal information will be kept confidential and will not be shared with any other third party.</h3>


                                                                        <?php if (Yii::app()->user->hasFlash('enuirysuccess')): ?>
                                                                                <div class="alert alert-success">
                                                                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                                                        <?php echo Yii::app()->user->getFlash('enuirysuccess'); ?>
                                                                                </div>
                                                                        <?php endif; ?>
                                                                        <div class="form">

                                                                                <?php
                                                                                $form = $this->beginWidget('CActiveForm', array(
                                                                                    'action' => Yii::app()->request->baseUrl . '/index.php/Products/Detail/name/' . $product->canonical_name,
                                                                                    'id' => 'product-enquiry-form',
                                                                                    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
                                                                                        // Please note: When you enable ajax validation, make sure the corresponding
                                                                                        // controller action is handling ajax validation correctly.
                                                                                        // There is a call to performAjaxValidation() commented in generated controller code.
                                                                                        // See class documentation of CActiveForm for details on this.
                                                                                        // 'enableAjaxValidation' => true,
                                                                                ));
                                                                                ?>


                                                                                <div class="row">
                                                                                        <div class="col-sm-5 padd_ing_r0">

                                                                                                <?php echo $form->hiddenField($model, 'product_id', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'value' => $product->id)); ?>
                                                                                                <?php echo $form->labelEx($model, 'name'); ?>
                                                                                                <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'placeholder' => 'Name')); ?>
                                                                                                <span style="color:red;"> <?php echo $form->error($model, 'name'); ?></span>
                                                                                        </div>

                                                                                        <div class="col-sm-7">
                                                                                                <?php echo $form->labelEx($model, 'country'); ?>
                                                                                                <?php echo CHtml::activeDropDownList($model, 'country', CHtml::listData(Countries::model()->findAll(), 'id', 'country_name'), array('empty' => '--Select Country--', 'class' => 'form-control enqc', 'id' => 'enqc')); ?>

                                                                                                <span style="color:red;"> <?php echo $form->error($model, 'country'); ?></span></div>
                                                                                </div>

                                                                                <div class="row">
                                                                                        <div class="col-sm-5 padd_ing_r0">
                                                                                                <?php echo $form->labelEx($model, 'email'); ?>
                                                                                                <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control hello_form', 'placeholder' => 'Email')); ?>
                                                                                                <span style="color:red;">  <?php echo $form->error($model, 'email'); ?></span>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                                <?php echo $form->labelEx($model, 'Code'); ?>
                                                                                                <?php //echo Yii::app()->session['user']['country'];   ?>                                                                        <?php
                                                                                                $phonecode = Countries::model()->findByPk(Yii::app()->session['user']['country'])->phonecode;
                                                                                                $state_options = array();

                                                                                                $states = Countries::model()->findAll();
                                                                                                if (!empty($states)) {
                                                                                                        $state_options[""] = "--Select--";
                                                                                                        foreach ($states as $state) {
                                                                                                                $state_options[$state->phonecode] = '+' . $state->phonecode;
                                                                                                        }
                                                                                                } else {
                                                                                                        $state_options[""] = "--Code--";
                                                                                                        $state_options[0] = "Other";
                                                                                                }
                                                                                                ?>
                                                                                                <?php echo CHtml::activeDropDownList($model, 'phonecode', $state_options, array('class' => 'form-control enqp', 'id' => 'enqp', 'options' => array($phonecode => array('selected' => 'selected')))); ?>
                                                                                                <?php echo $form->error($model, 'phonecode'); ?>




                                                                                        </div>



                                                                                        <div class="col-sm-4">
                                                                                                <?php echo $form->labelEx($model, 'phone'); ?>
                                                                                                <?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'placeholder' => 'Phone Number')); ?>

                                                                                                <span style="color:red;">  <?php echo $form->error($model, 'phone'); ?></span>
                                                                                        </div>



                                                                                </div>


                                                                                <div class="row">


                                                                                </div>
                                                                                <div class="row">
                                                                                        <input type="hidden" name="equ" value="equ">
                                                                                        <div class="col-sm-12">
                                                                                                <?php echo $form->labelEx($model, 'Enquiry'); ?>
                                                                                                <?php echo $form->textArea($model, 'requirement', array('maxlength' => 300, 'class' => 'form-control', 'placeholder' => 'Enquiry')); ?>
                                                                                                <?php echo $form->error($model, 'requirement'); ?></div>
                                                                                </div>

                                                                                <?php
//echo $this->widget('application.user.extensions.captcha.CaptchaExtendedAction', array(
//'model' => $model,
// 'attribute' => 'description',
// ));
                                                                                ?>
                                                                                <div class="row">

                                                                                        <!--<div class="col-sm-6">
                                                                                        <?php echo $form->labelEx($model, 'size'); ?>
                                                                                        <?php echo CHtml::activeDropDownList($model, 'size', CHtml::listData(MasterSize::model()->findAll(), 'id', 'size'), array('empty' => '--Select Size--', 'class' => 'form-control')); ?>
                                                                                        <?php echo $form->error($model, 'size'); ?>
                                                                                                                                                                               </div>-->


                                                                                        <div class="col-sm-6">
                                                                                                <?php echo $form->labelEx($model, 'verifyCode *'); ?><br /><div class="dsl">
                                                                                                <?php $this->widget("CCaptcha", array('buttonLabel' => '<i class="fa fa-refresh" aria-hidden="true"></i>', 'buttonOptions' => array('style' => 'padding-top: 18px;position: absolute; right: 13px;'))); ?>

                                                                                                        <?php echo $form->textField($model, 'verifyCode', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control codez', 'placeholder' => 'Type the captcha shown above', 'autocomplete' => 'off')); ?>
                                                                                                </div>

                                                                                                <span style="color:red;">  <?php echo $form->error($model, 'verifyCode'); ?></span>
                                                                                        </div>

                                                                                        <div class="col-sm-6" style="padding-top: 35px;text-align: center;">

                                                                                                <?php echo CHtml::submitButton($model->isNewRecord ? 'SUBMIT' : 'Save', array('class' => 'btn btn-primary', 'style' => 'width: 70%;')); ?>
                                                                                                <?php //echo CHtml::resetButton($model->isNewRecord ? 'Reset' : 'Save', array('class' => 'btn btn-default'));               ?>


                                                                                        </div>





                                                                                        <!--<div class="col-sm-6">
                                                                                        <?php echo $form->labelEx($model, 'verifyCode'); ?><br />
                                                                                        <?php $this->widget("CCaptcha", array('buttonLabel' => 'change one for me', 'buttonOptions' => array('style' => 'margin-bottom:20px;'))); ?>

                                                                                        <?php echo $form->textField($model, 'verifyCode', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control', 'placeholder' => 'Type the captcha shown above', 'autocomplete' => 'off')); ?>

                                                                                                <span style="color:red;">  <?php echo $form->error($model, 'verifyCode'); ?></span>
                                                                                        </div>-->
                                                                                        <!-- <div class=""col-sm-6" style="padding-top: 99px;text-align: center;">

                                                                                        <?php echo CHtml::submitButton($model->isNewRecord ? 'SUBMIT' : 'Save', array('class' => 'btn btn-primary', 'style' => 'width: 70%;')); ?>
                                                                                        <?php //echo CHtml::resetButton($model->isNewRecord ? 'Reset' : 'Save', array('class' => 'btn btn-default'));               ?>


                                                                                 </div>-->
                                                                                </div>
                                                                                <p class="terms_link" style="margin-top:18px;">View Laksyah.com's <?php echo CHtml::link('Terms', array('site/Terms')); ?> &amp; <?php echo CHtml::link('Policies', array('site/PrivacyPolicy')); ?></p>

                                                                                <?php $this->endWidget(); ?>
                                                                        </div><!-- form -->

                                                                </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                        </div>
                                        <!-- /.modal-->
                                        <!--            </div>-->




                                        <?php
//check wheather sale or enquiry,   1- Normal Product, 2- Celibrity Products//

                                        if ($product->enquiry_sale == 1) {

                                                //instock//

                                                if ($product->stock_availability == 1) {
                                                        if (empty($option_exists)) {
                                                                $total_stock = $product->quantity;


                                                                if ($total_stock >= 1) {
                                                                        ?>

                                                                        <?php if ($product->quantity <= 2) { ?>
                                                                                <div class="product_quantity">
                                                                                        <h3>Quantity</h3>
                                                                                        <div class="qntyerror"></div>
                                                                                        <div class="qunatity">
                                                                                                <select class="qty" >
                                                                                                        <?php
                                                                                                        for ($i = 1; $i <= $product->quantity; $i++) {
                                                                                                                ?>
                                                                                                                <option value="<?= $i; ?>"><?= $i; ?></option>
                                                                                                        <?php } ?>

                                                                                                </select>
                                                                                        </div>
                                                                                </div>
                                                                        <?php } else { ?>

                                                                                <div class="product_quantity">
                                                                                        <h3>Quantity</h3>
                                                                                        <div class="qntyerror"></div>
                                                                                        <div class="qunatity">
                                                                                                <select class="qty" >
                                                                                                        <option value="1">1</option>
                                                                                                        <option value="2">2</option>
                                                                                                        <option value="3">3</option>

                                                                                                </select>
                                                                                        </div>
                                                                                </div>
                                                                        <?php } ?>
                                                                        <!-- / Quantity-->
                                                                        <div class="shipping_info">
                                                                                <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                                <h4><a ><i class="fa fa-globe"></i> <span>We Ship Worldwide</span></a></h4>
                                                                                        </div>
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                                <h4><a ><i class="fa fa-truck"></i> <span>Free Shipping In India</span></a></h4>
                                                                                        </div>
                                                                                </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<p><a class="return_policies" style="cursor: pointer;" >View Shipping and Return Policies</a></p>-->


                                                                        </div>
                                                                        <!-- / Shipping_ifo-->
                                                                        <div class="product_button_group">
                                                                                <div class="row">
                                                                                        <div>
                                                                                                <?php if (Yii::app()->user->hasFlash('success')): ?>
                                                                                                        <div class="alert alert-success mesage">
                                                                                                                <?php echo Yii::app()->user->getFlash('success'); ?>
                                                                                                        </div>
                                                                                                <?php endif; ?>
                                                                                                <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                                                                        <div class="alert alert-danger mesage msg2">
                                                                                                                <?php echo Yii::app()->user->getFlash('error'); ?>
                                                                                                        </div>
                                                                                                <?php endif; ?>
                                                                                        </div>
                                                                                        <div class="col-md-7 col-xs-7">
                                                                                                <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $product->id ?>" class="add_to_wishlist btn btn-skel " ><i class="fa fa-heart-o"></i> ADD TO WISH LIST</a>

                                                                                        </div>
                                                                                        <div class="col-md-5 col-xs-5">
                                                                                                <button type="button" class="btn btn-skel" data-toggle="modal" data-target="#normalenquiry"><i class="fa fa-envelope-o"></i> ENQUIRE NOW</button>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="row">

                                                                                        <div class="col-xs-12">
                                                                                                <button class="btn-primary  add_to_cart" id="<?= $product->id; ?>"><strong><i class="fa fa-shopping-bag"></i> &nbsp;ADD TO BAG</strong></button>
                                                                                                <input type = "hidden" id = "opt_id" name = "opt">
                                                                                                <input type = "hidden" value = "<?= $product->canonical_name; ?>" id="cano_name_<?= $product->id; ?>" name="cano_name">
                                                                                        </div>
                                                                                        <!--                                                                                <div class="col-md-5 col-xs-5">
                                                                                                                                                                                <button type="button" class="btn-primary buy_now" id="<?= $product->id; ?>" ><i class="fa fa-envelope"></i> BUY NOW</button>
                                                                                                                                                                        </div>-->
                                                                                </div>
                                                                        </div>




                                                                        <?php
                                                                } else {
                                                                        ?>
                                                                        <div class="shipping_info">
                                                                                <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                                <h4><a><i class="fa fa-globe"></i> <span>We Ship Worldwide</span></a></h4>
                                                                                        </div>
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                                <h4><a ><i class="fa fa-truck"></i> <span>Free Shipping In India</span></a></h4>
                                                                                        </div>
                                                                                </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<p><a class="return_policies"  style="cursor: pointer;">View Shipping and Return Policies</a></p>-->


                                                                        </div>
                                                                        <!--                                                                        <div class="product_button_group">

                                                                                                                                                        <div class="row">
                                                                                                                                                                <div class="col-md-7 col-xs-7">
                                                                                                                                                                        <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $product->id ?>" class="add_to_wishlist btn btn-skel "><i class="fa fa-heart"></i> ADD TO WISH LIST</a>

                                                                                                                                                                </div>
                                                                                                                                                                <div class="col-md-5 col-xs-5">
                                                                                                                                                                        <button type="button" class="btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i> ENQUIRE NOW</button>
                                                                                                                                                                </div>
                                                                                                                                                        </div>



                                                                                                                                                </div>-->

                                                                        <?php
                                                                }
                                                        } else {

                                                                if ($total_stock >= 1) {
                                                                        ?>

                                                                        <?php if ($total_stock <= 2) { ?>
                                                                                <div class="product_quantity">
                                                                                        <h3>Quantity</h3>
                                                                                        <div class="qntyerror"></div>
                                                                                        <div class="qunatity">
                                                                                                <select class="qty" >
                                                                                                        <?php
                                                                                                        for ($i = 1; $i <= $total_stock; $i++) {
                                                                                                                ?>
                                                                                                                <option value="<?= $i; ?>"><?= $i; ?></option>
                                                                                                        <?php } ?>

                                                                                                </select>
                                                                                        </div>
                                                                                </div>
                                                                        <?php } else { ?>

                                                                                <div class="product_quantity">
                                                                                        <h3>Quantity</h3>
                                                                                        <div class="qntyerror" ></div>
                                                                                        <div class="qunatity">
                                                                                                <select class="qty" >
                                                                                                        <option value="1">1</option>
                                                                                                        <option value="2">2</option>
                                                                                                        <option value="3">3</option>

                                                                                                </select>
                                                                                        </div>
                                                                                </div>
                                                                        <?php } ?>
                                                                        <!-- / Quantity-->
                                                                        <div class="shipping_info">
                                                                                <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                                <h4><a><i class="fa fa-globe"></i> <span>We Ship Worldwide</span></a></h4>
                                                                                        </div>
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                                <h4><a ><i class="fa fa-truck"></i> <span>Free Shipping In India</span></a></h4>
                                                                                        </div>
                                                                                </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<p><a class="return_policies"  style="cursor: pointer;">View Shipping and Return Policies</a></p>-->


                                                                        </div>
                                                                        <!-- / Shipping_ifo-->
                                                                        <div class="product_button_group">
                                                                                <div class="row">
                                                                                        <div>
                                                                                                <?php if (Yii::app()->user->hasFlash('success')): ?>
                                                                                                        <div class="alert alert-success mesage">
                                                                                                                <?php echo Yii::app()->user->getFlash('success'); ?>
                                                                                                        </div>
                                                                                                <?php endif; ?>
                                                                                                <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                                                                        <div class="alert alert-danger mesage msg2">
                                                                                                                <?php echo Yii::app()->user->getFlash('error'); ?>
                                                                                                        </div>
                                                                                                <?php endif; ?>
                                                                                        </div>
                                                                                        <div class="col-md-7 col-xs-7">
                                                                                                <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $product->id ?>" class="add_to_wishlist btn btn-skel "><i class="fa fa-heart-o"></i> ADD TO WISH LIST</a>

                                                                                        </div>
                                                                                        <div class="col-md-5 col-xs-5">
                                                                                                <button type="button" class="btn btn-skel" data-toggle="modal" data-target="#normalenquiry"><i class="fa fa-envelope-o"></i> ENQUIRE NOW</button>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="row">

                                                                                        <div class="col-xs-12">
                                                                                                <button class="btn-primary  add_to_cart" id="<?= $product->id; ?>"><strong><i class="fa fa-shopping-bag"></i> &nbsp;ADD TO BAG</strong></button>
                                                                                                <input type = "hidden" id = "opt_id" name = "opt">
                                                                                                <input type = "hidden" value = "<?= $product->canonical_name; ?>" id="cano_name_<?= $product->id; ?>" name="cano_name">
                                                                                        </div>
                                                                                        <!--                                                                                <div class="col-md-5 col-xs-5">
                                                                                                                                                                                <button type="button" class="btn-primary buy_now" id="<?= $product->id; ?>" ><i class="fa fa-envelope"></i> BUY NOW</button>
                                                                                                                                                                        </div>-->
                                                                                </div>
                                                                        </div>




                                                                <?php } else {
                                                                        ?>

                                                                        <div class="shipping_info">
                                                                                <div class="row">
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                                <h4><a><i class="fa fa-globe"></i> <span>We Ship Worldwide</span></a></h4>
                                                                                        </div>
                                                                                        <div class="col-md-6 col-xs-6">
                                                                                                <h4><a ><i class="fa fa-truck"></i> <span>Free Shipping In India</span></a></h4>
                                                                                        </div>
                                                                                </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<p><a class="return_policies"  style="cursor: pointer;">View Shipping and Return Policies</a></p>-->


                                                                        </div>

                                                                        <!--                                                                        <div class="product_button_group">

                                                                                                                                                        <div class="row">
                                                                                                                                                                <div class="col-md-7 col-xs-7">
                                                                                                                                                                        <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $product->id ?>" class="add_to_wishlist btn btn-skel "><i class="fa fa-heart"></i> ADD TO WISH LIST</a>

                                                                                                                                                                </div>
                                                                                                                                                                <div class="col-md-5 col-xs-5">
                                                                                                                                                                        <button type="button" class="btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i> ENQUIRE NOW</button>
                                                                                                                                                                </div>
                                                                                                                                                        </div>



                                                                                                                                                </div>-->

                                                                <?php }
                                                                ?>

                                                                <?php
                                                        }
                                                }
                                                //out of stock//
                                                elseif ($product->stock_availability == 0) {
                                                        ?>
                                                        <div class="shipping_info">
                                                                <div class="row">
                                                                        <div class="col-md-6 col-xs-6">
                                                                                <h4><a><i class="fa fa-globe"></i> <span>We Ship Worldwide</span></a></h4>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-6">
                                                                                <h4><a ><i class="fa fa-truck"></i> <span>Free Shipping In India</span></a></h4>
                                                                        </div>
                                                                </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--<p><a class="return_policies"  style="cursor: pointer;">View Shipping and Return Policies</a></p>-->


                                                        </div>
                                                        <!--                                                        <div class="product_button_group">

                                                                                                                        <div class="row">
                                                                                                                                <div class="col-md-7 col-xs-7">
                                                                                                                                        <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $product->id ?>" class="add_to_wishlist btn btn-skel "><i class="fa fa-heart"></i> ADD TO WISH LIST</a>

                                                                                                                                </div>
                                                                                                                                <div class="col-md-5 col-xs-5">
                                                                                                                                        <button type="button" class="btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i> ENQUIRE NOW</button>
                                                                                                                                </div>
                                                                                                                        </div>


                                                                                                                </div>-->


                                                        <?php
                                                } else {
                                                        //other checking if availanle//
                                                }
                                        }
//enquiry//
                                        else {
                                                ?>
                                                <div class="shipping_info">
                                                        <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                        <h4><a><i class="fa fa-globe"></i> <span>We Ship Worldwide</span></a></h4>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                        <h4><a ><i class="fa fa-truck"></i> <span>Free Shipping In India</span></a></h4>
                                                                </div>
                                                        </div>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--<p><a class="return_policies"  style="cursor: pointer;">View Shipping and Return Policies</a></p>-->


                                                </div>
                                                <div class="product_button_group">
                                                        <div class="row">
                                                                <div>
                                                                        <?php if (Yii::app()->user->hasFlash('success')): ?>
                                                                                <div class="alert alert-success mesage">
                                                                                        <?php echo Yii::app()->user->getFlash('success'); ?>
                                                                                </div>
                                                                        <?php endif; ?>
                                                                        <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                                                <div class="alert alert-danger mesage msg2">
                                                                                        <?php echo Yii::app()->user->getFlash('error'); ?>
                                                                                </div>
                                                                        <?php endif; ?>
                                                                </div>
                                                                <!--<div class="col-md-7 col-xs-7">
                                                                        <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $product->id ?>" class="add_to_wishlist btn btn-skel "><i class="fa fa-heart"></i> ADD TO WISH LIST</a>

                                                                </div>
                                                                <div class="col-md-5 col-xs-5">
                                                                        <button type="button" class="btn btn-skel" data-toggle="modal" data-target="#normalenquiry"><i class="fa fa-envelope-o"></i> ENQUIRE NOW</button>
                                                                </div>-->
                                                                <div class="col-md-12 col-xs-12">
                                                                        <button type="button" class="btn-primary add_to_cart enq_button" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i>  REQUEST PRICING</button>
                                                                </div>
                                                        </div>
                                                </div>

                                                <!-- Modal -->

                                                <?php
                                        }
                                        ?>

                                        <!--                                                        <div class="product_button_group">
                                                                                                        <div class="row">
                                                                                                                <div class="col-md-7 col-xs-7">
                                                                                                                        <button class="btn btn-skel"><i class="fa fa-shopping-bag"></i> ADD TO SHOPPING BAG</button>
                                                                                                                </div>
                                                                                                                <div class="col-md-5 col-xs-5">
                                                                                                                        <button class="btn btn-skel" data-toggle="modal" data-target="#enquiryModal"><i class="fa fa-envelope"></i> ENQUIRY</button>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <div class="row">
                                                                                                                <div class="col-md-7 col-xs-7">
                                                                                                                        <button class="btn btn-skel"><i class="fa fa-heart"></i> ADD TO WISH LIST</button>
                                                                                                                </div>
                                                                                                                <div class="col-md-5 col-xs-5">
                                                                                                                        <button class="btn-primary"><i class="fa fa-envelope"></i> BUY NOW</button>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                </div>-->
                                        <!--/Button Group-->
                                        <div class="product_description">
                                                <div>
                                                        <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs" role="tablist">
                                                                <li role="presentation" class="active"><a href="#description" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>
                                                                <li role="presentation"><a href="#details" aria-controls="profile" role="tab" data-toggle="tab"><?= $product->enquiry_sale == 1 ? 'Features' : 'Features'; ?></a></li>
                                                                <li role="presentation"><a href="#sizechart" aria-controls="settings" role="tab" data-toggle="tab">Payment Modes</a></li>
                                                                                                                               <!-- <?php if ($product->enquiry_sale == 1) { ?><li role="presentation"><a href="#sizechart" aria-controls="settings" role="tab" data-toggle="tab">Size Charts</a></li><?php } else { ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <li role="presentation"><a href="#sizechart1" aria-controls="settings" role="tab" data-toggle="tab">Size Chart</a></li>
                                                                <?php } ?>-->
                                                        </ul>

                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active" id="description"><?= $product->product_details; ?> </div>
                                                                <div role="tabpanel" class="tab-pane" id="details"><!-- <?php echo CHtml::encode($product->product_details); ?>--><?= $product->description; ?></div>
                                                                <div role="tabpanel" class="tab-pane" id="sizechart">
                                                                        <ul>
                                                                                <li>Credit Card</li>
                                                                                <li>Debit Card</li>
                                                                                <li>Netbanking</li>
                                                                                <li>Paypal</li>
                                                                                <li>Cash on Delivery not Available</li>
                                                                        </ul>
                                                                </div>
                                                                <!-- <?php if ($product->enquiry_sale == 1) { ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <div role="tabpanel" class="tab-pane" id="sizechart"><a href="" data-toggle = "modal" data-target = "#sizechartModal"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/sample.jpg" alt=""/></a></div> <?php } else { ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <div role="tabpanel" class="tab-pane" id="sizechart1"><a href="" data-toggle = "modal" data-target = "#sizechartModal"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/HOW-TO-MEASURE.jpg" alt=""/></a></div>
                                                                <?php } ?>-->
                                                        </div>
                                                </div>
                                                <!--<p><a class="return_policies"  style="cursor: pointer;">View Shipping and Return Policies</a></p>-->

                                        </div>
                                        <p class="terms_link">View Laksyah.com <?php echo CHtml::link('Terms', array('site/Terms')); ?> &amp; <?php echo CHtml::link('Policies', array('site/PrivacyPolicy')); ?></p>
                                </div>
                        </div>
                </div>
        </div>
        <!-- / End Product Details-->
        <!--/ Start Related Products-->
        <?php
        $HiddenProducts = explode(',', $today_deal_products->deal_products);

        if ((in_array($product->id, $HiddenProducts)) && (!empty($today_deal_products))) {

        } else {
                ?>
                <div class="relatd_products">
                        <div class="section_title">
                                <h2>You May Also Like</h2>
                        </div>
                        <div class="related_itel_lists">
                                <div class="product_list ">
                                        <div class="row related_list_slider">
                                                <?php
                                                if (!empty($recently) && $recently != '') {
                                                        foreach ($recently as $recent) {

                                                                //$product_details = Products::model()->findByPk($recent->product_id);
                                                                $product_details = Products::model()->findByPk($recent->id);
                                                                if (!empty($product_details)) {
                                                                        ?>
                                                                        <?php
                                                                        $folder1 = Yii::app()->Upload->folderName(0, 1000, $product_details->id);
                                                                        ?>
                                                                        <div class="col-sm-2">
                                                                                <div class="products_item"> <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Products/Detail/name/<?php echo $product_details->canonical_name; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder1 ?>/<?= $product_details->id ?>/medium.<?= $product_details->main_image ?>" alt=""/></a>
                                                                                        <div class="list_title">
                                                                                                <h3><?= $product_details->product_name; ?></h3>
                                                                                                <!--<h4>Saree</h4>-->
                                                                                                <?php if ($product->enquiry_sale == 1) { ?>
                                                                                                        <p><?= Yii::app()->Currency->convert($product_details->price); ?></p>
                                                                                                <?php }
                                                                                                ?>



                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                        <?php
                                                                }
                                                        }
                                                }
                                                ?>
                                        </div>
                                </div>
                        </div>
                        <!--                <div class="section_title">
                                <h2>Related Products</h2>
                        </div>
                        <div class="related_itel_lists">
                                <div class="product_list ">
                                        <div class="row related_list_slider">

                        <?php
                        if (!empty($related_products) && $related_products != '') {
                                foreach ($related_products as $related_product) {

                                        $product_details1 = Products::model()->findByPk($related_product);
                                        if (!empty($product_details1)) {
                                                ?>
                                                <?php
                                                $folder2 = Yii::app()->Upload->folderName(0, 1000, $product_details1->id);
                                                ?>
                                                                                                                                                                                                                                                                <div class="col-sm-2">
                                                                                                                                                                                                                                                                <div class="products_item"> <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Products/Detail/name/<?php echo $product_details1->canonical_name; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?= $folder2 ?>/<?= $product_details1->id ?>/medium.<?= $product_details1->main_image ?>" alt=""/></a>
                                                                                                                                                                                                                                                                <div class="list_title">
                                                                                                                                                                                                                                                                <h3><?= $product_details1->product_name; ?></h3>
                                                                                                                                                                                                                                                                <h4>Saree</h4>
                                                                                                                                                                                                                                                                <p><?= Yii::app()->Currency->convert($product_details1->price); ?></p>
                                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                                                </div>
                                                                                                                                                                                                                                                                </div>
                                                <?php
                                        }
                                }
                        }
                        ?>
                                        </div>
                                </div>
                                        </div>-->
                </div>
                <?php
        }
        ?>
</div>
</div>
<div id="fb-root"></div>
<style>
        .codez{
                width: 45%;
                margin-left: 6px;
                margin-top: 9px;
        }
        .dsl{
                display:inline-flex;
        }
        .modal-header h3 {
                margin-bottom: 9px;
        }
        .main_container .row {
                margin-top: 3px;
        }

</style>
<script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                        return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.6";
                fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

<script>


        //                        $(document).ready(function () {
        //                                alert();
        //                                /*
        //                                 * cart remove funciton . remove individual item from cart
        //                                 */
        //                                $(".cart_box").on("click", ".cart_item>.cart_close", function () {
        //                                        var cartid = $(this).attr('cartid');
        //                                        var canname = $(this).attr('canname');
        //                                        removecart(cartid, canname);
        //                                });
        //                        });

        $(document).ready(function() {


                $(".enq_submit_btn").click(function() {

                        var color_val = $("#ProductEnquiry_color").val();
                        if (color_val == '') {
                                $(".color_error").html("Please select color");
                                return false;
                        }
                });

//                if ($('#clock').length) {
//                        $('#clock').countdown('<?= date('Y/m/d'); ?> 23:59:59').on('update.countdown', function (event) {
//                                var $this = $(this).html(event.strftime(''
//
//                                        + '<div class="digit">%H<span>Hrs</span></div><div class="digit">:</div>'
//                                        + '<div class="digit">%M<span>Min</span></div></div><div class="digit">:</div>'
//                                        + '<div class="last digit">%S<span>Sec</span></div>'));
//                        });
//                }


//return ploicy

                $(".return_policies").click(function() {

                        $.ajax({
                                type: "POST",
                                url: baseurl + 'Products/Policies',
                        }).done(function(data) {

                                if (data != "") {
                                        $("#returns").modal($('#return_policy').html(data));
                                }
                        });
                });
                $(".add_to_cart").click(function() {

                        var id = $(this).attr('id');
                        optionValidation(id);
                });
                $(".enq_button").click(function() {

                        var enqcolor = $('.enq_color').val();
                        if (enqcolor == '') {
                                return false;
                        }
                });
                $(".buy_now").click(function() {

                        var id = $(this).attr('id');
                        optionValidation(id);
                        //  window.location.origin + baseurl + "cart/Mycart";
                        setTimeout(function() {
                                window.location.replace(baseurl + "/cart/Mycart/");
                        }, 1000);
                        //  window.location.origin + baseurl + "cart/Mycart";



                });
                /*
                 * Product option
                 */
                // Size Selector
                $('.size_selector').delegate('label', 'click', function() {
                        var product_id = <?php echo $product->id; ?>;
                        $('#option_size').val('');
                        if ($(this).hasClass("disabled")) {

                        } else {
                                $('.size_selector label').removeClass('active');
                                $(this).addClass('active');
                                var size_opt = $('.size_selector .active').attr('id');
                                $('#option_size').val(size_opt);
                        }
                        getstock(product_id)
                        return false;
                });
                ///
                // color Selector
                $('.color_picker li').click(function() {
                        var option_type = $('#option_type').val();
                        var color = $(this).attr('color');
                        var option = $(this).attr('option_id');
                        var product_id = $(this).attr('product');
                        $('.enq_color').val(color);
                        selectgallery(color, product_id);
                        $(".qntyerror").hide();
                        if (option_type == 3) {
                                if ($(this).hasClass("disabled")) {

                                } else {
                                        color_size(option, color);
                                }
                        }
                        if ($(this).hasClass("disabled")) {

                        } else {

                                $('.color_picker li').removeClass('active');
                                $(this).addClass('active');
                                $('#option_color').val(color);
                                getstock(product_id);
                        }
                });
        });
        /*
         * product option validation
         */
        function checkquantity(canname) {

        }
        function optionValidation(id) {

                var canname = $("#cano_name_" + id).val();
                var qty = $(".qty").val();
                var option_color = $('#option_color').val();
                var option_size = $('#option_size').val();
                var option_type = $('#option_type').val();
                var master_option = $('#master_option').val();
                var product_id = <?php echo $product->id; ?>;
                //  checkquantity(canname);
                var extqty = getstockqnty(product_id);
                if (option_type == 3) {

                        if (option_color.length == 0 && option_size.length == 0) {
                                $('.option_errors').html('<p>Please select color</p><p>Please select size</p>').show();
                                return false;
                        } else if (option_color.length == 0) {
                                $('.option_errors').html('<p>Please select color</p>').show();
                                return false;
                        } else if (option_size.length == 0) {
                                $('.option_errors').html('<p>Please select size</p>').show();
                                return false;
                        } else if (extqty <= 0) {
                                $(".qntyerror").show();
                                $(".qntyerror").html("The product option combination is not in desired quantity.");
                                return false;
                        } else {
                                $('.option_errors').html("").hide();
                                addtocart(canname, qty, option_color, option_size, master_option);
                        }
                } else if (option_type == 1) {

                        if (option_color.length == 0) {
                                $('.option_errors').html('<p>Please select color</p>').show();
                                return false;
                        } else if (extqty <= 0) {
                                $(".qntyerror").show();
                                $(".qntyerror").html("The product option combination is not in desired quantity.");
                                return false;
                        } else {
                                $('.option_errors').html("").hide();
                                addtocart(canname, qty, option_color, option_size, master_option);
                        }
                } else if (option_type == 2) {
                        if (option_size.length == 0) {
                                $('.option_errors').html('<p>Please select size</p>').show();
                                return false;
                        } else if (extqty <= 0) {
                                $(".qntyerror").show();
                                $(".qntyerror").html("The product option combination is not in desired quantity.");
                                return false;
                        } else {
                                $('.option_errors').html("").hide();
                                addtocart(canname, qty, option_color, option_size, master_option);
                        }
                }
                else {
                        $('.option_errors').html("").hide();
                        addtocart(canname, qty, option_color = null, option_size = null, master_option = null);
                }
        }

        function selectgallery(color, product_id) {

                showLoader();
                $.ajax({
                        type: "POST",
                        url: baseurl + 'products/selectgallery',
                        data: {color: color, product_id: product_id}
                }).done(function(data) {
                        if (data != "") {
                                $(".zoomContainer").remove();
                                $('.image_gallery').html(data);

                        }
                        hideLoader();
                });
        }



        function getstock(product_id) {
                var color = $(".color_picker li.active").attr("color");
                var size = $(".size_selector label.active").attr("id");

                showLoader();
                $.ajax({
                        type: "POST",
                        url: baseurl + 'products/Getstock',
                        data: {color: color, size: size, product_id: product_id}
                }).done(function(data) {
//                        alert(data);
                        if (data > 0) {
//                                alert(1);
                                var text = "";
                                for (i = 1; i <= data; i++) {
                                        text += "<option value=" + i + ">" + i + "</option>";
                                }
                                $(".qntyerror").html("");
                                $(".qntyerror").hide();
                                $(".qty").html(text);
                                $(".qty").show();
                        } else if (data <= 0) {
//                                alert(2);
//                                $(".qty").hide();

//                                $(".qntyerror").html("The product option combination is not in desired quantity.");
//                                $(".qntyerror").show();
//                                hideLoader();
//                                return false;
                        }
                        hideLoader();
                });
        }
        function getstockqnty(product_id) {
                var color = $(".color_picker .product_colors li.active").attr("color");
                var size = $(".size_selector label.active").attr("id");

                var qty;
                $.ajax({
                        async: false,
                        type: "POST",
                        url: baseurl + 'products/Getstock',
                        data: {color: color, size: size, product_id: product_id}
                }).done(function(data) {

                        qty = data;

                });

                return qty;

        }







        function color_size(option, color) {

                //showLoader();
                $.ajax({
                        type: "POST",
                        url: baseurl + 'products/options',
                        data: {option: option, color: color}
                }).done(function(data) {

                        if (data != "") {

                                $('.size_selector').html(data);
                                hideLoader();
                        }
                });
        }

        function addtocart(canname, qty, option_color, option_size, master_option) {
                if (option_color === undefined) {
                        option_color = null;
                }
                if (option_size === undefined) {
                        option_size = null;
                }
                if (master_option === undefined) {
                        master_option = null;
                }
                $.ajax({
                        type: "POST",
                        url: baseurl + 'cart/Buynow',
                        data: {cano_name: canname, qty: qty, option_color: option_color, option_size: option_size, master_option: master_option}
                }).done(function(data) {
                        if (data == 9) {

                                $('.option_errors').html('<p>Invalid Product.Please try again</p>').show();
                        } else {

                                $('.option_errors').html("").hide();
                                getcartcount();
                                getcarttotal();
                                $(".cart_box").show();
                                $(".cart_box").html(data);
                                $("html, body").animate({scrollTop: 0}, "slow");
                        }
                        hideLoader();
                });
        }



        function showLoader() {
                $('.over-lay').show();
        }
        function hideLoader() {
                $('.over-lay').hide();
        }

</script>
<style>
        .product_metas .modal-body h3 {
                font-weight: normal !important;
                line-height: 1.428571 !important;
                text-transform: none !important;
        }
        .modal-body h3 {
                margin-bottom: 9px;
        }
        .modal-body h3 {
                font-size: 13px;
                margin: 9px;
                padding: 0px;
                font-family: Merriweather, serif;
                font-style: italic;
                margin-bottom: 20px;
                text-align: center;
        }
        .padd_ing_r0{
                padding-right: 0px !important;
        }
</style>