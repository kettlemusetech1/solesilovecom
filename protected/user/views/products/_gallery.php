<?php
$folder = Yii::app()->Upload->folderName(0, 1000, $model->id);
?>
<script>
        if ($('#laksyah_zoom').length) {
                $('#laksyah_zoom').elevateZoom({
                        gallery: 'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: true, responsive: true
                });

                $("#laksyah_zoom").bind("click", function (e) {
                        var ez = $('#laksyah_zoom').data('elevateZoom');
                        $.fancybox(ez.getGalleryList());
                        return false;
                });
        }
</script>
<div class="product_thumb">
        <ul id="gal1">
                <?php
                //  $folder = Yii::app()->Upload->folderName(0, 1000, $product->id);
                $big = Yii::app()->basePath . '/../uploads/producttoption/' . $folder . '/' . $model->id . '/gallery/big';
                $bigg = Yii::app()->request->baseUrl . '/uploads/producttoption/' . $folder . '/' . $model->id . '/gallery/big/';
                $thu = Yii::app()->basePath . '/../uploads/producttoption/' . $folder . '/' . $model->id . '/gallery/small';
                $thumbs = Yii::app()->request->baseUrl . '/uploads/producttoption/' . $folder . '/' . $model->id . '/gallery/small/';
                $zoo = Yii::app()->basePath . '/../uploads/producttoption/' . $folder . '/' . $model->id . '/gallery/zoom';
                $zoom = Yii::app()->request->baseUrl . '/uploads/producttoption/' . $folder . '/' . $model->id . '/gallery/zoom/';
                $file_display = array('jpg', 'jpeg', 'png', 'gif');

                if (file_exists($big) == false) {

                } else {


                        $dir_contents = scandir($big);
                        $i = 0;
                        foreach ($dir_contents as $file) {
                                $file_type = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                                if ($file !== '.' && $file !== '..' && in_array($file_type, $file_display) == true) {
                                        ?>

                                        <li> <a href="#" data-image="<?php echo $bigg . $file; ?>" data-zoom-image="<?php echo $zoom . $file; ?>"> <img src="<?php echo $thumbs . $file; ?>" alt=""/> </a> </li>
                                        <?php
                                }
                                ?>



                                <?php
                        }
                        $i++;
                }
                ?>

<!--                                                                <li><a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/images/product_big2.jpg" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/images/product_lg.jpg"> <img src="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" alt=""/> </a></li>
                                                                <li><a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/images/product_big.jpg"> <img src="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" alt=""/> </a></li>
                                                                <li><a href="#" data-image="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" data-zoom-image="<?= Yii::app()->request->baseUrl; ?>/images/product_big2.jpg"> <img src="<?= Yii::app()->request->baseUrl; ?>/images/product_small.jpg" alt=""/> </a></li>
                -->
                <?php if (empty($dir_contents)) { ?>
                        <li><a href="#" data-image="<?php echo Yii::app()->request->baseUrl; ?>/uploads/producttoption/<?= $folder ?>/<?= $model->id ?>/big.<?= $model->image ?>" data-zoom-image="<?php echo Yii::app()->request->baseUrl; ?>/uploads/producttoption/<?= $folder ?>/<?= $model->id ?>/zoom.<?= $model->image ?>"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/producttoption/<?= $folder ?>/<?= $model->id ?>/small.<?= $model->image ?>" alt=""/> </a></li>
                <?php } ?>
        </ul>
</div>
<?php
//$folder = Yii::app()->Upload->folderName(0, 1000, $model->id);
?>

<?php
if (!empty($dir_contents)) {

        foreach ($dir_contents as $file1) {

        }
        ?>
        <div class="product_big_image"> <img src="<?php echo $bigg . $file1; ?>" id="laksyah_zoom" data-zoom-image="<?php echo $zoom . $file1; ?>" alt=""/>
                <div class="product_social_shares"> <span>Share this look with your friends</span>
                        <a onclick="popWindow('https://www.facebook.com/sharer/sharer.php?u=https://laksyah.com/Products/Detail/name/<?php echo $product->canonical_name; ?>', 'facebook', 'width=1000,height=200,left=0,top=0,location=no,status=yes,scrollbars=yes,resizable=yes');"><i class="fa fa-facebook" ></i></a>
                        <a onclick="popWindow('http://twitter.com/share?ur=https://laksyah.com/new-look/amber.html?t=AMBER', 'twitter', '');"><i class="fa fa-twitter" ></i></a>
                        <a onclick="popWindow('https://pinterest.com/pin/create/button/?url=https://laksyah.com/new-look/amber.html?t=AMBER', 'pinterest', '');"><i class="fa fa-pinterest-p" ></i></a>
                        <?php // }  ?>
                        <a href="#" data-toggle="modal" data-target="#enquirymail"><i class="fa fa-envelope-o"></i></a> </div>
        </div>
<?php } else { ?>

        <div class="product_big_image"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/producttoption/<?= $folder ?>/<?= $model->id ?>/big.<?= $model->image ?>" id="laksyah_zoom" data-zoom-image="<?php echo Yii::app()->request->baseUrl; ?>/uploads/producttoption/<?= $folder ?>/<?= $model->id ?>/zoom.<?= $model->image ?>" alt=""/>
                <div class="product_social_shares"> <span>Share this look with your friends</span><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-pinterest-p"></i></a><a href="#" data-toggle="modal" data-target="#enquirymail"><i class="fa fa-envelope-o"></i></a> </div>
        </div>
<?php } ?>