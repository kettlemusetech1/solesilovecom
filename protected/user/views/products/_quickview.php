<div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="row">
                <div class="col-sm-6">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php
                        echo Yii::app()->Upload->folderName(0, 1000, $data->id)
                        ?>/<?php echo $data->id; ?>/medium.<?php echo $data->main_image; ?>" alt=""/>
                </div>
                <div class="col-sm-6 product-details">
                        <div class="product-meta">
                                <h4><?php echo $data->product_name; ?></h4>
                                <div class="product_srar"><a href="#" class="star1 active"><i class="fa fa-star"></i></a><a href="#" class="star2"><i class="fa fa-star"></i></a><a href="#" class="star3"><i class="fa fa-star"></i></a><a href="#" class="star4"><i class="fa fa-star"></i></a><a href="#" class="star5"><i class="fa fa-star"></i></a></div>
                                <?php
                                if ($data->discount_rate != 0) {
                                    ?>
                                    <del class="text-danger"><?php echo Yii::app()->Currency->convert($data->price); ?></del> <?php echo Yii::app()->Discount->Discount($data); ?>


                                    <?php
                                } else {
                                    ?>
                                    <div class="product-price">
                                            <?php echo Yii::app()->Discount->Discount($data); ?>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="product-des">
                                        <p><?php echo $data->description; ?> </p>
                                </div>

                                <div class="product_button_group">
                                        <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $data->id ?>" class="button button-lined"><i class="fa fa-heart-o"></i> ADD TO WISHLIST</a>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $data->canonical_name; ?>" class="button">SHOP NOW</a>
                                </div>

                        </div>
                </div>
        </div>




</div>