<div class="breadcrumb">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php">Home</a> <?php
        $action = Yii::app()->controller->action->id;

        if ($action == 'category') {
            $get_cat_name = ProductCategory::model()->findByAttributes(array('canonical_name' => $category_name, 'status' => 1));
            if ($get_cat_name->id == $get_cat_name->parent) {
                ?>
                / <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/<?= $get_cat_name->canonical_name; ?>"><?php echo $get_cat_name->category_name; ?></a>
                <?php
            } else {
                $get_parent = ProductCategory::model()->findByPk($get_cat_name->parent);
                if (($get_parent->category_name != 'Men') && ($get_parent->category_name != 'Women')) {
                    ?>
                    / <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/products/category/name/<?= $get_parent->canonical_name; ?>"><?php echo $get_parent->category_name; ?></a>
                    <?php
                }
                ?>
                / <?php echo $get_cat_name->category_name; ?>
                <?php
            }
        } else if ($action == 'detail') {
            ?>
            <input type="hidden" class="hdd" value="<?php echo Yii::app()->request->urlReferrer; ?>"/>
            <?php
            $expvar = Yii::app()->request->urlReferrer;

            if ($expvar != '') {
                Yii::app()->session['expvar'] = $expvar;
            }
            $newval = explode("/", Yii::app()->session['expvar']);
            $can_name = $newval[6];
            $get_cat_name = ProductCategory::model()->findByAttributes(array('canonical_name' => $can_name));
            if ($get_cat_name->category_name == '') {
                $varser = "Search";
            }

            if ($varser != '') {
                ?>
                <li class="active"> / Search </li>
                        <?php
                    } else if ($get_cat_name->id == $get_cat_name->parent) {

                    } else {
                        $get_parent = ProductCategory::model()->findByPk($get_cat_name->parent);
                        ?>
                / <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/category/<?= $get_cat_name->canonical_name; ?>"><?php echo $get_cat_name->category_name; ?></a>
                <?php if ($product->product_name) { ?>
                    / <?php echo $product->product_name; ?>
                    <?php
                }
            }
        } else {
            ?>
            / <?php echo $product->product_name; ?>
        <?php }
        ?>
</div>
