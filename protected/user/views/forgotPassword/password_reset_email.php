<?php echo $this->renderPartial('//site/mail/_email_header'); ?>

<tr>
        <td style="padding:40px 20px; font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;"><p>Hi <?php echo $model->first_name; ?><span>      <?php echo $model->last_name; ?></span>,<br/><br/> Greetings from solesilove.com.au! </p>


                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;">Your password has been successfully updated on <?php echo date("d-m-Y"); ?>  </p><p> Email id:      <?php echo $model->email; ?></p><p>   Password:  <?php echo $model->password; ?></p>

                <br>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;">solesilove.com.au!</p>

                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;"><a href="<?php
                        echo $this->siteURL() .
                        '/index.php/site/login';
                        ?>" style="text-transform: uppercase;background-color: #f47721;border-radius: 0;outline: none;border: none;height: 40px;line-height: 40px;padding: 0px 10px;padding-left: 30px;padding-right: 30px; color:#fff; text-decoration:none; display:inline-block; font-style:normal;">LOGIN HERE</a></p>

                <br/>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#acacb1;">* This is an automatically generated email, please do not reply to this email.</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#acacb1;">* This email address was provided on our registration page. If you own the email and did not register on our site, please send an email to support@solesilove.com.au</p>

        </td>
</tr>



<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>