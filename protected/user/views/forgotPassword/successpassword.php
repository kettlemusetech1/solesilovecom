<section class="login-wrp">
        <div class="container">
                <div>
                        <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#loginreg01" aria-controls="loginreg01" role="tab" data-toggle="tab">Reset Password</a></li>
                        </ul>
                        <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="loginreg01">
                                        <div class="login-form-inputs login-input-main material-slide-line">
                                                <?php if (Yii::app()->user->hasFlash('success')): ?>
                                                        <div class="alert1 alert-success" style="    background: transparent;">
                                                                <strong></strong> <?php echo Yii::app()->user->getFlash('success'); ?>
                                                        </div>
                                                <?php endif; ?>
                                                <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                        <div class="alert alert-danger">
                                                                <strong></strong><?php echo Yii::app()->user->getFlash('error'); ?>
                                                        </div>
                                                <?php endif; ?>
                                        </div>
                                </div>

                        </div>

                </div>
        </div>
</section>
