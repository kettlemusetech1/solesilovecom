<?php echo $this->renderPartial('//site/mainmodal'); ?>
<div class="clearfix"></div>
<script type="text/javascript">
        $(document).ready(function() {
                $("#form1").submit(function() {
                        var pass1 = $('#password1').val();
                        var pass2 = $('#password2').val();
                        if (pass1 && pass2 != "") {
                                if (pass1 != pass2) {
                                        $('#password_error').text('password doesnot match');
                                        return false;
                                } else {
                                        $('#password_error').text('');
                                        return true;
                                }

                        }
                });
                $('.pass').keyup(function() {
                        var pass1 = $('#password1').val();
                        var pass2 = $('#password2').val();
                        if (pass1 && pass2 != "") {
                                if (pass1 != pass2) {
                                        $('#password_error').text('password doesnot match');
                                } else {
                                        $('#password_error').text('');
                                }

                        }

                });
        });
</script>


<section class="login-wrp">
        <div class="container">
                <div>
                        <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#loginreg01" aria-controls="loginreg01" role="tab" data-toggle="tab">Reset Password</a></li>
                        </ul>
                        <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="loginreg01">
                                        <div class="login-form-inputs login-input-main material-slide-line">
                                                <?php if (Yii::app()->user->hasFlash('success')): ?>
                                                        <div class="alert alert-success">
                                                                <strong>Success!</strong> <?php echo Yii::app()->user->getFlash('success'); ?>
                                                        </div>
                                                <?php endif; ?>
                                                <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                        <div class="alert alert-danger">
                                                                <strong>Error!</strong><?php echo Yii::app()->user->getFlash('error'); ?>
                                                        </div>
                                                <?php endif; ?>
                                                <form action="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ForgotPassword/Newpassword/" method="post" id="form1" name="form1">

                                                        <span class="input input--akira input-half">
                                                                <input type="password" class="input__field input__field--akira" id="password1" name="password1" required="required">

                                                                <label class="input__label input__label--akira" for="input-1">
                                                                        <span class="input__label-content input__label-content--akira">New Password</span>
                                                                </label>
                                                        </span>

                                                        <span class="input input--akira input-half">
                                                                <label class="input__label input__label--akira" for="input-2">
                                                                        <span class="input__label-content input__label-content--akira">Confirm Password</span>
                                                                </label>
                                                                <input type="password" class="input__field input__field--akira" id="password2" name="password2" required="required">
                                                                <input type="hidden"  name="uid" value="<?php echo $id; ?>" required="required">
                                                                <div id="password_error" style="color:red;"></div>
                                                        </span>

                                                        <button class="butter login-btn" name="btn_submit">Reset Password</button>

                                        </div>
                                </div>

                        </div>

                </div>
        </div>
</section>
