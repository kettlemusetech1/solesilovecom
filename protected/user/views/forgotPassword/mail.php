<section class="login-wrp">
        <div class="container">
                <div>
                        <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#loginreg01" aria-controls="loginreg01" role="tab" data-toggle="tab">Reset Password</a></li>
                        </ul>
                        <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="loginreg01">
                                        <div class="login-form-inputs login-input-main material-slide-line">

                                                Thank you! You should receive an email with a link to reset your password momentarily.</div>
                                        <div class="text-center form_button">
                                        </div>
                                </div>

                        </div>

                </div>
        </div>
</section>

