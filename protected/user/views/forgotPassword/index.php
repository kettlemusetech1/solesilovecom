<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section class="login_page">
        <div class="container content-body listings_page">
                <div class="breadcrumb">
                        <a href="">Home</a> / Forgotpassword
                </div>
                <div class="login_form_container">
                        <div class="login_form reset">
                                <form action="<?= Yii::app()->baseUrl; ?>/index.php/forgotPassword/Index" method="post" id="form1" name="form1">
                                        <h1>PASSWORD RESET</h1>
                                        <label>Please enter your registerd email.</label>
                                        <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                <div class="alert alert-danger">
                                                        <strong>Error!</strong><?php echo Yii::app()->user->getFlash('error'); ?>
                                                </div>
                                        <?php endif; ?>
                                        <div class="form-group">
                                                <input type="text" name="emailaddres" id="" class="form-control" placeholder="Email Address">
                                        </div>
                                        <div class="form-group">
                                                <button class="button" name="submit">SUBMIT</button>
                                        </div>
                                </form>
                        </div>
                </div>
        </div>
</section>


<section class="login-wrp">
        <div class="container">
                <div>
                        <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#loginreg01" aria-controls="loginreg01" role="tab" data-toggle="tab">Reset Password</a></li>
                        </ul>
                        <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="loginreg01">
                                        <div class="login-form-inputs login-input-main material-slide-line">
                                                <div>Please enter your email address and we'll send you instructions to reset your password.</div>

                                                <?php if (Yii::app()->user->hasFlash('error')): ?>
                                                        <div class="alert alert-danger">
                                                                <strong>Error!</strong><?php echo Yii::app()->user->getFlash('error'); ?>
                                                        </div>
                                                <?php endif; ?>
                                                <form action="<?= Yii::app()->baseUrl; ?>/index.php/forgotPassword/Index" method="post" id="form1" name="form1">

                                                        <span class="input input--akira input-half">
                                                                <input type="email" class="input__field input__field--akira" id="input-1" name="emailaddres" placeholder="Enter your email" autocomplete="off" required="required">
                                                                <label class="input__label input__label--akira" for="input-1">
                                                                        <span class="input__label-content input__label-content--akira">Email Address</span>
                                                                </label>
                                                        </span>

                                                        <div class="forjetreg">
                                                                <a class="regis-pass" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/register">Register Now</a>
                                                        </div>
                                                        <button class="butter login-btn" name="submit">Submit</button>
                                                </form>
                                        </div>
                                </div>

                        </div>

                </div>
        </div>
</section>

