<?php echo $this->renderPartial('//site/mail/_email_header'); ?>


<tr>
        <td style="padding:0px 20px; font-family:'Open Sans',arial, sans-serif; font-size:13px">
                <table id="Table_01"  border="0" cellpadding="0" cellspacing="0" align="left" style="padding:13px 0px; font-family:'Open Sans',arial, sans-serif; font-size:13px">
                        <tr><td style="padding:40px 20px; font-family:'Open Sans',arial, sans-serif; font-size:13px"><p>Hi <?php echo $model->first_name; ?><span>      <?php echo $model->last_name; ?>,</p>



                                        <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Hello from Soles iLove <br/><br/>We received your request to reset the password on your solesilove.com.au account.</p>


                                        <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Please <a href="https://solesilove.com.au/index.php/ForgotPassword/Changepassword/token/<?php echo $token; ?>">Click Here </a> to do as soon as possible-this link expires in 24 hours from the send time of this email.</p>

                                        <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">If the above link is not working, please copy and paste the below link to your browser.</p>

                                        <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">https://solesilove.com.au/index.php/ForgotPassword/Changepassword/token/<?php echo $token; ?></p>

                                        <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Kindly note if this email was sent more than 30 minutes ago, please request to reset your password again by visiting solesilove.com.au, click 'Login' and then 'Forgot Password?'</p>



                                        <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Sincerely,<br/><br/>solesilove.com.au</p><br/>

                                        <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color: #acacb1;">* This is an automatically generated email, please do not reply to this email.</p>
                                        <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color: #acacb1;">* If this email was not intended for you and was sent to you by mistake, please call us at +9142202222 or email us at support@solesilove.com.au immediately.</p>
                                </td>

                        </tr></table>
        </td>
</tr>











<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>
