<?php

if (!empty($dataprovider) || $dataProvider != '') {
        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'template' => "{pager}\n{items}\n{pager}",
        ));
}
?>