<div class="side-menu">
        <h3>Category</h3>
        <ul class="side-menu-ul">
                <?php
                if (!isset(Yii::app()->session['category_typ'])) {
                        $this->redirect(array('Site/index'));
                }
                $cid = Yii::app()->session['category_typ'];

                $category_name = Yii::app()->request->getParam('name');
                $parent = ProductCategory::model()->findByAttributes(array('canonical_name' => $category_name));
                if ($parent->id == 2) {
                        $main_cats = ProductCategory::model()->findAllByAttributes(array(), array('condition' => 'id = 2'));
                } else if ($parent->id == 8) {
                        $main_cats = ProductCategory::model()->findAllByAttributes(array(), array('condition' => 'id = 8'));
                } else {
                        $main_cats = ProductCategory::model()->findAllByAttributes(array(), array('condition' => 'id=parent and id!=2 and id!=8 and category_type=' . $cid));
                }
                foreach ($main_cats as $main_cat) {
                        $subcats = ProductCategory::model()->findAllByAttributes(array('parent' => $main_cat->id), array('condition' => 'id !=' . $main_cat->id));
                        if (!empty($subcats)) {
                                $main_menus = '#';
                        } else {
                                $main_menus = Yii::app()->request->baseUrl . "/index.php/products/category/name/" . $main_cat->canonical_name;
                        }
                        ?>
                        <li class="<?= $main_cat->canonical_name == $category_name ? 'active open' : '' ?>maincat_<?= $main_cat->canonical_name; ?>">
                                <a href="<?php echo $main_menus; ?>"><i></i><?= $main_cat->category_name; ?></a>
                                <ul>
                                        <?php
                                        foreach ($subcats as $subcat) {

                                                if ($subcat->canonical_name == $category_name) {
                                                        $selected = 'active';
                                                } else {
                                                        $selected = '';
                                                }
                                                ?>

                                                <li class ="<?= $selected; ?>"> <a href = "<?= Yii::app()->request->baseUrl; ?>/index.php/products/category/name/<?= $subcat->canonical_name; ?>"><?= $subcat->category_name; ?></a></li>
                                                <?php
                                        }
                                        ?>
                                </ul>
                        </li>


                        <?php
                }
                ?>
        </ul>
</div>
