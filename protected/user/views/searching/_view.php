<div class="col-sm-6 col-md-4">

        <div class="listing-box">
                <div class="listing-box-img img-wrapper">
                        <?php if ($data->main_image == NULL) { ?>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Products/Detail/name/<?php echo $data->canonical_name; ?>">
                                        <img width="280" height="300" class="safe" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/no-productimage.jpg" alt="NO IMAGE"/></a>
                                <?php
                        } else {
                                ?>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Products/Detail/name/<?php echo $data->canonical_name; ?>">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php
                                        echo Yii::app()->Upload->folderName(0, 1000, $data->id)
                                        ?>/<?php echo $data->id; ?>/medium.<?php echo $data->main_image; ?>" alt="<?php echo $data->product_name; ?>">
                                </a>
                                <?php
                        }
                        ?>
                </div>
                <div class="listing-box-det">
                        <h4><?php echo $data->product_name; ?></h4>
                        <?php
                        if ($data->enquiry_sale == 1) {
                                ?>
                                <h6><i class="fa fa-usd"></i><?php echo $data->price; ?></h6>
                                <?php
                        } else {
                                ?>
                                <h6>&nbsp;</h6>
                                <?php
                        }
                        ?>
                        <div class="list-hovered">
                                <p>
                                        <a href="<?= Yii::app()->baseUrl; ?>/index.php/Products/Wishlist/id/<?= $data->id ?>"><i class="fa fa-heart-o"></i></a>
                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Products/Detail/name/<?php echo $data->canonical_name; ?>"><i class="fa fa-eye"></i></a>
                                </p>
                        </div>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Products/Detail/name/<?php echo $data->canonical_name; ?>" class="butter shop-btn">Shop Now</a>
                </div>
        </div>
</div>


