<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section class="about-wrp">
        <div class="container">
                <div class="row">
                  <?php echo $this->renderPartial('//myaccount/_menu'); ?>
                 <div class="col-sm-9 profile-content-area">
				<h1>Redeem your coupon</h1>
				<div class="profile_form">
				    
				    <?php if (Yii::app()->user->hasFlash('wallet_success')): ?>
                    <div class="row">
                        <div class="alert alert-success alert-dismissable" style="margin:0 auto;">
                            <?php echo Yii::app()->user->getFlash('wallet_success'); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (Yii::app()->user->hasFlash('wallet_error')): ?>
                    <div class="row">
                        <div class="alert alert-danger alert-dismissable" style="margin:0 auto;">
                            <?php echo Yii::app()->user->getFlash('wallet_error'); ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'wallet-add-form',
                    'action' => Yii::app()->baseUrl . '/MyWallet/',
                    'enableAjaxValidation' => false,
                ));
                ?>
                <?php if (Yii::app()->user->hasFlash('credit_wallet_success')): ?>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("#walletsuccess").modal({backdrop: 'static', keyboard: false});
                        });
                    </script>

                    <div class="modal fade" id="walletsuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4>Coupon Redemption</h4><br/>
                                </div>
                                <div class="modal-body" style="text-align:center;font-size:14px;">
                                    <?php echo Yii::app()->user->getFlash('credit_wallet_success'); ?>
                                </div><br/>
                                <div class="modal-footer">
                                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/myaccount" type="button" class="btn btn-primary" >Ok</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                
                
					<div class="form-group">
						<div class="row">
							
							<div class="col-xs-8">
							    <div class="row margin-normal">
                            <div class="col-xs-12 col-sm-10">
								<label for="">Coupon Code*</label>
								<?php echo $form->textField($wallet_add, 'unique_code', array('size' => 60, 'maxlength' => 100, 'placeholder' => "Enter Your Code", 'class' => 'form-control', 'id' => 'gift_code', 'required' => true)); ?>
                                <span class="gift_status"></span>
                                 </div>
                            <div class="col-xs-12 col-sm-2">
                                <?php echo $form->error($wallet_add,'unique_code', array('style' => 'padding-left:0px;')); ?><i style="margin-top: 36px; cursor: pointer; font-size: 15px; " class="fa fa-refresh gift_amount" aria-hidden="true"></i>
							</div>
							</div>
							<!--<div class="col-sm-4">-->
								
							<!--</div>-->
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-2">
								<label for="">Amount</label>
								<input type="text" name="" id="" class="form-control" value="AUD">
							</div>
							<div class="col-sm-6">
							    <label for="">&nbsp;</label>
								<?php echo $form->textField($wallet_add, 'amount', array('size' => 60, 'maxlength' => 100, 'placeholder' => "0.00", 'class' => 'form-control gift_amount', 'id' => 'gift_amount', 'required' => true)); ?>
                                <?php echo $form->error($wallet_add, 'amount', array('style' => 'padding-left:0px;')); ?>
							</div> 
							
						</div>
					</div>
				
					<div class="form-group">
						<div class="row">
							
							<div class="col-sm-8 text-left form_button">
								<button class="pull-right profile_top_btns button">REDEEM NOW</button>
							</div>
						</div>
						
					</div>
					<?php $this->endWidget(); ?>
				</div>
			</div>

                </div>
        </div>
</section>
<style>
    .custom_check a {
        color: #337ab7;
        text-decoration: none;
    }
    .custom_check a:hover {
        color: #f47721;
    }
</style>
<script>
    $(document).ready(function () {
        
        // Custom Radio
        $('.price_group1 .radio_group1').click(function () {
            $(this).parents('.price_group1').find('.radio_group1').removeClass('active');
            $(this).addClass('active');
            $(this).find('input').attr('checked', true);
        });

        $('.gift_amount').click(function () {
            var gift_code = $("#gift_code").val();
            if (gift_code != '') {

                $.ajax({
                    type: "POST",
                    url: baseurl + "MyWallet/GetAmount",
                    data: {gift_code: gift_code}
                }).done(function (data) {
                    if (data == 0) {
                        $('.errorMessage').hide();
                        $('.gift_status').html('<h5 style="color:red">Gift card code is Invalid</h5>');
                        $('#gift_amount').val(data);
                        $("#gift_amount").attr("readonly", "readonly");
                    } else {
                        $('.errorMessage').hide();
                        $('.gift_status').html('<h5 style="color:green">Gift card code is valid</h5>');
                        $('#gift_amount').val(data);
                        $("#gift_amount").attr("readonly", "readonly");
                    }
                });
            } else {
                $("#gift_amount").attr("readonly", "readonly");
            }
        });
    });
</script>