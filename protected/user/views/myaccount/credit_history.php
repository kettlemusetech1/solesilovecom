<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / <a href="">Credit History</a>
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Cart</h2>
                <div class="row">
                        <?php echo $this->renderPartial('_menu'); ?>
                        <div class="col-sm-9 profile-content-area">
                             <a href="<?= Yii::app()->baseUrl; ?>/index.php/AddCredit/" class="pull-right profile_top_btns button">Redeem Coupon</a>
                                <h1>My Credit</h1>
                               
                                <?php if (!empty($history)) { ?>
                                        <div class="my_credit_page">
                                                <div class="credit_balance">
                                                        <?php $user = UserDetails::model()->findByPk(Yii::app()->session['user']['id']); ?>
                                                        <label for="">My Credit Balance</label> <strong><?php echo Yii::app()->Currency->convert($user->wallet_amt); ?></strong>
                                                </div>
                                                <div class="credit_table">
                                                        <div class="row table_head">
                                                                <div class="col-xs-5">
                                                                        Action
                                                                </div>
                                                                <div class="col-xs-3">
                                                                        Deposit
                                                                </div>
                                                                <div class="col-xs-4">
                                                                        Redemption
                                                                </div>
                                                        </div>
                                                        <?php
                                                        foreach ($history as $credit_history) {
                                                                ?>
                                                                <div class="row body_table">
                                                                        <div class="col-xs-5">
                                                                             <p><strong><?php
                                                                                                if ($credit_history->unique_code != "" && $credit_history->credit_debit == 1) {
                                                                             echo "Coupon Redemption";
                                                                                                }
                                                                                                ?></strong></p>
                                                                                <p><strong><?php
                                                                                                if ($credit_history->ids != "" && $credit_history->ids != 0) {
                                                                                                        echo $credit_history->type->type;
                                                                                                }
                                                                                                ?></strong></p>
                                                                                <p><?php if ($credit_history->ids != "" && $credit_history->ids != 0 && $credit_history->type_id == "4") { ?>
                                                                                                Order No: <?php
                                                                                                echo "SLOR " . $credit_history->ids;
                                                                                        }
                                                                                        ?></p>
                                                                                <p><?php if ($credit_history->creditnote_no != '0') { ?>Credit Note No : Credit Note No </span>: <?php
                                                                                                echo $credit_history->creditnote_no;
                                                                                        }
                                                                                        ?></p>
                                                                                <p>Date : <?php echo date('d/m/Y - g:i:s A', strtotime(date($credit_history->entry_date))); ?></p>


                                                                        </div>
                                                                        <div class="col-xs-3 green">
                                                                                <?php
                                                                                if ($credit_history->credit_debit == 1) {
                                                                                        echo "+" . $credit_history->amount;
                                                                                }
                                                                                ?>

                                                                        </div>
                                                                        <div class="col-xs-4 red">
                                                                                <?php
                                                                                if ($credit_history->credit_debit == 2) {
                                                                                        echo "-" . $credit_history->amount;
                                                                                }
                                                                                ?>
                                                                        </div>
                                                                </div>
                                                                <?php
                                                        }
                                                        ?>
                                                </div>
                                        </div>
                                <?php } else {
                                        ?>
                                        <div>No Credit History found.</div>
                                        <?php
                                }
                                ?>
                        </div>
                </div>

        </div>
</section>