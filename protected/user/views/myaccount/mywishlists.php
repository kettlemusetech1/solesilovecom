<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="<?php Yii::app()->baseUrl;?>/index.php/Myaccount/index">Home</a> / <a href="">Men</a>  /  Shoes
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Cart</h2>
                <div class="row">
                        <?php echo $this->renderPartial('_menu'); ?>
                        <div class="col-sm-9 profile-content-area">
                                <h1>My Wishlist</h1>
                                <div class="my_credit_page">
                                        <?php if (!empty($wishlists)) { ?>
                                            <div class="wish_table">
                                                    <div class="row table_head">
                                                            <div class="col-xs-6">
                                                                    Product
                                                            </div>
                                                            <div class="col-xs-3">
                                                                    Price
                                                            </div>
                                                            <div class="col-xs-3 text-center">
                                                                    Action
                                                            </div>
                                                    </div>
                                                    <?php
                                                    foreach ($wishlists as $wishlist) {
                                                        $prod_name = Products::model()->findByPk($wishlist->prod_id);
                                                        ?>
                                                        <div class="row body_table">
                                                                <div class="col-xs-6 cart_details">
                                                                        <div class="row cart_item_row">
                                                                                <div class="col-xs-3 col-sm-4 cart_image">
                                                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/1000/<?php echo $prod_name->id; ?>/small.<?php echo $prod_name->main_image; ?>" alt=""/>
                                                                                </div>
                                                                                <div class="col-xs-9 col-sm-8 cart_item_details">
                                                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $prod_name->canonical_name; ?>" target="new"><h3><?= $prod_name->product_name; ?></h3></a>
                                                                                        <div class="product_srar disabled"><a href="#" class="star1 active"><i class="fa fa-star-o"></i></a><a href="#" class="star2"><i class="fa fa-star-o"></i></a><a href="#" class="star3"><i class="fa fa-star-o"></i></a><a href="#" class="star4"><i class="fa fa-star-o"></i></a><a href="#" class="star5"><i class="fa fa-star-o"></i></a></div>
                                                                                       <!-- <p><strong>Product Code : <?= $prod_name->product_code; ?></strong></p>-->
                                                                                        <div class="button_groups">
                                                                                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $prod_name->canonical_name; ?>"  target="new"><button class="button">VIEW PRODUCT</button></a>
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="col-xs-3 wish_full">
                                                                        <strong><?php echo Yii::app()->Discount->Discount($prod_name); ?></strong>
                                                                </div>
                                                                <div class="col-xs-3 wish_full text-center">
                                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/RemoveMywishlists/pid/<?php echo $prod_name->id; ?>" class="remove_btn"><i class="fa fa-times"></i></a>
                                                                </div>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                            </div>
                                        <?php } else {
                                            ?>
                                            <div>No products found in your wishlist</div>
                                            <?php
                                        }
                                        ?>

                                </div>
                        </div>
                </div>

        </div>
</section>