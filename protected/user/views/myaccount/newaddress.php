<?php echo $this->renderPartial('//site/mainmodal'); ?>
<style>
    .errorMessage {
    color: red;
    font-size: 11px;
    position: absolute;
    top: -13px;
}
.form-group {
    position: relative;
}
</style>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / <a href="">New Address</a>
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Cart</h2>
                <div class="row">
                        <?php echo $this->renderPartial('_menu'); ?>
                        <div class="col-sm-9 profile-content-area">
                                        <?php
                                        $form1 = $this->beginWidget('CActiveForm', array(
                                            'id' => 'dimension-class-form',
                                            'htmlOptions' => array('class' => ''),
                                            'action' => Yii::app()->baseUrl . '/Myaccount/Newaddress/id/' . $id,
                                            'enableAjaxValidation' => false,
                                        ));
                                        ?>
                                        <div class="profile_form">

                                        <div class="form-group">
                                                <div class="row">

                                                        <div class="col-sm-6">
                                                                <label for="">First Name</label>
                                                                <?php echo $form1->error($model, 'first_name'); ?>
                                                                <?php echo $form1->textField($model, 'first_name', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'id' => '')); ?>
                                                        </div>
                                                        <div class="col-sm-6">
                                                                <label for="">Last Name</label>
                                                                <?php echo $form1->error($model, 'last_name'); ?>
                                                                <?php echo $form1->textField($model, 'last_name', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'id' => '')); ?>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <div class="row">
                                                        <div class="col-sm-12">
                                                                <label for="">Address 1</label>
                                                                  <?php echo $form1->error($model, 'address_1'); ?>
                                                                <?php echo $form1->textField($model, 'address_1', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'id' => '')); ?>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <div class="row">
                                                        <div class="col-sm-12">
                                                                <label for="">Address 2</label>
                                                                  <?php echo $form1->error($model, 'address_2'); ?>
                                                                <?php echo $form1->textField($model, 'address_2', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'id' => '')); ?>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <div class="row">
                                                        <div class="col-sm-6">
                                                                <label for="">Country</label>
                                                                  <?php echo $form1->error($model, 'country'); ?>
                                                                <?php echo CHtml::activeDropDownList($model, 'country', CHtml::listData(Countries::model()->findAll(), 'id', 'country_name'), array('empty' => '--Select Country--', 'class' => 'form-control', 'id' => 'UserAddress_country')); ?>
                                                        </div>
                                                        <?php
                                                        $phone_code = array();

                                                        $phones = Countries::model()->findAll();
                                                        if (!empty($phones)) {
                                                                $phone_options[""] = "--Select--";
                                                                foreach ($phones as $phone) {
                                                                        $phone_options[$phone->phonecode] = '+' . $phone->phonecode;
                                                                }
                                                        } else {
                                                                $phone_options[""] = "--Code--";
                                                                $phone_options[0] = "Other";
                                                        }

                                                        $state_options = array();
                                                        if (Yii::app()->session['user']['country'] != 0) {
                                                                $states = States::model()->findAll();

                                                                if (!empty($states)) {
                                                                        $state_options[""] = "--Select--";
                                                                        foreach ($states as $state) {
                                                                                $state_options[$state->state_name] = $state->state_name;
                                                                        }
                                                                } else {
                                                                        $state_options[""] = "--Select--";
                                                                        $state_options[0] = "Other";
                                                                }
                                                        } else {
                                                                $state_options[""] = '--select--';
                                                        }
                                                        ?>
                                                        <div class="col-sm-3">
                                                                <label for="">State</label>
                                                                  <?php echo $form1->error($model, 'state'); ?>
                                                                <div class="cn_ext">
                                                                        <?php echo CHtml::activeDropDownList($model, 'state', $state_options, array('class' => 'form-control', 'options' => array('id' => array('selected' => 'selected')))); ?>
                                                                </div>
                                                                <div class="not_ext">
                                                                        <input placeholder="state " class="form-control aik" name="UserAddress[state1]" id="UserAddress_state" type="text" maxlength="111">
                                                                </div>
                                                        </div>

                                                        <div class="col-sm-3">
                                                                <label for="">Pin</label>
                                                                  <?php echo $form1->error($model, 'postcode'); ?>
                                                                <?php echo $form1->textField($model, 'postcode', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'id' => '')); ?>
                                                        </div>
                                                </div>

                                        </div>
                                        <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                                <label for="">City</label>
                                                                  <?php echo $form1->error($model, 'city'); ?>
                                                                <?php echo $form1->textField($model, 'city', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'id' => '')); ?>
                                                        </div>
                                                        <div class="col-sm-3">
                                                                <label for="">Country Code</label>
                                                                  <?php echo $form1->error($model, 'phonecode'); ?>
                                                                <?php echo CHtml::activeDropDownList($model, 'phonecode', $phone_options, array('class' => 'form-control aik', 'options' => array($phonecode => array('selected' => 'selected')))); ?>
                                                        </div>
                                                        <div class="col-sm-4">
                                                                <label for="">Mobile Number</label>
                                                                  <?php echo $form1->error($model, 'contact_number'); ?>
                                                                <?php echo $form1->textField($model, 'contact_number', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control', 'id' => '')); ?>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <div class="row">
                                                        <div class="col-sm-7">

                                                                <div class="newsletter"><input type="checkbox"> Make this Default Address</div>
                                                        </div>
                                                        <div class="col-sm-5 text-right">
                                                                <button class="button button_4">CANCEL</button>
                                                                <button class="button button_2">SAVE ADDRESS</button>
                                                        </div>
                                                </div>

                                        </div>

                                </div>

                                        <?php $this->endWidget(); ?>

                               </div>
                </div>

        </div>
</section>
<style>
        .not_ext{
                display: none;
        }
        .ship_not_ext{
                display: none;
        }
</style>