<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / <a href="">Myaccount</a>
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Dashboard</h2>
                <div class="row">
                        <?php echo $this->renderPartial('_menu'); ?>
                        <div class="col-sm-9 profile-content-area">

                                <h1>MY Dashboard</h1>
                                <div class="main-dashboard">
                                        <div class="dash_grid">
                                                <div class="dash_grid_item">
                                                        <i class="fa fa-user-circle-o watermark"></i>
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Profile">
                                                                <i class="fa fa-user-circle-o"></i>
                                                                <strong>MY PROFILE</strong>
                                                        </a>
                                                </div>
                                                <div class="dash_grid_item cl1">
                                                        <i class="fa fa-credit-card watermark"></i>
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/CreditHistory">
                                                                <i class="fa fa-credit-card"></i>
                                                                <strong>MY CREDIT</strong>
                                                        </a>
                                                </div>
                                                <div class="dash_grid_item cl2">
                                                        <i class="fa fa-address-book watermark"></i>
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Addressbook">
                                                                <i class="fa fa-address-book"></i>
                                                                <strong>ADDRESS BOOK</strong>
                                                        </a>
                                                </div>
                                                <div class="dash_grid_item cl3">
                                                        <i class="fa fa-shopping-cart watermark"></i>
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Myorders">
                                                                <i class="fa fa-shopping-cart"></i>
                                                                <strong>MY ORDERS</strong>
                                                        </a>
                                                </div>
                                                <div class="dash_grid_item cl4">
                                                        <i class="fa fa-heart-o watermark"></i>
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Mywishlists">
                                                                <i class="fa fa-heart-o"></i>
                                                                <strong>MY WISHLIST</strong>
                                                        </a>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>

        </div>
</section>
