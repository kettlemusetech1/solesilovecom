<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / <a href="">Men</a>  /  Shoes
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Cart</h2>
                <div class="row">
                        <?php echo $this->renderPartial('_menu'); ?>
                        <div class="col-sm-9 profile-content-area">
                                <a href="<?= Yii::app()->baseUrl; ?>/index.php/Myaccount/Changepassword/" class="pull-right profile_top_btns button">CHANGE PASSWORD</a>
                                <h1>Profile</h1>
                                <form name="frm" action="<?= Yii::app()->baseUrl; ?>/Myaccount/Profileedit" method="post">
                                        <div class="profile_form">
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-sm-6">
                                                                        <label for="">Name</label>
                                                                        <input type="text" name="name" value="<?= $model->first_name; ?>" id="" class="form-control">
                                                                </div>

                                                        </div>
                                                </div>
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-sm-6">
                                                                        <label for="">Emailid</label>
                                                                        <input type="text" value="<?= $model->email; ?>" disabled name="email" id="" class="form-control">
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-sm-6">
                                                                        <label for="">Mobile Number</label>
                                                                        <input type="number" value="<?= $model->phone_no_2; ?>" name="moble" id="" class="form-control">
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-sm-7">
                                                                        <label class="dark">Would you like to receive exclusive email updates?</label>
                                                                        <div class="newsletter"><input type="checkbox" name="newsletter" value="1" <?php
                                                                                if ($model->newsletter == 1) {
                                                                                        echo "checked";
                                                                                }
                                                                                ?>> Yes, sign me up to receive Soles iLove’s offers, promotions</div>
                                                                </div>
                                                                <div class="col-sm-5 text-right">
                                                                        <button class="button button_2" name="Submit">UPDATE MY INFO</button>
                                                                </div>
                                                        </div>

                                                </div>
                                        </div>
                                </form>
                        </div>
                </div>

        </div>
</section>