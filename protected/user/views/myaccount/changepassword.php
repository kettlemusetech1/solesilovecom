<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / <a href="">Edit Address</a>
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Cart</h2>
                <div class="row">
                        <?php echo $this->renderPartial('_menu'); ?>
                        <div class="col-sm-9 profile-content-area">
                                <h1>Change Password</h1>
                                <?php if (Yii::app()->user->hasFlash('success')): ?>
                                        <div class="alert alert-success">
                                                <strong></strong> <?php echo Yii::app()->user->getFlash('success'); ?>
                                        </div>
                                <?php endif; ?>
                                <?php if (Yii::app()->user->hasFlash('notice')): ?>
                                        <div class="alert alert-danger">
                                                <strong></strong> <?php echo Yii::app()->user->getFlash('notice'); ?>
                                        </div>
                                <?php endif; ?>
                                <form  method="post" id="reset_form" action="<?= Yii::app()->baseUrl . '/index.php/Myaccount/ChangePassword' ?>" class="chng_pass_frm">
                                        <div class="profile_form">

                                                <div class="form-group">
                                                        <div class="row">

                                                                <div class="col-sm-6">
                                                                        <label for="">Current Password</label>
                                                                        <input  class="form-control" type="password" name="current" id="current" required>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-sm-6">
                                                                        <label for="">New Password</label>
                                                                        <input  class="form-control" type="password" name="password" id="password" required>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-sm-6">
                                                                        <label for="">Confirm Password</label>
                                                                        <input  class="form-control " type="password" name="confirm" id="confirm" required>
                                                                </div>
                                                        </div>
                                                </div>
                                                <div class="form-group">
                                                        <div class="row">
                                                                <div class="col-sm-6">
                                                                        <span id="error" style="color: red"></span>
                                                                </div>
                                                        </div>
                                                </div>

                                                <div class="form-group">
                                                        <div class="row">

                                                                <div class="col-sm-5 text-right">
                                                                        <button class="button button_2" name="submit">Submit</button>
                                                                </div>
                                                        </div>

                                                </div>

                                        </div>
                                </form>
                        </div>
                </div>

        </div>
</section>
<style>
        .not_ext{
                display: none;
        }
        .ship_not_ext{
                display: none;
        }
</style>
<script>
        $(document).ready(function() {
                $('#confirm').keyup(function() {
                        Check(e = '');
                });
                $('#password').keyup(function() {
                        Check(e = '');
                });
                $('#reset_form').submit(function(e) {
                        Check(e);
                });
        });
        function Check(e) {
                var pass = $('#password').val();
                var confirm = $('#confirm').val();
                if (pass != '' && confirm != '') {

                        if (pass != confirm) {
                                $('#error').html(" Password doesn't match");
                                if (e != '') {
                                        e.preventDefault();
                                }
                        } else {
                                $('#error').html("");
                        }
                } else {
                        $('#error').html("");
                }
        }
</script>