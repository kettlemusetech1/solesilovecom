<div class="col-sm-3">
        <div class="profile_menu">
                <h3><i class="fa fa-bars"></i><?= substr(Yii::app()->session['user']['first_name'], 0, 6); ?>'s Account</h3>
                <ul class="dp">
                        <li class="active"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Profile/"><i class="fa fa-user-circle-o"></i>My Profile</a></li>
                        <li class="cl1"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/CreditHistory"><i class="fa fa-credit-card"></i>My Credit</a></li>
                        <li class="cl2"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Addressbook"><i class="fa fa-address-book"></i>Address Book</a></li>
                        <li class="cl3"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Myorders"><i class="fa fa-shopping-cart"></i>My Orders</a></li>
                        <li class="cl4"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Mywishlists"><i class="fa fa-heart-o"></i>My Wishlist</a></li>
        </div>
</div>