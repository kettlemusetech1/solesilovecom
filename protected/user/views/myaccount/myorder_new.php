<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
    
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / <a href="">Men</a>  /  Shoes
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Cart</h2>
                <div class="row">
                        <?php echo $this->renderPartial('_menu'); ?>
                        <div class="col-sm-9 profile-content-area">
                                <!--                                <div class="continue">
                                                                        <a href="" class="btn btn-primary">Continue Shopping</a>
                                                                </div>-->
                                <h1>My Orders</h1>
                                <div class="my_credit_page">
                                        <?php $oid = $_GET['id']; ?>
                                        <?php
                                        $ii = 1;
                                        if ($myorder) {
                                                foreach ($myorder as $myorder) {
                                                        $order_products = OrderProducts::model()->findAllByAttributes(array('order_id' => $myorder->id));

                                                        $order_history = OrderHistory::model()->findAllByAttributes(array('order_id' => $myorder->id), array('order' => 'id  ASC'));
                                                        $latest_order_history = OrderHistory::model()->findByAttributes(array('order_id' => $myorder->id), array('order' => 'id DESC'));

                                                        $latest_stats = OrderStatus::model()->findByPk($latest_order_history->order_status)->title;
                                                        if ($latest_stats == '') {
                                                                $latest_stats = "ORDER NOT PLACED";
                                                        }
                                                        $stat = $myorder->status;
                                                        $stats = OrderStatus::model()->findByPk($stat)->title;
                                                        $enqry = ProductEnquiry::model()->findByAttributes(array('order_id' => $myorder->id));
                                                        if ($enqry != '') {
                                                                $totalamt = $enqry->total_amount + $enqry->shipping_charge;
                                                        } else {
                                                                $totalamt = $myorder->total_amount + $myorder->shipping_charge;
                                                        }
                                                        ?>
                                                        <div class="wish_table">
                                                                <div class="row table_head">
                                                                        <div class="col-xs-6">
                                                                                Product
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                                Price
                                                                        </div>
                                                                        <div class="col-xs-3 text-center">
                                                                                Status
                                                                        </div>
                                                                </div>
                                                                <?php
                                                                foreach ($order_products as $order_product) {
                                                                        $product_details = Products::model()->findByPk($order_product->product_id);
                                                                        $option_details = OptionDetails::model()->findByPk($order_product->option_id);
                                                                        ?>
                                                                        <div class="row body_table">
                                                                                <div class="col-xs-6 cart_details">
                                                                                        <div class="row cart_item_row">
                                                                                                <div class="col-xs-3 col-sm-4 cart_image">
                                                                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php echo Yii::app()->Upload->folderName(0, 1000, $product_details->id) ?>/<?php echo $product_details->id; ?>/small.<?php echo $product_details->main_image; ?>" alt="<?php echo $product_details->product_name; ?>"/>
                                                                                                </div>
                                                                                                <div class="col-xs-9 col-sm-8 cart_item_details">
                                                                                                        <h3><?php echo $product_details->product_name; ?></h3>
                                                                                                        <div class="product_srar disabled"><a href="#" class="star1 active"><i class="fa fa-star-o"></i></a><a href="#" class="star2"><i class="fa fa-star-o"></i></a><a href="#" class="star3"><i class="fa fa-star-o"></i></a><a href="#" class="star4"><i class="fa fa-star-o"></i></a><a href="#" class="star5"><i class="fa fa-star-o"></i></a></div>
                                                                                                        <div class="size_color">
                                                                                                                <?php
                                                                                                                if (!empty($option_details)) {
                                                                                                                        if ($option_details->color_id != 0) {
                                                                                                                                $colour = OptionCategory::model()->findByPk($option_details->color_id)->color_name;
                                                                                                                                ?>
                                                                                                                                <label for="" class="margin">Color:</label> <span><?= $colour; ?></span>
                                                                                                                                <?php
                                                                                                                        }
                                                                                                                        if ($option_details->size_id != 0) {
                                                                                                                                $size = OptionCategory::model()->findByPk($option_details->size_id)->size;
                                                                                                                                ?>
                                                                                                                                <label for="">SIZE: </label> <span><?= $size ?></span>
                                                                                                                                <?php
                                                                                                                        }
                                                                                                                }
                                                                                                                ?>
                                                                                                        </div>
                                                                                                        <label for="">Quantity :</label>  <?= $order_product->quantity ?>
                                                                                                        <p><span class="danger">*</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>
                                                                                <div class="col-xs-3 wish_full my-order-col">
                                                                                        <strong><?php echo Yii::app()->Currency->convert($order_product->amount); ?></strong>
                                                                                </div>
                                                                                <div class="col-xs-3 wish_full text-center green my-order-col">
                                                                                        <p><?php echo $latest_stats; ?></p>
                                                                                        <a class="history_a" order_id="<?php echo $order_product->order_id; ?>" data-toggle="modal" data-target="#statpop">View All History</a>
                                                                                </div>
                                                                        </div>
                                                                        <?php
                                                                }
                                                                ?>

                                                        </div>
                                                        <?php
                                                }
                                        } else {
                                                ?>
                                                <li>You haven't placed any orders.</li>
                                                <?php
                                        }
                                        ?>
                                </div>
                        </div>

                        <div class="modal fadeIn animated login_modal" id="statpop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog zoomIn animated" role="document">
                                        <div class="modal-content">
                                                <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <div class="login_form">
                                                                <h4>ORDER HISTORY</h4>
                                                                <div class="form-group">
                                                                        <div class="all_history">
                                                                                <div class="row">
                                                                                        <div class="col-xs-12 col-sm-6 history_data"><strong>Status</strong></div>
                                                                                        <div class="col-xs-12 col-sm-6 history_data"><strong>Date</strong></div>
                                                                                </div>
                                                                                <div class="history_result"></div>

                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>

                                        </div>
                                        <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                        </div>
                        <script>
//                                $('.statpop').click(function() {
//                                        $("#statpop").modal('show');
//                                });
                                $('.history_a').click(function() {
                                        var order_id = $(this).attr('order_id');
                                        $.ajax({
                                                type: "POST",
                                                url: baseurl + "ajax/GetOrderHistory",
                                                data: {order_id: order_id}
                                        }).done(function(data) {
                                                $(".history_result").html(data);
                                                $("#statpop").modal('show');
                                        });
                                });
                        </script>
