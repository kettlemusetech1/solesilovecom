<?php $this->load->view('includes/_header'); ?>
<section class="inner_content_section"><!--inner_content_section-->

        <div class="wrapper">

                <?php $this->load->view('includes/_side_left_1'); ?>

                <article class="product_mainbox inner_mainbox">
                        <h2>Property Details</h2>
                        <div class="page_content">
                                <!--**********************************************************-->
                                <?php if (isset($msg)) echo $msg; ?>

                                <?php
                                if (isset($query)) {
                                        ?>
                                        <?php
                                        if ($query->num_rows() > 0) {
                                                $row = $query->row();
                                                ?>


                                                <table class="report_tbl">
                                                        <tr>
                                                                <td><label>Property ID</label></td>
                                                                <td>
                <?php echo $row->list_id; ?>
                                                                </td>
                                                        </tr>

                                                        <tr>
                                                                <td><label>Property Type</label></td>
                                                                <td>
                <?php echo get_option_name($property_types, $row->prop_type_id); ?>
                                                                </td>
                                                        </tr>


                                                        <tr>
                                                                <td><label>Transaction Type</label></td>
                                                                <td>
                <?php echo get_option_name($transaction_types, $row->trans_type_id); ?>
                                                                </td>
                                                        </tr>

                                                        <tr>
                                                                <td><label>Expected Price (Rs.)</label></td>
                                                                <td>Rs. <?php echo number_format($row->price_exp); ?> / <?php echo $row->price_area_unit; ?></td>
                                                        </tr>

                                                        <tr>
                                                                <td><label>Land Area</label></td>
                                                                <td><?php echo $row->land_area; ?> <?php echo get_option_name($area_units, $row->land_area_unit_id); ?>
                                                                </td>
                                                        </tr>

                                                        <tr>
                                                                <td><label>Built-up Area (Sq.Ft.)</label></td>
                                                                <td><?php echo $row->built_up_area; ?> Sq.Ft.</td>
                                                        </tr>


                                                        <tr>
                                                                <td><label>District</label></td>
                                                                <td>
                <?php if ($row->sub_region_id == 15) {
                        echo $row->other_loction;
                } else {
                        echo get_option_name($sub_regions, $row->sub_region_id);
                } ?>
                                                                </td>
                                                        </tr>

                                                        <tr>
                                                                <td><label>Location</label></td>
                                                                <td><?php echo $row->location; ?></td>
                                                        </tr>

                                                        <tr>
                                                                <td><label>Property Description</label></td>
                                                                <td><?php echo $row->prop_desc; ?></td>
                                                        </tr>

                                                        <tr>
                                                                <td colspan="2"><label>PHOTOS</label></td>
                                                        </tr>

                                                        <tr>
                                                                <td valign="middle">Photo 1 (Main Photo)</td>
                                                                <td><img src="<?php echo base_url('properties_upload'); ?>/<?php echo $row->photo_1; ?>" style="width:200px;"></td>
                                                        </tr>

                                                        <tr>
                                                                <td valign="middle">Photo 2</td>
                                                                <td><img src="<?php echo base_url('properties_upload'); ?>/<?php echo $row->photo_2; ?>" style="width:200px;"></td>
                                                        </tr>

                                                        <tr>
                                                                <td valign="middle">Photo 3</td>
                                                                <td><img src="<?php echo base_url('properties_upload'); ?>/<?php echo $row->photo_3; ?>" style="width:200px;"></td>
                                                        </tr>

                                                        <tr>
                                                                <td valign="middle">Photo 4</td>
                                                                <td><img src="<?php echo base_url('properties_upload'); ?>/<?php echo $row->photo_4; ?>" style="width:200px;"></td>
                                                        </tr>
                                                        <tr>
                                                                <td style="text-align:center;">
                                                                        <a href="<?php echo site_url('property/sell_list_edit') . "/" . $row->list_id; ?>" style="background-color:#009933;color:#fff;padding:5px; "/>Edit Property</a></td>
                                                                <td style="text-align:center;">
                                                                        <a href="<?php echo site_url('property/sell_list_photo_edit') . "/" . $row->list_id; ?>" style="background-color:#009933;color:#fff;padding:5px; "/>Edit Photos</a></td>
                                                        </tr>
                                                </table>
                                                <?php echo form_close(); ?>
                                                <?php
                                        } else {
                                                ?>
                                                <p>No data available.</p>
                                                <?php
                                        }
                                }
                                ?>

                                <!--**********************************************************-->
                        </div>
                </article>
                <?php $this->load->view('includes/_side_left_2'); ?>

        </div>
</section><!--inner_content_section-->
<?php $this->load->view('includes/_footer'); ?>