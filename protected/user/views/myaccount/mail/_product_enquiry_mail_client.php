<?php echo $this->renderPartial('//site/mail/_email_header'); ?>
<tr><td>
                <?php
                $user = UserDetails::model()->findByPk($model->user_id);
                ?>
        </td></tr>
<tr><td>
                <h4 style="padding: 5px 9px 6px 9px;font-family:'Open Sans',arial, sans-serif; font-size:13px">Dear <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>,<br/><br/> Greetings from Artstra.com!</h4>
                <h5 style="padding: 5px 9px 6px 9px;font-family:'Open Sans',arial, sans-serif; font-size:13px">Thank you for taking time to fill in the form. Your measurement  details added successfully. We will contact you if additional information is required.</h5>
        </td></tr>

<tr><td><a href="<?php echo $this->siteURL(); ?>/Myaccount/ViewChart/<?php echo $sizeno->id; ?>" style="    text-transform: uppercase;
           background-color: #ec9721;
           border-radius: 0;
           outline: none;
           height: 40px;
           line-height: 40px;
           padding: 9px 28px;
           border: solid 1px #ec9721;
           border-radius: 4px;
           margin-left: 37%;
           text-align: center;
           text-decoration: none;
           color: #fff;font-weight:bold">View Measurement Details</a></td></tr>
<br/>
<p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#acacb1;">* This is an automatically generated email, please do not reply to this email.</p>
<p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;color: #abaaaa; text-align:center;">* If you need assistance, please call +91 914 220 2222 during office hours: Mon to Sat 9:30am to 6:30pm IST or send an email to support@artstra.com</p>



<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>