<?php echo $this->renderPartial('//site/mail/_email_header'); ?>
<tr><td>
                <?php
                $user = UserDetails::model()->findByPk($model->user_id);
                ?>
        </td></tr>
<tr><td>
                <h4 style="padding: 5px 9px 6px 9px;font-family:'Open Sans',arial, sans-serif; font-size:13px">Dear <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>,<br/><br/> Greetings from Artstra.com!</h4>
                <h5 style="padding: 5px 9px 6px 9px;font-family:'Open Sans',arial, sans-serif; font-size:13px">Thank you for taking time to fill in the form. Your measurement  details added successfully. We will contact you if additional information is required.</h5>
        </td></tr>

<tr>
        <td style="padding:40px 20px; font-family:'Open Sans',arial, sans-serif; font-size:13px"><p>Hi Admin,<br/><br/>There is a new enquiry.</p>
                <?php if ($sizeno->type == 1) { ?>


                        <table id="Table_01"  border="0" cellpadding="0" cellspacing="0" align="left" style="padding:13px 0px; font-family:'Open Sans',arial, sans-serif; font-size:13px">
                                <tbody>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Fitting:</p></td>
                                                <td class="col_measure" style="padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"> Standard                                                                                               </td>
                                        </tr>
                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Size:</p></td>
                                                <td class="col_measure" style="padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"> <?php
                                                    if ($sizeno->standerd == 1) {
                                                            echo 'XS';
                                                    } elseif ($sizeno->standerd == 2) {
                                                            echo 'S';
                                                    } elseif ($sizeno->standerd == 3) {
                                                            echo 'M';
                                                    } elseif ($sizeno->standerd == 4) {
                                                            echo 'L';
                                                    } elseif ($sizeno->standerd == 5) {
                                                            echo 'XL';
                                                    } elseif ($sizeno->standerd == 6) {
                                                            echo 'XXL';
                                                    } elseif ($sizeno->standerd == 7) {
                                                            echo 'XXXL';
                                                    }
                                                    ?>                                                                                                </td>
                                        </tr>


                                </tbody>
                        </table>
                <?php } else { ?>
                        <table id="Table_01"  border="0" cellpadding="0" cellspacing="0" align="left" style="padding:13px 0px; font-family:'Open Sans',arial, sans-serif; font-size:13px">
                                <tbody>
                                        <tr>

                                                <th class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;">Custom Fitting </th>
                                                <th class="col_value" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;">Value</th>
                                        </tr>
                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Neck:</p></td>
                                                <td class="col_measure" style="padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"> <?php echo $sizeno->around_neck; ?>                                                                                                </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Neck Depth Front/ Back:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->neck_depth; ?>                                                                                                </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Upper Bust:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_upper_chest; ?>                                                                                                </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Bust:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_chest; ?>                                                                                               </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Lower Bust:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_lower_chest; ?>                                                                                               </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Shoulder to Bustpoint:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->shoulder_to_breastpoint; ?>                                                                                               </td>


                                        </tr><tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Waist:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_waist; ?>                                                                                                </td>


                                        </tr><tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Saree Blouse/Bodice Length:<br>(Give Total Length from Shoulder)</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->shoulder_to_waist; ?>                                                                                               </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Armhole:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_armhole; ?>                                                                                                </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Sleeve Length:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->sleeve_length; ?>                                                                                               </td>


                                        </tr><tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Arm Length:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->arm_length; ?>                                                                                                </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Upper Arm:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_upper_arm; ?>                                                                                            </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Elbow:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_elbow; ?>                                                                                             </td>


                                        </tr><tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Wrist:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_wrist; ?>                                                                                            </td>


                                        </tr><tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Top/Kameez/Kurthi Length:<br>(Give Total Length from Shoulder)</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->length_upper_garment; ?>                                                                                               </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Shoulder Width:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->shoulder_width; ?>                                                                                               </td>
                                        </tr>


                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Lower Waist:<br>(Skirt/Salwar/Pant Waist)</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_lower_waist; ?>                                                                                              </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Waist To Ankle:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->waist_to_ankle; ?>                                                                                              </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Inseam To Ankle:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->inseam_to_ankle; ?>                                                                                            </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Hip:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"> <?php echo $sizeno->around_hip; ?>                                                                                               </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Tigh:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_tigh; ?>                                                                                               </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Knee:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_knee; ?>                                                                                               </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Calf:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_calf; ?>                                                                                               </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Around Ankle:</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->around_ankle; ?>                                                                                                </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Skirt/Salwar/Pant Length:<br>(Give Total Length from Waist)</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->skirt_length; ?>                                                                                                </td>
                                        </tr>

                                        <tr>

                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Dress/Gown Full Length:<br>(Give Total Length from Shoulder)</p></td>
                                                <td class="col_measure" style="    padding: 0px 9px 0px 9px;
                                                    border-left: 1px solid #eaeaea;
                                                    border-bottom: 1px solid #eaeaea;
                                                    border-right: 1px solid #eaeaea;
                                                    border-top: 1px solid #eaeaea;"><?php echo $sizeno->gown_full_length; ?>                                                                                              </td>
                                        </tr>
                                </tbody>
                        </table>

                <?php } ?>
        </td>
</tr>
<br/>
<p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#acacb1;">* This is an automatically generated email, please do not reply to this email.</p>
<p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;color: #abaaaa; text-align:center;">* If you need assistance, please call +91 914 220 2222 during office hours: Mon to Sat 9:30am to 6:30pm IST or send an email to support@artstra.com</p>



<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>