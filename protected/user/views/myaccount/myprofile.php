<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="">Home</a> / <a href="">Myaccount</a>
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">My Profile</h2>
                <div class="row">
                        <?php echo $this->renderPartial('_menu'); ?>
                        <div class="col-sm-9 profile-content-area">
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/Myaccount/Profileedit/" class="pull-right profile_top_btns button">EDIT PROFILE</a>
                                <h1>Profile</h1>
                                <?php if (Yii::app()->user->hasFlash('success')): ?>
                                        <div class="alert alert-success">
                                                <strong></strong> <?php echo Yii::app()->user->getFlash('success'); ?>
                                        </div>
                                <?php endif; ?>
                                <?php if (Yii::app()->user->hasFlash('notice')): ?>
                                        <div class="alert alert-danger">
                                                <strong></strong> <?php echo Yii::app()->user->getFlash('notice'); ?>
                                        </div>
                                <?php endif; ?>
                                <div class="profile_feilds">
                                        <ul>
                                                <?php $user = UserDetails::model()->findByPk(array('id' => Yii::app()->session['user']['id'])); ?>
                                                <li><i class="fa fa-user-circle-o"></i><?= $user->first_name; ?></li>
                                                <li><i class="fa fa-envelope"></i> <?= $user->email; ?></li>
                                                <li><i class="fa fa-phone"></i> <?= $user->phone_no_2; ?> </li>

                                        </ul>
                                </div>
                        </div>
                </div>

        </div>
</section>