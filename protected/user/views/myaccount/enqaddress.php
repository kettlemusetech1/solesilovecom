<?php
/* @var $this UserAddressController */
/* @var $model UserAddress */
/* @var $form CActiveForm */
?>
<style>

        .not_ext{
                display: none;

        }
        .ship_not_ext{
                display: none;

        }
</style>

<div class="container main_container inner_pages ">
        <div class="breadcrumbs"> <?php echo CHtml::link('HOME', array('site/index')); ?> <span>/</span> <?php echo CHtml::link('My Account', array('Myaccount/index')); ?> <span>/</span> New Address </div>
        <div class="row">
                <?php echo $this->renderPartial('_menu'); ?>
                <div class="col-sm-9 user_content">
                        <h1>New Address <?php echo CHtml::link('Continue Shopping', array('products/category?name=women'), array('class' => 'account_link pull-right')); ?></h1>
                        <?php if (Yii::app()->user->hasFlash('success')): ?>
                                <div class="alert alert-success">
                                        <?php echo Yii::app()->user->getFlash('success'); ?>
                                </div>
                        <?php endif; ?>
                        <?php if (Yii::app()->user->hasFlash('notice')): ?>
                                <div class="alert alert-danger">
                                         <?php echo Yii::app()->user->getFlash('notice'); ?>
                                </div>
                        <?php endif; ?>
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'user-address-form',
                            'action' => Yii::app()->baseUrl . '/index.php/Myaccount/Enqaddress/id/'.$id,
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation' => false,
                        ));
                        ?>

                        <?php
                        $user1 = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                        $model->first_name = $user1->first_name;
                        ?>
                        <?php $model->last_name = $user1->last_name; ?>
                        <?php $model->country = $user1->country; ?>
                        <?php $model->contact_number = $user1->phone_no_2; ?>
                        <?php //if($form->errorSummary($model) != ""){ echo 'hai';}  ?>
<?php if($form->errorSummary($model) != ""){ ?>
<script>
$( document ).ready(function() {
 $('#bill_exist').find('option').each( function() {
      var $this = $(this);
      if ($this.val() == 0) {
         $this.attr('selected','selected');
         return false;
      }
 });
   $(".enq_address_book").show();
});
</script>
<?php }

?>



                        <div class="registration_form">

  <div class="row">
<div class="col-sm-12">
                                                       <label for="UserAddress_country" class="required">Pick an address from your address book</label>
                                                        <select  name="enq_address" class="enq_address_exist form-control" id="bill_exist">
                                                                <option  value="0">New Address</option>
                                                                <?php
                                                 if(count($model1) > 0){
                                                                foreach ($model1 as  $mod) {
                                                                        ?>
                                                                        <?php if ($mod->address_for == '') { ?>
                                                                                <option <?php
                                                                                if ($mod->default_billing_address == 1) {
                                                                                        echo 'selected';
                                                                                }
                                                                                ?>  value="<?php echo $mod->id; ?>"><?php echo $mod->first_name; ?> <?php echo $mod->last_name; ?> ,   <?php echo $mod->address_1; ?>
                                                                                        <?php echo $mod->address_2; ?> , <?php echo $mod->city; ?> ,
                                                                                        <?php echo $mod->state; ?> , <?php echo Countries::model()->findByPk($mod->country)->country_name; ?>
                                                                                        <?php echo $mod->postcode; ?></option>
                                                                        <?php } else { ?>
                                                                                <option <?php
                                                                                if ($mod->default_billing_address == 1) {
                                                                                        echo 'selected';
                                                                                }
                                                                                ?>  value="<?php echo $mod->id; ?>"><?php echo $mod->address_for; ?></option>
                                                                                <?php } ?>


                                                                        <?php
                                                                        
                                                                }
}
                                                                ?>
                                                        </select>
</div>
  
                                                </div>
<div class="enq_address_book" style="<?php if(count($model1) > 0){ echo 'display:none;';} ?>">

                                <div class="row">
                                        <div class="col-sm-6">
 <?php echo $form->labelEx($model, 'country'); ?>
                                                <?php echo CHtml::activeDropDownList($model, 'country', CHtml::listData(Countries::model()->findAll(), 'id', 'country_name'), array('class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'country'); ?>
                                                
                                        </div>
<div class="col-sm-2">
                                                 <?php echo $form->labelEx($model, ' Code', array('class' => 'control-label')); ?>
                                                                      
                                                                        <?php
                                                                        $phonecode = Countries::model()->findByPk(99)->phonecode;
                                                                        $state_options = array();
                                                                       
                                                                                $states = Countries::model()->findAll();
                                                                                if (!empty($states)) {
                                                                                        $state_options[""] = "--Select--";
                                                                                        foreach ($states as $state) {
                                                                                                $state_options[$state->phonecode] = '+'.$state->phonecode;
                                                                                        }
                                                                                } else {
                                                                                        $state_options[""] = "--Code--";
                                                                                        $state_options[0] = "Other";
                                                                                }
                                                                        
                                                                        ?>
                                                                        <?php echo CHtml::activeDropDownList($model, 'phonecode', $state_options, array('class' => 'form-control aik', 'options' => array($phonecode => array('selected' => 'selected')))); ?>
                                                                        <?php echo $form->error($model, 'phonecode'); ?>


                                        </div>
                                        <div class="col-sm-4">
                                                <?php echo $form->labelEx($model, 'contact_number'); ?>
                                                <?php echo $form->textField($model, 'contact_number', array('size' => 40, 'maxlength' => 100, 'class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'contact_number'); ?>
                                        </div>

                                </div>
                                <div class="row">
                                        <div class="col-sm-6">
                                                <?php echo $form->labelEx($model, 'first_name'); ?>
<input type="hidden" value="<?php echo $id;?>"  name="enq_id">
                                                <?php echo $form->textField($model, 'first_name', array('size' => 40, 'maxlength' => 100, 'class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'first_name'); ?>
                                        </div>
                                        <div class="col-sm-6">
                                                <?php echo $form->labelEx($model, 'last_name'); ?>
                                                <?php echo $form->textField($model, 'last_name', array('size' => 40, 'maxlength' => 100, 'class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'last_name'); ?>
                                        </div>
                                </div>
<div class="row">
                                        <div class="col-sm-6">
                                                <?php echo $form->labelEx($model, 'address_1'); ?>
                                                <?php echo $form->textArea($model, 'address_1', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'address_1'); ?>
                                        </div>
                                        <div class="col-sm-6">
                                                <?php echo $form->labelEx($model, 'address_2'); ?>
                                                <?php echo $form->textArea($model, 'address_2', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'address_2'); ?>
                                        </div>
                                </div>
 <div class="row">
                                        <div class="col-sm-6">
                                             


<?php echo $form->labelEx($model, 'state', array('class' => 'control-label')); ?>
                                                                        <div class="cn_ext">


                                                                                <?php
                                                                                //  echo Yii::app()->session['user']['country'];
                                                                                $state_options = array();
                                                                                if (Yii::app()->session['user']['country'] != 0) {
                                                                                        $states = States::model()->findAllByAttributes(array('country_id' => 99));

                                                                                        if (!empty($states)) {
                                                                                                $state_options[""] = "--Select--";
                                                                                                foreach ($states as $state) {
                                                                                                        $state_options[$state->state_name] = $state->state_name;
                                                                                                }
                                                                                        } else {
                                                                                                $state_options[""] = "--Select--";
                                                                                                $state_options[0] = "Other";
                                                                                        }
                                                                                } else {
                                                                                        $state_options[""] = '--select--';
                                                                                }
                                                                                ?>
                                                                                <?php echo CHtml::activeDropDownList($model, 'state', $state_options, array('class' => 'form-control aik', 'options' => array('id' => array('selected' => 'selected')))); ?>
                                                                        </div>
                                                                        <div class="not_ext">
                                                                         <input placeholder="state " class="form-control aik" name="UserAddress[state1]" id="UserAddress_state" type="text" maxlength="111">

                                                                        </div>
                                                                        <?php echo $form->error($model, 'state'); ?>
                                        </div>

<div class="col-sm-6">
                                                 <?php echo $form->labelEx($model, 'city'); ?>
                                                <?php echo $form->textField($model, 'city', array('size' => 40, 'maxlength' => 100, 'class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'city'); ?>

                                                                    
                                                                </div>


                                       
                                </div>
                               <!-- <div class="row">
                                        <div class="col-sm-6">
                                                <?php echo $form->labelEx($model, 'company'); ?>
                                                <?php echo $form->textField($model, 'company', array('size' => 40, 'maxlength' => 100, 'class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'company'); ?>
                                        </div>
                                        
                                </div>-->
                                
                                <div class="row">
                                        <div class="col-sm-6">
<?php echo $form->labelEx($model, 'postcode'); ?>
                                                <?php echo $form->textField($model, 'postcode', array('size' => 40, 'maxlength' => 111, 'class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'postcode'); ?>
                                                </div>
                                        <div class="col-sm-6">
<?php echo $form->labelEx($model, 'Address name'); ?>
                                                <?php echo $form->textField($model, 'address_for', array('size' => 40, 'maxlength' => 100, 'class' => 'form-control',Placeholder=>'Give this address a name')); ?>
                                                <?php echo $form->error($model, 'address_for'); ?> 
                                               <!-- <?php echo $form->labelEx($model, 'postcode'); ?>
                                                <?php echo $form->textField($model, 'postcode', array('size' => 40, 'maxlength' => 111, 'class' => 'form-control')); ?>
                                                <?php echo $form->error($model, 'postcode'); ?>-->
                                        </div>
                                </div>
                               
                                <div class="row">
                                        <div class="col-sm-12" style="text-align:center;">
<div class="c_check"><input class="form-controlbtn" id="UserAddress_default_shipping_address" hidden="" name="UserAddress[default_shipping_address]" value="1" type="checkbox"></div>

<?php //echo $form->checkBox($model, 'default_shipping_address', array('class' => 'form-controlbtn','style' => 'height: 18px; width: 18px;')); ?>
                                       Make this my default shipping address        <!-- <?php echo $form->labelEx($model, '&nbsp;&nbsp;Make this my default shipping address'); ?>-->
                                                
                                                <?php echo $form->error($model, 'default_shipping_address'); ?>
                                        </div>
                                </div>

</div>
                                <div class="btn-cntr" style="margin-left: 27px;">
                                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Submit', array('class' => 'btn-primary')); ?>

<button type="reset" style="border: none;
    background: transparent;
    font-size: 13px;
    text-decoration: underline;
    color: #23527c;">Cancel</button>
                                </div>
                        </div>

                        <?php $this->endWidget(); ?>

                </div><!-- form -->
        </div>
<style>
                        input.form-control::-webkit-input-placeholder {color: #8f8f92 !important; }
                        input.form-control::-moz-placeholder {color: #8f8f92 !important; }
                        input.form-control:-ms-input-placeholder {color: #8f8f92 !important; }
                        input.form-control:-moz-placeholder {color: #8f8f92 !important; }


                </style>
</div>
</div>
<script>

$( document ).ready(function() {
var extvalue =  $( ".enq_address_exist ").val();
if(extvalue == 0){
 $('.enq_address_book').show();
 }else{
 $('.enq_address_book').hide();
 }


   $( ".enq_address_exist ").change(function() {
 var value = $(this).val();
 if(value == 0){
 $('.enq_address_book').show();
 }else{
 $('.enq_address_book').hide();
 }
});

});
</script>