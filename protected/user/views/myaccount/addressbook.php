<?php echo $this->renderPartial('//site/mainmodal'); ?>
<section>
        <div class="container content-body listings_page dashboard-page">
                <div class="breadcrumb">
                        <a href="<?php Yii::app()->baseUrl;?>/index.php/Myaccount/index">Home</a> / <a href="">Addressbook</a>  /
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg mobile_page_header">Cart</h2>
                <div class="row">
                        <?php echo $this->renderPartial('_menu'); ?>
                        <div class="col-sm-9 profile-content-area">
                            <?php if (Yii::app()->user->hasFlash('success')): ?>
                                        <div class="alert alert-success">
                                                <strong></strong> <?php echo Yii::app()->user->getFlash('success'); ?>
                                        </div>
                                <?php endif; ?>
                                <a href="<?= Yii::app()->baseUrl; ?>/index.php/Myaccount/NewAddress/" class="pull-right profile_top_btns button">Add New Address</a>
                                <h1>Address Book</h1>
                                <div class="address_books">
                                        <div class="row">
                                                <?php
                                                $con = count($model);
                                                if ($con > 0) {
                                                        foreach ($model as $address) {
                                                                $con = count($address);
                                                                $country = Countries::model()->findByPk($address->country);
                                                                ?>
                                                                <div class="col-sm-6">
                                                                        <div class="panel panel-default">
                                                                                <div class="panel-body">
                                                                                        <p><?php echo $address->first_name; ?> <?php echo $address->last_name; ?><br/>
                                                                                                <?php echo $address->contact_number; ?><br/>
                                                                                                <?php
                                                                                                if ($address->address_1 != '') {
                                                                                                        echo $address->address_1;
                                                                                                }
                                                                                                ?><br/>


                                                                                                <?php
                                                                                                if ($address->address_2 != '') {
                                                                                                        echo $address->address_2;
                                                                                                }
                                                                                                ?><br/>
                                                                                                <?php echo $country->country_name; ?><br/>
                                                                                                <?php echo $address->state; ?><br/>
                                                                                                <?php echo $address->city; ?><br/>
                                                                                                <?php echo $address->postcode; ?><br/>
                                                                                                <a href="<?php echo Yii::app()->baseUrl; ?>/index.php/Myaccount/EditAddress/<?php echo $address->id; ?>" class="button button_2">EDIT</a> <a class="button" onclick="deleteaddress(<?php echo $address->id; ?>)">DELETE</a>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <?php
                                                        }
                                                }
                                                ?>

                                        </div>
                                </div>
                        </div>
                </div>

        </div>
</section>



<script>
        function deleteaddress(id)
        {
                var r = confirm("Are you sure you want to delete shipping address and billing address??");
                if (r == true)
                {
                        $.ajax({
                                type: "GET",
                                url: baseurl + 'Myaccount/DeleteAddress',
                                data: ({id: id}),
                                success: function(data)
                                {
                                        window.location.replace("<?= Yii::app()->baseUrl; ?>/index.php/Myaccount/Addressbook/" + id);
                                }
                        });
                }
                else
                {
                        window.location.replace("<?= Yii::app()->baseUrl; ?>/index.php/Myaccount/Addressbook/" + id);
                }
        }
</script>

<script>
        function deleteaddress(id)
        {
                var r = confirm("Are you sure you want to delete shipping address and billing address??");
                if (r == true)
                {
                        $.ajax({
                                type: "GET",
                                url: baseurl + 'Myaccount/DeleteAddress',
                                data: ({id: id}),
                                success: function(data)
                                {
                                        window.location.replace("<?= Yii::app()->baseUrl; ?>/index.php/Myaccount/Addressbook/" + id);
                                }
                        });
                }
                else
                {
                        window.location.replace("<?= Yii::app()->baseUrl; ?>/index.php/Myaccount/Addressbook/" + id);
                }
        }
</script>