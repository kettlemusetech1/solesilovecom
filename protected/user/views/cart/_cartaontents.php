<?php
$options = OptionDetails::model()->findByPk($cart_content->options);
$color_exist = OptionImages::model()->findByAttributes(array('color_id' => $options->color_id, 'product_id' => $prod_details->id));
?>


<li class="prd_rws_small">
        <div class="prdt_img">
                <a href="">
                        <?php if (!empty($color_exist)) { ?>
                                <img src = "<?php echo Yii::app()->request->baseUrl; ?>/uploads/producttoption/<?php echo $folder; ?>/<?php echo $color_exist->id; ?>/small.<?php echo $color_exist->image; ?>" />
                        <?php } else { ?>
                                <img src = "<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php echo $folder; ?>/<?php echo $prod_details->id; ?>/small.<?php echo $prod_details->main_image; ?>" />
                        <?php } ?>
                </a>

        </div>
        <div class="cart_prdt_details">
                <h3><a href="#"><?php echo $prod_details->product_name; ?><span> ( ID : <?php echo $prod_details->id; ?> )</span></a></h3>
                <div class="meta">
                        <?php if ($options->color_id != 0) { ?><p>Color:	<?php echo OptionCategory::model()->findByPk($options->color_id)->color_name; ?></p> <?php } ?>
                        <?php if ($options->size_id != 0) { ?><p>Size: <?php echo OptionCategory::model()->findByPk($options->size_id)->size; ?> </p><?php } ?>
                        <?php if ($options->width_id != 0) { ?><p>Width Fittings: <?php echo WidthFitting::model()->findByPk($options->width_id)->title; ?> </p><?php } ?>
                        <p>Quantity: <?php echo $cart_content->quantity; ?></p>
                        <?php
                        if (Yii::app()->controller->id != "checkOut") {
                                ?>
                                <p><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/cart/Delete/<?php echo $cart_content->id; ?>" class="remove">Remove</a></p>
                        <?php } ?>
                </div>

        </div>
        <div class="prdt_price">
                <p>Price</p>
                <?php echo $prod_details->price; ?>
        </div>
</li>