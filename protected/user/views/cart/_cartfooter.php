<div class="order-sub-total">
        <h4>ORDER SUB TOTAL</h4>
        <h5><?php echo Yii::app()->Currency->convert($subtotoal); ?></h5>
        <div class="clearfix"></div>
</div>

<div class="order-btns">
        <div class="row">
                <div class="col-xs-4">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/cart/Mycart/" class="butter btn btn-default btn-1 waves-effect waves-float waves-light">Checkout</a>
                </div>
                <div class="col-xs-3">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/cart/Mycart/" class="butter btn btn-default btn-2 waves-effect waves-float waves-light">View All</a>
                </div>
                <div class="col-xs-5">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/<?= Yii::app()->session['category_typ'] == 1 ? 'men' : 'women'; ?>" class="butter btn btn-default btn-3 waves-effect waves-float waves-light">Continue Shopping</a>
                </div>
        </div>
</div>