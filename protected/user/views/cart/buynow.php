s<?php echo $this->renderPartial('//site/mainmodal'); ?>
<style>
        h4.cart_qty_check {
                color: red;
                font-weight: 600;
                margin-left: 25px;
        }
</style>
<section>
        <div class="container content-body listings_page cart-page">
                <div class="breadcrumb">
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>">Home</a>  /  Cart
                </div>
                <h2 class="hidden-sm hidden-md hidden-lg">Cart</h2>
                <div class="row cart_details">
                        <div class="col-sm-9">
                                <div class="panel panel-default cart_panel">
                                        <div class="panel-heading cart_panel_head">
                                                <h1>YOUR CART</h1>
                                        </div>
                                        <div class="panel-body cart_panel_body">
                                                <?php
                                                foreach ($carts as $cart) {
                                                    $prod_details = Products::model()->findByPk($cart->product_id);
                                                    $folder = Yii::app()->Upload->folderName(0, 1000, $prod_details->id);
                                                    $colors = OptionDetails::model()->findByPk($cart->options);
                                                    $sizes = OptionDetails::model()->findByPk($cart->options);
                                                    if (!empty($colors)) {
                                                        $color_name = OptionCategory::model()->findByPk($colors->color_id);
                                                    }
                                                    if (!empty($sizes)) {
                                                        $size_name = OptionCategory::model()->findByPk($sizes->size_id);
                                                    }
                                                    if(($sizes->width_id != 0)){
                                                        $widthset= WidthFitting::model()->findByPk($sizes->width_id)->title;
                                                    }
                                                    ?>
                                                    <div class="row cart_item_row" id="cart_<?php echo $cart->id; ?>">
                                                            <h4 class='cart_qty_check'></h4>
                                                            <div class="col-xs-3 cart_image">
                                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php echo $folder; ?>/<?php echo $prod_details->id; ?>/small.<?php echo $prod_details->main_image; ?>" class="img-responsive crt" align="absmiddle" style="display: block;">
                                                            </div>
                                                            <div class="col-xs-5 cart_item_details">
                                                                    <h3><a href="<?= Yii::app()->request->baseUrl; ?>/index.php/product/<?php echo $prod_details->canonical_name; ?>"><?php echo $prod_details->product_name; ?></a></h3>
                                                                   <?php $product_reviews = UserReviews::model()->findAllByAttributes(['product_id' => $cart->product_id]); ?>
                                    <div class="product_srar disabled">
                                            <ul class="list-unstyled list-inline">
                                                    <?php
                                                    $cn = count($product_reviews);
                                                    foreach ($product_reviews as $product_review) {
                                                        $total_r += $product_review->rating;
                                                    }
                                                    if ($cn > 0) {
                                                        $cn = $cn;
                                                    } else {
                                                        $cn = 1;
                                                    }
                                                    $total_rating = ceil($total_r / $cn);
                                                    $j = $total_rating;
                                                    $k = 5 - $j;
                                                    ?>
                                                    <?php
                                                    for ($i = 1; $i <= $j; $i++) {
                                                        ?>
                                                        <li><i class="fa stars fa-star"></i></li>
                                                    <?php } ?>
                                                    <?php
                                                    for ($i = 1; $i <= $k; $i++) {
                                                        ?>
                                                        <li><i class="fa stars fa-star blank"></i></li>
                                                    <?php }
                                                    ?>
                                                    <?php
                                                    if ($total_rating == 0) {
                                                    }
                                                    ?>

                                            </ul>
                                    </div>
                                                                    
                                                                    <div class="size_color">
                                                                            <?php if (!empty($size_name)) { ?> <span class="tds"><label for="">SIZE: </label> <span style="margin-right: 20px;"><?php echo $size_name->size; ?></span></span> <?php } ?> 
                                                                            <?php if (!empty($color_name)) { ?> <span class="tds"><label for="" >Color:</label> <span style="width: 30px; height: 30px;padding-bottom: 6px; padding-top: 6px; background-image:url('<?php echo Yii::app()->request->baseUrl; ?>/uploads/colors/<?php echo $color_name->id; ?>.<?php echo $color_name->image; ?>'); background-size: cover;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><?php } ?>
                                                                            <?php if (($sizes->width_id != 0)){ ?> <span class="tds"><label for="">WIDTH: </label> <span style="margin-right: 20px;"><?php echo $widthset; ?></span></span><?php }?>
                                                                    </div>
                                                                    <div class="button_groups">
                                                                            <a href="<?= Yii::app()->request->baseUrl; ?>/index.php/cart/Delete/<?= $cart->id; ?>"> <button class="button button_remove">REMOVE</button></a>
                                                                            <a href="<?= Yii::app()->request->baseUrl; ?>/index.php/products/updateproduct/name/<?php echo $prod_details->canonical_name; ?>/cart/<?= base64_encode($cart->id); ?>"><button class="button button_add">EDIT</button></a>
                                                                    </div>
                                                                    

                                                            </div>
                                                            <div class="col-xs-2 cart_quantity">
                                                                    <div class="form-group">
                                                                            <div class="input-group">
                                                                                    <div product_id="<?php echo $prod_details->id; ?>" cart="<?php echo $cart->id; ?>" add_type="1" class="input-group-addon qminus qty_no"><a href="#minus" class="qminus_a"><i class="fa fa-angle-left"></i></a></div>
                                                                                    <input product_id="<?php echo $prod_details->id; ?>" cart="<?php echo $cart->id; ?>" type="text" class="form-control mquantity" id="quantity1" readonly placeholder="Qty" value="<?php echo $cart->quantity; ?>">
                                                                                    <div product_id="<?php echo $prod_details->id; ?>" cart="<?php echo $cart->id; ?>" add_type="2" class="input-group-addon qplus qty_no"><a href="#plus" class="qplus_a"><i class="fa fa-angle-right"></i></a></div>
                                                                            </div>
                                                                    </div>
                                                            </div>
                                                            
                                                            
                                                            <?php
                                                            $trimstring = substr($prod_details->description, 0, 100);
                                                            $price = Yii::app()->Discount->DiscountAmount($prod_details);
                                                            $cart_qty = $cart->quantity;
                                                            $tot_price = ($cart_qty * $price);
                                                            $cart_rate = $cart->rate;
                                                            ?>
                                                            <div class="col-xs-2">
                                                                    <div class="product_price text-right "><strong class="range_<?php echo $cart->id; ?>"><?php echo Yii::app()->Currency->convert($tot_price); ?></strong></div>
                                                            </div>
                                                    </div>
                                                    <?php
                                                    $total+= $tot_price;
                                                }
                                                ?>

                                        </div>
                                </div>
                                <div class="cart_info hidden_phone">
                                        <div class="row">
                                                <div class="col-xs-12 ita">
                                                        <h2>Free Shipping</h2>
                                                        <?php
                                                        $stat = StaticPage::model()->findByPk(4);
                                                        ?>
                                                        
                                                        Qualify for free shipping on your first order when registering with Soles iLove.
                                                            <a href="JavaScript:Void(0);" class="newuser continue">Click here</a> to register
                                                    
                                                </div>
                                                
                                        </div>
                                </div>
                        </div>
                        <div class="col-sm-3 summary_col">
                                <div class="cart_summary">
                                        <h2>Summary</h2>
                                        <div class="summary_row shipping">
                                                <div class="col-left">
                                                        <label for="">SUB TOTAL </label>
                                                </div>
                                                <dov class="col-right">
                                                        <strong id="ordertotal"><?php echo Yii::app()->Currency->convert($total); ?></strong>
                                                </dov>
                                        </div>
                                        <div class="summary_row shipping">
                                                <div class="col-left">
                                                        <label for="">Shipping </label>
                                                </div>
                                                <dov class="col-right">
                                                        <strong>
                                                                <span id="shipping">
                                                                        <?php
                                                                        $user = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                                                                        if (!empty($user)) {
                                                                                if ($user->first_purchase == 0 && $user->account_type == 1) {

                                                                                        echo Yii::app()->Currency->convert(0);
                                                                                } else {

                                                                                        echo Yii::app()->Currency->convert($this->shipping($total));
                                                                                }
                                                                        } else {
                                                                                echo Yii::app()->Currency->convert($this->shipping($total));
                                                                        }
                                                                        ?>
                                                                </span>
                                                        </strong>
                                                </dov>
                                        </div>
                                        <div class="summary_row shipping">
                                                <div class="col-left">
                                                        <label for="">Order Total </label>
                                                </div>
                                                <dov class="col-right">
                                                        <strong>
                                                                <span id="grant_total">
                                                                        <?php echo Yii::app()->Currency->convert($total + $this->shipping($total)); ?>
                                                                </span>
                                                        </strong>
                                                </dov>
                                        </div>
                                        <!--<div class="summary_row shipping">
                                                <div class="col-left">
                                                        <label for="">GST (<?php echo Settings::model()->findByAttributes(array('id' => 1))->tax_rate; ?>%)</label>
                                                </div>
                                                <dov class="col-right">
                                                        <strong>

                                                                <span id="total_tax"><?php echo Yii::app()->Currency->convert($this->Tax($total)); ?></span>
                                                        </strong>
                                                </dov>
                                        </div>-->
                                        <div class="summary_row shipping">
                                                <div class="col-left">
                                                        <label for="">Total to pay</label>
                                                </div>
                                                <dov class="col-right">
                                                        <strong>
                                                                <span id="total_pay"><?php echo Yii::app()->Currency->convert($this->Tax($total) + $total + $this->shipping($total)); ?></span>
                                                        </strong>
                                                </dov>
                                        </div>
                                        <div class="summary_row shipping">
                                                <?php if (isset(Yii::app()->session['user'])) { ?>
                                                    <div class="check_out_btn">
                                                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/cart/Proceed/<?php echo $prod->id; ?>" class="btn btn-primary">PROCEED TO CHECKOUT</a>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="check_out_btn">
                                                            <a href="JavaScript:Void(0);" class="userlogin btn btn-primary">LOGIN TO CONTINUE</a>
                                                    </div>
                                                    <div class="check_out_btn">
                                                            <a href="JavaScript:Void(0);" class="guestcheckout btn btn-primary">GUEST CHECKOUT</a>
                                                    </div>
                                                <?php } ?>

                                                <div class="text-centre">
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>" class="continue">CONTINUE SHOPPING <i class="fa fa-angle-right"></i> </a>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="cart_info hidden visible-mobile">
                                <div class="row">
                                        <div class="col-xs-12 ita">
                                                <h2>Free Shipping</h2>
                                                Qualify for free shipping on your first order when registering with Soles iLove.<a href="JavaScript:Void(0);" class="userlogin continue">Click here</a> to register
                                                 
                                        </div>
                                        
                                </div>
                        </div>
                </div>

        </div>
</section>

<div class="modal fade" id="logreg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                                <div class="material-slide-line">
                                        <h3>Sign In</h3>
                                        <?php if (Yii::app()->user->hasFlash('login_list')): ?>
                                            <div class="alert alert-danger mesage">
                                                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                    <?php echo Yii::app()->user->getFlash('login_list'); ?>
                                            </div>
                                        <?php endif; ?>
                                        <form  name="login" id="login_form" action="<?= Yii::app()->baseUrl; ?>/index.php/Site/login" method="POST">
                                                <input type="hidden" name="return" value="<?php echo Yii::app()->request->url; ?>"/>
                                                <span class="input input--akira">
                                                        <input class="input__field input__field--akira"  type="text" name="UserDetails[email]" id="input-1" />
                                                        <label class="input__label input__label--akira" for="input-1">
                                                                <span class="input__label-content input__label-content--akira">Email Address</span>
                                                        </label>
                                                </span>
                                                <span class="input input--akira">
                                                        <input class="input__field input__field--akira"  type="text" name="UserDetails[password]" id="input-1" />
                                                        <label class="input__label input__label--akira" for="input-1">
                                                                <span class="input__label-content input__label-content--akira">Password</span>
                                                        </label>
                                                </span>
                                                <button class="butter getsub-btn">Submit</button>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
</div>





<script>
    $(document).ready(function () {
        $("#TempUserGifts_message").keyup(function () {
            var StrLengths = $("#TempUserGifts_message").val().length;
            var myLength = 100 - StrLengths;
            var totallength = 100;
            
            if (myLength <= 0) {
                $('.mx').hide();
                $('.mc').show();
                document.getElementById("stringLengrth").innerHTML = "Maximum characters has been reached.";
            }
            else {
                document.getElementById("stringLengrth").innerHTML = "";
            }

        });
    });</script>
<script>
    $(document).ready(function () {

<?php if ($loginform->hasErrors()) { ?>

            $(".reg").removeClass('active');
            $(".log").addClass('active');
            $("#register").removeClass('active');
            $("#signin").addClass('active');
            $("#logreg").modal('show');
<?php } ?>
<?php if ($regform->hasErrors()) { ?>
            $(".log").removeClass('active');
            $(".reg").addClass('active');
            $("#signin").removeClass('active');
            $("#register").addClass('active');
            $("#logreg").modal('show');
<?php } ?>


    });</script>
<script>
    $(document).ready(function () {
        $('.newuser').click(function () {
             $("#register").modal('show');
        });

        $('.qty_no').click(function () {
            showLoader();
            var cart = $(this).attr('cart');
            var add_type = $(this).attr('add_type');
            var qty = $(this).siblings('.mquantity').val();
            if (add_type == 1) {
                qty = parseInt(qty) - parseInt(1);
            } else {
                qty = parseInt(qty) + parseInt(1);
            }
            var product_id = $(this).attr('product_id');

            if (qty > 0) {
                quantityChange(cart, qty, product_id);
            }

        });
    });
    function getorderproduct(product_id, order_id, option) {

        $.ajax({
            type: "POST",
            cache: 'false',
            async: false,
            url: baseurl + 'Cart/Getorderproduct',
            data: {product_id: product_id, order_id: order_id, option: option},
        }).done(function (data) {
            $("#order_product_id").val(data);
            hideLoader();
        });
    }
    function quantityChange(cart, qty, product_id) {
        $('#cart_' + cart).find('.cart_qty_check').html(' ');
        $.ajax({
            type: "POST",
            cache: 'false',
            async: false,
            url: baseurl + 'Cart/UpdateCart',
            data: {cart_id: cart, Qty: qty, prod_id: product_id},
        }).done(function (data) {
            var obj = jQuery.parseJSON(data);
            if (obj.stock_status == 0) {
                
                $('#cart_' + cart).find('.cart_qty_check').html('Product Not in desired quantity.');
            } else {
                $(".range_" + cart).html(obj.producttotal);
                $("#subtotal").html(obj.subtotal);
                $("#ordertotal").html(obj.granttotal);
                $("#shipping").html(obj.ship);
                $("#grant_total").html(obj.ordtot);
                $("#total_tax").html(obj.tottax);
                $("#total_pay").html(obj.totpay);
            }
            hideLoader();
        });
    }

    function total() {
        $.ajax({
            type: "POST",
            cache: 'false',
            async: false,
            url: baseurl + 'Cart/Total',
            data: {}
        }).done(function (data) {
            $(".range").html('Rs.' + data);
            hideLoader();
        });
    }

    function showLoader() {
        $('.over-lay').show();
    }
    function hideLoader() {
        $('.over-lay').hide();
    }
</script>
<style>
        .ita{
                font-style: italic;
        }
</style>








