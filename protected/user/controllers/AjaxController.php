<?php

class AjaxController extends Controller {

    public function actionSelectState() {
        //$_POST['country'] = 99;
        $country = Countries::model()->findByPk($_POST['country'])->country_name;
        $model = States::model()->findAllByAttributes(array('country_id' => (int) $_POST['country']));
        if (!empty($model)) {
            $options = "<option value=''>Select " . $country . " States</option>";
            foreach ($model as $state) {
                $options .= '<option value="' . $state->state_name . '">' . $state->state_name . '</option>';
            }
        } else {
            $options = 0;
        }
        echo $options;
    }

    public function actionSelectPhonecode() {
        $phonecode = Countries::model()->findByPk($_POST['country'])->phonecode;
        $model = Countries::model()->findAll();
        if (!empty($model)) {

            foreach ($model as $city) {
                if ($city->phonecode == $phonecode) {
                    $selected = 'selected';
                } else {
                    $selected = '';
                }
                $options .= "<option $selected  value ='" . $city->phonecode . "'>+" . $city->phonecode . "</option>";
            }
        }
        echo $options;
    }

    public function actionselectOptionImage() {
        $color_id = $_POST['color_id'];
        $product = $_POST['product_id'];
        if (($color_id != '' && $product != '')) {
            $get_option = OptionImages::model()->findByAttributes(['color_id' => $color_id, 'product_id' => $product, 'status' => 1]);
            if (!empty($get_option)) {
                $folder = Yii::app()->Upload->folderName(0, 1000, $product);
                ?>
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/products/<?php echo $folder; ?>/<?php echo $product; ?>/option/<?php echo $color_id; ?>/<?php echo $color_id; ?>.<?php echo $get_option->image; ?>" />
                <?php
            } else {
                echo '<p> No Image Uploaded Yet</p>';
            }
        } else {
            echo '<p> No Image Uploaded Yet</p>';
        }
    }

    public function actionSelectShipState() {
        //$_POST['country'] = 99;
        $country = Countries::model()->findByPk($_POST['country'])->country_name;
        $model = States::model()->findAllByAttributes(['country_id' => (int) $_POST['country']]);
        if (!empty($model)) {
            $options = "<option value=''>Select " . $country . " States</option>";
            foreach ($model as $state) {
                if ($_POST['state'] == $state->state_name) {
                    $sel = 'selected';
                } else {
                    $sel = "";
                }
                $options .= '<option ' . $sel . ' value="' . $state->state_name . '">' . $state->state_name . '</option>';
            }
        } else {
            $options = 0;
        }
        echo $options;
    }

    public function actionSelectAddress() {
        $aid = $_POST['aid'];
        $get_user_address = UserAddress::model()->findByPk($aid);
        if (!empty($get_user_address)) {

            $array = array('address' => $get_user_address);
//            $json = Json::encode($array);
            $json = CJSON::encode($array);
            echo $json;
        }
    }

    public function actionSetPriceSession() {
        $min_name = $_POST['min_name'];
        $min_value = $_POST['min_value'];
        $max_name = $_POST['max_name'];
        $max_value = $_POST['max_value'];

        Yii::app()->session[$min_name] = $min_value;
        Yii::app()->session[$max_name] = $max_value;
    }

    public function actionSetSession() {
        $session_name = $_POST['session_name'];
        $session_value = $_POST['session_value'];
        $exp_val = explode('|', $session_value);
        $imp_val = implode(',', $exp_val);
//        $imp_val = rtrim(',', $imp_val);
        Yii::app()->session[$session_name] = $imp_val;
    }

    public function actionGetOrderHistory() {
        $order_id = $_POST['order_id'];
        $order_history = OrderHistory::model()->findAllByAttributes(['order_id' => $order_id], ['order' => 'id desc']);
        if (!empty($order_history)) {
            $result = $this->renderPartial('_order_history', ['model' => $order_history]);
        } else {
            $result = 'No Result';
        }
        echo $result;
    }

}
