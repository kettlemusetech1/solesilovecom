<?php

ob_start();

class SiteController extends Controller {

        public function actions() {
                return array(
                    'captcha' => array(
                        'class' => 'CCaptchaAction',
                        'backColor' => 0xFFFFFF,
                    ),
                    'page' => array(
                        'class' => 'CViewAction',
                    ),
                );
        }

        public function actionIndex() {
                if (!isset(Yii::app()->session['currency'])) {

                        Yii::app()->session['currency'] = Currency::model()->findByPk(5);
                }
                $slider = Slider::model()->findAllByAttributes(array('status' => 1));
                $testi = Testimonial::model()->findAllByAttributes(array('status' => 1), array('order' => 'id DESC'));
                $this->render('index', array('slider' => $slider, 'testi' => $testi));
        }

        public function actionBrands() {
                $this->render('brands');
        }

        public function actionRegmod() {
         // echo $email='inboxreacherapril@gmail.com';
                $modell = UserDetails::model()->findByAttributes(array('email' => $_REQUEST['emailid']));
            //  $modell = UserDetails::model()->findByAttributes(array('email' => $email));
              
                if (!empty($modell)) {
                        if ($modell->email_verification == 0) {
                                $this->VerificationMail($modell); //Uncomment in future
                                //$array = array('status' => '300', 'message' => 'Your account not verified . Please check your mail and fill the form.You may also check your junk/spam email');
                                 $array = array('status' => '300', 'message' => 'Please Fill this form for activate your account');
                                $json = CJSON::encode($array);
                                echo $json;
                                exit;
                        } else {
                                $array = array('status' => '301', 'message' => 'You have already registered. Please Login');
                                $json = CJSON::encode($array);
                                echo $json;
                                exit;
                        }
                } else {
                        $model = new UserDetails('create');
                        $model->email = $_REQUEST['emailid'];
                        $model->phone_no_2 = $_REQUEST['mobile'];
                        $model->CB = 1;
                        $model->UB = 1;
                        $model->DOC = date('Y-m-d');
                        $model->verify_code = rand(1000, 9999);
                        $model->email_verification = 0;
                        if ($model->save(false)) {
                               // $this->VerificationMail($model); //Uncomment in future
                              //  $array = array('status' => '200', 'message' => 'Please verify your account. A Verification Code sent to your email.Please check your mail.You may also check your junk/spam email');
                                $array = array('status' => '200', 'message' => 'Please Fill this form for activate your account');
                                $json = CJSON::encode($array);
                                echo $json;
                                exit;
                        }
                }
        }

        public function actionRegistr() {
                $model = UserDetails::model()->findByAttributes(array('email' => $_REQUEST['emailid']));
                if (!empty($model)) {
                        if (($_REQUEST['mobile'] == ' ') || ($_REQUEST['name'] == ' ') || ($_REQUEST['password'] == ' ') || ($_REQUEST['password1'] == ' ')) {
                                $array = array('status' => '305', 'message' => 'Please fill out all fields');
                                $json = CJSON::encode($array);
                                echo $json;
                                exit;
                        } else {
                                $model->phone_no_2 = $_REQUEST['mobile'];
                                $model->status = 1;
                                $model->email_verification = 1;
                                $model->DOC = date('Y-m-d');
                                $model->first_name = $_REQUEST['name'];
                                $model->account_type = 1;
                                $model->password = $_REQUEST['password'];
                                $model->confirm = $_REQUEST['password1'];
                                if ($model->update(false)) {
                                        if (isset(Yii::app()->session['temp_user'])) {
                                                Cart::model()->updateAll(array("user_id" => $model->id), 'session_id=' . Yii::app()->session['temp_user']);
                                        }
                                        Yii::app()->session['user'] = $model;
                                        //$this->RegisterMail($model); //Uncomment in future
                                        $array = array('status' => '1', 'message' => 'Registration Success');
                                        $json = CJSON::encode($array);
                                        echo $json;
                                        exit;
                                }
                        }
                } else {
                        $array = array('status' => '304', 'message' => 'Incorrect Email');
                        $json = CJSON::encode($array);
                        echo $json;
                        exit;
                }
        }


    public function actionGuestregistr() {
        $model = UserDetails::model()->findByAttributes(array('email' => $_REQUEST['emailid']));
        if (!empty($model)) {

            $array = array('status' => '301', 'message' => 'You have already registered. Please Login');
            $json = CJSON::encode($array);
            echo $json;
            exit;
        } else {
            $model = new UserDetails('create');
            if (($_REQUEST['emailid'] == ' ') || ($_REQUEST['name'] == ' ')) {
                $array = array('status' => '305', 'message' => 'Please fill out all fields');
                $json = CJSON::encode($array);
                echo $json;
                exit;
            } else {

                $model->status = 1;
                $model->account_type = $_POST['account_type'];
                $model->email_verification = 0;
                $model->verify_code = rand(1000, 9999);
                $model->email = $_REQUEST['emailid'];
                $model->DOC = date('Y-m-d');
                $model->first_name = $_REQUEST['name'];
                if ($model->save(false)) {
                    if (isset(Yii::app()->session['temp_user'])) {
                        Cart::model()->updateAll(array("user_id" => $model->id), 'session_id=' . Yii::app()->session['temp_user']);
                    }
                    Yii::app()->session['user'] = $model;
                    //  $this->RegisterMail($model); //Uncomment in future
                    $array = array('status' => '1', 'message' => 'Registration Success');
                    $json = CJSON::encode($array);
                    echo $json;
                    exit;
                }
            }
        }
    }

        public function actionLoginmod() {
                $modell = UserDetails::model()->findByAttributes(array('email' => $_REQUEST['email'], 'password' => $_REQUEST['password']));
                if (!empty($modell)) {
                        if ($modell->status == 0) {
                                $array = array('status' => '301', 'message' => 'Access Denied.Contact Administrator');
                                $json = CJSON::encode($array);
                                echo $json;
                                exit;
                        } else {
                                if ($modell->email_verification == 0) {
                                        $array = array('status' => '302', 'message' => 'Your Account cant verified. Please Verify.');
                                        $json = CJSON::encode($array);
                                        echo $json;
                                        exit;
                                } else {
                                        if (isset(Yii::app()->session['temp_user'])) {
                                                Cart::model()->updateAll(array("user_id" => $modell->id), 'session_id=' . Yii::app()->session['temp_user']);
                                        }
                                        Yii::app()->session['user'] = $modell;
                                        $array = array('status' => '200', 'message' => 'Successfully Completed');
                                        $json = CJSON::encode($array);
                                        echo $json;
                                        exit;
                                }
                        }
                } else {
                        $array = array('status' => '300', 'message' => 'Invalid Username and Password');
                        $json = CJSON::encode($array);
                        echo $json;
                        exit;
                } 
        }

        public function actionLogin() {
                if (!isset(Yii::app()->session['user'])) {
                    if ($_REQUEST['email']) {
                        
                                $modell = UserDetails::model()->findByAttributes(array('email' => $_REQUEST['email'], 'password' => $_REQUEST['password']));
                                if (!empty($modell)) {
                                        if ($modell->status == 0) {
                                                Yii::app()->user->setFlash('login_list', "Access Denied.Contact Solesilove Administrator");
                                        } else {
                                                if ($modell->email_verification == 0) {
                                                        Yii::app()->user->setFlash('login_list', "Your Account is not verified. Please Verify.");
                                                } else {
                                                    Yii::app()->session['user'] = $modell;
                                                        if (isset(Yii::app()->session['temp_user'])) {
                                                                Cart::model()->updateAll(array("user_id" => $modell->id), 'session_id=' . Yii::app()->session['temp_user']);
                                                        }
                                                         $this->redirect(Yii::app()->homeUrl);exit;
                                                }
                                        }
                                } else {
                                        Yii::app()->user->setFlash('login_list', "Invalid Username and Password");
                                        $this->render('login');
                                        exit;
                                }
                        }
                      
                
        }
          $this->render('login');
        }

        public function actionReotp() {
                $modell = UserDetails::model()->findByAttributes(array('email' => $_REQUEST['email']));
                $modell->verify_code = rand(1000, 9999);
                if ($modell->update(false)) {
                     $this->VerificationMail($modell); //Uncomment in future
                        $array = array('status' => '200', 'message' => 'Verification Code Sent.Please check mail.You may also check your junk/spam email');
                        $json = CJSON::encode($array);
                        echo $json;
                        exit;
                       
                }
        }

        public function actionFgtpwd() {
               $modell = UserDetails::model()->findByAttributes(array('email' => $_REQUEST['email']));
                if (!empty($modell)) {
                        if ($modell->email_verification == 0) {
                                $this->VerificationMail($modell); //Uncomment in future
                                $array = array('status' => '300', 'message' => 'Your account not verified . Please check your mail and fill the form.You may also check your junk/spam email');
                                $json = CJSON::encode($array);
                                echo $json;
                                exit;
                        } else {
                                $modell->verify_code = rand(1000, 9999);
                                if ($modell->update(false)) {
                                        $this->VerificationMail($modell); //Uncomment in future
                                        $array = array('status' => '200', 'message' => 'Please verify your account. A Verification Code sent to your email.Please check your mail and fill the form.You may also check your junk/spam email');
                                        $json = CJSON::encode($array);
                                        echo $json; 
                                        exit;
                                }
                        }
                } else {
                        $array = array('status' => '303', 'message' => 'Invalid email.Please register.');
                        $json = CJSON::encode($array);
                        echo $json;
                        exit;
                }
        }

        public function ActionUpdpwd() {
                $model = UserDetails::model()->findByAttributes(array('email' => $_REQUEST['email'], 'verify_code' => $_REQUEST['otp']));
                if (!empty($model)) {
                        if ($_REQUEST['password'] == ' ') {
                                $array = array('status' => '305', 'message' => 'Please fill out all fields');
                                $json = CJSON::encode($array);
                                echo $json;
                                exit;
                        } else {
                                $model->password = $_REQUEST['password'];
                                $model->confirm = $_REQUEST['password'];
                                if ($model->update(false)) {
                                        Yii::app()->session['user'] = $model;
                                        $array = array('status' => '1', 'message' => 'Registration Success');
                                        $json = CJSON::encode($array);
                                        echo $json;
                                        exit;
                                }
                        }
                } else {
                        $array = array('status' => '304', 'message' => 'Incorrect Email/Verification Code');
                        $json = CJSON::encode($array);
                        echo $json;
                        exit;
                }
        }

        public function actionLogout() {
// Cart::model()->deleteAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                unset(Yii::app()->session['user']);
                unset(Yii::app()->session['gift_card_option']);
                unset($_SESSION);

                Yii::app()->user->logout();

                $this->redirect(Yii::app()->homeUrl);
        }

        // public function RegisterMail($model) {
        //         $user = $model->email;
        //         $user_subject = 'Welcome to Solesilove';
        //         $admin = AdminUser::model()->findByPk(4)->email;
        //         $admin_subject = $model->first_name . ' registered with Solesilove.com.au';

        //         $message = new YiiMailMessage;
        //         $message->view = "_register_user_mail";  // view file name
        //         $params = array('model' => $model); // parameters
        //         $message->subject = $user_subject;
        //         $message->setBody($params, 'text/html');
        //         $message->addTo($user);
        //         $message->setFrom('no-reply@solesilove.com.au', 'Solesilove.com.au');
        //         Yii::app()->mail->send($message);



        //         $message1 = new YiiMailMessage;
        //         $message1->view = "_register_admin_mail";  // view file name
        //         $params1 = array('model' => $model); // parameters
        //         $message1->subject = $admin_subject;
        //         $message1->setBody($params1, 'text/html');
        //         $message1->addTo($admin);
        //         $message1->setFrom('no-reply@solesilove.com.au', 'Solesilove.com.au');
        //         Yii::app()->mail->send($message1);
        // }

        /* mail to user and admin */

        // public function VerificationMail($model) {
        //         $user = $model->email;

        //         $user_subject = 'Solesilove Registration - ' . $model->verify_code . ' is your verification code for secure access';

        //         $message = new YiiMailMessage;
        //         $message->view = "_verify_user_mail";  // view file name
        //         $params = array('model' => $model); // parameters
        //         $message->subject = $user_subject;
        //         $message->setBody($params, 'text/html');
        //         $message->addTo($user);
        //         $message->setFrom('no-reply@solesilove.com.au', 'Solesilove.com.au');

        //         Yii::app()->mail->send($message);
        // }

        public function actionAboutUs() {
                $model = StaticPage::model()->findByPk(1);
                $this->render('about_us', array('model' => $model));
        }

        public function actionBlog() {
                $model = Blog::model()->findAllByAttributes(array('status' => 1));
                $this->render('blogs', array('model' => $model));
        }

        public function actionBlogDetails($blog) {
                $models = Blog::model()->findByPk($blog);
                $last_id = Blog::model()->find(array('order' => 'id DESC'));
                $first_id = Blog::model()->find(array('order' => 'id ASC'));
                $this->render('blog_details', array('modelz' => $models, 'last_id' => $last_id, 'first_id' => $first_id));
        }

        public function actionPolicy() {
                $this->render('privacy_policy');
        }

        public function actionReturn() {
                $this->render('Return_Policy');
        }
        public function actionShipping() {
                $this->render('shipping_delivery');
        }

        public function actionFaq() {
                $this->render('faq');
        }
        public function actionAbout() {
                $this->render('about_us');
        }
        public function actionCareers() {
                $model = StaticPage::model()->findByPk(2);
                $this->render('careers', array('model' => $model));
        }

        public function actionLocation() {
                $this->render('location');
        }

        public function actionNewsLetter() {
                if (isset($_REQUEST['email'])) {
                        $newsmail = Newsletter::model()->findByAttributes(array('email' => $_REQUEST['email']));
                        if ($newsmail) {
                                echo '2';
                                exit;
                        } else {
                             echo '1';
                                $model = new Newsletter;
                                $model->email = $_REQUEST['email'];
                                $model->date = date('Y-m-d');
                                $model->save(false);
                                $admin = AdminUser::model()->findByAttributes(array('admin_post_id' => '1'));
                                Yii::import('user.extensions.yii-mail.YiiMail');
                                $message = new YiiMailMessage;
                                $message->view = "_newsletter_mail";
                                $params = array('model' => $model);
                                $message->subject = 'Solesilove';
                                $message->setBody($params, 'text/html');
                                $message->addTo($model->email);
                                $message->addTo($admin->email);
                                $message->from = 'no-reply@solesilove.com.au';
                                if (Yii::app()->mail->send($message)) {
                                
                                }
                        }
                }
        }

        public function actionError() {
                $error = Yii::app()->errorHandler->error;
                if ($error)
                        $this->render('error', array('error' => $error));
                else
                        throw new CHttpException(404, 'Page not found.');
        }

        public function actioncontactUs() {
                $model = new ContactUs;
                if (isset($_POST['ContactUs'])) {
                        $model->attributes = $_POST['ContactUs'];
                        $model->date = date("Y-m-d");
                        // if ($model->validate()) {
                         
                                if ($model->save(false)) {
                                    
                                        $this->contactmail($model);
                                        Yii::app()->user->setFlash('success', " Your email sent successfully");
                                } else {
                                         echo 222;
                                    exit;
                                        Yii::app()->user->setFlash('error', "Error Occured");
                                }
                                $this->render('contact_success');
                                exit;
                        // }
                }
                $this->render('contact_us', array('model' => $model));
        }

        /**
         * Displays the contact page
         */
        public function actionContact() {
                $model = new ContactForm;

                if (isset($_POST['ContactForm'])) {
                        $model->attributes = $_POST['ContactForm'];
                        if ($model->validate()) {
                                $name = ' = ?UTF-8?B?' . base64_encode($model->name) . '? = ';
                                $subject = ' = ?UTF-8?B?' . base64_encode($model->subject) . '? = ';
                                $headers = "From: $name <{$model->email}>\r\n" .
                                        "Reply-To: {$model->email}\r\n" .
                                        "MIME-Version: 1.0\r\n" .
                                        "Content-Type: text/plain; charset=UTF-8";

                                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                                $this->refresh();
                        }
                }
                $this->render('contact', array('model' => $model));
        }

        public function actionMywishlists() {

                if (!isset(Yii::app()->session['user'])) {
                        Yii::app()->session['wishlist_user'] = 1;
                        $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
                }
        }

        public function actionRegisterpop() {

                if (!isset(Yii::app()->session['user'])) {
                        Yii::app()->session['wishlist_user'] = 1;
                        $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
                }
        }

        /**
         * Displays the login page
         */
        public function actionRegister() {
                if (isset(Yii::app()->session['user'])) {
                        $this->redirect($this->createUrl('index'));
                } else {
                        $model = new UserDetails('create');
                        if (isset($_POST['UserDetails'])) {

                                $model->attributes = $_POST['UserDetails'];
                                $model->first_name = $_POST['UserDetails']['first_name'];
                                $model->email = $_POST['UserDetails']['email'];
                                $model->last_name = $_POST['UserDetails']['last_name'];
                                $model->country = $_POST['UserDetails']['country'];
                                $model->phone_no_1 = $_POST['UserDetails']['phone_no_1'];
                                $model->phone_no_2 = $_POST['UserDetails']['phone_no_2'];
                                $model->wallet_amt = '0.00';
                                $model->email_verification = 0;
                                if ($model->validate()) {
                                        $model->status = 1;
                                        $model->CB = 1;
                                        $model->UB = 1;
                                        $model->DOC = date('Y-m-d');
                                        $model->verify_code = rand(1000, 9999);
                                        if ($model->password == $model->confirm) {
                                                if ($model->save()) {
                                                        if ($model->email_verification == 0) {
                                                                if ($model->country == 99) {
                                                                        Yii::app()->user->setFlash('emailverify', "Verification Code has been sent to your email <b>" . $model->email . "</b> and mobile <b>" . $model->phone_no_2 . "</b>, please enter the same here to access your account.");
                                                                } else {
                                                                        Yii::app()->user->setFlash('emailverify', "Verification Code has been sent to your email <b>" . $model->email . "</b>, please enter the same here to access your account.");
                                                                }
                                                                Yii::app()->user->setFlash('verify_code', $model->id);
                                                                Yii::app()->session['user_email_verify'] = $model->id;
                                                                Yii::app()->session['gift_card_option'] = $_POST['gift_id'];
                                                                $this->VerificationMail($model);
                                                        }
                                                        $this->siteNavigator($model);
                                                }
                                        } else {
                                                $model->addError('confirm', 'password mismatch');
                                        }
                                }
                        }



                        if (isset($_POST['verify_email'])) {

                                $unverified_user = UserDetails::model()->findByPk(Yii::app()->session['user_email_verify']);
                                if ($unverified_user->verify_code == $_POST['verify_code']) {
                                        $unverified_user->email_verification = 1;
                                        $unverified_user->status = 1;
                                        $unverified_user->save(FALSE);
                                        $this->RegisterMail($unverified_user);
                                        Yii::app()->user->setFlash('user_account_create_success', "We're glad you're here :)");
                                        $this->siteNavigator($unverified_user);
                                } else {
                                        Yii::app()->user->setFlash('email_verification1', "Invalid Verification Code.Try Again..");
                                }
                        }
                        $this->render('register', array('model' => $model));
                }
        }

        public function actionResendmail($id) {


                $modell = UserDetails::model()->findByPk($id);

                if (!empty($modell)) {

                        if ($modell->email_verification == 0) {

                                Yii::app()->user->setFlash('emailverify', "Verification Code has been sent to your email <b>" . $modell->email . "</b>, please enter the same here to access your account.");

                                Yii::app()->user->setFlash('verify_code', $modell->id);
                                Yii::app()->session['user_email_verify'] = $modell->id;

                                $this->VerificationMail($modell);
                                $this->redirect(array('index'));
                        } else if ($modell->email_verification == 1 && $modell->status == 1) {
                                Yii::app()->user->setFlash('emailverify', null);
                                Yii::app()->user->setFlash('email_verification1', null);
                                Yii::app()->session['gift_card_option'] = $_POST['gift_id'];
                                $this->siteNavigator($modell);
                        }
                } else {
                        $this->redirect(Yii::app()->request->urlReferrer);
                }
        }

        /**
         * Displays the login page
         */
        /*   public function actionLogin() {

          if (isset(Yii::app()->session['user'])) {

          $this->redirect($this->createUrl('index'));
          } else {

          $model = new UserDetails();
          if (isset($_REQUEST['UserDetails'])) {

          $modell = UserDetails::model()->findByAttributes(array('email' => $_REQUEST['UserDetails']['email'], 'password' => $_REQUEST['UserDetails']['password']));
          if (!empty($modell)) {
          if ($modell->status == 0) {
          Yii::app()->user->setFlash('login_list', "Access Denied.Contact Artstra");
          } else if ($modell->email_verification == 0) {

          Yii::app()->user->setFlash('emailverify', "One Time Password (OTP) has been sent to your email <b>" . $modell->email . "</b>, please enter the same here to access your account.");

          Yii::app()->user->setFlash('verify_code', $modell->id);
          Yii::app()->session['user_email_verify'] = $modell->id;

          $this->VerificationMail($modell);
          } else if ($modell->email_verification == 1 && $modell->status == 1) {
          if (isset(Yii::app()->session['temp_user'])) {
          Cart::model()->updateAll(array("user_id" => $modell->id, 'session_id' => ''), 'session_id=' . Yii::app()->session['temp_user']);
          }
          Yii::app()->user->setFlash('emailverify', null);
          Yii::app()->user->setFlash('email_verification1', null);
          Yii::app()->session['gift_card_option'] = $_POST['gift_id'];
          Yii::app()->session['user'] = $modell;
          if (isset($_POST['return'])) {
          $this->redirect($_POST['return']);
          } else {
          $this->siteNavigator($modell);
          }
          }
          } else {
          Yii::app()->user->setFlash('login_list', "Invalid Username or Password");
          }
          }

          if (isset($_POST['verify_email'])) {

          $unverified_user = UserDetails::model()->findByPk(Yii::app()->session['user_email_verify']);

          if ($unverified_user->verify_code == $_POST['verify_code']) {
          $unverified_user->email_verification = 1;
          $unverified_user->status = 1;
          $unverified_user->save(FALSE);
          $this->RegisterMail($unverified_user);
          Yii::app()->user->setFlash('user_account_create_success', "We're glad you're here :)");
          unset(Yii::app()->session['user_email_verify']);
          $this->siteNavigator($unverified_user);
          } else {
          Yii::app()->user->setFlash('email_verification1', "Invalid OTP.Try Again..");
          }
          }

          $this->render('login', array('model' => $model));
          }
          }

          public function siteNavigator($model) {

          if ($model->email_verification == 1) {


          Yii::app()->session['user'] = $model;

          if (Yii::app()->session['gift_card_option'] != "") {

          $this->redirect($this->createUrl('/giftcard/index', array('card_id' => Yii::app()->session['gift_card_option'])));
          } else if (Yii::app()->session['enq_address'] != '') {

          $this->redirect('/Myaccount/Enqaddress/id/' . Yii::app()->session['enq_address']);
          } else if (isset(Yii::app()->session['temp_user'])) {

          Cart::model()->updateAll(array("user_id" => $model->id, 'session_id' => ''), 'session_id=' . Yii::app()->session['temp_user']);
          UserWishlist::model()->updateAll(array("user_id" => $model->id, 'session_id' => ''), 'session_id=' . Yii::app()->session['temp_user']);
          ProductViewed::model()->updateAll(array("user_id" => $model->id, 'session_id' => ''), 'session_id=' . Yii::app()->session['temp_user']);

          unset(Yii::app()->session['temp_user']);
          } else if (Yii::app()->session['measure_details'] != '') {

          $this->redirect('/Myaccount/SizeChartType?m=' . Yii::app()->session['measure_details']);
          } else if (Yii::app()->session['make_paid'] != '') {

          $this->redirect('/Myaccount/Makepayment?p=' . Yii::app()->session['make_paid']);
          } else if (Yii::app()->session['make_partial_paid'] != '') {
          $this->redirect('/Myaccount/MakepaymentPartial?p=' . Yii::app()->session['make_partial_paid']);
          } else if (Yii::app()->session['temp_makepayment'] == 1) {

          $this->redirect(array('Myaccount/Makepayment'));
          } else if (Yii::app()->session['measure_details'] != '') {
          $this->redirect('/Myaccount/SizeChartType?m=' . Yii::app()->session['measure_details']);
          } else if (Yii::app()->session['login_flag'] != '' && Yii::app()->session['login_flag'] == 1) {

          unset(Yii::app()->session['login_flag']);

          $this->redirect('/Cart/Proceed');
          } else if (Yii::app()->session["myorder"] != "") {
          $this->redirect('/Myaccount/Myordernew/id/' . Yii::app()->session["myorder"]);
          } else if (Yii::app()->session["temp_credit"] == "1") {
          $this->redirect(Yii::app()->request->baseUrl . '/index.php/CreditHistory');
          } else {

          unset(Yii::app()->session['wishlist_user']);
          //  Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
          //$this->redirect(Yii::app()->user->returnUrl);
          $this->redirect(Yii::app()->request->baseUrl . '/index.php/myaccount');
          }

          $this->redirect(Yii::app()->request->urlReferrer);
          if (isset(Yii::app()->session['wishlist_user'])) {

          Yii::app()->user->setFlash('wishlist_user', "Dear, You must login to see Wishlist Items");
          }
          // $this->redirect(Yii::app()->request->baseUrl . '/index.php/Myaccount');
          }
          }
         */
        /* mail to user and admin */

        /**
         * Logs out the current user and redirect to homepage.
         */
        public function contactmail($model) {
          
                $admin = AdminUser::model()->findByPk(4)->email;
                $admin_subject = 'New Enquiry Recieved';

                $message = new YiiMailMessage;
               
                $message->view = "_admin_contact_email";  // view file name
                $params = array('model' => $model); // parameters
                $message->subject = $admin_subject;
                    
                $message->setBody($params, 'text/html');
                 
                $message->addTo($admin);
             
                $message->setFrom('info@solesilove.com.au', 'Solesilove.com.au');
               
                Yii::app()->mail->send($message);
        }

        public function actionCurrencyChange($id) {
                $data = Currency::model()->findByPk($id);
                Yii::app()->session['currency'] = $data;
                $this->redirect(Yii::app()->request->urlReferrer);
        }

        public function actionSendmail() {
                $email = $_POST['email'];
                $news_exist = Newsletter::find()->where(['email' => $email])->one();
                if (empty($news_exist)) {
                        $model = new Newsletter();
                        $model->email = $email;
                        $model->status = 1;
                        if ($model->save(FALSE)) {
                                echo 1;
                        } else {
                                echo 2;
                        }
                } else {
                        echo 3;
                }
        }

        public function actionBlogDetailsPrevious($currentId) {
                $prevId = $this->getNextOrPrevId($currentId, 'prev');
                $model = Blog::model()->findByPk($prevId);
                $this->render('blog_details', array('model' => $model));
        }

        public function actionBlogDetailsNext($currentId) {
                $nextId = $this->getNextOrPrevId($currentId, 'next');
                $model = Blog::model()->findByPk($nextId);
                $this->render('blog_details', array('model' => $model));
        }

        public static function getNextOrPrevId($currentId, $nextOrPrev) {
                $records = NULL;
                if ($nextOrPrev == "prev")
                        $order = "id DESC";
                if ($nextOrPrev == "next")
                        $order = "id ASC";

                $records = Blog::model()->findAll(
                        array('select' => 'id', 'order' => $order)
                );

                foreach ($records as $i => $r)
                        if ($r->id == $currentId)
                                return $records[$i + 1]->id ? $records[$i + 1]->id : NULL;

                return NULL;
        }

        public function siteURL() {
                $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
                $domainName = $_SERVER['HTTP_HOST'];
                return $protocol . $domainName;
        }

}
