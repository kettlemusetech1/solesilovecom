<?php

class BrandsController extends Controller {

    public function actionIndex() {
        $brands = Brands::model()->findAllByAttributes(array('status' => 1));
        $this->render('brands', array('brands' => $brands));
    }

    public function actionList($name) {
        $get_brand = Brands::model()->findByAttributes(array('canonical_name' => $name));
        if (!Yii::app()->request->isAjaxRequest) {
            unset(Yii::app()->session['toy_style']);
            unset(Yii::app()->session['width_fitting']);
            unset(Yii::app()->session['color']);
            unset(Yii::app()->session['size']);
            unset(Yii::app()->session['max']);
            unset(Yii::app()->session['min']);
        }
        if (!empty($get_brand)) {
            $product_exist = Products::model()->findAllByAttributes(array("brand" => $get_brand->id));
            if (isset($_POST['pagesize'])) {
                $pagesize = $_POST['pagesize'];
                Yii::app()->session['pagesize'] = $pagesize;
            }

            if (!isset($pagesize)) {
                $pagesize = 15;
            }

            if (isset(Yii::app()->session['min'])) {
                $min = Yii::app()->session['min'];
            }

            if (isset(Yii::app()->session['max'])) {
                $max = Yii::app()->session['max'];
            }

            if (isset($_POST['sorting'])) {
                $get_sort_value = $_POST['sorting'];
                if ($get_sort_value == 1) {
                    $srt = ' id DESC ';
                } else if ($get_sort_value == 2) {
                    $srt = 'price ASC';
                } else if ($get_sort_value == 3) {
                    $srt = 'price DESC';
                } else if ($get_sort_value == 4) {
                    $srt = 'product_name ASC';
                } else if ($get_sort_value == 5) {
                    $srt = 'product_name DESC';
                }
            }
            if (isset(Yii::app()->session['sorting'])) {

            }
            if (!empty($product_exist)) {
                foreach ($product_exist as $product_data) {
                    $pids[] = $product_data->id;
                }
                $finalcondition = Yii::app()->Menu->SearchByProducts($pids, $parent, $categ, $min = '', $max = '', $pagesize);
                $getproducts = Products::model()->findAll("(" . $finalcondition . ") AND status = 1");
                $dataProvider = new CActiveDataProvider('Products', array(
                    'criteria' => array(
                        'condition' => $finalcondition,
                    ),
                    'pagination' => array(
                        'pageSize' => $pagesize,
                    ),
                    'sort' => array(
                        'defaultOrder' => $srt,
                    )
                        )
                );
            } else {
                $dataProvider = NULL;
                $getproducts = NULL;
            }
            if ($dataProvider != NULL && $getproducts != NULL) {
                $this->render('brandresult', array('dataProvider' => $dataProvider, 'getproducts' => $getproducts, 'parent' => $parent, 'brand' => $get_brand, 'name' => $name, 'pagesize' => $pagesize));
            } else {
                $this->render('brandresult', array('dataProvider' => $dataProvider, 'getproducts' => $getproducts, 'parent' => $parent, 'brand' => $get_brand, 'name' => $name, 'pagesize' => $pagesize));

//                $this->render('no_result_found');
            }
//            $this->render('searchresult', array('dataProvider' => $dataProvider, 'file_name' => '_searchresult', 'parameter' => $_REQUEST['saerchterm'], 'search_parm' => $category, 'searchterm' => $searchterm));
            exit;
        }
    }

}
