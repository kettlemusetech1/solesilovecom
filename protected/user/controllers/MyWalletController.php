<?php

class MyWalletController extends Controller {

        public function init() {
                date_default_timezone_set('Asia/Kolkata');
                if (!isset(Yii::app()->session['user'])) {

                        $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
                }
        }

        /*
         * Add money to user wallet by user
         */

        public function actionIndex() {
                $model = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                $order_billing_details = UserAddress::model()->findByAttributes(array('userid' => Yii::app()->session['user']['id']));
                if (!empty($model)) {
                        $wallet_amount = $model->wallet_amt;
                        $wallet_add = new WalletHistory('addWallet1');
                        if (isset($_POST['WalletHistory'])) {

                                $wallet_add->attributes = $_POST['WalletHistory'];
                                $unique_code = $_POST['WalletHistory']['unique_code'];
                                $entry_amount = $_POST['WalletHistory']['amount'];
                                $wallet_exist = UserGiftscardHistory::model()->findByAttributes(array('unique_code' => $unique_code, 'status' => 1, 'amount' => $entry_amount));
                                if (!empty($wallet_exist)) {
                                        $wallet_add->user_id = $model->id;

                                        $wallet_add->entry_date = date('Y-m-d H:i:s');
                                        $wallet_add->unique_code = $unique_code;
                                        $wallet_add->credit_debit = 1;
                                        $wallet_add->field2 = 1;
                                        $wallet_add->balance_amt = $wallet_amount + $entry_amount;

                                        if ($wallet_add->validate()) {

                                                if ($wallet_add->save(false)) {


                                                        $wallet_exist->status = 2;
                                                        if ($wallet_exist->save()) {
                                                                Yii::app()->user->setFlash('credit_wallet_success', "$ " . $entry_amount . ' added to your account. Your new balance is $ ' . $wallet_add->balance_amt);
                                                        }
                                                        $user_model = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                                                        $user_model->wallet_amt = $wallet_add->balance_amt;
                                                        if ($user_model->save(false)) {
                                                               // $this->gift_card_added_mail_customer($wallet_add, $user_model);
                                                        }

                                                        $wallet_add->unsetAttributes();
                                                        $wallet_add->unsetAttributes();
                                                }
                                        }
                                } else {
                                        $wallet_add->addError('unique_code', 'The entered giftcard code is not valid. Please try again.');
                                }
                        }
                        $this->render('index', array('wallet_add' => $wallet_add));
                }
        }

        /*
         * if payment success
         */

        public function actionCreditSuccess($payid, $user_id, $wallet_id, $tid, $amt) {

                $user_wallet = UserDetails::model()->findByPk($user_id);
                $wallet_history = WalletHistory::model()->findByPk($wallet_id);
                if (!empty($user_wallet) && !empty($wallet_history)) {

                        $amount = $user_wallet->wallet_amt + $wallet_history->amount;
                        $user_wallet->wallet_amt = $amount;
                        $wallet_history->field2 = 1; //success
                        $wallet_history->transaction_id = $payid;
                        $wallet_history_exist = WalletHistory::model()->findByAttributes(array('transaction_id' => $payid));
                        if (empty($wallet_history_exist)) {

                                if ($wallet_history->save()) {
                                        if ($user_wallet->save()) {
                                                Yii::app()->session['user'] = $user_wallet;
                                                Yii::app()->user->setFlash('wallet_success', "Money Added Successfully");
                                                $this->SuccessMail($wallet_history->id);

                                                $this->render('wallet_success', ['payid' => $payid, 'tid' => $tid, 'amt' => $amt]);
                                        } else {
                                                $wallet_history->delete();
                                        }
                                } else {
                                        Yii::app()->user->setFlash('wallet_error', "Oops some error occured.Transaction rejected.");
                                        $this->redirect(array('CreditError', 'wallet_id' => $wallet_history->id));
                                }
                        } else {
                                Yii::app()->user->setFlash('wallet_error', "Oops some error occured.Transaction rejected.");
                                $this->redirect(array('CreditError', 'wallet_id' => $wallet_history->id));
                        }
                } else {
                        Yii::app()->user->setFlash('wallet_error', "Oops some error occured.Transaction rejected.");
                        $this->redirect(array('CreditError', 'wallet_id' => $wallet_history->id));
                }
        }

        /*  send mail to admin and user */

        public function gift_card_added_mail_customer($wallet_add, $user_model) {
                $user = $user_model->email;
                $user_subject = 'Gift Card Redemption';
                $user_message = $this->renderPartial('_user_gift_added_mail', array('entry_amount' => $wallet_add, 'user_model' => $user_model), true);

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From:Solesilove.com.au <no-reply@solesilove.com.au>' . "\r\n";

                mail($user, $user_subject, $user_message, $headers);
        }

        public function SuccessMail($wallet_id) {
                $user_wallet = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                $wallet_history = WalletHistory::model()->findByPk($wallet_id);


                $credit_amount = Yii::app()->Currency->convert($wallet_history->amount);

                $user = $user_wallet->email;
                $user_subject = 'Solesilove.com.au : Credit Money ' . $credit_amount . ' has been successfully added!';
                $user_message = $this->renderPartial('_user_wallet_mail', array('user_wallet' => $user_wallet, 'wallet_history' => $wallet_history), true);

                $admin = AdminUser::model()->findByPk(4)->email;
                $admin_subject = 'Solesilove.com.au : Credit Money ' . $credit_amount . ' has been successfully added to ' . $user_wallet->first_name;
                $admin_message = $this->renderPartial('_admin_wallet_mail', array('user_wallet' => $user_wallet, 'wallet_history' => $wallet_history), true);
                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                // More headers
                $headers .= 'From: Solesilove.com.au<no-reply@solesilove.com.au>' . "\r\n";

                mail($user, $user_subject, $user_message, $headers);
                mail($admin, $admin_subject, $admin_message, $headers);
        }

        /*
         * if payment got any error
         */

        public function actionCreditError($wallet_id) {
                $wallet_history = WalletHistory::model()->findByPk($wallet_id);

                $username = UserDetails::model()->findByPk($wallet_history->user_id);
                if (!empty($wallet_history) && !empty($username)) {

                        $this->errorMail($wallet_history->id);
                        $wallet_history->delete();
                        Yii::app()->user->setFlash('wallet_error', "Oops some error occured.Transaction rejected.");
                        $this->redirect(array('Index'));
                } else {
                        die('114:Error Occured');
                }
        }

        /* error mail for user */

        public function ErrorMail($wallet_id) {


                $user_wallet = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                $wallet_history = WalletHistory::model()->findByPk($wallet_id);
                $user = $user_wallet->email;

                $user_subject = 'Solesilove.com.au : Transaction Failure - Credit Money!';
                $user_message = $this->renderPartial('_error_wallet_mail', array('user_wallet' => $user_wallet, 'wallet_history' => $wallet_history), true);

                $admin = AdminUser::model()->findByPk(4)->email;
                $admin_subject = 'Solesilove.com.au : Transaction Failure - Credit Money!';
                $admin_message = $this->renderPartial('_admin_wallet_mail', array('user_wallet' => $user_wallet, 'wallet_history' => $wallet_history), true);
                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                // More headers
                $headers .= 'From: Solesilove.com.au<no-reply@solesilove.com.au>' . "\r\n";

                mail($user, $user_subject, $user_message, $headers);
                mail($admin, $admin_subject, $admin_message, $headers);
        }

        public function actionGetAmount() {
                $gift_code = $_POST['gift_code'];
                $model = UserGiftscardHistory::model()->findByAttributes(array('unique_code' => $gift_code, 'status' => 1));
                if (!empty($model)) {
                    if($model->user_id != 0){
                        if(Yii::app()->session['user']['id'] == $model->user_id){
                            echo $model->amount;
                        }else{
                             echo 0;
                        }
                    }else{
                        echo $model->amount;
                    }
                } else {
                        echo 0;
                }
        }

        public function siteURL() {
                $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
                $domainName = $_SERVER['HTTP_HOST'];
                return $protocol . $domainName;
        }

}
