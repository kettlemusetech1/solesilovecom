<?php

class ForgotPasswordController extends Controller {

        public function actionIndex() {

                if (isset($_POST['submit'])) {
                        $email = $_POST['emailaddres'];
                        $user = UserDetails::model()->findByAttributes(array('email' => $email));
                        if ($user != '') {
                                $details = UserDetails::model()->findByPk($user->id);
                                $forgot = new ForgotPassword;
                                $forgot->user_id = $details->id;
                                $forgot->code = rand(10000, 1000000);
                                $token = base64_encode($forgot->user_id . ':' . $forgot->code);
                                // $token = $forgot->code;
                                $forgot->status = 1;
                                $forgot->DOC = date('Y-m-d');
                                if ($forgot->save(FALSE)) {
                                        $this->SuccessMail($forgot, $token, $details);
                                        $this->render('mail');
                                } else {
                                        $this->render('sorry');
                                }exit;
                        }
                }
                $this->render('index');
        }

        public function SuccessMail($forgot, $token, $details) {

                $user = $details->email;
                $user_subject = 'Please reset your Artstra.com password';
                $user_message = $this->renderPartial('_password_reset_link_email', array('token' => $token, 'model' => $details), true);
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: Artstra.com <no-reply@arstrafashion.com/>' . "\r\n";
                mail($user, $user_subject, $user_message, $headers);
        }

        public function actionChangepassword($token) {

                $var = base64_decode($token);
                $arr = explode(':', $var);
                $id = $arr[0];
                $token2 = $arr[1];

                $token_test = ForgotPassword::model()->findByAttributes(array('code' => $token2, 'user_id' => $id));
                $pass1 = UserDetails::model()->findByPk($id);

                if ($token_test != '') {
                        Yii::app()->session['frgt_usrid'] = $id;
                        $token_test->delete();
                        $this->render('changepassword', array('id' => $id));
                } else {

                        Yii::app()->user->setFlash('error', " Invalid user...");
                        $this->render('index');
                }
        }

        public function actionNewpassword() {
                if (isset($_POST['btn_submit'])) {

                        if (isset(Yii::app()->session['frgt_usrid'])) {

                                $id = $_POST['uid'];
                                $pass1 = UserDetails::model()->findByPk($id);
                                $newpass = $_POST['password1'];
                                $pass1->password = $newpass;
                                $pass1->email_verification = 1;
                                if ($pass1->save(false)) {
                                        Yii::app()->user->setFlash('success', "<p style='font-size:16px;'>Your password has been successfully updated! </p><br/><a class='btn-primary' style='text-transform:none;display: inline-block;' href='" . Yii::app()->baseUrl . "/index.php/site/login'>CLICK HERE TO LOGIN</a>");
                                        $this->PasswordResetMail($pass1);
                                        $this->render('successpassword');
                                } else {

                                        Yii::app()->user->setFlash('error', " Invalid User...");
                                        $this->render('changepassword');
                                }
                        }
                }
                $this->render('changepassword');
        }

        public function PasswordResetMail($model) {
                $user = $model->email;

                $user_subject = 'Your password has been successfully updated! ';
                $user_message = $this->renderPartial('password_reset_email', array('model' => $model), true);
                $admin = AdminUser::model()->findByPk(4)->email;

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: Artstra.com<no-reply@artstrafashion.com>' . "\r\n";

                mail($user, $user_subject, $user_message, $headers);
        }

        public function siteURL() {
                $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
                $domainName = $_SERVER['HTTP_HOST'];
                return $protocol . $domainName;
        }

}
