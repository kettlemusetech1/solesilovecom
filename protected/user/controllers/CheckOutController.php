<?php

class CheckOutController extends Controller {

        public function init() {

                if (!isset(Yii::app()->session['user'])) {

                        $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
                }
        }

        public function actionAddresspreview() {
                $id = $_REQUEST['id'];
                $address = UserAddress::model()->findByPk($id);

                $array = array('fname' => $address->first_name,
                    'lname' => $address->last_name,
                    'add1' => $address->address_1,
                    'add2' => $address->address_2,
                    'country' => Countries::model()->findByPk($address->country)->country_name,
                    'state' => $address->state,
                    'city' => $address->city,
                    'postcode' => $address->postcode,
                    'phonecode' => $address->phonecode,
                    'phone' => $address->contact_number
                );
                $json = CJSON::encode($array);
                echo $json;
        }

        public function actionGiftOption() {
                if (isset($_POST['btn_submit'])) {
                        $model = UserGifts::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id'], 'order_id' => Yii::app()->session['orderid']));

                        if (!empty($model)) {
                                Yii::app()->user->setFlash('error', "already applied.... ");
                                $this->redirect(Yii::app()->request->urlReferrer);
                        } else {
                                $data = new UserGifts();
                                $data->user_id = Yii::app()->session['user']['id'];
                                $data->order_id = Yii::app()->session['orderid'];
                                $data->from = $_POST['from'];
                                $data->to = $_POST['to'];
                                $data->status = 1;
                                $data->date = date('Y-m-d');
                                $data->message = $_POST['message'];
                                if ($data->validate()) {
                                        if ($data->save()) {
                                                $order = Order::model()->findByPk($data->order_id);
                                                $order->gift_option = 1;
                                                if ($order->total_amount > 2249) {
                                                        $order->rate = 0;
                                                } else {
                                                        $order->rate = 250;
                                                }
                                                $order->save();
                                                Yii::app()->user->setFlash('success', "Item added as gift");
                                                $this->redirect(Yii::app()->request->urlReferrer);
                                        } else {
//data not saved
                                        }
                                } else {
//validation error
                                }
                        }
                } else {
                        echo "sorry";
                        exit;
                }
        }

        public function actionCartItems() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $cart = Cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));

                        if (!empty($cart)) {

                                $this->render('confirmation', array('carts' => $cart));
                        } else {
//todo cart is empty message
                        }
                } else {
//todo invalid user
                }
        }

        public function actionGetCountry() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $country = $_POST['country'];

                        $shipping_charge = UserAddress::model()->findByPk($country);

                        if (!empty($shipping_charge)) {
                                echo $shipping_charge->country;
                        } else {
                                echo 0;
                        }
                }
        }


        public function actionGettotalpay() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $country = $_POST['country'];
                        $shipping_charge = ShippingCharges::model()->findByAttributes(array('country' => $country));

                        $carts = Cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));

                        foreach ($carts as $cart) {
                                $prod_details = Products::model()->findByPk($cart->product_id);
                                $producttotal = Yii::app()->Discount->DiscountAmount($prod_details) * $cart->quantity;
                                if ($cart->gift_option != 0) {
                                        $gift += $cart->rate;
                                }
                                $product_price += $producttotal;
                        }
                        $discount = CouponHistory::model()->findByAttributes(array('order_id' => Yii::app()->session['orderid']))->total_amount;
                        $subtotal = $gift + $product_price - $discount;
//aramex
                        if ($shipping_charge->shipping_type == 3) {
                                $grant_total = $subtotal + $shipping_charge->shipping_rate;
                                $totalpay = $subtotal + $shipping_charge->shipping_rate;
                        } else {
                                $grant_total = $subtotal + $shipping_charge->shipping_rate;
                                $totalpay = $subtotal + $shipping_charge->shipping_rate;
                        }
                      
                        $grant_total = Yii::app()->Currency->convert($grant_total);

                        if (!empty($shipping_charge)) {
                                if ($shipping_charge->shipping_type == 3) {
                                        $ship_amount = Yii::app()->Currency->convert($shipping_charge->shipping_rate);
                                } else {
                                        $ship_amount = Yii::app()->Currency->convert($shipping_charge->shipping_rate);
                                }

                                $array = array('granttotal' => $grant_total, 'totalpay' => $totalpay, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total);
                                $json = CJSON::encode($array);
                                echo $json;
                        } else {
                                $ship_amount = 0;
                                $array = array('granttotal' => $grant_total, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total);
                                $json = CJSON::encode($array);
                                echo $json;
                        }
                }
        }

        public function actiontotalcalculate() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $wallet = $_POST['wallet'];
                        if ($wallet < 0) {
                                $wallet = 0;
                        }
                        // $grant = $_POST['grant'];
                        $country = $_POST['country'];

                        if ($country == 99) {
                                $total_shipping_rate = 0;
                        } else {
                                $get_zone = Countries::model()->findByPk($country);
                                $get_total_weight = $this->GetTotalWeight();
                                $get_total_weight = $get_total_weight / 1000;

                                $total_weight = $get_total_weight;
                                /* 13% Fuel Charge and 15 % Service chARGE is applicable */
                                $shipping_rate = ShippingCharges::model()->findByAttributes(array('zone' => $get_zone->zone, 'weight' => $total_weight));
                                if (!empty($shipping_rate)) {
                                        if ($shipping_rate->shipping_type == 3) {
                                                $service_charge = $shipping_rate->shipping_rate * .15;
                                                $total_shipping_rate = ceil($shipping_rate->shipping_rate + $service_charge);
                                        } else {
                                                $fuel_charge = $shipping_rate->shipping_rate * .15;
                                                $service_charge = ($shipping_rate->shipping_rate + $fuel_charge) * .15;
                                                $total_shipping_rate = ceil($shipping_rate->shipping_rate + $fuel_charge + $service_charge);
                                        }
                                } else {
                                        $total_shipping_rate = 0;
                                }
                        }
                        $carts = Cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                        foreach ($carts as $cart) {
                                $prod_details = Products::model()->findByPk($cart->product_id);
                                $producttotal = Yii::app()->Discount->DiscountAmount($prod_details) * $cart->quantity;
                                if ($cart->gift_option != 0) {
                                        $gift += $cart->rate;
                                }
                                $product_price += $producttotal;
                        }
                        $subtotal = $gift + $product_price;
                        $totalpay = $subtotal + $this->Shipping();
                       
                        $tax = $this->Tax($totalpay);
                        $grant_total = $subtotal + $this->Shipping() + $tax;
                         $checkout_amount = $grant_total;
                        if (isset(Yii::app()->session['currency'])) {
                                $currency_rate = Yii::app()->session['currency']['rate'];
                        } else {
                                $currency_rate = 1;
                        }
                        $discount = CouponHistory::model()->findByAttributes(array('order_id' => Yii::app()->session['orderid'], 'user_id' => Yii::app()->session['user']['id']));

                        $grant_total = round(($currency_rate * $grant_total ) - ($currency_rate * $discount->total_amount), 2);

                        $cwallet = UserDetails::model()->findByPk(Yii::app()->session['user']['id'])->wallet_amt;
                        if (isset(Yii::app()->session['currency'])) {
                                $currentwallet = round($cwallet * $currency_rate, 2);
                        } else {
                                $currentwallet = $cwallet;
                        }
                        
                        //$total_balance_to_pay = $grant_total + $tax;
                        
                        $wallet_balance = $currentwallet - $wallet;
                        /*
                         * Calculate wallet balance and total balance
                         *                          */
                        if ($grant_total >= $currentwallet) {
                                if ($wallet_balance >= 0) {
                                        $total_balance_to_pay = $grant_total - $wallet;
                                        $wallet = $wallet;
                                } else {
                                        $total_balance_to_pay = $grant_total - $currentwallet;

                                        $wallet = $currentwallet;
                                        $wallet_balance = $currentwallet - $wallet;
                                }
                        } else {
                                if ($wallet_balance >= 0) {
                                        $total_balance_to_pay = $grant_total  - $wallet;
                                        $wallet = $wallet;
                                } else {
                                        $total_balance_to_pay = $grant_total - $currentwallet;
                                        $wallet = $grant_total;
                                        $wallet_balance = $currentwallet - $wallet;
                                }
                        }
$totalamount = $total_balance_to_pay;

                        if ($total_balance_to_pay < 0) {
                                $total_balance_to_pay = 0;
                        }
                        
                        
                        $update_order = Order::model()->findByPk(Yii::app()->session['orderid']);
                  $update_order->total_amount = $grant_total;
                        $update_order->save(false);
                        if (isset(Yii::app()->session['currency'])) {
                                if (Yii::app()->session['currency']['rate'] == 1) {
                                        $totalamount = $totalamount;
                                } else {
                                        $totalamount = ceil($totalamount / Yii::app()->session['currency']['rate']);
                                }
                        }
                       
                        if (isset(Yii::app()->session['currency'])) {
                                if (Yii::app()->session['currency']->symbol != "") {
                                        $total_balance_to_pay = '<i class="fa ' . Yii::app()->session['currency']['symbol'] . '"></i>' . number_format($total_balance_to_pay, 2);
                                        $wallet_balance = '<i class="fa ' . Yii::app()->session['currency']['symbol'] . '"></i>' . number_format($wallet_balance, 2);
                                          $new_tax = '<i class="fa ' . Yii::app()->session['currency']['symbol'] . '"></i>' . number_format($tax, 2);
                                } else {
                                        $total_balance_to_pay = number_format($total_balance_to_pay, 2) . " " . Yii::app()->session['currency']->currency_code;

                                        $wallet_balance = number_format($wallet_balance, 2) . " " . Yii::app()->session['currency']->currency_code;
                                        $new_tax = number_format($tax, 2) . " " . Yii::app()->session['currency']->currency_code;
                                }
                        } else {
                                $total_balance_to_pay = '<i class="fa fa-usd"></i>' . number_format($total_balance_to_pay, 2);
                                $wallet_balance = '<i class="fa fa-usd"></i>' . number_format($wallet_balance, 2);
                                $new_tax = '<i class="fa fa-usd"></i>' . number_format($tax, 2);
                        }
                       
//            $total_balance_to_pay = $total_balance_to_pay + $this->Shipping();
                        $array = array('totalamounttopay' => $total_balance_to_pay, 'wallet_balance' => $wallet_balance, 'wallet' => $wallet, 'total' => $totalamount,'tax'=>$new_tax,'checkout_amount'=>$checkout_amount);
                        $json = CJSON::encode($array);
                        echo $json;
                }
        }

        public function actionGetshippingcharge() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $country = $_POST['country'];

                        if ($country == 99) {
                                $total_shipping_rate = 0;
                        } else {
                                $get_zone = Countries::model()->findByPk($country);
                                $get_total_weight = $this->GetTotalWeight();
                                $get_total_weight = $get_total_weight / 1000;

                                $total_weight = $get_total_weight;
                                /* 13% Fuel Charge and 15 % Service chARGE is applicable */
                                $shipping_rate = ShippingCharges::model()->findByAttributes(array('zone' => $get_zone->zone, 'weight' => $total_weight));

                                if (!empty($shipping_rate)) {
                                        if ($shipping_rate->shipping_type == 3) {
                                                $service_charge = $shipping_rate->shipping_rate * .15;
                                                $total_shipping_rate = ceil($shipping_rate->shipping_rate + $service_charge);
                                        } else {
                                                $fuel_charge = $shipping_rate->shipping_rate * .15;

                                                $service_charge = ($shipping_rate->shipping_rate + $fuel_charge) * .15;
                                                $total_shipping_rate = ceil($shipping_rate->shipping_rate + $fuel_charge + $service_charge);
                                        }
                                } else {
                                        $total_shipping_rate = 0;
                                }
                        }




                        $carts = Cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));

                        foreach ($carts as $cart) {
                                $prod_details = Products::model()->findByPk($cart->product_id);
                                $producttotal = Yii::app()->Discount->DiscountAmount($prod_details) * $cart->quantity;
                                if ($cart->gift_option != 0) {
                                        $gift += $cart->rate;
                                }
                                $product_price += $producttotal;
                        }
                        $subtotal = $gift + $product_price;

                        $discount = CouponHistory::model()->findByAttributes(array('order_id' => Yii::app()->session['orderid']));
                        $grant_total = ceil($subtotal + $total_shipping_rate - $discount->total_amount);
                        $totalpay = $subtotal + $total_shipping_rate;
                        if (isset(Yii::app()->session['currency'])) {
                                if (Yii::app()->session['currency']['rate'] == 1) {
                                        $totalpay = $totalpay;
                                } else {
                                        $totalpay = ceil($totalpay / Yii::app()->session['currency']['rate']);
                                }
                        }
                        $grant_total = Yii::app()->Currency->convert($grant_total);

                        if (!empty($shipping_rate)) {
                                $ship_amount = Yii::app()->Currency->convert($total_shipping_rate);
                                $array = array('granttotal' => $grant_total, 'totalpay' => $totalpay, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total);
                                $json = CJSON::encode($array);
                                echo $json;
                        } else {
                                $ship_amount = 0;
                                $array = array('granttotal' => $grant_total, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total);
                                $json = CJSON::encode($array);
                                echo $json;
                        }
                }
        }
 public function Tax($total) {
  /*   $gettax = Settings::model()->findByAttributes(array('id'=>1));
     $calculate_tax = $total *  $gettax->tax_rate/100;
    $calculate_tax =  round($calculate_tax,2);
     return $calculate_tax;
     */
     return 0;
 }
        public function GetTotalWeight() {
                $carts = Cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                if (!empty($carts)) {
                        foreach ($carts as $cart) {
                                $prod_details = Products::model()->findByPk($cart->product_id);
                                if ($cart->options != 0) {
                                        $option_stock = OptionDetails::model()->findByPk($cart->options);
                                        if (!empty($option_stock)) {
                                                if ($option_stock->stock >= $cart->quantity) {
                                                        $quantity = $cart->quantity;
                                                } else {
                                                        $quantity = 0;
                                                }
                                        }
                                } else {
                                        if ($prod_details->quantity >= $cart->quantity) {

                                                $quantity = $cart->quantity;
                                        } else {
                                                $quantity = 0;
                                        }
                                }

                                $tot = $prod_details->weight * $quantity;

                                $total_weight += $tot;
                        }
                        return $total_weight;
                } else {
                        return 0;
                }
        }

        public function actionGetShippingMethod() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $country = $_POST['country'];
                        if ($country == 99) {
                                $total_shipping_rate = 0;
                        } else {
                                $get_zone = Countries::model()->findByPk($country);
                                $get_total_weight = $this->GetTotalWeight();
                                $get_total_weight = $get_total_weight / 1000;


                                $total_weight = $get_total_weight;


                                /* 13% Fuel Charge and 15 % Service chARGE is applicable */
                                $shipping_rate = ShippingCharges::model()->findByAttributes(array('zone' => $get_zone->zone, 'weight' => $total_weight));
                                if (!empty($shipping_rate)) {
                                        if ($shipping_rate->shipping_type == 3) {
                                                $service_charge = $shipping_rate->shipping_rate * .15;
                                                $total_shipping_rate = ceil($shipping_rate->shipping_rate + $service_charge);
                                        } else {
                                                $fuel_charge = $shipping_rate->shipping_rate * .15;
                                                $service_charge = ($shipping_rate->shipping_rate + $fuel_charge) * .15;
                                                $total_shipping_rate = ceil($shipping_rate->shipping_rate + $fuel_charge + $service_charge);
                                        }
                                } else {
                                        $total_shipping_rate = 0;
                                }
                        }

                        if ($country == 99) {
                                $this->renderPartial('_shipping_indian', array('shipping_charge' => $total_shipping_rate));
                        } else {
                                if (!empty($shipping_rate)) {
                                        $this->renderPartial('_shipping_other', array('shipping_charge' => $total_shipping_rate));
                                } else {
                                        echo 'Please select country.';
                                }
                        }
                } else {
//todo invalid user
                }
        }

        public function actionCheckOut() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $user = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                        $order = Order::model()->findByPk(Yii::app()->session['orderid']);
                        $cart = Cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));

                        if (!empty($cart)) {

                                $deafult_shipping = UserAddress::model()->findByAttributes(array('userid' => Yii::app()->session['user']['id'], 'default_shipping_address' => 1));
                                $deafult_billing = UserAddress::model()->findByAttributes(array('userid' => Yii::app()->session['user']['id'], 'default_billing_address' => 1));
                                $addresss = UserAddress::model()->findAllByAttributes(array('userid' => Yii::app()->session['user']['id']));

                                if (!empty($deafult_billing)) {
                                        $billing = $deafult_billing;
                                } else {
                                        $billing = new UserAddress;
                                }
                                if (!empty($deafult_shipping)) {
                                        $shipping = $deafult_shipping;
                                } else {
                                        $shipping = new UserAddress;
                                }
//                              $order = Order::model()->findbypk(Yii::app()->session['orderid']);
                                $address_id = '';

                                if ($_POST['payment_method']) { 
                                        $post_wallet = $_POST['wallet_amount'];
                                        if ($post_wallet != '') {
                                                $post_wallet = $post_wallet;
                                        } else {
                                                $post_wallet = 0;
                                        }
                                        $post_total_pay = $_POST['total_pay'];


                                        if ($_POST['bill_address'] == 0) {

                                                $useraddress = new UserAddress;
                                                $useraddress->userid = Yii::app()->session['user']['id'];
                                                $useraddress->attributes = $_POST['UserAddress']['bill'];

                                                $useraddress->phonecode = $_POST['UserAddress']['bill']['phonecode'];
                                                if (isset($_POST['UserAddress']['bill']['state1']) && $_POST['UserAddress']['bill']['state1'] != '') {

                                                        $useraddress->state = $_POST['UserAddress']['bill']['state1'];
                                                } else {

                                                        $useraddress->state = $_POST['UserAddress']['bill']['state'];
                                                }

                                                if ($useraddress->validate()) {
                                                        $billing_address = $this->addAddress($billing, $_POST['UserAddress']['bill']);
                                                        $bill_address_id = $billing_address;
                                                } else {
                                                        $this->render('checkout', array('carts' => $cart, 'user' => $user, 'deafult_shipping' => $deafult_shipping, 'addresss' => $addresss, 'shipping' => $shipping, 'orderid' => $order->id, 'billing' => $useraddress));
                                                        exit;
                                                }
                                                //    $billing_address = $this->addAddress($billing, $_POST['UserAddress']['bill']);
                                        } else {
                                                $bill_address_id = $_POST['bill_address'];
                                                $billing = UserAddress::model()->findByPk($bill_address_id);
                                        }
                                        if ($_POST['billing_same'] == NULL) {
                                                if ($_POST['ship_address'] == 0) {
                                                        $useraddressship = new UserAddress;
                                                        $useraddressship->userid = Yii::app()->session['user']['id'];
                                                        $useraddressship->attributes = $_POST['UserAddress']['ship'];
                                                        $useraddressship->phonecode = $_POST['UserAddress']['ship']['phonecode'];
                                                        if (isset($_POST['UserAddress']['ship']['state1']) && $_POST['UserAddress']['ship']['state1'] != '') {
                                                                $useraddressship->state = $_POST['UserAddress']['ship']['state1'];
                                                        } else {
                                                                $useraddressship->state = $_POST['UserAddress']['ship']['state'];
                                                        }
                                                        if ($useraddressship->validate()) {
                                                                $shipping_address = $this->addAddress($shipping, $_POST['UserAddress']['ship']);
                                                                $ship_address_id = $shipping_address;
                                                        } else {
                                                                //  var_dump($useraddressship);
                                                                // exit;
                                                                $this->render('checkout', array('carts' => $cart, 'user' => $user, 'deafult_shipping' => $deafult_shipping, 'addresss' => $addresss, 'shipping' => $useraddressship, 'orderid' => $order->id, 'billing' => $billing));
                                                                exit;
                                                        }
//                                $shipping_address = $this->addAddress($shipping, $_POST['UserAddress']['ship']);
                                                } else {
                                                        $useraddressship = new UserAddress;
                                                        $useraddressship->userid = Yii::app()->session['user']['id'];
                                                        $useraddressship->attributes = $_POST['UserAddress']['ship'];
                                                        $useraddressship->phonecode = $_POST['UserAddress']['ship']['phonecode'];
                                                        if (isset($_POST['UserAddress']['ship']['state1']) && $_POST['UserAddress']['ship']['state1'] != '') {
                                                                $useraddressship->state = $_POST['UserAddress']['ship']['state1'];
                                                        } else {
                                                                $useraddressship->state = $_POST['UserAddress']['ship']['state'];
                                                        }
                                                        if ($useraddressship->validate()) {
                                                                $shipping_address = $this->addAddress($billing, $_POST['UserAddress']['ship']);
                                                                $ship_address_id = $shipping_address;
                                                        } else {
                                                                //  var_dump($useraddressship);
                                                                // exit;
                                                                $this->render('checkout', array('carts' => $cart, 'user' => $user, 'deafult_shipping' => $deafult_shipping, 'addresss' => $addresss, 'shipping' => $useraddressship, 'orderid' => $order->id, 'billing' => $billing));
                                                                exit;
                                                        }
//                            $ship_address_id = $_POST['ship_address'];
                                                }
                                        } else {
                                                $useraddressship = new UserAddress;
                                                $useraddressship->userid = Yii::app()->session['user']['id'];
                                                $useraddressship->attributes = $_POST['UserAddress']['ship'];
                                                $useraddressship->phonecode = $_POST['UserAddress']['ship']['phonecode'];
                                                if (isset($_POST['UserAddress']['ship']['state1']) && $_POST['UserAddress']['ship']['state1'] != '') {
                                                        $useraddressship->state = $_POST['UserAddress']['ship']['state1'];
                                                } else {
                                                        $useraddressship->state = $_POST['UserAddress']['ship']['state'];
                                                }
                                                if ($useraddressship->validate()) {
                                                        $shipping_address = $this->addAddress($billing, $_POST['UserAddress']['ship']);
                                                        $ship_address_id = $shipping_address;
                                                } else {
                                                        $this->render('checkout', array('carts' => $cart, 'user' => $user, 'deafult_shipping' => $deafult_shipping, 'addresss' => $addresss, 'shipping' => $useraddressship, 'orderid' => $order->id, 'billing' => $billing));
                                                        exit;
                                                }

                                        }
                                        $shipp_address = UserAddress::model()->findByPk($ship_address_id);
                                        if ($shipp_address->country == 99) {
                                                $total_shipping_rate = 0;
                                        } else {
                                                $total_shipping_rate = 0;
                                        }

                                        if ($_POST['wallet_amount'] != '') {
                                                $wallet = $_POST['wallet_amount'];
                                        } else {
                                                $wallet = 0;
                                        }
                                        $currency_rate = 1;
                                        $subtotal = $this->subtotal();
                                        $granttotal = round(($currency_rate * $subtotal) + ($currency_rate * $total_shipping_rate), 2);
                                        $cwallet = UserDetails::model()->findByPk(Yii::app()->session['user']['id'])->wallet_amt;
                                        $currentwallet = round($cwallet * $currency_rate, 2);
                                } else {
                                        $currentwallet = $cwallet;
                                }
                                $wallet_balance = $currentwallet - $wallet;

                                if (((int) $wallet == $wallet ) && ($wallet > 0)) {
                                        $total_balance_to_pay = $granttotal + $this->Shipping() - $wallet;
                                } else {
                                        $total_balance_to_pay = $granttotal + $this->Shipping();
                                }

                                $total_balance_to_pay = $total_balance_to_pay * $currency_rate;
                                $tax = $this->Tax($total_balance_to_pay);
                                $total_balance_to_pay = $total_balance_to_pay + $tax;
                                /////////////////check later/////////////////
                              /* if ($wallet != $post_wallet || $total_balance_to_pay != $post_total_pay) {
                                        Yii::app()->user->setFlash('checkout_error', "There is an error found on checkout. Please fill carefully and check out again");
                                        $this->redirect(array('CheckOut/CheckOut'));
                                }*/
                                if ($bill_address_id != '' && $ship_address_id != '') {

                                        $order_id = Yii::app()->session['orderid'];

                                        $this->addOrder($bill_address_id, $ship_address_id, $cart, $order_id);

                                        $order->shipping_method = 1;

                                        if ($wallet > 0) {

                                                /* wallet entry starts */
                                                $wallet_amount = new WalletHistory;
                                                $wallet_amount->user_id = Yii::app()->session['user']['id'];
                                                $wallet_amount->type_id = 4;
                                                $wallet_amount->amount = $wallet;
                                                $wallet_amount->entry_date = date('Y-m-d H:i:s');
                                                $wallet_amount->credit_debit = 2;
                                                $wallet_amount->balance_amt = $currentwallet - $wallet;
                                                $wallet_amount->ids = $order_id;
                                                $wallet_amount->field2 = 0;
                                                $wallet_amount->save(FALSE);

                                                /* wallet entry ends */

                                                if ($post_total_pay == 0) {
                                                        $order->payment_mode = 1;
                                                        $order->wallet = $wallet;
                                                } else {
                                                        if ($post_total_pay != 0 && $_POST['wallet_amount'] != 0) {
                                                                $order->payment_mode = 4;
                                                                if ($_POST['payment_method'] == 2) {
                                                                $order->netbanking = $post_total_pay;
                                                                $order->wallet = $wallet;
                                                                $order->paypal ='0.00';
                                                                } else if ($_POST['payment_method'] == 3) {
                                                                $order->paypal = $post_total_pay;
                                                                $order->wallet = $wallet;
                                                                $order->netbanking ='0.00';
                                                                }
                                                        } else {
                                                           $order->payment_mode = $_POST['payment_method'];
                                                           if ($_POST['payment_method'] == 2) {
                                                           $order->netbanking = $post_total_pay;
                                                           $order->paypal ='0.00';
                                                           } else if ($_POST['payment_method'] == 3) {
                                                           $order->paypal = $post_total_pay;
                                                           $order->netbanking ='0.00';
                                                           }
                                                                //$order->payment_mode = 2;
                                                               // $order->netbanking = $post_total_pay;
                                                        }
                                                }

                                                $order->bill_address_id = $bill_address_id;
                                                $order->ship_address_id = $ship_address_id;
                                                $order->order_date = date('Y-m-d H:i:s');
                                                $order->shipping_charge = $total_shipping_rate;
                                                $order_billing_details = UserAddress::model()->findBypk($bill_address_id);
                                                $order_shipping_detils = UserAddress::model()->findBypk($ship_address_id);
                                                if ($order->validate()) {
//                            if (Yii::app()->user->hasFlash('shipp_availability') != 1) {
                                                        if ($order->save()) {
                                                                Cart::model()->deleteAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                                                                $this->updateorderproduct($order->id);
                                                                if ($user->save()) {
                                                                        Yii::app()->session['user'] = $user;
                                                                }
                                                                if ($post_total_pay != 0) {

                                                                        /* payment action goes here */

                                                                        if ($order->netbanking != '') {
                                                                                $hdfc_details = array();
                                                                                $hdfc_details['description'] = 'SolesiLove Products';
                                                                                $hdfc_details['order'] = $order->id;
                                                                                $hdfc_details['totaltopay'] = $order->netbanking * 100;
                                                                               
                                                                                $hdfc_details['bill_firstname'] = $order_billing_details->first_name;
                                                                                $hdfc_details['bill_lastname'] = $order_billing_details->last_name;
                                                                                $hdfc_details['bill_address'] = $order_billing_details->address_1 . ' ' . $order_billing_details->address_2;
                                                                                $hdfc_details['bill_city'] = $order_billing_details->city;
                                                                                $hdfc_details['bill_state'] = $order_billing_details->state;
                                                                                $hdfc_details['bill_postal_code'] = $order_billing_details->postcode;
                                                                                $hdfc_details['bill_country'] = Countries::model()->findbypk($order_billing_details->country)->country_name;
                                                                                $hdfc_details['bill_email'] = Yii::app()->session['user']['email'];
                                                                                $hdfc_details['bill_phone_number'] = $order_billing_details->contact_number;

                                                                                $hdfc_details['ship_firstname'] = $order_shipping_detils->first_name;
                                                                                $hdfc_details['ship_lastname'] =$order_shipping_detils->last_name;
                                                                                $hdfc_details['ship_address'] = $order_shipping_detils->address_1 . ' ' . $order_shipping_detils->address_2;
                                                                                $hdfc_details['ship_city'] = $order_shipping_detils->city;
                                                                                $hdfc_details['ship_state'] = $order_shipping_detils->state;
                                                                                $hdfc_details['ship_postal_code'] = $order_shipping_detils->postcode;
                                                                                $hdfc_details['ship_country'] = Countries::model()->findbypk($order_shipping_detils->country)->country_name;
                                                                                $hdfc_details['ship_email'] = Yii::app()->session['user']['email'];
                                                                                $hdfc_details['ship_phone_number'] = $order_shipping_details->contact_number;
                                                                                
                                                                        $this->renderPartial('eway', array('hdfc_details' => $hdfc_details));
                                                                        exit;
                                                                        }
                                                                        else if ($order->paypal != '') {

                                                                        $pid = time();
                                                                        $paypaltotalamount = $order->paypal;
                                                                        $this->render('paypalpay', array('itemname' => 'SolesiLove Products', 'order' => $order->id, 'totaltopay' => $paypaltotalamount, 'pid' => $pid));
                                                                        exit;
                                                                        }
                                                                }
                                                                $this->redirect(array('OrderSuccess'));
                                                        }
//                            }
                                                }
                                        } else {

                                                if ($post_total_pay == 0) {
                                                        $order->payment_mode = 1;
                                                        $order->wallet = $wallet;
                                                } else {
                                                        if ($post_total_pay != 0 && $_POST['wallet_amount'] != 0) {
                                                                $order->payment_mode = 4;
                                                                if ($_POST['payment_method'] == 2) {
                                                                $order->netbanking = $post_total_pay;
                                                                $order->wallet = $wallet;
                                                                $order->paypal ='0.00';
                                                                } else if ($_POST['payment_method'] == 3) {
                                                                $order->paypal = $post_total_pay;
                                                                $order->wallet = $wallet;
                                                                $order->netbanking ='0.00';
                                                                }
                                                        } else {
                                                           $order->payment_mode = $_POST['payment_method'];
                                                           if ($_POST['payment_method'] == 2) {
                                                           $order->netbanking = $post_total_pay;
                                                           $order->paypal ='0.00';
                                                           } else if ($_POST['payment_method'] == 3) {
                                                           $order->paypal = $post_total_pay;
                                                           $order->netbanking ='0.00';
                                                           }
                                                        }
                                                }

                                                $order->payment_mode = $_POST['payment_method'];
                                               // $order->netbanking = $_REQUEST['total_pay'];
                                                $order->bill_address_id = $bill_address_id;
                                                $order->ship_address_id = $ship_address_id;
                                                $order->shipping_charge = $total_shipping_rate;
                                                $order_billing_details = UserAddress::model()->findBypk($bill_address_id);
                                                $order_shipping_detils = UserAddress::model()->findBypk($ship_address_id);
//  $order->status = 1;
                                                if ($order->validate()) {
//                            if (Yii::app()->user->hasFlash('shipp_availability') != 1) {

                                                        if ($order->save()) {


                                                                $this->updateorderproduct($order->id);
//$this->redirect(array('OrderHistory'));
                                                                if ($post_total_pay != 0) {

                                                                        /* payment action goes here */

                                                                        if ($order->netbanking != '0.00') {
                                                                                $hdfc_details = array();
                                                                                $hdfc_details['description'] = 'SolesiLove Products';
                                                                                $hdfc_details['order'] = $order->id;
                                                                                $hdfc_details['totaltopay'] = $order->netbanking * 100;
                                                                               
                                                                                $hdfc_details['bill_firstname'] = $order_billing_details->first_name;
                                                                                $hdfc_details['bill_lastname'] = $order_billing_details->last_name;
                                                                                $hdfc_details['bill_address'] = $order_billing_details->address_1 . ' ' . $order_billing_details->address_2;
                                                                                $hdfc_details['bill_city'] = $order_billing_details->city;
                                                                                $hdfc_details['bill_state'] = $order_billing_details->state;
                                                                                $hdfc_details['bill_postal_code'] = $order_billing_details->postcode;
                                                                                $hdfc_details['bill_country'] = Countries::model()->findbypk($order_billing_details->country)->country_name;
                                                                                $hdfc_details['bill_email'] = Yii::app()->session['user']['email'];
                                                                                $hdfc_details['bill_phone_number'] = $order_billing_details->contact_number;
                                                                                $hdfc_details['ship_name'] = $order_shipping_detils->first_name . ' ' . $order_shipping_detils->last_name;
                                                                                $hdfc_details['ship_address'] = $order_shipping_detils->address_1 . ' ' . $order_shipping_detils->address_2;
                                                                                $hdfc_details['ship_city'] = $order_shipping_detils->city;
                                                                                $hdfc_details['ship_state'] = $order_shipping_detils->state;
                                                                                $hdfc_details['ship_postal_code'] = $order_shipping_detils->postcode;
                                                                                $hdfc_details['ship_country'] = Countries::model()->findbypk($order_shipping_detils->country)->country_name;
                                                                                $hdfc_details['ship_email'] = Yii::app()->session['user']['email'];
                                                                                $hdfc_details['ship_phone_number'] = $order_shipping_details->contact_number;
                                                                       $this->renderPartial('eway', array('hdfc_details' => $hdfc_details));
                                                                       exit;
                                                                            
                                                                        }
                                                                        else if ($order->paypal != '') {

                                                                        $pid = time();
                                                                        $paypaltotalamount = $order->paypal;
                                                                        $this->render('paypalpay', array('itemname' => 'SolesiLove Products', 'order' => $order->id, 'totaltopay' => $paypaltotalamount, 'pid' => $pid));
                                                                        exit;
                                                                        }
                                                                } else {
                                                                        $this->redirect(array('OrderSuccess'));
                                                                }
                                                        }
//                            }
                                                }
//
                                        }
                                }
                        } else {
                                $this->redirect(array('Cart/Mycart'));
                        }
                        $this->render('checkout', array('carts' => $cart, 'user' => $user, 'deafult_shipping' => $deafult_shipping, 'addresss' => $addresss, 'shipping' => $shipping, 'orderid' => $order_id, 'billing' => $billing));
                } else {
                        $this->redirect(array('site/login'));

//todo render a cart empty page here
                }
        }

        public function updateorderproduct($orderid) {
           
             $ord=   OrderProducts::model()->updateAll(array('status' => 1), array('condition' => 'order_id = ' . $orderid));
           //  var_dump($ord);exit;
        }

        public function actionPayment() { 
                $this->renderPartial('eway');
        }


        public function giftpack($orderid) {
                $giftpacks = OrderProducts::model()->findAllByAttributes(array('order_id' => $orderid));
                foreach ($giftpacks as $giftpack) {
                        $tot_price = $giftpack->rate;
                }
                $total += $tot_price;
                return $total;
        }

        public function updatecoupon($coupen) {
                $coupen->status = 2;
                if ($coupen->save()) {
                        $coupen_used = new CouponsUsed;
                        $coupen_used->attributes = $coupen->attributes;
                        $coupen_used->user_id = Yii::app()->session['user']['id'];
                        $coupen_used->DOC = date('Y-m-d');
                        $coupen_used->status = 2;
                        $coupen_used->save();
                }
        }

        public function addOrder($bill_address_id, $ship_address_id, $cart, $order_id) {
                $model1 = $this->loadModel($order_id);
                $total_amt = $this->total($cart);
                $model1->ship_address_id = $ship_address_id;
                $model1->bill_address_id = $bill_address_id;
                // $model1->total_amount = $total_amt;

                if ($model1->save(false)) {
                        return $model1->id;
                }
        }

        public function actionCurrencyconvert() {
                $value = $_POST['total'];
                $result = Yii::app()->Currency->convert($value);
                echo $result;
        }

        public function addAddress($model, $data) {

                $model->attributes = $data;
                $model->address_1 = $data['address_1'];
                $model->address_2 = $data['address_2'];
                $model->contact_number = $data['contact_number'];
                $model->phonecode = $data['phonecode'];
                if ($data['state1'] != "") {

                        $model->state = $data['state1'];
                } else {
                        $model->state = $data['state'];
                }
                $model->CB = Yii::app()->session['user']['id'];
                $model->DOC = date('Y-m-d');
                $model->userid = Yii::app()->session['user']['id'];
                // if ($model->validate()) {
                if ($model->save(false)) {
                        return $model->id;
                } else {

                        return false;
                }
                // } else {
                return false;
                // }
        }

        public function subtotal() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $cart = cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                        if (!empty($cart)) {

                        }
                        foreach ($cart as $car) {
                                $product_value = Products::model()->findByPk($car->product_id);
                                $subtotal = $subtotal + ($car->quantity * Yii::app()->Discount->DiscountAmount($product_value)) + $car->rate;
                        }
                }

                return $subtotal;
        }

        public function granttotal() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $cart = cart::model()->findByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                }
                if (!empty($cart)) {
                        foreach ($cart as $car) {
                                $product_value = Products::model()->findByPk($car->product_id);
                                $subtotal = $subtotal + ($car->quantity * Yii::app()->Discount->DiscountAmount($product_value));
                        }
                }
                return Yii::app()->Currency->convert($subtotal);
        }

        public function totalamount() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $cart = cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                }
                if (!empty($cart)) {
                        foreach ($cart as $car) {
                                $product_value = Products::model()->findByPk($car->product_id);
                                $subtotal = $subtotal + ($car->quantity * Yii::app()->Discount->DiscountAmount($product_value));
                               
                        }
                }
                return $subtotal;
        }

        public function addCoupens() {
                $coupen = Coupons::model()->findByPk(Yii::app()->session['coupen_id']);
                $model1 = new Order;
                $model1->user_id = Yii::app()->session['user']['id'];
                $total_amt = $this->total($cart);
                $model1->total_amount = $total_amt;
                $model1->status = 0;
                $model1->coupen_id = $coupen->id;
                $model1->discount_rate = $coupen->discount;
                $model1->order_date = date('Y-m-d H:i:s');
                $model1->DOC = date('Y-m-d');

                if ($model1->save()) {
                        return $model1->id;
                }
        }

        public function total($cart) {

                foreach ($cart as $carts) {
                        $prod_details = Products::model()->findByPk($carts->product_id);
                        $cart_qty = $carts->quantity;
                        if ($carts->options != 0) {

                                $price = Options::model()->findByPk($carts->options)->amount;
                        } else {
                                $price = Yii::app()->Discount->DiscountAmount($prod_details);
                        }
                        $tot_price = $cart_qty * $price;
                        $total+= $tot_price;
                }
                return $total;
        }

        public function Shipping() {
                $subtotal = $this->totalamount();
                $get_settings = Settings::model()->findByPk(1);
                $user = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                if ($user->first_purchase == 0 && $user->account_type ==1) {
                    
                        $shipping_rate = 0;
                } else {
                   
                        if ($subtotal >= $get_settings->free_shipping_limit) {

                                $shipping_rate = 0;
                        } else {
                                $shipping_rate = $get_settings->shipping_charge;
                        }
                }

                return $shipping_rate;
        }

        public function actionOrderHistory() {
                if (isset(Yii::app()->session['user']['id'])) {
                        $cart = Cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                        $order = Order::model()->findByPk(Yii::app()->session['orderid']);
                        if (!empty($cart)) {
                                if (isset($_POST['mycomment'])) {
                                        $comment = $_POST['comment'];
                                        $order->comment = $comment;
                                        $order->status = 1;
                                        $order->payment_status = 1;
                                        $order->currency_id = Yii::app()->session['currency']->id;
                                        $order->update();
                                        $orderprod = OrderProducts::model()->findByAttributes(array('order_id' => $order->id));
                                        $products = Products::model()->findByPk($orderprod->product_id);

                                        if ($order->payment_status = 1 && $products->subtract_stock = 1) {

                                                $products->quantity = $products->quantity - $orderprod->quantity;
                                                $products->update();
                                        } else {

                                        }
                                        Cart::model()->deleteAllByAttributes(array(), array('condition' => 'user_id = ' . Yii::app()->session['user']['id']));
                                        $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/index');
                                }
                                $user_details = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                                $ship_address = UserAddress::model()->findByPk($order->ship_address_id);
                                $bill_address = UserAddress::model()->findByPk($order->bill_address_id);

                                $order->save();
                                $this->render('order_history', array('carts' => $cart, 'ship_address' => $ship_address, 'bill_address' => $bill_address, 'user_details' => $user_details, 'order' => $order));
                        } else {
//todo cart is empty message
                        }
                } else {
//todo invalid user
                }
        }

        /*

         * Order Success Action         */

        /* mail send to admin and user */

        public function actionOrderSuccess($payid = '', $tid = '', $amt = '') {
                if (isset(Yii::app()->session['orderid']) && Yii::app()->session['user']['id'] != '') {
                        $order = Order::model()->findByPk(Yii::app()->session['orderid']);
                        $userdetails = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                        $userdetails->first_purchase = 1;
                        $userdetails->save(false);
                        $check_product_option = OrderProducts::model()->findAllByAttributes(array('order_id' => $order->id, 'status' => 1));
                        Cart::model()->deleteAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                        $wallet_history = WalletHistory::model()->findByAttributes(array('user_id' => Yii::app()->session['user']['id'], 'type_id' => 4, 'ids' => $order->id));
                        if (!empty($order) && !empty($userdetails)) {
                                if ($order->payment_mode == 1 || $order->payment_mode == 4 || $order->payment_mode == 5) {
                                        $userdetails->wallet_amt = $userdetails->wallet_amt - $order->wallet;
                                        $wallet_history->field2 = 1;
                                        $userdetails->save(false);
                                        $wallet_history->save();
                                }
                                if (!empty($check_product_option)) {
                                        foreach ($check_product_option as $product_options) {

                                                if ($product_options->option_id == 0) {
                                                        $product = Products::model()->findByPk($product_options->product_id);
                                                        if ($product->quantity >= $product_options->quantity) {
                                                                $product->quantity = $product->quantity - $product_options->quantity;
                                                        }
                                                        $product->save(false);
                                                } else {
                                                        $option_details = OptionDetails::model()->findByPk($product_options->option_id);
                                                        if ($option_details->stock >= $product_options->quantity) {
                                                                $option_details->stock = $option_details->stock - $product_options->quantity;
                                                        }
                                                        $option_details->save(false);
                                                }
                                        }
                                }
                                $order->payment_status = 1;
                                $order->status = 1;
                                $order->transaction_id = $payid;
                                if ($order->save(false)) {

                                        $this->SuccessMail();
                                        $this->OrderHistory($order->id, 1, 'Order Placed');

                                        Yii::app()->session['user'] = $userdetails;
                                        unset(Yii::app()->session['orderid']);
                                        if ($order->payment_mode == 1) {
                                                $amt = Yii::app()->Currency->convert($order->wallet);
                                                $tid = $order->id;
                                        }
                                        $tid = $order->id;
                                        //$amt = Yii::app()->Currency->convert($order->total_amount);
                                        $this->redirect(array('CheckOut/Successmessage/payid/' . $payid . '/tid/' . $tid . '/amt/' . $amt));
                                }
                        } else {
                                $this->redirect(array('OrderFailed'));
                        }
                } else {
                        $this->render('//site/error');
                }
        }

        public function actionSuccessmessage($payid = '', $tid = '', $amt = '') {
                $this->render('payment_success', ['payid' => $payid, 'tid' => $tid, 'amt' => $amt]);
        }

        public function SuccessMail() {
                $order = Order::model()->findByPk(yii::app()->session['orderid']);

                $userdetails = UserDetails::model()->findByPk($order->user_id);
                $user_address = UserAddress::model()->findByPk($order->ship_address_id);

                $bill_address = UserAddress::model()->findByPk($order->bill_address_id);
                $order_details = OrderProducts::model()->findAllByAttributes(array('order_id' => $order->id));
                $shiping_charge = ShippingCharges::model()->findByAttributes(array('country' => $user_address->country));
                $cats = Cart::model()->findAllByAttributes(array('session_id' => Yii::app()->session['temp_user'], 'user_id' => $order->user_id));
                $temp_user_gifts = UserGifts::model()->findAllByAttributes(array('order_id' => $order->id));
                $user = $userdetails->email;
                $user_subject = 'Soles Order Confirmation - Your Order #SLOR' . $order->id . ' has been successfully placed!';

                $admin = AdminUser::model()->findByPk(4)->email;
                $admin_subject = 'New Order from ' . $userdetails->first_name . ' '. $userdetails->last_name. '#SLOR' . $order->id;

                $user_message = $this->renderPartial('_user_order_success_mail', array('order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge, 'temp' => $temp_user_gifts), true);
                $admin = AdminUser::model()->findByPk(4)->email;

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: Solesilove.com<no-reply@solesilove.com.au>' . "\r\n";
                mail($user, $user_subject, $user_message, $headers);


                $admin_message = $this->renderPartial('_admin_order_success_mail', array('order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge, 'temp' => $temp_user_gifts), true);
                $admin = AdminUser::model()->findByPk(4)->email;

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: Solesilove.com<no-reply@solesilove.com.au>' . "\r\n";
                mail($admin, $admin_subject, $admin_message, $headers);
                
                
        }

        /* Order Error Action */

        public function actionOrderFailed($payid = '', $tid = '', $amt = '') {
                if (isset(Yii::app()->session['orderid']) && Yii::app()->session['user']['id'] != '') {
                        $order = Order::model()->findByPk(yii::app()->session['orderid']);
                        $userdetails = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                        $wallet_history = WalletHistory::model()->findByAttributes(array('user_id' => Yii::app()->session['user']['id'], 'type_id' => 4, 'ids' => $order->id));

                        if ($order->payment_mode == 4) {

                                $wallet_history->field2 = 0;
                                $wallet_history->save();
                        }

                        $order->payment_status = 2;
                        $order->status = 3;
                        if ($order->save()) {

                                $this->ErrorMail($userdetails);
                                $this->OrderHistory($order->id, 9, 'Payment Failure');
                                Yii::app()->session['user'] = $userdetails;
                                unset(Yii::app()->session['orderid']);
                                $this->render('order_failed');
                        }
                } else {
                        $this->render('//site/error');
                }
        }

        public function OrderHistory($id, $status, $status_comment) {
                $orderHistory = new OrderHistory;
                $orderHistory->order_id = $id;
                $orderHistory->order_status_comment = $status_comment;
                $orderHistory->order_status = $status;
                $orderHistory->shipping_type = 0;
                $orderHistory->tracking_id = 0;
                $orderHistory->date = date('Y-m-d H:i:s');
                $orderHistory->status = 1;
                $orderHistory->save(false);
        }

        /* ckeck out error mail  */

        public function ErrorMail($userdetails) {
                $order = Order::model()->findByPk(Yii::app()->session['orderid']);
                $user_address = UserAddress::model()->findByPk($order->ship_address_id);
                $bill_address = UserAddress::model()->findByPk($order->bill_address_id);
                $order_details = OrderProducts::model()->findAllByAttributes(array('order_id' => $order->id));
                $shiping_charge = ShippingCharges::model()->findByAttributes(array('country' => $user_address->country));
                $user = $userdetails->email;
                $user_subject = 'Solesilove.com.au: Order # SLOR ' . $order->id . ' :: Transaction Failure';
                $user_message = $this->renderPartial('_user_order_error_mail', array('order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge, 'userdetails' => $userdetails), true);


                $admin = AdminUser::model()->findByPk(4)->email;
                $admin_subject = 'Solesilove.com.au: Order # SLOR' . $order->id . ' :: Transaction Failure';
                $admin_message = $this->renderPartial('_admin_order_error_mail', array('order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge), true);

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                $headers .= 'From: Solesilove.com.au<no-reply@solesilove.com.au>' . "\r\n";
                // mail($user, $user_subject, $user_message, $headers);
                //  mail($admin, $user_subject, $user_message, $headers);
                 
                 
                $admin = AdminUser::model()->findByPk(4)->email;
               // $admin_subject = 'New Enquiry Recieved';

                $message = new YiiMailMessage;
                $message->view = "_user_order_error_mail";  // view file name
                $params = array('order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge, 'userdetails' => $userdetails); // parameters
                $message->subject = $user_subject;
                $message->setBody($params, 'text/html');
                $message->addTo($user);
                $message->setFrom('info@solesilove.com.au', 'Solesilove.com.au');
                Yii::app()->mail->send($message);
                
                
                
                 $message1 = new YiiMailMessage;
                $message1->view = "_admin_order_error_mail";  // view file name
                $params1 = array('order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge); // parameters
                $message1->subject = $admin_subject;
                $message1->setBody($params1, 'text/html');
                $message1->addTo($admin);
                $message1->setFrom('info@solesilove.com.au', 'Solesilove.com.au');
                Yii::app()->mail->send($message1);
                
                
                
        }


   public function contactmail($model) {
          
                $admin = AdminUser::model()->findByPk(4)->email;
                $admin_subject = 'New Enquiry Recieved';

                $message = new YiiMailMessage;
               
                $message->view = "_admin_contact_email";  // view file name
                $params = array('model' => $model); // parameters
                $message->subject = $admin_subject;
                    
                $message->setBody($params, 'text/html');
                 
                $message->addTo($admin);
             
                $message->setFrom('info@solesilove.com.au', 'Solesilove.com.au');
               
                Yii::app()->mail->send($message);
        }

        public function actionDeletGift($id) {
                $gift_details = UserGifts::model()->findByPk($id);
                if (!empty($gift_details)) {
                        $order = Order::model()->findByPk(Yii::app()->session['orderid']);
                        $order->gift_option = 0;
                        $order->rate = 0;
                        $order->save();
                        $model = UserGifts::model()->deleteByPk($id);
                        $this->redirect(Yii::app()->request->urlReferrer);
                } else {
                        echo "test";
                        exit;
                }
        }

        public function actionCoupon() {

                if (isset(Yii::app()->session['user']['id'])) {
                        $cart = Cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
                        $order = Order::model()->findByPk(Yii::app()->session['orderid']);
                        if (!empty($cart)) {

                                if (isset($_POST['btn_submit'])) {
// $id = Yii::app()->session['user']['id'];

                                        $coupon_code = Coupons::model()->findByAttributes(array('code' => $_POST['coupon'], 'status' => 1));
                                        $order_product = OrderProducts::model()->findByAttributes(array('order_id' => Yii::app()->session['orderid']));
                                        $user_id = false;
                                        $prod_id = false;
                                        $limit = false;
                                        $expry_dte = false;
                                        $strt_dte = false;
                                        $coupon_hist = CouponHistory::model()->findByAttributes(array('order_id' => Yii::app()->session['orderid'], 'coupon_id' => $coupon_code->id));
                                        if ($coupon_hist) {
                                                Yii::app()->user->setFlash('error', "Sorry coupon used");
                                        } else {
                                                if (!empty($coupon_code)) {

                                                        if ($coupon_code->user_id != '') {
                                                                $uid = explode(',', $coupon_code->user_id);
                                                                if (in_array(Yii::app()->session['user']['id'], $uid)) {
                                                                        $user_id = true;
                                                                } else {
                                                                        $user_id = false;
                                                                }
                                                        } else {

                                                                $user_id = true;
                                                        }
                                                        if ($coupon_code->product_id != '') {

                                                                $pid = explode(',', $coupon_code->product_id);
                                                                if (in_array($order_product->product_id, $pid)) {
                                                                        $prod_id = true;
                                                                } else {
                                                                        $prod_id = false;
                                                                }
                                                        } else {
                                                                $prod_id = true;
                                                        }

                                                        if ($coupon_code->expiry_date >= date('Y-m-d')) {
                                                                $expry_dte = true;
                                                        } else if ($coupon_code->expiry_date == 0000 - 00 - 00) {
                                                                $expry_dte = true;
                                                        }

                                                        if ($coupon_code->starting_date >= date('Y-m-d')) {
                                                                $strt_dte = false;
                                                        } else {
                                                                $strt_dte = true;
                                                        }
                                                        if ($coupon_code->starting_date == 0000 - 00 - 00) {
                                                                $strt_dte = true;
                                                        }
                                                        if ($coupon_code->cash_limit != '') {
                                                                if ($order->total_amount > $coupon_code->cash_limit) {
                                                                        $limit = true;
                                                                }
                                                        } else {
                                                                $limit = true;
                                                        }

                                                        if ($user_id == true && $prod_id == true && $strt_dte == true && $expry_dte == true && $limit == true) {
                                                                $this->discount($order, $coupon_code);
                                                                $this->couponcodes($order, $coupon_code);
                                                                if ($coupon_code->unique == 1) {
                                                                        $coupon_code->status = 0;
                                                                        $coupon_code->save();
                                                                }
                                                                Yii::app()->user->setFlash('success', "Your coupon code is submitted...");
                                                        } else {
                                                                Yii::app()->user->setFlash('error', "Sorry! Invalid coupon code..");
                                                        }
                                                } else {
                                                        Yii::app()->user->setFlash('error', "coupon is invalid");
                                                }
                                        }
                                }
                                $this->redirect(Yii::app()->request->urlReferrer);
                        } else {
//cart empty //
                        }
                } else {
// to do invalid user//
                }
        }

        public function discount($order, $coupon) {
                $model = $this->loadModel($order->id);
                if ($coupon->discount_type == 1) {
                        if ($order->discount_rate == 0)
                                $model->discount_rate = $order->total_amount - $coupon->discount;
                        else
                                $model->discount_rate = $order->discount_rate - $coupon->discount;
                } else {
                        if ($order->discount_rate == 0)
                                $model->discount_rate = (($order->total_amount) - (($coupon->discount / 100) * ($order->total_amount)));
                        else
                                $model->discount_rate = ($order->discount_rate - (($coupon->discount / 100) * ($order->total_amount)));
                }

                $model->coupon_id = $coupon->id;
                $model->update();
        }

        /* coupon histories */

        public function couponcodes($order, $coupon) {
                $model = new CouponHistory;
                $model->order_id = $order->id;
                $model->coupon_id = $coupon->id;
                if ($order->discount_rate == 0)
                        $model->total_amount = $order->total_amount - $coupon->discount;
                else
                        $model->total_amount = $order->discount_rate - $coupon->discount;
                $model->save();
        }

        public function loadModel($id) {
                $model = Order::model()->findByPk($id);
                if ($model === null)
                        throw new CHttpException(404, 'The requested page does not exist.');
                return $model;
        }

        public function siteURL() {
                $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
                $domainName = $_SERVER['HTTP_HOST'];
                return $protocol . $domainName;
        }

}
