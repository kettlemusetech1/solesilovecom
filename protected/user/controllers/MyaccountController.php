<?php

class MyaccountController extends Controller {

    public function init() {
        date_default_timezone_set('Asia/Kolkata');
    }

    public function actionIndex() {


        if (!isset(Yii::app()->session['user'])) {

            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $this->render('myaccount');
        }
    }

    public function actionProfile() {
        if (!isset(Yii::app()->session['user'])) {

            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        }
        $model = UserDetails::model()->findByPk(array('id' => Yii::app()->session['user']['id']));
        $this->render('myprofile', array('model' => $model));
    }

    public function actionProfileedit() {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $model = UserDetails::model()->findByPk(array('id' => Yii::app()->session['user']['id']));
            if (isset($_POST['Submit'])) {
                $model->first_name = $_POST['name'];
               // $model->email = $_POST['email'];
                $model->phone_no_2 = $_POST['moble'];
                $model->newsletter = $_POST['newsletter'];
                $model->UB = Yii::app()->session['user']['id'];
                if ($model->save(false)) {
                    Yii::app()->user->setFlash('Hi', " Your account has been updated.");
                    $this->render('myprofile');
                    exit;
                } else {
                    Yii::app()->user->setFlash('error', " Something went wrong..");
                }
            }
            $this->render('myprofile_edit', array('model' => $model));
        }
    }

    public function actionMyordernew($id = "") {
        if (!isset(Yii::app()->session['user'])) {
            Yii::app()->session["myorder"] = $id;
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            unset(Yii::app()->session["myorder"]);
            $myorders = Order::model()->findAllByAttributes(array('user_id' =>
                Yii::app()->session['user']['id']), array('condition' => 'status != 0', 'order' => 'id DESC'));
            $this->render('myorder_new', array('myorders' => $myorders));
        }
    }

    public function actionCreditHistory() {
        $history = WalletHistory::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']), ['order' => 'entry_date desc']);
        $this->render('credit_history', array('history' => $history));
    }

    public function actionChangePassword() {

        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {

            $model = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
            if (isset($_POST['submit'])) {
                if ($_REQUEST['current'] == $model->password) {
                    $model->password = $_POST['password'];
                    $model->confirm = $_POST['confirm'];
                    if($model->password == $model->confirm){
                        $model->save(FALSE);

                    Yii::app()->user->setFlash('success', 'Password successfully changed!');
                    $this->render('myprofile');
                    exit;
                    }else{
                          Yii::app()->user->setFlash('notice', 'New Password and confirm Password does not match.');
                    $this->render('changepassword');
                    exit;
                    }
                    
                } else {
                    Yii::app()->user->setFlash('notice', ' Incorrect Current Password');
                    $this->render('changepassword');
                    exit;
                }
            } else {
                $this->render('changepassword', array('model' => $model));
            }
        }
    }

    public function actionAddressbook() {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            if (Yii::app()->session['user']['id'] != '') {
                $user = UserDetails::model()->findByPk(Yii::app()->session['user']['id']);
                $model = UserAddress::model()->findAllByAttributes(array('userid' => Yii::app()->session['user']['id']));
                $this->render('addressbook', array('model' => $model));
            } else {
                $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
            }
        }
    }

    public function actionDeleteAddress() {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            if (isset($_GET['id'])) {
                $model = UserAddress::model()->deleteByPk($_GET['id']);
                $this->redirect(array('index'));
            }
        }
    }

    public function actionMywishlists() {
        if (!isset(Yii::app()->session['user'])) {
            Yii::app()->session['wishlist_user'] = 1;
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $wishlists = UserWishlist::model()->findAllByAttributes(array(), array('select' => 't.prod_id', 'distinct' => true, 'condition' => 'user_id = ' . Yii::app()->session['user']['id']));
            $this->render('mywishlists', array('wishlists' => $wishlists));
        }
    }

    public function actionRemoveMywishlists($pid) {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            UserWishlist::model()->deleteAllByAttributes(array('prod_id' => $pid, 'user_id' => Yii::app()->session['user']['id']));
            $this->redirect(Yii::app()->request->urlReferrer);
        }
    }

    public function actionMyorders() {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $myorder = Order::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id'], 'status' => 1), array('order' => 'id desc'));
            $this->render('myorder_new', array('myorder' => $myorder));
        }
    }

    public function actionOrderitems($id) {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $myorder = Order::model()->findByPk($id);
            $this->render('orders', array('myorders' => $myorder));
        }
    }

    public function actionPaymentHistory() {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $history = MakePayment::model()->findAllByAttributes(['userid' => Yii::app()->session['user']['id']], ['order' => 'date desc']);
            $this->render('make_payment_history', array('history' => $history));
        }
    }

    public function actionNewaddress() {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $model = new UserAddress;
            if (isset($_POST['UserAddress'])) {
                $model->attributes = $_POST['UserAddress'];
                if ($_POST['UserAddress']['state1'] != "") {

                    $model->state = $_POST['UserAddress']['state1'];
                } else {
                    $model->state = $_POST['UserAddress']['state'];
                }
                $model->address_for = $_POST['UserAddress']['address_for'];
                $model->phonecode = $_POST['UserAddress']['phonecode'];
                $model->address_1 = $_POST['UserAddress']['address_1'];
                $model->address_2 = $_POST['UserAddress']['address_2'];
                $model->userid = Yii::app()->session['user']['id'];
                $model = $this->checkDefault($model, 'default_billing_address');
                $model = $this->checkDefault($model, 'default_shipping_address');
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Your address has been  successfully Added");
                    $this->redirect(array('addressbook'));
                } else {
                    Yii::app()->user->setFlash('error', "Sorry! There is some error..");
                }
            }
            $this->render('newaddress', array('model' => $model));
        }
    }

    public function actionPaymentHistoryn($ord) {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $history = MakePayment::model()->findAllByAttributes(array('userid' => Yii::app()->session['user']['id'], 'order_id' => $ord), array('order' => 'date desc'));
            $this->render('make_payment_history', array('history' => $history));
        }
    }

    public function ProductEnquiryMailNormal($model) {

        $user = UserDetails::model()->findByPk($model->user_id);
        $toclient = Yii::app()->session['user']['email'];
        $toadmin = AdminUser::model()->findByPk(4)->email;
        $sizechartno = UserSizechart::model()->findByAttributes(array('id' => $model->id));

        $subject = 'Artstra Measurement Details';
        $subject1 = "$user->first_name" . " " . "$user->last_name" . 'Measurement Details';
        $message = $this->renderPartial('mail/_product_enquiry_mail_client', array('model' => $model, 'sizeno' => $sizechartno), true);

        $message1 = $this->renderPartial('mail/_product_enquiry_mail_admin', array('model' => $model, 'sizeno' => $sizechartno), true);
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        $headers .= 'From: Artstra.com <no-reply@intersmart.co.in.com>' . "\r\n";
        mail($toclient, $subject, $message, $headers);
    }

    public function checkDefault($model, $default) {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $default_address = UserAddress::model()->findAllByAttributes(array('userid' => Yii::app()->session['user']['id'], $default => 1));
            if (empty($default_address)) {
                $model->$default = 1;
            } elseif ($model->$default == 1) {
                $address = UserAddress::model()->updateAll(array($default => 0), 'userid = ' . Yii::app()->session['user']['id']);
                $model->$default = 1;
            } elseif ($model->$default == 0) {
                $model->$default = 0;
            }
            return $model;
        }
    }

    public function actionReview($id) {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $order = OrderProducts::model()->findByAttributes(array('id' => $id));
            $user = Order::model()->findByPk($order->order_id);
            if (Yii::app()->session['user']['id'] == $user->user_id && $user->status == 1) {
                $model = new UserReviews;
                if ($id != '') {
                    if (isset($_POST['UserReviews'])) {
                        $model->attributes = $_POST['UserReviews'];
                        if ($model->validate()) {
                            $model->product_id = $order->product_id;
                            $model->user_id = Yii::app()->session['user']['id'];
                            $model->review = $_POST['UserReviews']['review'];
                            $model->date = date('Y-m-d');
                            if ($model->save()) {

                                Yii::app()->user->setFlash('success', "your review has been  successfully added");
                            } else {
                                Yii::app()->user->setFlash('error', "Sorry! There is some error..");
                            }
                        }
                    }
                } else {
                    echo 'Invalid data.....';
                }
                $this->render('user_review', array('model' => $model, 'id' => $id));
            }
        }
    }

    public function ProductEnquiryMails($celib_history_update) {

        $toclient = ProductEnquiry::model()->findByPk($celib_history_update->enq_id)->email;

        $toadmin = AdminUser::model()->findByPk(4)->email;
        $enq_data = ProductEnquiry::model()->findByPk($celib_history_update->enq_id);
        $pdts = Products::model()->findByPk($enq_data->product_id);
        $message = $this->renderPartial('mail/_product_enquiry_mail_client_paymentinitiate', array('model' => $celib_history_update, 'enq_data' => $enq_data), true);

        $message1 = $this->renderPartial('mail/_product_enquiry_mail_admin_paymentinitiate', array('model' => $celib_history_update, 'enq_data' => $enq_data), true);
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        if ($celib_history_update->status == 3) {
            $subject = 'Artstra ' . "$pdts->product_name - " . 'Payment Details';
            $subject1 = "$enq_data->name " . ' - Payment Details';
            $subject2 = 'Celebstyle - ' . "$pdts->product_name - " . "$enq_data->name -" . ' Payment Details';
        }

        $headers .= 'From: Artstra.com<no-reply@intersmart.co.in>' . "\r\n";
        mail($toclient, $subject, $message, $headers);
        mail($toadmin, $subject2, $message, $headers);
    }

    public function actionGetshippingcharge() {
        if (isset(Yii::app()->session['user']['id'])) {

            $country = $_POST['country'];
            $product_id = $_POST['pdt'];

            if ($country == 99) {
                $total_shipping_rate = 0;
            } else {
                $get_zone = Countries::model()->findByPk($country);
                $get_total_weight = $this->GetTotalWeight($product_id);
                $get_total_weight = $get_total_weight / 1000;

                $total_weight = $get_total_weight;
                /* 13% Fuel Charge and 15 % Service chARGE is applicable */
                $shipping_rate = ShippingCharges::model()->findByAttributes(array('zone' => $get_zone->zone, 'weight' => $total_weight));

                if (!empty($shipping_rate)) {
                    if ($shipping_rate->shipping_type == 3) {
                        $service_charge = $shipping_rate->shipping_rate * .15;
                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $service_charge);
                    } else {
                        $fuel_charge = $shipping_rate->shipping_rate * .15;

                        $service_charge = ($shipping_rate->shipping_rate + $fuel_charge) * .15;
                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $fuel_charge + $service_charge);
                    }
                } else {
                    $total_shipping_rate = 0;
                }
            }





            $prod_details = Products::model()->findByPk($product_id);
            $producttotal = Yii::app()->Discount->DiscountAmount($prod_details) * 1;
            $product_price = $producttotal;

            $subtotal = $product_price;


            $grant_total = ceil($subtotal + $total_shipping_rate);
            $totalpay = $subtotal + $total_shipping_rate;

            if (isset(Yii::app()->session['currency'])) {
                if (Yii::app()->session['currency']['rate'] == 1) {
                    $totalpay = $totalpay;
                } else {
                    $totalpay = round($totalpay * Yii::app()->session['currency']['rate']);
                }
            }
            $grant_total = Yii::app()->Currency->convertNew($grant_total);

            if (!empty($shipping_rate)) {
                $ship_amount = Yii::app()->Currency->convertNew($total_shipping_rate);
                $array = array('granttotal' => $grant_total, 'totalpay' => $totalpay, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total);
                $json = CJSON::encode($array);
                echo $json;
            } else {
                $ship_amount = Yii::app()->Currency->convertNew(0);
                ;
                $array = array('granttotal' => $grant_total, 'totalpay' => $totalpay, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total);
                $json = CJSON::encode($array);
                echo $json;
            }
        }
    }

    public function actionGetshippingchargepartialfull() {
        if (isset(Yii::app()->session['user']['id'])) {
            $partial = $_POST['partial'];
            $country = $_POST['country'];
            $product_id = $_POST['pdt'];

            if ($country == 99) {
                $total_shipping_rate = 0;
            } else {
                $get_zone = Countries::model()->findByPk($country);
                $get_total_weight = $this->GetTotalWeight($product_id);
                $get_total_weight = $get_total_weight / 1000;

                $total_weight = $get_total_weight;

                /* 13% Fuel Charge and 15 % Service chARGE is applicable */
                $shipping_rate = ShippingCharges::model()->findByAttributes(array('zone' => $get_zone->zone, 'weight' => $total_weight));

                if (!empty($shipping_rate)) {
                    if ($shipping_rate->shipping_type == 3) {
                        $service_charge = $shipping_rate->shipping_rate * .15;
                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $service_charge);
                    } else {
                        $fuel_charge = $shipping_rate->shipping_rate * .15;

                        $service_charge = ($shipping_rate->shipping_rate + $fuel_charge) * .15;
                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $fuel_charge + $service_charge);
                    }
                } else {
                    $total_shipping_rate = 0;
                }
            }





            $prod_details = Products::model()->findByPk($product_id);
            $producttotal = Yii::app()->Discount->DiscountAmount($prod_details) * 1;
            $product_price = $producttotal;

            $subtotal = $product_price;


            $grant_total = ceil($subtotal + $total_shipping_rate);
            $totalpay = $subtotal + $total_shipping_rate;
            if ($partial == 1) {
                $grant_total = $totalpay / 2;
                $grant_total = ceil($grant_total);
                $totalpay = $totalpay / 2;
            }
            if (isset(Yii::app()->session['currency'])) {
                if (Yii::app()->session['currency']['rate'] == 1) {
                    $totalpay = ceil($totalpay);
                } else {
                    $totalpay = round($totalpay * Yii::app()->session['currency']['rate']);
                }
            }



            $grant_total = Yii::app()->Currency->convertNew($grant_total);
            if (!empty($shipping_rate)) {
                $ship_amount = Yii::app()->Currency->convert($total_shipping_rate);
                $array = array('granttotal' => $grant_total, 'totalpay' => $totalpay, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total);
                $json = CJSON::encode($array);
                echo $json;
            } else {
                $ship_amount = 0;
                $array = array('granttotal' => $grant_total, 'totalpay' => $totalpay, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total);
                $json = CJSON::encode($array);
                echo $json;
            }
        }
    }

    public function GetTotalWeight($product_id) {


        $prod_details = Products::model()->findByPk($product_id);
        $tot = $prod_details->weight;

        $total_weight = $tot;

        return $total_weight;
    }

    public function actionEditAddress($id) {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {

            $model = UserAddress::model()->findByPk($id);
            if (isset($_POST['UserAddress'])) {
                $model->attributes = $_POST['UserAddress'];
                if ($_POST['UserAddress']['state1'] != "") {

                    $model->state = $_POST['UserAddress']['state1'];
                } else {
                    $model->state = $_POST['UserAddress']['state'];
                }
                $model->address_for = $_POST['UserAddress']['address_for'];
                $model = $this->checkDefault($model, 'default_billing_address');
                $model = $this->checkDefault($model, 'default_shipping_address');
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Your address has been  successfully updated");
                    $this->redirect(array('addressbook'));
                } else {
                    Yii::app()->user->setFlash('error', "Sorry! There is some error..");
                }
            }
            $this->render('editaddress', array('model' => $model));
        }
    }

    public function actionGetShippingMethod($pdt_id) {
        if (isset(Yii::app()->session['user']['id'])) {
            $country = $_POST['country'];
            if ($country == 99) {
                $total_shipping_rate = 0;
            } else {
                $get_zone = Countries::model()->findByPk($country);
                $get_total_weight = $this->GetTotalWeight($pdt_id);
                $get_total_weight = $get_total_weight / 1000;


                $total_weight = $get_total_weight;
                /* 13% Fuel Charge and 15 % Service chARGE is applicable */
                $shipping_rate = ShippingCharges::model()->findByAttributes(array('zone' => $get_zone->zone, 'weight' => $total_weight));
                if (!empty($shipping_rate)) {
                    if ($shipping_rate->shipping_type == 3) {
                        $service_charge = $shipping_rate->shipping_rate * .15;
                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $service_charge);
                    } else {
                        $fuel_charge = $shipping_rate->shipping_rate * .15;
                        $service_charge = ($shipping_rate->shipping_rate + $fuel_charge) * .15;
                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $fuel_charge + $service_charge);
                    }
                } else {
                    $total_shipping_rate = 0;
                }
            }

            if ($country == 99) {
                $this->renderPartial('_shipping_indian', array('shipping_charge' => $total_shipping_rate));
            } else {
                if (!empty($shipping_rate)) {
                    $this->renderPartial('_shipping_other', array('shipping_charge' => $total_shipping_rate));
                } else {
                    echo 'Sorry, no shipping available at this place.';
                }
            }
        } else {

        }
    }

    public function addAddress($model, $data) {

        $model->attributes = $data;
        $model->first_name = $data['first_name'];
        $model->last_name = $data['last_name'];
        $model->city = $data['city'];
        $model->postcode = $data['postcode'];
        $model->country = $data['country'];
        $model->phonecode = $data['phonecode'];
        $model->state = $data['state'];
        $model->address_1 = $data['address_1'];
        $model->address_2 = $data['address_2'];
        $model->contact_number = $data['contact_number'];

        if ($data['state1'] != "") {

            $model->state = $data['state1'];
        } else {
            $model->state = $data['state'];
        }
        $model->CB = Yii::app()->session['user']['id'];
        $model->DOC = date('Y-m-d');
        $model->userid = Yii::app()->session['user']['id'];
        if ($model->save()) {
            return $model->id;
        } else {

            return false;
        }
    }

    public function actionSuccessmessage($payid = '', $tid = '', $amt = '', $oid = '') {
        $this->render('wallet_success', [ 'payid' => $tranid, 'tid' => $tid, 'amt' =>
            $amt]);
    }

    public function actionTest() {
        if (!
                isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            $myorders = Order::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
            $this->render('myorders', array('myorders' =>
                $myorders));
        }
    }

    public function actionSuccess($user_id, $wallet_id) {
        if (!isset(Yii::app()->session['user'])) {
            $this->redirect(Yii::app()->request->baseUrl . '/index.php/site/login');
        } else {
            if (!empty($user_id) && !empty($wallet_id) && $user_id != '' && $wallet_id != '') {
                $user_wallet = UserDetails::model()->findByPk($user_id);
                $wallet_history = WalletHistory::model()->findByPk($wallet_id);


                $amount = $user_wallet->wallet_amt + $wallet_history->amount;
                $user_wallet->wallet_amt = $amount;
                $wallet_history->field2 = 1; //success
                if ($wallet_history->save()) {
                    if ($user_wallet->save()) {
                        Yii::app()->session[
                                'user'] = $user_wallet;
                        Yii::app()->user->setFlash('wallet_success', "Money Added Successfully");
                        $this->redirect(array('Index'));
                    } else {
                        $wallet_history->delete();
                    }
                } else {
                    Yii::app()->user->setFlash('wallet_error', "Oops some error occured.Transaction rejected.");
                    $this->redirect(array
                        ('AddToWallet'));
                }
            } else {
                Yii::app()->user->setFlash('wallet_error', "Oops some error occured.Transaction rejected.");
                $this->redirect(array('AddToWallet'));
            }
        }
    }

    public function siteURL() {
        $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
        $domainName = $_SERVER['HTTP_HOST'];

        return $protocol .
                $domainName;
    }

    public function encrypt_decrypt($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';

// hash
        $key = hash('sha256', $secret_key);

// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return

                $output;
    }

    public function actionAddresspreview() {
        $id = $_REQUEST['id'];
        $address = UserAddress::model()->findByPk($id);

        $array = array('fname' => $address->first_name,
            'lname' => $address->last_name,
            'add1' => $address->address_1,
            'add2' => $address->address_2,
            'country' => Countries::model()->findByPk($address->country)->country_name,
            'state' => $address->state,
            'city' => $address->city,
            'postcode' => $address->postcode,
            'phonecode' => $address->phonecode,
            'phone' => $address->contact_number
        );
        $json = CJSON::encode($array);
        echo

        $json;
    }

    public function actionCurrencyconvert() {
        $value = $_POST['total'];
        $result = Yii::app()->Currency->convert($value);
        echo

        $result;
    }

    public function actiontotalcalculate() {
        if (isset(Yii::app()->session['user']['id'])) {
            $wallet = $_POST['wallet'];
            if ($wallet < 0) {
                $wallet = 0;
            }
            $country = $_POST['country'];

            if ($country == 99) {
                $total_shipping_rate = 0;
            } else {
                $get_zone = Countries::model()->findByPk($country);
                $get_total_weight = $this->GetTotalWeight();
                $get_total_weight = $get_total_weight / 1000;

                $total_weight = $get_total_weight;
                /* 13% Fuel Charge and 15 % Service chARGE is applicable */
                $shipping_rate = ShippingCharges::model()->findByAttributes(array('zone' => $get_zone->zone, 'weight' => $total_weight));
                if (!empty($shipping_rate)) {
                    if ($shipping_rate->shipping_type == 3) {
                        $service_charge = $shipping_rate->shipping_rate * .15;
                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $service_charge);
                    } else {
                        $fuel_charge = $shipping_rate->shipping_rate * .15;
                        $service_charge = ($shipping_rate->shipping_rate + $fuel_charge) * .15;
                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $fuel_charge + $service_charge);
                    }
                } else {
                    $total_shipping_rate = 0;
                }
            }
            $carts = Cart::model()->findAllByAttributes(array('user_id' => Yii::app()->session['user']['id']));
            foreach ($carts as $cart) {
                $prod_details = Products::model()->findByPk($cart->product_id);
                $producttotal = Yii::app()->Discount->DiscountAmount($prod_details) * $cart->quantity;
                if ($cart->gift_option != 0) {
                    $gift += $cart->rate;
                }
                $product_price += $producttotal;
            }
            $subtotal = $gift + $product_price;
            $grant_total = $subtotal + $total_shipping_rate;
            $totalpay = $subtotal + $total_shipping_rate;
            if (isset(Yii::app()->session['currency'])) {
                $currency_rate = Yii::app()->session['currency']['rate'];
            } else {
                $currency_rate = 1;
            }
            $discount = CouponHistory::model()->findByAttributes(array('order_id' => Yii::app()->session['orderid'], 'user_id' => Yii::app()->session['user']['id']));

            $grant_total = round(($currency_rate * $grant_total ) - ($currency_rate * $discount->total_amount), 2);

            $cwallet = UserDetails::model()->findByPk(Yii::app()->session['user']['id'])->wallet_amt;
            if (isset(Yii::app()->session['currency'])) {
                $currentwallet = round($cwallet * $currency_rate, 2);
            } else {
                $currentwallet = $cwallet;
            }
            $wallet_balance = $currentwallet - $wallet;
            /*
             * Calculate wallet balance and total balance
             *                          */
            if ($grant_total >= $currentwallet) {
                if ($wallet_balance >= 0) {
                    $total_balance_to_pay = $grant_total - $wallet;
                    $wallet = $wallet;
                } else {
                    $total_balance_to_pay = $grant_total - $currentwallet;

                    $wallet = $currentwallet;
                    $wallet_balance = $currentwallet - $wallet;
                }
            } else {
                if ($wallet_balance >= 0) {
                    $total_balance_to_pay = $grant_total - $wallet;
                    $wallet = $wallet;
                } else {
                    $total_balance_to_pay = $grant_total - $currentwallet;
                    $wallet = $grant_total;
                    $wallet_balance = $currentwallet - $wallet;
                }
            }


            if ($total_balance_to_pay < 0) {
                $total_balance_to_pay = 0;
            }

            $totalamount = $total_balance_to_pay;
            if (isset(Yii::app()->session['currency'])) {
                if (Yii::app()->session ['currency']['rate'] == 1) {
                    $totalamount = $totalamount;
                } else {
                    $totalamount = ceil($totalamount / Yii::app()->session['currency']['rate']);
                }
            }

            if (isset(Yii::app()->session['currency'])) {
                if (Yii::app()->session ['currency']->symbol != "") {
                    $total_balance_to_pay = '<i class="fa ' . Yii::app()->session ['currency'] ['symbol'] . '"></i>' . number_format($total_balance_to_pay, 2);
                    $wallet_balance = '<i class="fa ' . Yii::app()->session ['currency'] ['symbol'] . '"></i>' . number_format($wallet_balance, 2);
                } else {
                    $total_balance_to_pay = number_format($total_balance_to_pay, 2) . " " . Yii::app()->session['currency']->currency_code;

                    $wallet_balance = number_format($wallet_balance, 2) . " " . Yii::app()->session['currency']->currency_code;
                }
            } else {
                $total_balance_to_pay = '<i class="fa fa-usd"></i>' . number_format($total_balance_to_pay, 2);
                $wallet_balance = '<i class="fa fa-usd"></i>' . number_format($wallet_balance, 2);
            }
            $array = array('totalamounttopay' => $total_balance_to_pay, 'wallet_balance' => $wallet_balance, 'wallet' => $wallet, 'total' => $totalamount);
            $json = CJSON::encode($array);
            echo $json;
        }
    }

    public function actionGetCountry() {
        if (isset(Yii::app()->session['user']['id'])) {
            $country = $_POST['country'];

            $shipping_charge = UserAddress::model()->findByPk($country);

            if (!empty($shipping_charge)) {
                echo $shipping_charge->country;
            } else {
                echo 0;
            }
        }
    }

}
