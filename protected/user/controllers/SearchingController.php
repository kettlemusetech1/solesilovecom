<?php

class SearchingController extends Controller {

    public function actionIndex() {
        if (Yii::app()->request->isAjaxRequest) {

            if (isset($_REQUEST['SearchValue'])) { 
                $model = ProductCategory::model()->findAll(array('select' => 'category_name', 'limit' => 10,
                    "condition" => "category_name LIKE '" . $_REQUEST['SearchValue'] . "%'"));

                $model1 = MasterCategoryTags::model()->findAll(array('select' => 'category_tag', 'limit' => 10,
                    "condition" => "category_tag LIKE '" . $_REQUEST['SearchValue'] . "%'"));
                $this->renderPartial('_ajaxSearchDealers', array('model' => array_merge($model, $model1)));
            }
        } else {
            die("Can't access this url.");
        }
    }

    public function actionSearchList() {
        if (!Yii::app()->request->isAjaxRequest) { 
            unset(Yii::app()->session['toy_style']);
            unset(Yii::app()->session['width_fitting']);
            unset(Yii::app()->session['color']);
            unset(Yii::app()->session['size']);
            unset(Yii::app()->session['max']);
            unset(Yii::app()->session['min']);
        }
        if (isset($_REQUEST['keyword']) || isset(Yii::app()->session['keyword'])) {
            if (isset($_REQUEST['keyword'])) {
                $keyword = str_replace('"', "'", $_REQUEST['keyword']);
                Yii::app()->session['keyword'] = $keyword;
            } else if (isset(Yii::app()->session['keyword'])) {

            }

            $category_exist = ProductCategory::model()->find(array("condition" => 'category_name = "' . Yii::app()->session["keyword"] . '" OR search_tag LIKE "%' . Yii::app()->session["keyword"] . '%"'));
       
            $product_exist = Products::model()->findAll(array("condition" => 'product_name LIKE "%' . Yii::app()->session["keyword"] . '%"'));
            $brand_exist = Brands::model()->find(array("condition" => 'brand_name LIKE "%' . Yii::app()->session["keyword"] . '%"'));
            if (isset($_POST['pagesize'])) {
                $pagesize = $_POST['pagesize'];
                Yii::app()->session['pagesize'] = $pagesize;
            }

            if (!isset($pagesize)) {
                $pagesize = 15;
            }

            if (isset(Yii::app()->session['min'])) {
                $min = Yii::app()->session['min'];
            }

            if (isset(Yii::app()->session['max'])) {
                $max = Yii::app()->session['max'];
            }

            if (isset($_POST['sorting'])) {
                $categ = $_POST['sorting'];
                Yii::app()->session['sort_id'] = $_REQUEST['sorting'];
            } else {
                if (Yii::app()->request->isAjaxRequest) {
                    $categ = Yii::app()->session['sort_id'];
                } else {
                    //Yii::app()->session['sort_id'] = 1;
                    $categ = Yii::app()->session['sort_id'];
                }
            }
            if (isset($_POST['sorting'])) {
                $get_sort_value = $_POST['sorting'];
                if ($get_sort_value == 1) {
                    $srt = ' id DESC ';
                } else if ($get_sort_value == 2) {
                    $srt = 'price ASC';
                } else if ($get_sort_value == 3) {
                    $srt = 'price DESC';
                } else if ($get_sort_value == 4) {
                    $srt = 'product_name ASC';
                } else if ($get_sort_value == 5) {
                    $srt = 'product_name DESC';
                }
            } else {
                $srt = ' id DESC ';
            }
            if (isset(Yii::app()->session['sorting'])) {

            }
            if (!empty($category_exist)) {
                $categ = '';
                $parent = $category_exist;

                $category = ProductCategory::model()->findAllByAttributes(array('parent' => $parent->parent));

                $cats = ProductCategory::model()->findAllByattributes(array('parent' => $parent->id));
//var_dump($cats);
//exit;
                $finalcondition = Yii::app()->Menu->SearchByCategory($cats, $parent, $categ, $min = '', $max = '', $pagesize);
                // var_dump($finalcondition);
                // exit;
                $getproducts = Products::model()->findAll("(" . $finalcondition . ") AND status = 1");
                $dataProvider = new CActiveDataProvider('Products', array(
                    'criteria' => array(
                        'condition' => $finalcondition,
                    ),
                    'pagination' => array(
                        'pageSize' => $pagesize,
                    ),
                    'sort' => array(
                        'defaultOrder' => $srt,
                    )
                        )
                );
            } else if (!empty($product_exist)) {
                foreach ($product_exist as $product_data) {
                    $pids[] = $product_data->id;
                }
                $finalcondition = Yii::app()->Menu->SearchByProducts($pids, $parent, $categ, $min = '', $max = '', $pagesize);
                $getproducts = Products::model()->findAll("(" . $finalcondition . ") AND status = 1");
                $dataProvider = new CActiveDataProvider('Products', array(
                    'criteria' => array(
                        'condition' => $finalcondition,
                    ),
                    'pagination' => array(
                        'pageSize' => $pagesize,
                    ),
                    'sort' => array(
                        'defaultOrder' => $srt,
                    )
                        )
                );
            } else if (!empty($brand_exist)) {
                $get_products = Products::model()->findAllByAttributes(['brand' => $brand_exist->id]);
                if (!empty($get_products)) {
                    foreach ($get_products as $product_data) {
                        $pids[] = $product_data->id;
                    }
                    $finalcondition = Yii::app()->Menu->SearchByBrands($pids, $parent, $categ, $min = '', $max = '', $pagesize);
                    $getproducts = Products::model()->findAll("(" . $finalcondition . ") AND status = 1");
                    $dataProvider = new CActiveDataProvider('Products', array(
                        'criteria' => array(
                            'condition' => $finalcondition,
                        ),
                        'pagination' => array(
                            'pageSize' => $pagesize,
                        ),
                        'sort' => array(
                            'defaultOrder' => $srt,
                        )
                            )
                    );
                } else {
                    $dataProvider = NULL;
                    $getproducts = NULL;
                }
            } else {
                $dataProvider = NULL;
                $getproducts = NULL;
            }
            if ($dataProvider != NULL && $getproducts != NULL) {
                $this->render('searchresult', array('dataProvider' => $dataProvider, 'getproducts' => $getproducts, 'parent' => $parent, 'category' => $category, 'name' => $name, 'pagesize' => $pagesize));
            } else {
                $this->render('searchresult', array('dataProvider' => $dataProvider, 'getproducts' => $getproducts, 'parent' => $parent, 'category' => $category, 'name' => $name, 'pagesize' => $pagesize));

//                $this->render('no_result_found');
            }
//            $this->render('searchresult', array('dataProvider' => $dataProvider, 'file_name' => '_searchresult', 'parameter' => $_REQUEST['saerchterm'], 'search_parm' => $category, 'searchterm' => $searchterm));
            exit;
        }
    }

    public function actionPriceRange() {
        if (Yii::app()->request->isAjaxRequest) {

            $min = $_REQUEST['min'];
            $max = $_REQUEST['max'];
            $cat = $_REQUEST['cat'];
            $size_type = $_REQUEST['size'];
            $data[0] = $min;
            $data[1] = $max;
            $data[3] = $cat;

            if ($cat != '' && $min != '' && $max != '') {
                Yii::app()->session['temp_product_filter'] = $data;
            }
            if ($size_type != '') {
                $sizes = OptionCategory::model()->findByAttributes(array('option_type_id' => 2, 'id' => $size_type));
                if (!empty($sizes)) {
                    $data[4] = $size_type;
                    Yii::app()->session['temp_product_filter'][4] = $size_type;
                    Yii::app()->session['temp_product_filter_check'] = 1;
                } else {
                    $size_type = '';
                }
            }
            if ($cat != '' && $min != '' && $max != '') {
                $condition = "product_name LIKE '%" . $cat . "%'"
                        . " OR search_tag LIKE '%" . $cat . "%' ";
                $products = Products::model()->findAllByAttributes(array(), array('condition' => $condition));
                $b = 1;
                foreach ($products as $prods) {
                    if ($b == 1) {
                        $prod_ids .= $prods->id;
                    } else {
                        $prod_ids .= ',' . $prods->id;
                    }
                    $b++;
                }

                $dataProvider = Yii::app()->Menu->filterMenuProducts($prod_ids, $min, $max, $size_type);
            } else {
                $condition = "product_name LIKE '%" . Yii::app()->session['temp_product_filter'][3] . "%'"
                        . " OR search_tag LIKE '%" . Yii::app()->session['temp_product_filter'][3] . "%' ";
                $products = Products::model()->findAllByAttributes(array(), array('condition' => $condition));
                $b = 1;
                foreach ($products as $prods) {
                    if ($b == 1) {
                        $prod_ids .= $prods->id;
                    } else {
                        $prod_ids .= ',' . $prods->id;
                    }
                    $b++;
                }
                $dataProvider = Yii::app()->Menu->filterMenuProducts($prod_ids, Yii::app()->session['temp_product_filter'][0], Yii::app()->session['temp_product_filter'][1], Yii::app()->session['temp_product_filter'][4]);
            }

            $this->renderPartial('_view1', array('dataProvider' => $dataProvider, 'parent' => $parent, 'name' => $cat));
        } else {

        }
    }

}
