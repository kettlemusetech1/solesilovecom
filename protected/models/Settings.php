<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $id
 * @property string $email
 * @property string $email_1
 * @property string $phone
 * @property integer $free_shipping_limit
 * @property integer $shipping_charge
 * @property integer $tax_rate
 */
class Settings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, email_1, phone, free_shipping_limit, shipping_charge, tax_rate', 'required'),
			array('free_shipping_limit, tax_rate', 'numerical', 'integerOnly'=>true),
			array('email, email_1', 'length', 'max'=>50),
			array('phone', 'length', 'max'=>14),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, email_1, phone, free_shipping_limit, shipping_charge, tax_rate, shipping_charge', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'email_1' => 'Email 1',
			'phone' => 'Phone',
			'free_shipping_limit' => 'Free Shipping Limit',
			'shipping_charge' => 'Shipping Charge',
			'tax_rate' => 'Tax Rate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('email_1',$this->email_1,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('free_shipping_limit',$this->free_shipping_limit);
		$criteria->compare('shipping_charge',$this->shipping_charge);
		$criteria->compare('tax_rate',$this->tax_rate);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
