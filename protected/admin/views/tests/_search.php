<?php
/* @var $this TestsController */
/* @var $model Tests */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'f1'); ?>
		<?php echo $form->textField($model,'f1',array('class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'f2'); ?>
		<?php echo $form->textField($model,'f2',array('class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'f3'); ?>
		<?php echo $form->textField($model,'f3',array('class' => 'form-control')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->