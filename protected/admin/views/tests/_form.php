<?php
/* @var $this TestsController */
/* @var $model Tests */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tests-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <br/>
    <div class="form-inline">
                        <div class="form-group">
                    <?php echo $form->labelEx($model,'f1'); ?>
                    <?php echo $form->textField($model,'f1',array('class' => 'form-control')); ?>
                    <?php echo $form->error($model,'f1'); ?>
                </div>

                                <div class="form-group">
                    <?php echo $form->labelEx($model,'f2'); ?>
                    <?php echo $form->textField($model,'f2',array('class' => 'form-control')); ?>
                    <?php echo $form->error($model,'f2'); ?>
                </div>

                                <div class="form-group">
                    <?php echo $form->labelEx($model,'f3'); ?>
                    <?php echo $form->textField($model,'f3',array('class' => 'form-control')); ?>
                    <?php echo $form->error($model,'f3'); ?>
                </div>

                    </div>
    <div class="form-group btns">
        <label>&nbsp;</label><br/>
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-secondary btn-single pull-right', 'style' => 'border-radius:0px;padding: 10px 50px;')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->