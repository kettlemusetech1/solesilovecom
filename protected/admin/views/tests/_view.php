<?php
/* @var $this TestsController */
/* @var $data Tests */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f1')); ?>:</b>
	<?php echo CHtml::encode($data->f1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f2')); ?>:</b>
	<?php echo CHtml::encode($data->f2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f3')); ?>:</b>
	<?php echo CHtml::encode($data->f3); ?>
	<br />


</div>