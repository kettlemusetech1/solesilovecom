<?php
/* @var $this LatestNewsController */
/* @var $model LatestNews */

$this->breadcrumbs=array(
	'Latest News'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LatestNews', 'url'=>array('index')),
	array('label'=>'Create LatestNews', 'url'=>array('create')),
	array('label'=>'Update LatestNews', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LatestNews', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LatestNews', 'url'=>array('admin')),
);
?>

<h1>View LatestNews #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'news',
		'cb',
		'doc',
	),
)); ?>
