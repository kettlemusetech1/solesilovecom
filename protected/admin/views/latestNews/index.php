<?php
/* @var $this LatestNewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Latest News',
);

$this->menu=array(
	array('label'=>'Create LatestNews', 'url'=>array('create')),
	array('label'=>'Manage LatestNews', 'url'=>array('admin')),
);
?>

<h1>Latest News</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
