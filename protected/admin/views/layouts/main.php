<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>Solesilove | Admin</title>
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/admin.css">
                <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                <link rel="icon" href="<?php echo Yii::app()->baseUrl; ?>/images/fav.ico" type="image/x-icon" />
                <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/images/fav.ico" type="image/x-icon" />
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/dist/css/skins/skin-laksyah.min.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/admin.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
                <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/dist/css/AdminLTE.min.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/plugins/iCheck/flat/blue.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/plugins/morris/morris.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/plugins/datepicker/datepicker3.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/plugins/daterangepicker/daterangepicker-bs3.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/bootstrap-multiselect.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
                <script src="<?php echo Yii::app()->baseUrl ?>/admin-themes/plugins/slimScroll/jquery.slimscroll.min.js"></script>
                <script src="<?php echo Yii::app()->baseUrl ?>/admin-themes/plugins/fastclick/fastclick.min.js"></script>
                <script src="<?php echo Yii::app()->baseUrl ?>/admin-themes/dist/js/app.min.js"></script>
                <script src="<?php echo Yii::app()->baseUrl ?>/js/bootstrap-multiselect.js"></script>
                <script>
                        var baseurl = "<?php print Yii::app()->request->baseUrl . "/admin.php/"; ?>";
                        var basepath = "<?php print Yii::app()->basePath; ?>";
                </script>
        </head>
        <body class="hold-transition skin-laksyah sidebar-mini  fixed">
                <div class="wrapper">
                        <header class="main-header">
                                <a href="#" class="logo">
                                        <span class="logo-mini"><img src="<?php echo Yii::app()->baseUrl; ?>/images/logo_small.png" /></span>
                                        <span class="logo-lg"><img src="<?php echo Yii::app()->baseUrl; ?>/images/logo_small.png" /></span>

                                </a>
                                <nav class="navbar navbar-static-top" role="navigation">
                                        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                                                <span class="sr-only">Toggle navigation</span>
                                        </a>
                                        <div class="navbar-custom-menu">
                                                <ul class="nav navbar-nav">
                                                        <li class="notifications-menu">

                                                                <a class="header_info"> Logged in as admin</a>

                                                        </li>
                                                        <li class="notifications-menu">

                                                                <a class="header_info"> <?php echo date("l, F j, Y", strtotime(date('Y-m-d'))); ?></a>

                                                        </li>
                                                        <li class=" messages-menu">
                                                                <a href="<?php echo Yii::app()->baseUrl; ?>/admin.php/site/logOut" class="header_info"style="color: #F7931E;"><i class="fa fa-sign-out"></i> Logout</a>

                                                        </li>


                                                </ul>
                                        </div>


                                </nav>



                        </header>
                        <!-- Left side column. contains the logo and sidebar -->
                        <aside class="main-sidebar">

                                <!-- sidebar: style can be found in sidebar.less -->
                                <section class="sidebar">

                                        <?php
                                        $actionId = Yii::app()->controller->id;
                                        //  echo $actionId;
                                        switch ($actionId) {
                                                case "productCategory":
                                                        $action1 = "active";
                                                        $actionn1 = "active";
                                                        $actions1 = "active";
                                                        break;
                                                case "products":
                                                        $action11 = "active";
                                                        $actionn11 = "active";
                                                        $actions11 = "active";
                                                        break;
                                                case "masterOptions":
                                                        $action12 = "active";
                                                        $actionn12 = "active";
                                                        $actions12 = "active";
                                                        break;
                                                case "optionCategory":
                                                        $action13 = "active";
                                                        $actionn13 = "active";
                                                        $actions13 = "active";
                                                        break;
                                                case "giftCard":
                                                        $action2 = "active";
                                                        $actionn2 = "active";
                                                        $actions2 = "active";
                                                        break;
                                                case "Enquiry":
                                                        $action3 = "active";
                                                        break;
                                                case "Wallet":
                                                        $action4 = "active";
                                                        $actionn4 = "active";
                                                        break;
                                                case "Orders":
                                                        $action5 = "active";
                                                        break;
                                                case "OrderStatus":
                                                        $action6 = "active";
                                                        break;
                                                case "CMS":
                                                        $action7 = "active";
                                                        $actionn7 = "active";
                                                        $actions7 = "active";
                                                        $actionss7 = "active";
                                                        break;
                                                case "Testimonials":
                                                        $action8 = "active";
                                                        break;
                                                case "Masters":
                                                        $action9 = "active";
                                                        $actionn9 = "active";
                                                        break;
                                                case "UserDetails":
                                                        $action10 = "active";
                                                        $actionn10 = "active";
                                                        break;
                                                case "site":
                                                        if (Yii::app()->controller->action->id == "home") {
                                                                $action18 = "active";
                                                        }

                                                        break;
                                                default:
                                                        $action18 = "active";
                                                        break;
                                        }
                                        ?>
                                        <ul class="sidebar-menu">

                                                <li class="<?php echo $action18 ?>">
                                                        <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/site/home">
                                                                <i class="fa fa-dashboard"></i><span> Dash Board</span>
                                                        </a>
                                                </li>
                                                <li class="<?php echo $actionn1 ?> treeview">
                                                        <a href="#"><i class="fa fa-bars"></i> <span>Products</span> <i class="fa fa-angle-left pull-right"></i></a>
                                                        <ul class="treeview-menu">
                                                                <li class="<?php echo $actions1 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/products/ProductCategory/admin"><i class="fa fa-circle-o"></i> Product Category</a></li>
                                                                <li class="<?php echo $action20 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/products/Products/admin"><i class="fa fa-circle-o"></i> Products</a></li>
                                                                <li class="<?php echo $action20 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/ProductsFeatured/admin"><i class="fa fa-circle-o"></i>Featured-New Products </a></li>
                                                                <li class="<?php echo $action20 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/products/UserReviews/admin"><i class="fa fa-circle-o"></i>Product Review </a></li>
                                                                <li class="<?php echo $action21 ?> treeview">
                                                                        <a href="#"><i class="fa fa-circle-o"></i> <span>Products Option</span> <i class="fa fa-angle-left pull-right"></i></a>
                                                                        <ul class="treeview-menu">

                                                                                <li class="<?php echo $action21001 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/products/masterOptions/admin"><i class="fa fa-minus"></i> Option</a></li>
                                                                                <li class="<?php echo $action21002 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/products/optionCategory/create"><i class="fa fa fa-minus"></i> Option Category </a></li>
                                                                        </ul>
                                                                </li>

                                                        </ul>
                                                </li>
                                                <li class="<?php echo $action3 ?>">
                                                        <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/Blog/admin">
                                                                <i class="glyphicon glyphicon-bold"></i><span>Blogs</span>
                                                        </a>
                                                </li>
                                                <li class="<?php echo $actionn5 ?> treeview">
                                                        <a href="#"> <i class="glyphicon glyphicon-gift"></i> <span>Gift Coupon</span> <i class="fa fa-angle-left pull-right"></i></a>
                                                        <ul class="treeview-menu">

                                                                <li class="<?php echo $action55 ?>">
                                                                        <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/giftcard/GiftCard/admin">
                                                                                <i class="glyphicon glyphicon-gift"></i><span>Gift Coupon creation</span>
                                                                        </a>
                                                                </li>
                                                                <li class="<?php echo $action3 ?>">
                                                                        <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/giftcard/userGiftscardHistory/admin">
                                                                                <i class="fa fa-users"></i><span>Gift Coupon Code Creation</span>
                                                                        </a>
                                                                </li>
                                                        </ul>
                                                </li>
                                                <li class="<?php echo $actionn4 ?> treeview">
                                                        <a href="#"> <i class="fa fa-suitcase"></i> <span>Credit / Wallet</span> <i class="fa fa-angle-left pull-right"></i></a>
                                                        <ul class="treeview-menu">
                                                             <li class="<?php echo $action3 ?>">
                                                                        <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/products/walletHistory/addcredit">
                                                                                <i class="fa fa-users"></i><span>Add Money to Wallet</span>
                                                                        </a>
                                                                </li>
                                                                <li class="<?php echo $action3 ?>">
                                                                        <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/products/walletHistory/admin">
                                                                                <i class="fa fa-users"></i><span>Wallet History</span>
                                                                        </a>
                                                                </li>
                                                        </ul>
                                                </li>



                                                <?php if (isset(Yii::app()->session['post']['products']) && Yii::app()->session['post']['products'] == 1) { ?>
                                                        <li class="<?php echo $actionn5 ?> treeview">
                                                                <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/products/Order/admin"><i class="fa fa-bars"></i> <span>Orders</span> </a></li>

                                                <?php } ?>

                                                <?php if (isset(Yii::app()->session['post']['products']) && Yii::app()->session['post']['products'] == 1) { ?>
                                                        <li class="<?php echo $actionn6 ?> treeview">
                                                                <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/products/OrderStatus/admin"><i class="fa fa-bars"></i> <span>Order Status</span> </a>

                                                        </li>
                                                <?php } ?>

                                                <?php if (isset(Yii::app()->session['post']['products']) && Yii::app()->session['post']['products'] == 1) { ?>
                                                        <li class="<?php echo $actionn6 ?> treeview">
                                                                <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/contactDetails/admin"><i class="fa fa-bars"></i> <span>Contact Details</span> </a>

                                                        </li>
                                                <?php } ?>
                                                <?php if (isset(Yii::app()->session['post']['products']) && Yii::app()->session['post']['products'] == 1) { ?>
                                                        <li class="<?php echo $actionn6 ?> treeview">
                                                                <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/fileUploads/admin"><i class="fa fa-bars"></i> <span>Sizechart Uploads</span> </a>

                                                        </li>
                                                <?php } ?>
                                                <?php if (isset(Yii::app()->session['post']['products']) && Yii::app()->session['post']['products'] == 1) { ?>
                                                        <li class="<?php echo $actionn6 ?> treeview">
                                                                <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/settings/settings/admin"><i class="fa fa-bars"></i> <span>Settings</span> </a>

                                                        </li>
                                                <?php } ?>
                                                <?php if (isset(Yii::app()->session['post']['cms']) && Yii::app()->session['post']['cms'] == 1) { ?>
                                                        <li class="<?php echo $actionn7 ?> treeview">
                                                                <a href="#"><i class="fa fa-book"></i> <span>CMS</span> <i class="fa fa-angle-left pull-right"></i></a>
                                                                <ul class="treeview-menu">
                                                                        <li class="<?php echo $action4 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/cms/Slider/admin"><i class="fa fa-circle-o"></i> Sliders</a></li>
                                                                        <!--<li class="<?php echo $action5 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/cms/SocialMedia/admin"><i class="fa fa-circle-o"></i> Social Media</a></li>-->
                                                                        <li class="<?php echo $action6 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/cms/StaticPage/admin"><i class="fa fa-circle-o"></i> Static Pages</a></li>
                                                                        <li class="<?php echo $action245 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/cms/Banner/admin"><i class="fa fa-circle-o"></i> Banner Images</a></li>
                                                                        <li class="<?php echo $action245 ?>"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/brands/admin"><i class="fa fa-circle-o"></i> Brands</a></li>
                                                                </ul>
                                                        </li>
                                                        <li class="<?php echo $actionn8 ?>">
                                                                <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/testimonials/Testimonial/admin">
                                                                        <i class="fa fa-adn"></i><span>Testimonials</span>
                                                                </a>
                                                        </li>
                                                        <li class="<?php echo $actionn8 ?>">
                                                                <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/brands/admin">
                                                                        <i class="fa fa-adn"></i><span>Brands</span>
                                                                </a>
                                                        </li>

                                                <?php } ?>

                                                <?php if (isset(Yii::app()->session['post']['coupons']) && Yii::app()->session['post']['coupons'] == 1) { ?>

                                                <?php } ?>
                                                <?php if (isset(Yii::app()->session['post']['masters']) && Yii::app()->session['post']['masters'] == 1) { ?>
                                                        <li class="<?php echo $actionn9 ?> treeview">
                                                                <a href="#"><i class="fa fa-database"></i> <span>Masters</span> <i class="fa fa-angle-left pull-right"></i></a>
                                                                <ul class="treeview-menu">
                                                                        <li class="<?php echo $action23 ?> treeview"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/masters/MasterShippingTypes/admin"><i class="fa fa-circle-o"></i>Shipping Types</a></li>
                                                                        <!--<li class="<?php echo $action23 ?> treeview"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/masters/shippingCharges/admin"><i class="fa fa-circle-o"></i>Shipping Rates</a></li>-->
                                                                        <li class="<?php echo $action15 ?> treeview"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/masters/Countries/admin"><i class="fa fa-circle-o"></i> Countries</a></li>
                                                                        <li class="<?php echo $action17 ?> treeview"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/masters/States/admin"><i class="fa fa-circle-o"></i> States</a></li>
                                                                        <!--<li class="<?php echo $action17 ?> treeview"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/masters/Currency/admin"><i class="fa fa-circle-o"></i> Currency</a></li>-->
                                                                        <li class="<?php echo $action17 ?> treeview"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/WidthFitting/admin"><i class="fa fa-circle-o"></i> Width Fittings</a></li>
                                                                        <li class="<?php echo $action17 ?> treeview"><a href="<?php echo Yii::app()->baseUrl ?>/admin.php/ToyStyle/admin"><i class="fa fa-circle-o"></i> Toe Style</a></li>
                                                                </ul>
                                                        </li>
                                                        <li class="<?php echo $action10 ?>">
                                                                <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/user/UserDetails/admin">
                                                                        <i class="fa fa-users"></i><span> User Details</span>
                                                                </a>
                                                        </li>
                                                        <li class="<?php echo $action10 ?>">
                                                                <a href="<?php echo Yii::app()->baseUrl ?>/admin.php/newsletter/admin">
                                                                        <i class="fa fa-users"></i><span> Newsletter Subscribers</span>
                                                                </a>
                                                        </li>


                                                <?php } ?>

                                                <?php if (isset(Yii::app()->session['post']['reports']) && Yii::app()->session['post']['reports'] == 1) { ?>

                                                <?php } ?>

                                        </ul>
                                </section>

                        </aside>


                        <div class="content-wrapper">


                                <?php echo $content; ?>



                        </div>
                        <footer class="main-footer">

                                <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#">Intersmart Solutions</a>.</strong> All rights reserved.
                        </footer>



                </div>




        </body>
</html>
