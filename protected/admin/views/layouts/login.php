<!DOCTYPE html>
<html>
        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>SolesiLove | Admin</title>
                <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                <link rel="icon" href="<?php echo Yii::app()->baseUrl; ?>/images/fav.ico" type="image/x-icon" />
                <link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl; ?>/images/fav.ico" type="image/x-icon" />
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?> /admin-themes/bootstrap/css/bootstrap.min.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
                <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/dist/css/AdminLTE.min.css">
                <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/admin-themes/plugins/iCheck/square/blue.css">
        </head>
        <body class="hold-transition login-page">
                <?php echo $content; ?>
                <script src="<?php echo Yii::app()->baseUrl; ?>/admin-themes/plugins/jQuery/jQuery-2.1.4.min.js"></script>
                <script src="<?php echo Yii::app()->baseUrl; ?>/admin-themes/bootstrap/js/bootstrap.min.js"></script>
                <script src="<?php echo Yii::app()->baseUrl; ?>/admin-themes/plugins/iCheck/icheck.min.js"></script>
                <script>
                        $(function() {
                                $('input').iCheck({
                                        checkboxClass: 'icheckbox_square-blue',
                                        radioClass: 'iradio_square-blue',
                                        increaseArea: '20%' // optional
                                });
                        });
                </script>
        </body>
</html>
