<?php
/* @var $this WidthFittingController */
/* @var $model WidthFitting */

$this->breadcrumbs=array(
	'Width Fittings'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List WidthFitting', 'url'=>array('index')),
	array('label'=>'Create WidthFitting', 'url'=>array('create')),
	array('label'=>'Update WidthFitting', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete WidthFitting', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage WidthFitting', 'url'=>array('admin')),
);
?>

<h1>View WidthFitting #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'status',
		'doc',
		'sort_order',
		'dou',
	),
)); ?>
