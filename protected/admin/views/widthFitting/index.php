<?php
/* @var $this WidthFittingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Width Fittings',
);

$this->menu=array(
	array('label'=>'Create WidthFitting', 'url'=>array('create')),
	array('label'=>'Manage WidthFitting', 'url'=>array('admin')),
);
?>

<h1>Width Fittings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
