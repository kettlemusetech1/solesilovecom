<?php
/* @var $this ToyStyleController */
/* @var $model ToyStyle */

$this->breadcrumbs=array(
	'Toe Styles'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Toe Style', 'url'=>array('index')),
	array('label'=>'Create Toe Style', 'url'=>array('create')),
	array('label'=>'Update Toe Style', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Toe Style', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Toe Style', 'url'=>array('admin')),
);
?>

<h1>View Toe Style #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'status',
		'sort_order',
		'doc',
		'dou',
	),
)); ?>
