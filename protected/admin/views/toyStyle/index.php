<?php
/* @var $this ToyStyleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Toe Styles',
);

$this->menu=array(
	array('label'=>'Create Toe Style', 'url'=>array('create')),
	array('label'=>'Manage Toe Style', 'url'=>array('admin')),
);
?>

<h1>Toy Styles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
