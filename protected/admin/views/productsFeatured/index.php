<?php
/* @var $this ProductsFeaturedController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Products Featureds',
);

$this->menu=array(
	array('label'=>'Create ProductsFeatured', 'url'=>array('create')),
	array('label'=>'Manage ProductsFeatured', 'url'=>array('admin')),
);
?>

<h1>Products Featureds</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
