<?php
/* @var $this ProductsFeaturedController */
/* @var $model ProductsFeatured */

$this->breadcrumbs=array(
	'Products Featureds'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProductsFeatured', 'url'=>array('index')),
	array('label'=>'Create ProductsFeatured', 'url'=>array('create')),
	array('label'=>'Update ProductsFeatured', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProductsFeatured', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProductsFeatured', 'url'=>array('admin')),
);
?>

<h1>View ProductsFeatured #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'product_id',
		'featured',
		'new',
		'status',
		'cb',
		'doc',
	),
)); ?>
