<?php
/* @var $this ProductsFeaturedController */
/* @var $model ProductsFeatured */
/* @var $form CActiveForm */
?>

<div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'products-featured-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>
        <br/>
        <div class="form-inlines">
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'product_id'); ?>
                        <?php
                        $models = Products::model()->findAllByAttributes(array('status' => 1));
                        $data = array();
                        foreach ($models as $model1) {
                                $data[$model1->id] = $model1->product_name;
                        }

                        echo $form->dropDownList($model, 'product_id', $data, array('empty' => '--Select Product--', 'class' => 'form-control', 'required' => 'required', 'options' => array('id' => array('selected' => 'selected'))));
                        ?>
                        <?php echo $form->error($model, 'product_id'); ?>
                </div>

                <div class="form-group">
                        <?php echo $form->labelEx($model, 'featured'); ?>
                        <?php echo $form->dropDownList($model, 'featured', array('1' => "Yes", '0' => "No"), array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'featured'); ?>
                </div>

                <div class="form-group">
                        <?php echo $form->labelEx($model, 'new'); ?>
                        <?php echo $form->dropDownList($model, 'new', array('1' => "Yes", '0' => "No"), array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'new'); ?>
                </div>

                <div class="form-group">
                        <?php echo $form->labelEx($model, 'status'); ?>
                        <?php echo $form->dropDownList($model, 'status', array('1' => "Enabled", '0' => "Disabled"), array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'status'); ?>
                </div>



        </div>
        <div class="form-group btns">
                <label>&nbsp;</label><br/>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-secondary btn-single pull-right', 'style' => 'border-radius:0px;padding: 10px 50px;')); ?>
        </div>

        <?php $this->endWidget(); ?>

</div><!-- form -->
<script>
        $(document).ready(function() {


                $('#ProductsFeatured_product_id').multiselect({
                        selectAllText: ' Select all',
                        includeSelectAllOption: true,
                        enableFiltering: true,
                        filterPlaceholder: 'Search',
                        enableCaseInsensitiveFiltering: true,
                });
        });
</script>
<style>
    ul.multiselect-container.dropdown-menu {
    height: 250px;
    overflow-y: scroll;
}
</style>