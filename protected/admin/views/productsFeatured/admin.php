<?php
/* @var $this ProductsFeaturedController */
/* @var $model ProductsFeatured */
?>
<style>
        .table th, td{
                text-align: center;
        }
        .table td{
                text-align: center;
        }
</style>


<div class="page-title">

        <div class="title-env">
                <h1 style="float: left;" class="title">ProductsFeatured</h1>
                <p style="float: left;margin-top: 8px;margin-left: 11px;" class="description">Manage ProductsFeatured</p>
        </div>

        <div class="breadcrumb-env">

                <ol class="breadcrumb bc-1" >
                        <li>
                                <a href="<?php echo Yii::app()->request->baseurl . '/admin.php/site/home'; ?>"><i class="fa-home"></i>Home</a>
                        </li>

                        <li class="active">

                                <strong>Manage ProductsFeatured</strong>
                        </li>
                </ol>

        </div>

</div>
<div class="row">


        <div class="col-sm-12">

                <a class="btn btn-secondary btn-icon btn-icon-standalone" href="<?php echo Yii::app()->request->baseurl . '/admin.php/productsFeatured/create'; ?>" id="add-note">
                        <i class="fa-pencil"></i>
                        <span>Add ProductsFeatured</span>
                </a>
                <div class="panel panel-default">
                        <?php
                        $this->widget('booster.widgets.TbGridView', array(
                            'type' => ' bordered condensed hover',
                            'id' => 'products-featured-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'columns' => array(
                                //   'id',
                                 array(
                        'name' => 'product_id',
                        'value' => function($data) {
                                $value = Products::model()->findByPk($data->product_id);
                                return $value->product_name;
                        },
                    ),
                                 array(
                        'name' => 'featured',
                        'value' => function($data) {
                                if($data->featured == 1) {
                                        return "Yes";
                                } elseif($data->featured == 0) {
                                        return "No";
                                } 
                        },
                        'filter' => array('1' => "Yes", '0' => "No")
                    ),
                                 array(
                        'name' => 'new',
                        'value' => function($data) {
                                if($data->new == 1) {
                                        return "Yes";
                                } elseif($data->new == 0) {
                                        return "No";
                                } else {
                                        return "Invalid";
                                }
                        },
                        'filter' => array('1' => "Yes", '0' => "No")
                    ),
                                //  'status',
                                //   'cb',
                                /*
                                  'doc',
                                 */
                                array(
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{update}{delete}',
                                ),
                            ),
                        ));
                        ?>
                </div>

        </div>


</div>

