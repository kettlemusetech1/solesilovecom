<?php
/* @var $this BrandsController */
/* @var $model Brands */
/* @var $form CActiveForm */
?>

<div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'brands-form',
            'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>
        <br/>
        <div class="">

                <div class="form-group">
                        <?php echo $form->labelEx($model, 'brand_name', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'brand_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control slug')); ?></div>
                        <?php echo $form->error($model, ' brand_name'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->hiddenField($model, 'canonical_name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                </div>


                <div class="form-group">
                        <?php echo $form->labelEx($model, 'image', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"> <?php echo $form->fileField($model, 'image', array('size' => 60, 'maxlength' => 200, 'class' => 'form-control')); ?>
                                <?php
                                if ($model->image != '') {
                                    echo '<img width = "125" style = "border: 2px solid #d2d2d2;" src = "' . Yii::app()->baseUrl . '/uploads/brands/' . $model->id . '.' . $model->image . '" />';
                                }
                                ?>
                                <?php echo $form->error($model, 'image'); ?></div>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->dropDownList($model, 'status', array('1' => 'Enabled', '0' => 'Disabled'), array('class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'status'); ?>
                </div>



        </div>
        <div class="form-group btns">
                <label>&nbsp;</label><br/>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-secondary btn-single pull-right', 'style' => 'border-radius:0px;
                                padding: 10px 50px;
                                                  ')); ?>
        </div>

        <?php $this->endWidget(); ?>

</div><!-- form -->


<script>
    $(document).ready(function () {
            $('.slug').keyup(function () {
                    $('#Brands_canonical_name').val(slug($(this).val()));
            });
    });
    var slug = function (str) {
            var $slug = '';
            var trimmed = $.trim(str);
            $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
                    replace(/-+/g, '-').
                    replace(/^-|-$/g, '');
            return $slug.toLowerCase();
    };
</script>
