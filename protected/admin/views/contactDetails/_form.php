<?php
/* @var $this ContactDetailsController */
/* @var $model ContactDetails */
/* @var $form CActiveForm */
?>

<div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'contact-details-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>
        <br/>
       
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'address'); ?>
                        <?php echo $form->textField($model, 'address', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'address'); ?>
                </div>

                <div class="form-group">
                        <?php echo $form->labelEx($model, 'email'); ?>
                        <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'email'); ?>
                </div>

                <div class="form-group">
                        <?php echo $form->labelEx($model, 'contactno_1'); ?>
                        <?php echo $form->textField($model, 'contactno_1', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'contactno_1'); ?>
                </div>

                <div class="form-group">
                        <?php echo $form->labelEx($model, 'contactno_2'); ?>
                        <?php echo $form->textField($model, 'contactno_2', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'contactno_2'); ?>
                </div>

                <div class="form-group">
                        <?php echo $form->labelEx($model, 'working_hrs'); ?>
                        <?php echo $form->textField($model, 'working_hrs', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'working_hrs'); ?>
                </div>


       
        <div class="form-group btns">
                <label>&nbsp;</label><br/>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-secondary btn-single pull-right', 'style' => 'border-radius:0px;padding: 10px 50px;')); ?>
        </div>

        <?php $this->endWidget(); ?>

</div><!-- form -->