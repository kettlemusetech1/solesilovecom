<?php
/* @var $this ContactDetailsController */
/* @var $data ContactDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contactno_1')); ?>:</b>
	<?php echo CHtml::encode($data->contactno_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contactno_2')); ?>:</b>
	<?php echo CHtml::encode($data->contactno_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('working_hrs')); ?>:</b>
	<?php echo CHtml::encode($data->working_hrs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cb')); ?>:</b>
	<?php echo CHtml::encode($data->cb); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('doc')); ?>:</b>
	<?php echo CHtml::encode($data->doc); ?>
	<br />

	*/ ?>

</div>