<?php

class AjaxController extends Controller {

        public function actionSelectState() {
//$_POST['country'] = 99;
                $country = Countries::model()->findByPk($_POST['country'])->country_name;
                $model = States::model()->findAllByAttributes(array('country_id' => (int) $_POST['country']));
                if (!empty($model)) {
                        $options = "<option value=''>Select " . $country . " States</option>";
                        foreach ($model as $state) {
                                $options .= '<option value="' . $state->state_name . '">' . $state->state_name . '</option>';
                        }
                } else {
                        $options = 0;
                }
                echo $options;
        }

        public function actionPutProductData() {
                $cux_name = $_POST['cux_name'];
                $fname = $_POST['fname'];
                $lname = $_POST['lname'];
                $country = $_POST['country'];
                $email = $_POST['email'];
                $phonecode = $_POST['phonecode'];
                $title = $_POST['title'];
                $mobile = $_POST['mobile'];
                $product = $_POST['product'];
                $color = $_POST['color'];
                $size = $_POST['size'];
                $quantity = $_POST['quantity'];

                if ($cux_name != "") {
                        if ($cux_name == 0) {
                                $userdet = new UserDetails("admincreate");
                                $userdet->title = $title;
                                $userdet->first_name = $fname;
                                $userdet->last_name = $lname;
                                $userdet->email = $email;
                                $userdet->country = $country;
                                $userdet->phonecode = $phonecode;
                                $userdet->phone_no_2 = $mobile;
                                $userdet->status = 1;
                                $userdet->email_verification = 1;
                                $userdet->newsletter = 1;
                                $userdet->CB = Yii::app()->session['admin']['id'];
                                $userdet->DOC = date("Y-m-d");
                                $userdet->save(false);
                        } else {
                                $userdet = UserDetails::model()->findByPk($cux_name);
                        }
                        $cart = new Cart;
                        $cart->user_id = $userdet->id;
                        $cart->session_id = Yii::app()->session['admintemp'];
                        $cart->product_id = $product;
                        $cart->quantity = $quantity;
                        $cart->options = OptionDetails::model()->findByAttributes(array('color_id' => $color, 'size_id' => $size, 'product_id' => $product))->id;
                        $cart->date = date("Y-m-d");
                        $cart->save(false);
                } else {

                }
                $getcartdata = Cart::model()->findAllByAttributes(array('user_id' => $userdet->id, 'session_id' => Yii::app()->session['admintemp']));
                if (!empty($getcartdata)) {
                        $cartdata = $this->renderPartial("_cartdata", array('getcartdata' => $getcartdata), true);
                        $userdetails = UserDetails::model()->findAllByAttributes(array('email_verification' => 1));
                        $userdata = '<option value="">--Silect User--</option>';
                        foreach ($userdetails as $use) {
                                if ($use->id == $userdet->id) {
                                        $selected = 'selected';
                                } else {
                                        $selected = '';
                                }
                                $userdata .= '<option' . $selected . '  value="' . $use->id . '">' . $use->first_name . ' (' . $use->email . ' )' . '</option>';
                        }
                        ?>

                        <?php
                        $array = array('userdata' => $userdata, 'cartdata' => $cartdata);
                        $json = CJSON::encode($array);
                        echo $json;
                        ?>

                        <?php
                } else {
                        echo "";
                }
        }

        public function actionPutProductDataGift() {
                $cux_name = $_POST['cux_name'];
                $fname = $_POST['fname'];
                $lname = $_POST['lname'];
                $country = $_POST['country'];
                $email = $_POST['email'];
                $phonecode = $_POST['phonecode'];
                $title = $_POST['title'];
                $mobile = $_POST['mobile'];
                $product = $_POST['product'];


                if ($cux_name != "") {
                        if ($cux_name == 0) {
                                $userdet = new UserDetails("admincreate");
                                $userdet->title = $title;
                                $userdet->first_name = $fname;
                                $userdet->last_name = $lname;
                                $userdet->email = $email;
                                $userdet->country = $country;
                                $userdet->phonecode = $phonecode;
                                $userdet->phone_no_2 = $mobile;
                                $userdet->status = 1;
                                $userdet->email_verification = 1;
                                $userdet->newsletter = 1;
                                $userdet->CB = Yii::app()->session['admin']['id'];
                                $userdet->DOC = date("Y-m-d");
                                $userdet->save(false);
                        } else {
                                $userdet = UserDetails::model()->findByPk($cux_name);
                        }
                        $cart = new Cart;
                        $cart->user_id = $userdet->id;
                        $cart->session_id = Yii::app()->session['admintemp'];
                        $cart->product_id = $product;
                        $cart->quantity = 1;
                        $cart->date = date("Y-m-d");
                        $cart->save(false);
                } else {

                }
                $getcartdata = Cart::model()->findAllByAttributes(array('user_id' => $userdet->id, 'session_id' => Yii::app()->session['admintemp']));
                if (!empty($getcartdata)) {
                        $cartdata = $this->renderPartial("_cartdata", array('getcartdata' => $getcartdata), true);
                        $userdetails = UserDetails::model()->findAllByAttributes(array('email_verification' => 1));
                        $userdata = '<option value="">--Silect User--</option>';
                        foreach ($userdetails as $use) {
                                if ($use->id == $userdet->id) {
                                        $selected = 'selected';
                                } else {
                                        $selected = '';
                                }
                                $userdata .= '<option' . $selected . '  value="' . $use->id . '">' . $use->first_name . ' (' . $use->email . ' )' . '</option>';
                        }
                        ?>

                        <?php
                        $array = array('userdata' => $userdata, 'cartdata' => $cartdata);
                        $json = CJSON::encode($array);
                        echo $json;
                        ?>

                        <?php
                } else {
                        echo "";
                }
        }

        public function actionPutProductDataCeleb() {
                $cux_name = $_POST['cux_name'];
                $fname = $_POST['fname'];
                $lname = $_POST['lname'];
                $country = $_POST['country'];
                $email = $_POST['email'];
                $phonecode = $_POST['phonecode'];
                $title = $_POST['title'];
                $mobile = $_POST['mobile'];
                $product = $_POST['product'];
                $color = $_POST['color'];
                $size = $_POST['size'];
                $quantity = $_POST['quantity'];

                if ($cux_name != "") {
                        if ($cux_name == 0) {
                                $userdet = new UserDetails("admincreate");
                                $userdet->title = $title;
                                $userdet->first_name = $fname;
                                $userdet->last_name = $lname;
                                $userdet->email = $email;
                                $userdet->country = $country;
                                $userdet->phonecode = $phonecode;
                                $userdet->phone_no_2 = $mobile;
                                $userdet->status = 1;
                                $userdet->email_verification = 1;
                                $userdet->newsletter = 1;
                                $userdet->CB = Yii::app()->session['admin']['id'];
                                $userdet->DOC = date("Y-m-d");
                                $userdet->save(false);
                        } else {
                                $userdet = UserDetails::model()->findByPk($cux_name);
                        }
                        $cart = new Cart;
                        $cart->user_id = $userdet->id;
                        $cart->session_id = Yii::app()->session['admintemp'];
                        $cart->product_id = $product;
                        $cart->quantity = $quantity;
                        $cart->options = OptionDetails::model()->findByAttributes(array('color_id' => $color, 'size_id' => $size, 'product_id' => $product))->id;
                        $cart->date = date("Y-m-d");
                        $cart->save(false);
                } else {

                }
                $getcartdata = Cart::model()->findAllByAttributes(array('user_id' => $userdet->id, 'session_id' => Yii::app()->session['admintemp']));
                if (!empty($getcartdata)) {
                        $cartdata = $this->renderPartial("_cartdata", array('getcartdata' => $getcartdata), true);
                        $userdetails = UserDetails::model()->findAllByAttributes(array('email_verification' => 1));
                        $userdata = '<option value="">--Silect User--</option>';
                        foreach ($userdetails as $use) {
                                if ($use->id == $userdet->id) {
                                        $selected = 'selected';
                                } else {
                                        $selected = '';
                                }
                                $userdata .= '<option' . $selected . '  value="' . $use->id . '">' . $use->first_name . ' (' . $use->email . ' )' . '</option>';
                        }
                        ?>

                        <?php
                        $array = array('userdata' => $userdata, 'cartdata' => $cartdata);
                        $json = CJSON::encode($array);
                        echo $json;
                        ?>

                        <?php
                } else {
                        echo "";
                }
        }

        public function actionUpdateProductDataCeleb() {
                $cid = $_POST['value'];


                if ($cid != "") {

                        $cart = Cart::model()->findByPk($cid);
                        $uid = $cart->user_id;
                        $cart->delete();
                } else {

                }
                $getcartdata = Cart::model()->findAllByAttributes(array('user_id' => $uid, 'session_id' => Yii::app()->session['admintemp']));
                if (!empty($getcartdata)) {
                        ?>
                        <div class="pdata">
                                <table>
                                        <thead>
                                                <tr>
                                                        <td>Sl No</td>
                                                        <td>Product Name</td>
                                                        <td>Product Code</td>
                                                        <td>Quantity</td>
                                                        <td>Amount</td>
                                                        <td>Color</td>
                                                        <td>Size</td>
                                                        <td>Delete</td>
                                                        <td>Create Enquiry</td>
                                                </tr>
                                        </thead>

                                        <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($getcartdata as $getdata) {
                                                        ?>
                                                        <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo Products::model()->findByPk($getdata->product_id)->product_name; ?></td>
                                                                <td><?php echo Products::model()->findByPk($getdata->product_id)->product_code; ?></td>
                                                                <td><?php echo $getdata->quantity; ?></td>
                                                                <td><i class="fa fa-usd"></i><?php echo Products::model()->findByPk($getdata->product_id)->price; ?></td>
                                                                <td><?php echo OptionCategory::model()->findByPk(OptionDetails::model()->findByPk($getdata->options)->color_id)->color_name; ?></td>
                                                                <td><?php echo OptionCategory::model()->findByPk(OptionDetails::model()->findByPk($getdata->options)->size_id)->size; ?></td>
                                                                <td><a href="javascript:void(0)" cid="<?php echo $getdata->id; ?>" class="delete_product"><i class="fa fa-trash"></i></a></td>
                                                                <td><a href="javascript:void(0)" pid="<?php echo $getdata->product_id; ?>" class="create_enquiry"><i class="fa fa-trash"></i></a></td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                }
                                                ?>
                                        </tbody>
                                </table>
                        </div>
                        <?php
                } else {
                        $options = 0;
                }
                echo $options;
        }

        public function actionUpdateProductDataGift() {
                $cid = $_POST['value'];


                if ($cid != "") {

                        $cart = Cart::model()->findByPk($cid);
                        $uid = $cart->user_id;
                        $cart->delete();
                } else {

                }
                $getcartdata = Cart::model()->findAllByAttributes(array('user_id' => $uid, 'session_id' => Yii::app()->session['admintemp']));
                if (!empty($getcartdata)) {
                        ?>
                        <div class="pdata">
                                <table>
                                        <thead>
                                                <tr>
                                                        <td>Sl No</td>
                                                        <td>Giftcard Name</td>
                                                        <td>Amount</td>
                                                        <td>Delete</td>

                                                </tr>
                                        </thead>

                                        <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($getcartdata as $getdata) {
                                                        ?>
                                                        <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo Giftcard::model()->findByPk($getdata->product_id)->name; ?></td>
                                                                <td><i class="fa fa-usd"></i><?php echo Giftcard::model()->findByPk($getdata->product_id)->amount; ?></td>
                                                                <td><a href="javascript:void(0)" cid="<?php echo $getdata->id; ?>" class="delete_product"><i class="fa fa-trash"></i></a></td>

                                                        </tr>
                                                        <?php
                                                        $i++;
                                                }
                                                ?>
                                        </tbody>
                                </table>
                        </div>
                        <?php
                } else {
                        $options = 0;
                }
                echo $options;
        }

        public function actionCreateProductDataCeleb() {
                $cid = $_POST['value'];


                if ($cid != "") {

                        $cart = Cart::model()->findByPk($cid);
                        $uid = $cart->user_id;
                        $cart->delete();
                } else {

                }
                $getcartdata = Cart::model()->findAllByAttributes(array('user_id' => $uid, 'session_id' => Yii::app()->session['admintemp']));
                if (!empty($getcartdata)) {
                        ?>
                        <div class="pdata">
                                <table>
                                        <thead>
                                                <tr>
                                                        <td>Sl No</td>
                                                        <td>Product Name</td>
                                                        <td>Product Code</td>
                                                        <td>Quantity</td>
                                                        <td>Amount</td>
                                                        <td>Color</td>
                                                        <td>Size</td>
                                                        <td>Delete</td>
                                                        <td>Create Enquiry</td>
                                                </tr>
                                        </thead>

                                        <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($getcartdata as $getdata) {
                                                        ?>
                                                        <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo Products::model()->findByPk($getdata->product_id)->product_name; ?></td>
                                                                <td><?php echo Products::model()->findByPk($getdata->product_id)->product_code; ?></td>
                                                                <td><?php echo $getdata->quantity; ?></td>
                                                                <td><i class="fa fa-usd"></i><?php echo Products::model()->findByPk($getdata->product_id)->price; ?></td>
                                                                <td><?php echo OptionCategory::model()->findByPk(OptionDetails::model()->findByPk($getdata->options)->color_id)->color_name; ?></td>
                                                                <td><?php echo OptionCategory::model()->findByPk(OptionDetails::model()->findByPk($getdata->options)->size_id)->size; ?></td>
                                                                <td><a href="javascript:void(0)" cid="<?php echo $getdata->id; ?>" class="delete_product"><i class="fa fa-trash"></i></a></td>
                                                                <td><a href="javascript:void(0)" cid="<?php echo $getdata->id; ?>" class="create_enquiry"><i class="fa fa-trash"></i></a></td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                }
                                                ?>
                                        </tbody>
                                </table>
                        </div>
                        <?php
                } else {
                        $options = 0;
                }
                echo $options;
        }

        public function actionUpdateProductData() {
                $cid = $_POST['value'];


                if ($cid != "") {

                        $cart = Cart::model()->findByPk($cid);
                        $uid = $cart->user_id;
                        $cart->delete();
                } else {

                }
                $getcartdata = Cart::model()->findAllByAttributes(array('user_id' => $uid, 'session_id' => Yii::app()->session['admintemp']));
                if (!empty($getcartdata)) {
                        ?>
                        <div class="pdata">
                                <table>
                                        <thead>
                                                <tr>
                                                        <td>Sl No</td>
                                                        <td>Product Name</td>
                                                        <td>Product Code</td>
                                                        <td>Quantity</td>
                                                        <td>Amount</td>
                                                        <td>Color</td>
                                                        <td>Size</td>
                                                        <td>Delete</td>
                                                </tr>
                                        </thead>

                                        <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($getcartdata as $getdata) {
                                                        ?>
                                                        <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo Products::model()->findByPk($getdata->product_id)->product_name; ?></td>
                                                                <td><?php echo Products::model()->findByPk($getdata->product_id)->product_code; ?></td>
                                                                <td><?php echo $getdata->quantity; ?></td>
                                                                <td><i class="fa fa-usd"></i><?php echo Products::model()->findByPk($getdata->product_id)->price; ?></td>
                                                                <td><?php echo OptionCategory::model()->findByPk(OptionDetails::model()->findByPk($getdata->options)->color_id)->color_name; ?></td>
                                                                <td><?php echo OptionCategory::model()->findByPk(OptionDetails::model()->findByPk($getdata->options)->size_id)->size; ?></td>
                                                                <td><a href="javascript:void(0)" cid="<?php echo $getdata->id; ?>" class="delete_product"><i class="fa fa-trash"></i></a></td>
                                                        </tr>
                                                        <?php
                                                        $i++;
                                                }
                                                ?>
                                        </tbody>
                                </table>
                        </div>
                        <?php
                } else {
                        $options = 0;
                }
                echo $options;
        }

        public function actionSelectUser() {
//$_POST['country'] = 99;
                $model = UserDetails::model()->findByPk($_POST['value']);
                if (!empty($model)) {
                        $title = $model->title;
                        $fname = $model->first_name;
                        $lname = $model->last_name;
                        $email = $model->email;
                        $country = Countries::model()->findByPk($model->country)->country_name;
                        $phonecode = $model->phonecode;
                        $mobile = $model->phone_no_2;
                } else {

                }
                $array = array('title' => $title, 'fname' => $fname, 'lname' => $lname, 'email' => $email, 'country' => $country, 'phonecode' => $phonecode, 'mobile' => $mobile);
                $json = CJSON::encode($array);
                echo $json;
        }

        public function actionGetProductColor() {
                $val = $_POST['val'];
                $product = Products::model()->findByPk($_POST['val']);
                $model = OptionDetails::model()->findAllByAttributes(array('status' => 1, 'product_id' => $val), array('condition' => 'stock > 0', 'group' => 'color_id'));
                if (!empty($model)) {
                        ?>
                        <div class="form-group">
                                <label class="col-sm-2 control-label" for="Order_total_amount">Color</label>
                                <div class="col-sm-10">
                                        <select class="form-control" name="product_color" id="product_color">
                                                <option value="">--Select Color--</option>
                                                <?php foreach ($model as $mod) { ?>
                                                        <option value="<?php echo $mod->color_id; ?>"><?php echo OptionCategory::model()->findByPk($mod->color_id)->color_name; ?></option>
                                                <?php } ?>
                                        </select>
                                </div>

                        </div>
                        <?php
                } else {

                }
        }

        public function actionGetProductSize() {
                $val = $_POST['product'];
                $color = $_POST['color'];
                $product = Products::model()->findByPk($_POST['product']);

                $model = OptionDetails::model()->findAllByAttributes(array('status' => 1, 'product_id' => $val, 'color_id' => $color), array('condition' => 'stock > 0', 'group' => 'size_id'));
                if (!empty($model)) {
                        ?>
                        <div class="form-group">
                                <label class="col-sm-2 control-label" for="Order_total_amount">Size</label>
                                <div class="col-sm-10">
                                        <select class="form-control" name="product_size" id="product_size">
                                                <option value="">--Select Size--</option>
                                                <?php
                                                foreach ($model as $mod) {
                                                        if ($mod->size_id != 0) {
                                                                ?>
                                                                <option value="<?php echo $mod->size_id; ?>"><?php echo OptionCategory::model()->findByPk($mod->size_id)->size; ?></option>
                                                                <?php
                                                        }
                                                }
                                                ?>
                                        </select>
                                </div>

                        </div>
                        <?php
                } else {

                }
        }

        public function actionSelectPhonecode() {
                $phonecode = Countries::model()->findByPk($_POST['country'])->phonecode;
                $model = Countries::model()->findAll();
                if (!empty($model)) {

                        foreach ($model as $city) {
                                if ($city->phonecode == $phonecode) {
                                        $selected = 'selected';
                                } else {
                                        $selected = '';
                                }
                                $options .= "<option $selected  value ='" . $city->phonecode . "'>+" . $city->phonecode . "</option>";
                        }
                }
                echo $options;
        }

        public function actionGetshippingcharge() {
                $country = $_POST['country'];
                $user = $_POST['user'];

                if ($country == 99) {
                        $total_shipping_rate = 0;
                } else {
                        $get_zone = Countries::model()->findByPk($country);
                        $get_total_weight = $this->GetTotalWeight($user);
                        $get_total_weight = $get_total_weight / 1000;
                        $value = explode('.', $get_total_weight);
                        if (strlen($value[1]) == 1) {
                                $value[1] = $value[1] . '0';
                        }
                        if ($value[1] <= 50) {
                                $total_weight = $value[0] + .5;
                        } else {
                                $total_weight = $value[0] + 1;
                        }

                        /* 13% Fuel Charge and 15 % Service chARGE is applicable */
                        $shipping_rate = ShippingCharges::model()->findByAttributes(array('zone' => $get_zone->zone, 'weight' => $total_weight));

                        if (!empty($shipping_rate)) {
                                if ($shipping_rate->shipping_type == 3) {
                                        $fuel_charge = ($shipping_rate->shipping_rate + ($shipping_rate->shipping_rate * .15)) * .13;

                                        $service_charge = (($shipping_rate->shipping_rate + ($shipping_rate->shipping_rate * .15)) + $fuel_charge) * .15;
                                        $total_shipping_rate = ceil(($shipping_rate->shipping_rate + ($shipping_rate->shipping_rate * .15)) + $fuel_charge + $service_charge);
                                } else {
                                        $fuel_charge = $shipping_rate->shipping_rate * .13;

                                        $service_charge = ($shipping_rate->shipping_rate + $fuel_charge) * .15;
                                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $fuel_charge + $service_charge);
                                }
                        } else {
                                $total_shipping_rate = 0;
                        }
                }
        }

        public function actionGetshippingchargeCeleb() {
                echo 12;
                exit;
                $country = $_POST['country'];
                $user = $_POST['user'];
                $product = $_POST['product'];

                if ($country == 99) {
                        $total_shipping_rate = 0;
                } else {
                        $get_zone = Countries::model()->findByPk($country);
                        $get_total_weight = $this->GetTotalWeightCelib($product);
                        $get_total_weight = $get_total_weight / 1000;
                        $value = explode('.', $get_total_weight);
                        if (strlen($value[1]) == 1) {
                                $value[1] = $value[1] . '0';
                        }
                        if ($value[1] <= 50) {
                                $total_weight = $value[0] + .5;
                        } else {
                                $total_weight = $value[0] + 1;
                        }

                        /* 13% Fuel Charge and 15 % Service chARGE is applicable */
                        $shipping_rate = ShippingCharges::model()->findByAttributes(array('zone' => $get_zone->zone, 'weight' => $total_weight));

                        if (!empty($shipping_rate)) {
                                if ($shipping_rate->shipping_type == 3) {
                                        $fuel_charge = ($shipping_rate->shipping_rate + ($shipping_rate->shipping_rate * .15)) * .13;

                                        $service_charge = (($shipping_rate->shipping_rate + ($shipping_rate->shipping_rate * .15)) + $fuel_charge) * .15;
                                        $total_shipping_rate = ceil(($shipping_rate->shipping_rate + ($shipping_rate->shipping_rate * .15)) + $fuel_charge + $service_charge);
                                } else {
                                        $fuel_charge = $shipping_rate->shipping_rate * .13;

                                        $service_charge = ($shipping_rate->shipping_rate + $fuel_charge) * .15;
                                        $total_shipping_rate = ceil($shipping_rate->shipping_rate + $fuel_charge + $service_charge);
                                }
                        } else {
                                $total_shipping_rate = 0;
                        }
                }

                $prod_details = Products::model()->findByPk($product);
                $producttotal = $this->DiscountAmount($prod_details);
                $product_price = $producttotal;
                $subtotal = $product_price;

                $grant_total = ceil($subtotal + $total_shipping_rate);
                $totalpay = $subtotal + $total_shipping_rate;

                $grant_total = $this->convert($grant_total);

                if (!empty($shipping_rate)) {

                        $ship_amount = $this->convert($total_shipping_rate);
                        $array = array('granttotal' => $grant_total, 'totalpay' => $totalpay, 'total_shipping_rate' => $total_shipping_rate, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total);
                        $json = CJSON::encode($array);
                        echo $json;
                } else {
                        $ship_amount = 0;
                        $array = array('granttotal' => $grant_total, 'shippingcharge' => $ship_amount, 'subtotal' => $sub_total, 'total_shipping_rate' => $total_shipping_rate);
                        $json = CJSON::encode($array);
                        echo $json;
                }
        }

        public function convert($price) {
                if (Yii::app()->session['currency'] != "") {
                        $pri_val = Yii::app()->session['currency']->rate * $price;
                        $cur_val = number_format((float) $pri_val, 2, '.', '');
                } else {
                        $result = '<i class="fa fa-usd"></i>' . number_format((float) $price, 2, '.', '');
                }
                return $result;
        }

        public function GetTotalWeight($user) {
                $carts = Cart::model()->findAllByAttributes(array('user_id' => $user));
                if (!empty($carts)) {
                        foreach ($carts as $cart) {
                                $prod_details = Products::model()->findByPk($cart->product_id);
                                if ($cart->options != 0) {
                                        $option_stock = OptionDetails::model()->findByPk($cart->options);
                                        if (!empty($option_stock)) {
                                                if ($option_stock->stock >= $cart->quantity) {
                                                        $quantity = $cart->quantity;
                                                } else {
                                                        $quantity = 0;
                                                }
                                        }
                                } else {
                                        if ($prod_details->quantity >= $cart->quantity) {

                                                $quantity = $cart->quantity;
                                        } else {
                                                $quantity = 0;
                                        }
                                }

                                $tot = $prod_details->weight * $quantity;

                                $total_weight += $tot;
                        }
                        return $total_weight;
                } else {
                        return 0;
                }
        }

        public function GetTotalWeightCelib($product) {

                $prod_details = Products::model()->findByPk($product);
                return $prod_details->weight;
        }

        public function DiscountAmount($model) {

                if ($model->discount_rate != 0) {
                        $today_deal_products = DealProducts::model()->findByAttributes(array('date' => date('Y-m-d')));
                        if (!empty($today_deal_products)) {
                                $HiddenProducts = explode(',', $today_deal_products->deal_products);
                                if (in_array($model->id, $HiddenProducts)) {
                                        $value = $this->DiscountType($model);
                                        return $value;
                                } else {
                                        return $model->price;
                                }
                        } else {
                                return $model->price;
                        }
                } else {
                        return $model->price;
                }
        }

}
