<?php

class OrderHistoryController extends Controller {

        /**
         * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
         * using two-column layout. See 'protected/views/layouts/column2.php'.
         */
        public $layout = '//layouts/column2';

        public function init() {
                date_default_timezone_set('Asia/Kolkata');
                if (!isset(Yii::app()->session['admin']) || Yii::app()->session['post']['orders'] != 1) {
                        $this->redirect(Yii::app()->request->baseUrl . '/admin.php/site/logOut');
                }
        }

        /**
         * @return array action filters
         */
        public function filters() {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete', // we only allow deletion via POST request
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules() {
                return array(
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'invoice', 'invoiceto'),
                        'users' => array('*'),
                    ),
                    array('allow', // allow authenticated user to perform 'create' and 'update' actions
                        'actions' => array('create', 'update'),
                        'users' => array('@'),
                    ),
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => array('admin', 'delete'),
                        'users' => array('admin'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        /**
         * Displays a particular model.
         * @param integer $id the ID of the model to be displayed
         */
        public function actionView($id) {
                $this->render('view', array(
                    'model' => $this->loadModel($id),
                ));
        }

        public function actionInvoice($id) {

                $order = Order::model()->findByPk($id);
                $user_address = UserAddress::model()->findByPk($order->ship_address_id);
                $bill_address = UserAddress::model()->findByPk($order->bill_address_id);

                $order_details = OrderProducts::model()->findAllByAttributes(array('order_id' => $id));



                $this->render('invoice', array(
                    'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'order' => $order));
        }

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate($id) {
                $model = new OrderHistory;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

                if (isset($_POST['OrderHistory'])) {

                        $model->attributes = $_POST['OrderHistory'];
                        $model->order_id = $id;
                        $model->date = $_POST['OrderHistory']['date'];
                        if ($model->save()) {

                                if ($_POST['sent_to_user'] != NULL) {


                                         $this->ClientOrderNotificationMail($model);//uncomment in future
                                }

                                $this->createUrl('products/order/view/id/' . $id);
                        }
                }

                $this->render('create', array(
                    'model' => $model,
                ));
        }

        public function ClientOrderNotificationMail($model) {

                $order = Order::model()->findByPk($model->order_id);

                $orderhistory = OrderHistory::model()->findAllByAttributes(array('order_id' => $model->order_id), array('order' => 'ID DESC'));
                $user_details = UserDetails::model()->findByPk($order->user_id);
                $enq = ProductEnquiry::model()->findAllByAttributes(array('order_id' => $model->order_id));
                $pdts = Products::model()->findByPk($enq->product_id);
                $ordstat = OrderStatus::model()->findByPk($model->order_status)->title;
                $to = $user_details->email;
                $admin = AdminUser::model()->findByPk(4)->email;
                if ($enq) {
                        $subject = 'Notification - ' . "$pdts->product_name - " . 'Order #SLOR' . "$model->order_id - " . "$ordstat";
                } else {
                        $subject = 'Notification - ' . 'Order #SLOR' . "$model->order_id - " . "$ordstat";
                }
                $message = $this->renderPartial('order_notification_client', array('model' => $model, 'order' => $order), true);
               $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                $headers .= 'From: SolesiLove <no-reply@solesilove.com>' . "\r\n";
                mail($to, $subject, $message, $headers);
                
        }

        public function actionInvoiceto($id) {

                $order = Order::model()->findByPk($id);
                $user_details = UserDetails::model()->findByPk($order->user_id);

                //$pdts=Products::model()->findByPk($model->product_id);
                $to = $user_details->email;
                $admin = AdminUser::model()->findByPk(4)->email;
                $subject = 'Order Invoice';
                $subject1 = 'Order Invoice Customer Copy';
                $message = $this->renderPartial('_invoice', array('order' => $order), true);

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                // More headers
                $headers .= 'From: SolesiLove <no-reply@solesilove>' . "\r\n";
                if (mail($to, $subject, $message, $headers)) {
                        mail($admin, $subject1, $message, $headers);
                        $this->redirect(array('invoice', 'id' => $id));
                }
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
                $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

                if (isset($_POST['OrderHistory'])) {


                        $model->attributes = $_POST['OrderHistory'];
                        $model->date = $_POST['OrderHistory']['date'];
                        if ($model->save())
                                $this->redirect(array('update', 'id' => $model->id));
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
                $this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }

        /**
         * Lists all models.
         */
        public function actionIndex() {
                $dataProvider = new CActiveDataProvider('OrderHistory');
                $this->render('index', array(
                    'dataProvider' => $dataProvider,
                ));
        }

        /**
         * Manages all models.
         */
        public function actionAdmin() {
                $model = new OrderHistory('search');
                $model->unsetAttributes();  // clear any default values
                if (isset($_GET['OrderHistory']))
                        $model->attributes = $_GET['OrderHistory'];

                $this->render('admin', array(
                    'model' => $model,
                ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer $id the ID of the model to be loaded
         * @return OrderHistory the loaded model
         * @throws CHttpException
         */
        public function loadModel($id) {
                $model = OrderHistory::model()->findByPk($id);
                if ($model === null)
                        throw new CHttpException(404, 'The requested page does not exist.');
                return $model;
        }

        /**
         * Performs the AJAX validation.
         * @param OrderHistory $model the model to be validated
         */
        protected function performAjaxValidation($model) {
                if (isset($_POST['ajax']) && $_POST['ajax'] === 'order-history-form') {
                        echo CActiveForm::validate($model);
                        Yii::app()->end();
                }
        }

        public function siteURL() {
                $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
                $domainName = $_SERVER['HTTP_HOST'];
                return $protocol . $domainName;
        }

}

?>
