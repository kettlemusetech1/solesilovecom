<?php

class OrderController extends Controller {

        /**
         * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
         * using two-column layout. See 'protected/views/layouts/column2.php'.
         */
        public $layout = '//layouts/column2';

        public function init() {
                if (!isset(Yii::app()->session['admin']) || Yii::app()->session['post']['orders'] != 1) {
                        $this->redirect(Yii::app()->request->baseUrl . '/admin.php/site/logOut');
                }
        }

        /**
         * @return array action filters
         */
        public function filters() {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete', // we only allow deletion via POST request
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules() {
                return array(
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array('index', 'view', 'create', 'update', 'admins', 'admin', 'delete', 'print', 'notpaidnotify', 'completed', 'selectcat', 'Createnormal', 'Createceleb', 'Creategiftcard', 'Createcustom'),
                        'users' => array('*'),
                    ),
                    array('allow', // allow authenticated user to perform 'create' and 'update' actions
                        'actions' => array('create', 'update'),
                        'users' => array('@'),
                    ),
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => array('admin', 'delete'),
                        'users' => array('admin'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        public function actionNotpaidnotify($id) {
                $order = Order::model()->findByPk($id);
                $order->notpaid_notify = $order->notpaid_notify + 1;
                $order->save();
                $order_details = OrderProducts::model()->findAllByAttributes(array('order_id' => $id));
                $userdetails = UserDetails::model()->findByPk($order->user_id);
                // $to = $userdetails->email;
                $to = "dhanya@intersmart.in";

                $message = new YiiMailMessage;
                $message->view = "_notpaid_order_mail";  // view file name
                $params = array('order' => $order, 'order_details' => $order_details, 'userdetails' => $userdetails); // parameters
                $message->subject = "SolesiLove Incomplete Order";
                $message->setBody($params, 'text/html');
                $message->addTo($to);
                //$message->from = 'no-reply@laksyah.com';
                $message->from = array('no-reply@solesilove.com' => 'SolesiLove');
                Yii::app()->mail->send($message);
                $this->redirect(array('admin'));
        }

        /**
         * Displays a particular model.
         * @param integer $id the ID of the model to be displayed
         */
        public function actionView($id) {

                $products = new OrderProducts('search');
                $history = new OrderHistory('search');
                $products->order_id = $id;
                $history->order_id = $id;
                $this->render('view', array('model' => $this->loadModel($id), 'products' => $products, 'history' => $history));
        }

        public function actionSelectcat() {

                $this->render('selectcat');
        }

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreatenormal() {
                $model = new Order;
                $user = new UserDetails;
                $billing = new UserAddress;
                $shipping = new UserAddress;
                if (!isset(Yii::app()->session['admintemp'])) {
                        Yii::app()->session['admintemp'] = microtime(true);
                }
                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if (isset($_POST['Order'])) {
                        $model->attributes = $_POST['Order'];

                        $model->user_id = $_POST['Order']['user_id'];
                        $model->total_amount = $_POST['Order']['total_amount'] - $_POST['Order']['shipping_charge'];
                        $model->order_date = date("Y-m-d H:i:s");
                        $model->ship_address_id = $this->addAddress($shipping, $_POST['UserAddress']['ship']);
                        $model->bill_address_id = $this->addAddress($billing, $_POST['UserAddress']['bill']);
                        $model->comment = $_POST['Order']['comment'];
                        $model->payment_mode = $_POST['Order']['payment_mode'];
                        $model->admin_comment = $_POST['Order']['comment'];
                        $model->status = 1;
                        $model->payment_status = 1;
                        $model->transaction_id = $_POST['Order']['transaction_id'];
                        // $model->payment_status = $_POST['Order']['payment_status'];
                        // $model->status = $_POST['Order']['status'];
                        $model->DOC = date("Y-m-d");
                        $model->netbanking = $_POST['Order']['total_amount'];
                        $model->shipping_charge = $_POST['Order']['shipping_charge'];
                        $cart = Cart::model()->findAllByAttributes(array('user_id' => $_POST['Order']['user_id'], 'session_id' => Yii::app()->session['admintemp']));
                        if ($model->save()) {
                                $this->orderProducts($model->id, $cart);
                                // $this->orderHistoryCeleb($model->id, $pdt_det);

                                $model_hist = new OrderHistory;
                                $model_hist->order_id = $model->id;
                                $model_hist->date = date('Y-m-d H:i:s');
                                $model_hist->order_status_comment = "Order Placed";
                                $model_hist->order_status = 1;
                                $model_hist->status = 1;
                                $model_hist->save();
                                Cart::model()->deleteAllByAttributes(array(), array('condition' => 'user_id = ' . $_POST['Order']['user_id']));
                                $this->redirect(array('admin'));
                        }
                }

                $this->render('create', array(
                    'model' => $model,
                    'user' => $user,
                    'shipping' => $shipping,
                    'billing' => $billing,
                ));
        }

        public function actionCreateceleb() {
                $model = new Order;
                $user = new UserDetails;
                $billing = new UserAddress;
                $shipping = new UserAddress;

                $enquiry = new ProductEnquiry;
                if (!isset(Yii::app()->session['admintemp'])) {
                        Yii::app()->session['admintemp'] = microtime(true);
                }
                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if (isset($_POST['Order'])) {



                        $user_det = UserDetails::model()->findByPk($_POST['Order']['user_id']);
                        $pdt_det = Products::model()->findByPk($_POST['productid']);
                        $model->attributes = $_POST['Order'];
                        $userid = $_POST['Order']['user_id'];
                        $model->user_id = $_POST['Order']['user_id'];
                        $model->total_amount = $pdt_det->price;
                        $model->order_date = date("Y-m-d H:i:s");
                        $model->ship_address_id = $this->addAddress($shipping, $_POST['UserAddress']['ship'], $userid);
                        $model->bill_address_id = $this->addAddress($billing, $_POST['UserAddress']['bill'], $userid);
                        $model->comment = $_POST['Order']['comment'];
                        $model->payment_mode = $_POST['Order']['payment_mode'];
                        $model->admin_comment = $_POST['Order']['comment'];
                        $model->transaction_id = $_POST['Order']['transaction_id'];
                        // $model->payment_status = $_POST['Order']['payment_status'];
                        //   $model->status = $_POST['Order']['status'];
                        $model->status = 1;
                        $model->payment_status = 1;
                        $model->DOC = date("Y-m-d");
                        $model->netbanking = $_POST['Order']['total_amount'];
                        $model->shipping_charge = $_POST['Order']['shipping_charge'];
                        $cart = Cart::model()->findAllByAttributes(array('user_id' => $_POST['Order']['user_id'], 'session_id' => Yii::app()->session['admintemp']));
                        if ($model->save()) {
                                $billing1 = UserAddress::model()->findByPk($model->bill_address_id);
                                $enquiry->name = $user_det->first_name;
                                $enquiry->email = $user_det->email;
                                $enquiry->phone = $billing1->contact_number;
                                $enquiry->country = $billing1->country;
                                $enquiry->requirement = $_POST['Order']['comment'];
                                $enquiry->product_id = $_POST['productid'];
                                $enquiry->doc = date('Y-m-d');
                                $enquiry->user_id = $user_det->id;
                                $enquiry->total_amount = $pdt_det->price;
                                $enquiry->shipping_charge = $_POST['Order']['shipping_charge'];
                                $enquiry->balance_to_pay = ($enquiry->total_amount + $enquiry->shipping_charge) - $_POST['Order']['total_amount'];
                                $enquiry->status = 2;
                                $enquiry->enquiry_type = 1;
                                $enquiry->color = $_POST['product_color'];
                                $enquiry->phonecode = $billing1->phonecode;
                                $enquiry->shipping_address = $model->ship_address_id;
                                $enquiry->billing_address = $model->bill_address_id;
                                $enquiry->order_id = $model->id;
                                $enquiry->browser_agent = $_SERVER['HTTP_USER_AGENT'];
                                $enquiry->save(false);
                                $colr = OptionDetails::model()->findByAttributes(array('color_id' => $_POST['product_color']));

                                $this->orderProductsCeleb($model->id, $pdt_det, $colr, $enquiry->id);
                                $this->orderHistoryCeleb($model->id, $pdt_det);
                                Cart::model()->deleteAllByAttributes(array(), array('condition' => 'user_id = ' . $_POST['Order']['user_id']));
                                $this->redirect(array('admin'));
                        }
                }

                $this->render('createceleb', array(
                    'model' => $model,
                    'user' => $user,
                    'shipping' => $shipping,
                    'billing' => $billing,
                ));
        }

        public function actionCreategiftcard() {
                $model = new Order;
                $user = new UserDetails;
                $billing = new UserAddress;
                $shipping = new UserAddress;
                if (!isset(Yii::app()->session['admintemp'])) {
                        Yii::app()->session['admintemp'] = microtime(true);
                }
                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if (isset($_POST['Order'])) {
                        $model->attributes = $_POST['Order'];

                        $model->user_id = $_POST['Order']['user_id'];
                        $model->total_amount = $_POST['Order']['total_amount'] - $_POST['Order']['shipping_charge'];
                        $model->order_date = date("Y-m-d H:i:s");
                        if ($_POST['cardtype'] == 2) {
                                $model->ship_address_id = $this->addAddress($shipping, $_POST['UserAddress']['ship']);
                        } else {
                                $model->ship_address_id = 0;
                        }
                        $model->status = 1;
                        $model->payment_status = 1;
                        $model->bill_address_id = $this->addAddress($billing, $_POST['UserAddress']['bill']);
                        $model->comment = $_POST['Order']['comment'];
                        $model->payment_mode = $_POST['Order']['payment_mode'];
                        $model->admin_comment = $_POST['Order']['comment'];
                        $model->transaction_id = $_POST['Order']['transaction_id'];
                        // $model->payment_status = $_POST['Order']['payment_status'];
                        // $model->status = $_POST['Order']['status'];
                        $model->DOC = date("Y-m-d");
                        if ($model->payment_mode == 5) {
                                $model->neft = $_POST['Order']['total_amount'];
                        } else if ($model->payment_mode == 2) {
                                $model->netbanking = $_POST['Order']['total_amount'];
                        } else if ($model->payment_mode == 3) {
                                $model->paypal = $_POST['Order']['total_amount'];
                        }
                        $model->laksyah_gift = 1;
                        $model->laksyah_gift_transportaion = $_POST['cardtype'];
                        if ($model->laksyah_gift_transportaion == 1) {
                                $model->gift_email = $_POST['receiveremail'];
                        }
                        $model->status = 1;
                        $model->payment_status = 1;
                        $model->gift_via_status = 2;
                        $model->gift_card_id = $_POST['productid'];

                        $model->gift_sender_name = $_POST['sendername'];
                        $model->gift_personal_mesage = $_POST['message'];
                        $model->gift_receiver_name = $_POST['receivername'];



                        $model->shipping_charge = $_POST['Order']['shipping_charge'];

                        if ($model->save(false)) {

                                $model_hist = new OrderHistory;
                                $model_hist->order_id = $model->id;
                                $model_hist->date = date('Y-m-d H:i:s');
                                $model_hist->order_status_comment = "Order Placed";
                                $model_hist->order_status = 1;
                                $model_hist->status = 1;
                                $model_hist->save();
                                $this->SuccessMailGift($model->id);
                                $this->redirect(array('admin'));
                        }
                }

                $this->render('creategiftcard', array(
                    'model' => $model,
                    'user' => $user,
                    'shipping' => $shipping,
                    'billing' => $billing,
                ));
        }

        public function actionCreatecustom() {
                $model = new Order;
                $user = new UserDetails;
                $billing = new UserAddress;
                $shipping = new UserAddress;
                $enquiry = new ProductEnquiry;
                if (!isset(Yii::app()->session['admintemp'])) {
                        Yii::app()->session['admintemp'] = microtime(true);
                }
                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);
                $user_det = UserDetails::model()->findByPk($_POST['Order']['user_id']);
                $pdt_det = Products::model()->findByPk($_POST['productid']);
                if (isset($_POST['Order'])) {
                        $model->attributes = $_POST['Order'];

                        $model->user_id = $_POST['Order']['user_id'];
                        $model->total_amount = $_POST['Order']['total_amount'];
                        $model->order_date = date("Y-m-d H:i:s");
                        $model->ship_address_id = $this->addAddress($shipping, $_POST['UserAddress']['ship']);
                        $model->bill_address_id = $this->addAddress($billing, $_POST['UserAddress']['bill']);
                        $model->comment = $_POST['Order']['comment'];
                        $model->payment_mode = $_POST['Order']['payment_mode'];
                        $model->status = 1;
                        $model->payment_status = 1;
                        $model->admin_comment = $_POST['Order']['comment'];
                        $model->transaction_id = $_POST['Order']['transaction_id'];
                        // $model->payment_status = $_POST['Order']['payment_status'];
                        //  $model->status = $_POST['Order']['status'];
                        $model->DOC = date("Y-m-d");
                        $model->netbanking = $_POST['Order']['total_amount'];
//                        $model->shipping_method = $_POST['Order']['ss'];
                        $model->shipping_charge = $_POST['Order']['shipping_charge'];
                        /* echo "T=".$_POST['Order']['total_amount'];
                          echo "S=".$_POST['Order']['shipping_charge'];
                          echo "A=".$_POST['amount_paid'];
                          exit;
                         */ $cart = Cart::model()->findAllByAttributes(array('user_id' => $_POST['Order']['user_id'], 'session_id' => Yii::app()->session['admintemp']));
                        if ($model->save()) {


                                $billing1 = UserAddress::model()->findByPk($model->bill_address_id);
                                $enquiry->name = $user_det->first_name;
                                $enquiry->email = $user_det->email;
                                $enquiry->phone = $billing1->contact_number;
                                $enquiry->country = $billing1->country;
                                $enquiry->requirement = $_POST['Order']['comment'];
                                $enquiry->product_id = $_POST['productid'];
                                $enquiry->doc = date('Y-m-d');
                                $enquiry->user_id = $user_det->id;
                                $enquiry->total_amount = $pdt_det->price;
                                $enquiry->shipping_charge = $_POST['Order']['shipping_charge'];
                                $enquiry->balance_to_pay = $enquiry->total_amount + $enquiry->shipping_charge - $_POST['amount_paid'];
                                $enquiry->status = 2;
                                $enquiry->enquiry_type = 1;
                                $enquiry->color = $_POST['product_color'];
                                $enquiry->phonecode = $billing1->phonecode;
                                $enquiry->shipping_address = $model->ship_address_id;
                                $enquiry->billing_address = $model->bill_address_id;
                                $enquiry->order_id = $model->id;
                                $enquiry->browser_agent = $_SERVER['HTTP_USER_AGENT'];
                                $enquiry->save(false);
                                $colr = OptionDetails::model()->findByAttributes(array('color_id' => $_POST['product_color']));

                                $this->orderProductsCeleb($model->id, $pdt_det, $colr, $enquiry->id);
                                $this->orderHistoryCeleb($model->id, $pdt_det);
                                Cart::model()->deleteAllByAttributes(array(), array('condition' => 'user_id = ' . $_POST['Order']['user_id']));
                                $this->redirect(array('admin'));
                        }
                }

                $this->render('createcustom', array(
                    'model' => $model,
                    'user' => $user,
                    'shipping' => $shipping,
                    'billing' => $billing,
                ));
        }

        public function orderProductsCeleb($orderid, $prod_details, $colr, $enq) {


                $check = OrderProducts::model()->findAllByAttributes(array('order_id' => $orderid, 'product_id' => $prod_details->id));

                if (!empty($check)) {

                        $this->redirect(array('CheckOut/CheckOut'));
                } else {
                        $model_prod = new OrderProducts;
                        $model_prod->order_id = $orderid;
                        $model_prod->product_id = $prod_details->id;
                        $model_prod->option_id = $colr->id;
                        $model_prod->quantity = 1;
                        //$model_prod->gift_option = $cart->gift_option;
                        //$model_prod->rate = $prod_details->price;
                        if ($prod_details->discount) {
                                $price = $prod_details->price - $prod_details->discount;
                        } else {
                                $price = $prod_details->price;
                        }
                        $model_prod->amount = 1 * ($this->DiscountAmount($prod_details));

                        $model_prod->DOC = date('Y-m-d');
                        if ($model_prod->save()) {
                                $user_id = Order::model()->findByPk($model_prod->order_id)->user_id;
                                $order_product_id = $model_prod->id;
                                $cart_id = $cart->id;

                                $enquiry = ProductEnquiry::model()->findByPk($enq);

                                $celib_history = new CelibStyleHistory;
                                $celib_history->enq_id = $enq;
                                $celib_history->status = 3;
                                $celib_history->pay_amount = $enquiry->balance_to_pay;
                                if ($celib_history->save()) {
                                        $celib_history_update = CelibStyleHistory::model()->findByPk($celib_history->id);
                                        $enc_enq_id = $enq;
                                        $enc_celib_history_id = $celib_history->id;
                                        $getToken1 = $this->encrypt_decrypt('encrypt', 'enquiry_id=' . $enc_enq_id . ',history_id=' . $enc_celib_history_id);
                                        $celib_history_update->link = Yii::app()->request->baseUrl . '/index.php/Myaccount/SizeChartType?m=' . $getToken1;
                                        if ($celib_history_update->save()) {

                                                $this->SuccessMailCeleb($model_prod->order_id, $celib_history_update);
                                        }
                                }




                                //$this->UserGift($orderid, $user_id, $order_product_id, $cart_id);
                        }
                }
        }

        public function orderHistoryCeleb($orderid, $prod_details) {


                $check = OrderProducts::model()->findAllByAttributes(array('order_id' => $orderid, 'product_id' => $prod_details->id));


                $model_hist = new OrderHistory;
                $model_hist->order_id = $orderid;
                $model_hist->date = date('Y-m-d H:i:s');
                $model_hist->order_status_comment = "Order Placed";
                $model_hist->order_status = 1;
                $model_hist->status = 1;
                $model_hist->save();
        }

        public function orderProducts($orderid, $carts) {
                foreach ($carts as $cart) {
                        $prod_details = Products::model()->findByPk($cart->product_id);
                        $check = OrderProducts::model()->findAllByAttributes(array('order_id' => $orderid, 'product_id' => $cart->product_id, 'option_id' => $cart->options));

                        if (!empty($check)) {

                                //   $this->redirect(array('CheckOut/CheckOut'));
                        } else {
                                $model_prod = new OrderProducts;
                                $model_prod->order_id = $orderid;
                                $model_prod->product_id = $cart->product_id;
                                $model_prod->option_id = $cart->options;
                                $model_prod->quantity = $cart->quantity;
                                $model_prod->gift_option = $cart->gift_option;
                                $model_prod->rate = $cart->rate;
                                if ($prod_details->discount) {
                                        $price = $prod_details->price - $prod_details->discount;
                                } else {
                                        $price = $prod_details->price;
                                }
                                $model_prod->amount = ($cart->quantity) * ($this->DiscountAmount($prod_details));

                                $model_prod->DOC = date('Y-m-d');
                                if ($model_prod->save()) {

                                        $check_product_option = OrderProducts::model()->findAllByAttributes(array('order_id' => $orderid, 'product_id' => $cart->product_id, 'option_id' => $cart->options));
                                        foreach ($check_product_option as $product_options) {
                                                if ($product_options->option_id == 0) {
                                                        $product = Products::model()->findByPk($product_options->product_id);
                                                        if ($product->quantity >= $product_options->quantity) {
                                                                $product->quantity = $product->quantity - $product_options->quantity;
                                                        }
                                                        $product->save(false);
                                                } else {
                                                        $option_details = OptionDetails::model()->findByPk($product_options->option_id);
                                                        if ($option_details->stock >= $product_options->quantity) {
                                                                $option_details->stock = $option_details->stock - $product_options->quantity;
                                                        }
                                                        $option_details->save(false);
                                                }
                                        }

                                        $user_id = Order::model()->findByPk($model_prod->order_id)->user_id;
                                        $order_product_id = $model_prod->id;
                                        $cart_id = $cart->id;
                                        $this->SuccessMail($model_prod->order_id);
                                        //$this->UserGift($orderid, $user_id, $order_product_id, $cart_id);
                                }
                        }
                }
        }

        public function SuccessMail($orderid) {
                $order = Order::model()->findByPk($orderid);

                $userdetails = UserDetails::model()->findByPk($order->user_id);
                $user_address = UserAddress::model()->findByPk($order->ship_address_id);

                $bill_address = UserAddress::model()->findByPk($order->bill_address_id);
                $order_details = OrderProducts::model()->findAllByAttributes(array('order_id' => $order->id));
                $shiping_charge = ShippingCharges::model()->findByAttributes(array('country' => $user_address->country));
                $cats = Cart::model()->findAllByAttributes(array('session_id' => Yii::app()->session['temp_user'], 'user_id' => $order->user_id));

                $temp_user_gifts = UserGifts::model()->findAllByAttributes(array('order_id' => $order->id));

                $user = $userdetails->email;
                $user_subject = 'SolesiLove Order Confirmation - Your Order #SLOR' . $order->id . ' has been successfully placed!';
                $user_message = $this->renderPartial('_user_order_success_mail', array('order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge, 'temp' => $temp_user_gifts), true);
                //$admin = "dhanya@intersmart.in";
                $admin = AdminUser::model()->findByPk(4)->email;
                $admin_subject = 'New Order from ' . " $userdetails->first_name " . " $userdetails->last_name " . '#SLOR' . $order->id;
                $admin_message = $this->renderPartial('_admin_order_success_mail', array('userdetails' => $userdetails, 'order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge, 'temp' => $temp_user_gifts), true);

// Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// More headers
                $headers .= 'From: SolesiLove.com<sales@solesilove.com>' . "\r\n";
                mail($user, $user_subject, $user_message, $headers);
                mail($admin, $admin_subject, $admin_message, $headers);
        }

        public function SuccessMailCeleb($orderid, $celib_history_update) {


                $order = Order::model()->findByPk($orderid);

                $userdetails = UserDetails::model()->findByPk($order->user_id);
                $user_address = UserAddress::model()->findByPk($order->ship_address_id);

                $bill_address = UserAddress::model()->findByPk($order->bill_address_id);
                $order_details = OrderProducts::model()->findAllByAttributes(array('order_id' => $order->id));
                $shiping_charge = ShippingCharges::model()->findByAttributes(array('country' => $user_address->country));
                $cats = Cart::model()->findAllByAttributes(array('session_id' => Yii::app()->session['temp_user'], 'user_id' => $order->user_id));
                $enq_data = ProductEnquiry::model()->findByPk($celib_history_update->enq_id);
                $pdts = Products::model()->findByPk($enq_data->product_id);
                $temp_user_gifts = UserGifts::model()->findAllByAttributes(array('order_id' => $order->id));

                $user = $userdetails->email;
                $user_subject = 'SolesiLove Order Confirmation - Your Order #SLOR' . $order->id . ' has been successfully placed!';
                $user_message = $this->renderPartial('_user_celeb_success_mail', array('model' => $celib_history_update, 'enq_data' => $enq_data, 'eid' => $eid), true);
                //$admin = "dhanya@intersmart.in";
                $admin = AdminUser::model()->findByPk(4)->email;
                $admin_subject = 'New Order from ' . " $userdetails->first_name " . " $userdetails->last_name " . '#SLOR' . $order->id;
                $admin_message = $this->renderPartial('_admin_celeb_success_mail', array('model' => $celib_history_update, 'enq_data' => $enq_data, 'eid' => $eid), true);

// Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                $subject = "Order Confirmation - " . "$pdts->product_name - " . " Order #SLOR" . "$enq_data->order_id";
                $subject2 = "$enq_data->name - " . "$pdts->product_name - " . ' Order Confirmation sent - Order #SLOR' . "$enq_data->order_id";

// More headers
                $headers .= 'From: SolesiLove<sales@solesilove.com>' . "\r\n";
                mail($user, $subject, $user_message, $headers);
                mail($admin, $subject2, $user_message, $headers);
        }

        public function SuccessMailGift($orderid) {
                $order = Order::model()->findByPk($orderid);

                $userdetails = UserDetails::model()->findByPk($order->user_id);
                $user_address = UserAddress::model()->findByPk($order->ship_address_id);

                $bill_address = UserAddress::model()->findByPk($order->bill_address_id);
                $order_details = OrderProducts::model()->findAllByAttributes(array('order_id' => $order->id));
                $shiping_charge = ShippingCharges::model()->findByAttributes(array('country' => $user_address->country));
                $cats = Cart::model()->findAllByAttributes(array('session_id' => Yii::app()->session['temp_user'], 'user_id' => $order->user_id));

                $temp_user_gifts = UserGifts::model()->findAllByAttributes(array('order_id' => $order->id));

                $user = $userdetails->email;
                $user_subject = 'SolesiLove Order Confirmation - Your Order #SLOR' . $order->id . ' has been successfully placed!';
                $user_message = $this->renderPartial('_user_giftcard_success_mail', array('order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge, 'temp' => $temp_user_gifts), true);
                //$admin = "dhanya@intersmart.in";
                $admin = AdminUser::model()->findByPk(4)->email;
                $admin_subject = 'New Order from ' . " $userdetails->first_name " . " $userdetails->last_name " . '#SLOR' . $order->id;
                $admin_message = $this->renderPartial('_admin_giftcard_success_mail', array('userdetails' => $userdetails, 'order' => $order, 'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'shiping_charge' => $shiping_charge, 'temp' => $temp_user_gifts), true);

// Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// More headers
                $headers .= 'From: SolesiLove.com<sales@solesilove.com>' . "\r\n";
                mail($user, $user_subject, $user_message, $headers);
                mail($admin, $admin_subject, $admin_message, $headers);
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
                $model = $this->loadModel($id);

                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if (isset($_POST['Order'])) {
                        $model->attributes = $_POST['Order'];

                            $model->status = $_POST['Order']['status'];
                            $model->payment_status = $_POST['Order']['payment_status'];
                            $model->transaction_id = $_POST['Order']['transaction_id'];
                            $model->payment_mode = $_POST['Order']['payment_mode'];
                       // $model->admin_comment = $_POST['Order']['admin_comment'];
                       // $model->admin_status = $_POST['Order']['admin_status'];
                        if ($model->save(false))
                                $this->redirect(array('admin', 'id' => $model->id));
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }

        public function actionCompleted() {
                //$model = $this->loadModel($id);
                $order_history = OrderHistory::model()->findAllByAttributes(array('order_status' => 20), array('order' => 'id  DESC'));
                $order = Order::model()->findByPk(array('order_id' => $order_history->order_id));
                $this->render('completed', array('order' => $order));
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }

        /**
         * Lists all models.
         */
        public function actionIndex() {
                $dataProvider = new CActiveDataProvider('Order');
                $this->render('index', array(
                    'dataProvider' => $dataProvider,
                ));
        }

        /**
         * Manages all models.
         */
        public function actionAdmin($id = "") {
                $model = new Order('search');
                $model->unsetAttributes();  // clear any default values
                if (isset($_GET['Order']))
                        $model->attributes = $_GET['Order'];

                $this->render('admin', array(
                    'model' => $model,
                ));
        }

        public function actionAdmins($id = "") {
                $model = new Order('search');
                $model->unsetAttributes();  // clear any default values
                if (isset($_GET['Order']))
                        $model->attributes = $_GET['Order'];

                $this->render('admins', array(
                    'model' => $model,
                ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer $id the ID of the model to be loaded
         * @return Order the loaded model
         * @throws CHttpException
         */
        public function loadModel($id) {
                $model = Order::model()->findByPk($id);
                if ($model === null)
                        throw new CHttpException(404, 'The requested page does not exist.');
                return $model;
        }

        /**
         * Performs the AJAX validation.
         * @param Order $model the model to be validated
         */
        protected function performAjaxValidation($model) {
                if (isset($_POST['ajax']) && $_POST['ajax'] === 'order-form') {
                        echo CActiveForm::validate($model);
                        Yii::app()->end();
                }
        }

        public function actionPrint($id) {
                $order = Order::model()->findByPk($id);
                $user_address = UserAddress::model()->findByPk($order->ship_address_id);
                $bill_address = UserAddress::model()->findByPk($order->bill_address_id);

                $order_details = OrderProducts::model()->findAllByAttributes(array('order_id' => $id));



                $this->renderPartial('_invoice', array(
                    'user_address' => $user_address, 'bill_address' => $bill_address, 'order_details' => $order_details, 'order' => $order));
        }

        public function DiscountAmount($model) {

                //discount rate value not equal to null//
                if ($model->discount_rate != 0) {
                        $today_deal_products = DealProducts::model()->findByAttributes(array('date' => date('Y-m-d')));
                        if (!empty($today_deal_products)) {
                                $HiddenProducts = explode(',', $today_deal_products->deal_products);
                                if (in_array($model->id, $HiddenProducts)) {
//                                        if ($product->discount_type == 1) {
//                                                $discountRate = $product->price - $product->discount_rate;
//                                        } else {
//                                                $discountRate = $product->price - ( $product->price * ($product->discount_rate / 100));
//                                        }
                                        $value = $this->DiscountType($model);
                                        return $value;
                                } else {
                                        return $model->price;
                                }
                        } else {
                                return $model->price;
                        }

                        //check date for special price //
                        //no date limitation//
                } else {
                        return $model->price;
                }
        }

        public function addAddress($model, $data, $userid = "") {

                $model->attributes = $data;
                $model->first_name = $data['first_name'];
                $model->last_name = $data['last_name'];
                $model->city = $data['city'];
                $model->postcode = $data['postcode'];
                $model->country = $data['country'];
                $model->phonecode = $data['phonecode'];
                $model->state = $data['state'];
                $model->address_1 = $data['address_1'];
                $model->address_2 = $data['address_2'];
                $model->contact_number = $data['contact_number'];

                if ($data['state1'] != "" && $data['state1'] != " ") {

                        $model->state = $data['state1'];
                } else {
                        $model->state = $data['state'];
                }
                $model->CB = Yii::app()->session['admin']['id'];
                $model->DOC = date('Y-m-d');
                $model->userid = $userid;
//                if ($model->validate()) {
                if ($model->save(false)) {
                        return $model->id;
                } else {

                        return false;
                }
//                } else {
                // return false;
//                }
        }

        public function encrypt_decrypt($action, $string) {
                $output = false;

                $encrypt_method = "AES-256-CBC";
                $secret_key = 'This is my secret key';
                $secret_iv = 'This is my secret iv';

// hash
                $key = hash('sha256', $secret_key);

// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
                $iv = substr(hash('sha256', $secret_iv), 0, 16);

                if ($action == 'encrypt') {
                        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
                        $output = base64_encode($output);
                } else if ($action == 'decrypt') {
                        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
                }

                return $output;
        }

        public function siteURL() {
                $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
                $domainName = $_SERVER['HTTP_HOST'];
                return $protocol . $domainName;
        }

}
