<?php

class ProductEnquiryController extends Controller {

        /**
         * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
         * using two-column layout. See 'protected/views/layouts/column2.php'.
         */
        public $layout = '//layouts/column2';

        public function init() {
                if (!isset(Yii::app()->session['admin']) || Yii::app()->session['post']['products'] != 1) {
                        $this->redirect(Yii::app()->request->baseUrl . '/managermode.php/site/logOut');
                }
        }

        /**
         * @return array action filters
         */
        public function filters() {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete', // we only allow deletion via POST request
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules() {
                return array(
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'eroductenquiryeail', 'order', 'foraddress'),
                        'users' => array('*'),
                    ),
                    array('allow', // allow authenticated user to perform 'create' and 'update' actions
                        'actions' => array('create', 'update'),
                        'users' => array('@'),
                    ),
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => array('admin', 'delete'),
                        'users' => array('admin'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        public function siteURL() {
                $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
                $domainName = $_SERVER['HTTP_HOST'];
                return $protocol . $domainName;
        }

        public function actionView($id) {
                $this->render('view', array(
                    'model' => $this->loadModel($id),
                ));
        }

        public function actionCreate() {
                $model = new ProductEnquiry;


                if (isset($_POST['ProductEnquiry'])) {
                        $model->attributes = $_POST['ProductEnquiry'];
                        $model->add_to_order = $_POST['ProductEnquiry']['add_to_order'];
                        if ($model->save()) {
                                $this->redirect(array('admin'));
                        }
                }

                $this->render('create', array(
                    'model' => $model,
                ));
        }

        public function actionUpdate($id) {
                $model = $this->loadModel($id);


                if (isset($_POST['ProductEnquiry'])) {

                        $model->requirement = $_POST['ProductEnquiry']['requirement'];
                        if ($_POST['ProductEnquiry']['status'] == 2) {
                                $celib_history = new CelibStyleHistory;
                                $celib_history->enq_id = $model->id;
                                $celib_history->status = 2;
                                if ($celib_history->save()) {
                                        $celib_history_update = CelibStyleHistory::model()->findByPk($celib_history->id);
                                        $enc_enq_id = $model->id;
                                        $enc_celib_history_id = $celib_history->id;
                                        $getToken = $this->encrypt_decrypt('encrypt', 'enquiry_id=' . $enc_enq_id . ',history_id=' . $enc_celib_history_id);
                                        $celib_history_update->link = Yii::app()->request->baseUrl . '/index.php/Myaccount/SizeChartType?m=' . $getToken;
                                        if ($celib_history_update->save()) {
                                                $model->status = 2;
                                                if ($model->save()) {
                                                        $model->add_to_order = 3;
                                                        $this->ProductEnquiryMail(2, $celib_history_update);
                                                }
                                        }
                                }
                        } else if ($_POST['ProductEnquiry']['status'] == 3) {
                                $celib_history = new CelibStyleHistory;
                                $celib_history->enq_id = $model->id;
                                $celib_history->status = 3;
                                $celib_history->pay_amount = $_POST['amount'];
                                if ($celib_history->save()) {
                                        $celib_history_update = CelibStyleHistory::model()->findByPk($celib_history->id);
                                        $enc_enq_id = $model->id;
                                        $enc_celib_history_id = $celib_history->id;
                                        $getToken1 = $this->encrypt_decrypt('encrypt', 'enquiry_id=' . $enc_enq_id . ',history_id=' . $enc_celib_history_id);
                                        $celib_history_update->link = Yii::app()->request->baseUrl . '/index.php/Myaccount/MakepaymentPartial?p=' . $getToken1;
                                        if ($celib_history_update->save()) {
                                                $model->status = 3;
                                                if ($model->save()) {
                                                        $model->add_to_order = 3;
                                                        $this->ProductEnquiryMail(3, $celib_history_update);
                                                }
                                        }
                                }
                        } else if ($_POST['ProductEnquiry']['status'] == 5) {
                                $model->status = 5;
                        }

                        if ($model->save()) {
                                if ($model->enquiry_type != 0) {
                                        $ctype = $model->enquiry_type;
                                }
                                if ($model->status != 0) {
                                        $cat = $model->status;
                                }
                                $model->add_to_order = 3;
                                $this->redirect(array('admin', 'ctype' => '', 'cat' => ''));
                        }
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }

        public function ProductEnquiryMail($eid, $celib_history_update) {

                $toclient = ProductEnquiry::model()->findByPk($celib_history_update->enq_id)->email;
                $toadmin = AdminUser::model()->findByPk(4)->email;
                $enq_data = ProductEnquiry::model()->findByPk($celib_history_update->enq_id);
                $pdts = Products::model()->findByPk($enq_data->product_id);

                $toadmin = "dhanya@intersmart.in";
                if ($eid == 3) {
                        $subject = "$pdts->product_name - " . " Order #SLOR" . "$enq_data->order_id - " . 'Please Make Balance Payment';
                        $subject1 = "$enq_data->name " . "- $enq_data->email " . '- Payment Details';
                        $subject2 = 'Celebstyle - ' . "$pdts->product_name - " . "$enq_data->name " . '- Payment Details';
                } else if ($eid == 2) {
                        $subject = "Order Confirmation - " . "$pdts->product_name - " . " Order #SLOR" . "$enq_data->order_id";
                        $subject2 = "$enq_data->name - " . "$pdts->product_name - " . ' Order Confirmation sent - Order #SLOR' . "$enq_data->order_id";
                }


                $message = new YiiMailMessage;
                $message->view = "_product_enquiry_mail_client";  // view file name
                $params = array('model' => $celib_history_update, 'enq_data' => $enq_data, 'eid' => $eid); // parameters
                $message->subject = $subject;
                $message->setBody($params, 'text/html');
                $message->addTo($user);
                $message->setFrom('no-reply@solesilove.com', 'SolesiLove.com');
                Yii::app()->mail->send($message);


                $message1 = new YiiMailMessage;
                $message1->view = "_product_enquiry_mail_admin";  // view file name
                $params1 = array('model' => $celib_history_update, 'enq_data' => $enq_data); // parameters
                $message1->subject = $subject2;
                $message1->setBody($params1, 'text/html');
                $message1->addTo($admin);
                $message1->setFrom('no-reply@solesilove.com', 'SolesiLove.com');
                Yii::app()->mail->send($message1);
        }

        public function actionDelete($id) {
                $this->loadModel($id)->delete();


                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }

        public function actionForaddress($id) {
                $model = $this->loadModel($id);


                $to = $model->email;
                $subject = 'Reminder for address';
                $message = $this->renderPartial('_product_enquiry_add_address_client', array('model' => $model), true);
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: SolesiLove <no-reply@solesilove.com>' . "\r\n";
                mail($to, $subject, $message, $headers);
                Yii::app()->user->setFlash('success', "Address reminder sent successfully");
                if ($model->enquiry_type != 0) {
                        $ctype = $model->enquiry_type;
                }
                if ($model->status != 0) {
                        $cat = $model->status;
                }
                $this->redirect(array('admin', 'ctype' => $ctype, 'cat' => $cat));
        }

        public function actionIndex() {
                $dataProvider = new CActiveDataProvider('ProductEnquiry');
                $this->render('index', array(
                    'dataProvider' => $dataProvider,
                ));
        }

        /**
         * Manages all models.
         */
        public function actionAdmin($ctype = '', $cat = '') {
                $model = new ProductEnquiry('search');
                $model->unsetAttributes();  // clear any default values
                if (isset($_GET['ProductEnquiry']))
                        $model->attributes = $_GET['ProductEnquiry'];


                $this->render('admin', array(
                    'model' => $model,
                ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer $id the ID of the model to be loaded
         * @return ProductEnquiry the loaded model
         * @throws CHttpException
         */
        public function loadModel($id) {
                $model = ProductEnquiry::model()->findByPk($id);
                if ($model === null)
                        throw new CHttpException(404, 'The requested page does not exist.');
                return $model;
        }

        /**
         * Performs the AJAX validation.
         * @param ProductEnquiry $model the model to be validated
         */
        protected function performAjaxValidation($model) {
                if (isset($_POST['ajax']) && $_POST['ajax'] === 'product-enquiry-form') {
                        echo CActiveForm::validate($model);
                        Yii::app()->end();
                }
        }

        public function encrypt_decrypt($action, $string) {
                $output = false;

                $encrypt_method = "AES-256-CBC";
                $secret_key = 'This is my secret key';
                $secret_iv = 'This is my secret iv';

// hash
                $key = hash('sha256', $secret_key);

// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
                $iv = substr(hash('sha256', $secret_iv), 0, 16);

                if ($action == 'encrypt') {
                        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
                        $output = base64_encode($output);
                } else if ($action == 'decrypt') {
                        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
                }

                return $output;
        }

}
