<?php

class OptionImagesController extends Controller {

        /**
         * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
         * using two-column layout. See 'protected/views/layouts/column2.php'.
         */
        public $layout = '//layouts/column2';
public function Init() {
               if (!isset(Yii::app()->session['admin']) && Yii::app()->session['admin'] == '') {
                       $this->redirect(Yii::app()->baseUrl . '/admin.php/site/out');
               }

       }
        /**
         * @return array action filters
         */
        public function filters() {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete', // we only allow deletion via POST request
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules() {
                return array(
                    array('allow', // allow all users to perform 'index' and 'view' actions
                        'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'imageOptions'),
                        'users' => array('*'),
                    ),
                    array('allow', // allow authenticated user to perform 'create' and 'update' actions
                        'actions' => array('create', 'update'),
                        'users' => array('@'),
                    ),
                    array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'actions' => array('admin', 'delete'),
                        'users' => array('admin'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        /**
         * Displays a particular model.
         * @param integer $id the ID of the model to be displayed
         */
        public function actionView($id) {
                $this->render('view', array(
                    'model' => $this->loadModel($id),
                ));
        }

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate() {
                $model = new OptionImages;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

                if (isset($_POST['OptionImages'])) {
                        $model->attributes = $_POST['OptionImages'];
                        $image = CUploadedFile::getInstance($model, 'image');
                        $images = CUploadedFile::getInstancesByName('gallery_images');
                        $model->image = $image->extensionName;
                        if ($model->save()) {
                                if ($image != "") {
                                        $id = $model->id;
                                        $dimension[0] = array('width' => '116', 'height' => '155', 'name' => 'small');
                                        $dimension[1] = array('width' => '322', 'height' => '500', 'name' => 'medium');
                                        $dimension[2] = array('width' => '580', 'height' => '775', 'name' => 'big');
                                        $dimension[3] = array('width' => '1508', 'height' => '2015', 'name' => 'zoom');
                                        Yii::app()->Upload->uploadOptionImage($image, $id, true, $dimension);
                                }
                                if ($images != "") {
                                        $id = $model->id;
                                        $dimension[0] = array('width' => '116', 'height' => '155', 'name' => 'small');
                                        $dimension[1] = array('width' => '580', 'height' => '775', 'name' => 'big');
                                        $dimension[3] = array('width' => '1508', 'height' => '2015', 'name' => 'zoom');
                                        Yii::app()->Upload->uploadoptionMultipleImage($images, $id, true, $dimension);
                                }
                                $this->redirect(array('admin'));
                        }
                }

                $this->render('create', array(
                    'model' => $model,
                ));
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
                $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

                if (isset($_POST['OptionImages'])) {
                        $model->attributes = $_POST['OptionImages'];
                        if ($model->save())
                                $this->redirect(array('update', 'id' => $model->id));
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
                $this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }

        public function actionNewDelete() {
                $image = $_GET['path'];
                $id = $_GET['id'];
                $model = $this->loadModel($id);
                $folder = Yii::app()->Upload->folderName(0, 1000, $model->id);

                if (is_dir(Yii::app()->basePath . '/../uploads/producttoption/' . $folder)) {
                        $big = Yii::app()->basePath . '/../uploads/producttoption/' . $folder . '/' . $id . '/gallery/big/' . $image;
                        $small = Yii::app()->basePath . '/../uploads/producttoption/' . $folder . '/' . $id . '/gallery/small/' . $image;
                        $zoom = Yii::app()->basePath . '/../uploads/producttoption/' . $folder . '/' . $id . '/gallery/zoom/' . $image;
//                        $this->imageDelete($path);
                        unlink($big);
                        unlink($small);
                        unlink($zoom);
                        $this->redirect(Yii::app()->request->urlReferrer);
                }
        }

        public function actionImageOptions() {
                if (Yii::app()->request->isAjaxRequest) {


                        if ($_REQUEST['product_id'] != "") {
                                $imageOptions = OptionDetails::model()->findAllByAttributes(array('product_id' => $_REQUEST['product_id']), array(
                                    'select' => 't.color_id',
                                    'distinct' => true,
                                ));
                                if (!empty($imageOptions)) {
                                        $data .= '<option > --Select--</option>';
                                        foreach ($imageOptions as $imageOption) {
                                                $color_det = OptionCategory::model()->findByPk($imageOption->color_id);
                                                $data .= '<option value = "' . $imageOption->color_id . '">' . $color_det->color_name . '</option>';
                                        }

                                        echo $data;
                                } else {
                                        echo 0;
                                }
                        } else {
                                echo "0";
                        }
                }
        }

        /**
         * Lists all models.
         */
        public function actionIndex() {
                $dataProvider = new CActiveDataProvider('OptionImages');
                $this->render('index', array(
                    'dataProvider' => $dataProvider,
                ));
        }

        /**
         * Manages all models.
         */
        public function actionAdmin() {
                $model = new OptionImages('search');
                $model->unsetAttributes();  // clear any default values
                if (isset($_GET['OptionImages']))
                        $model->attributes = $_GET['OptionImages'];

                $this->render('admin', array(
                    'model' => $model,
                ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer $id the ID of the model to be loaded
         * @return OptionImages the loaded model
         * @throws CHttpException
         */
        public function loadModel($id) {
                $model = OptionImages::model()->findByPk($id);
                if ($model === null)
                        throw new CHttpException(404, 'The requested page does not exist.');
                return $model;
        }

        /**
         * Performs the AJAX validation.
         * @param OptionImages $model the model to be validated
         */
        protected function performAjaxValidation($model) {
                if (isset($_POST['ajax']) && $_POST['ajax'] === 'option-images-form') {
                        echo CActiveForm::validate($model);
                        Yii::app()->end();
                }
        }

}
