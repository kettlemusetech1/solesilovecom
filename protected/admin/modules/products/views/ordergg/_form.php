<section class="content-header">
        <h1>View Order #<?php echo $model->id; ?></h1>
        <ol class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl . '/admin.php/products/order/admin'; ?>"><i class="fa fa-dashboard"></i>  Order</a></li>
                <li class="active">Manage</li>
        </ol>
</section>
<!--<a href="<?php echo Yii::app()->request->baseUrl . '/admin.php/products/order/create'; ?>" class='btn  btn-success manage'>Add Order</a>-->
<div class="col-xs-12 form-page">
        <div class="box">
                <div class="box-body table-responsive no-padding">

                        <?php
                        $this->widget('booster.widgets.TbDetailView', array(
                            'data' => $model,
                            'attributes' => array(
                                array('name' => 'user_id',
                                    'value' => function($data) {
                                    return $data->user->first_name . ' ' . $data->user->last_name;
                            },
                                ),
                                array('name' => 'total_amount',
                                    'value' => function($data) {
                                    $enqy = ProductEnquiry::model()->findByAttributes(array('order_id' => $data->id));
                                    if (($enqy != '') && ($enqy != 0)) {
                                            $total = $enqy->total_amount + $enqy->shipping_charge;


                                            return $total;
                                    } else {
                                            $totalamt = $data->total_amount + $data->shipping_charge;
                                            return $totalamt;
                                    }
                            },
                                ),
                                array('name' => 'Amount Paid',
                                    'value' => function($data) {
                                    $enqy = ProductEnquiry::model()->findByAttributes(array('order_id' => $data->id));
                                    if (($enqy != '') && ($enqy != 0)) {

                                            $total = $enqy->total_amount + $enqy->shipping_charge - $enqy->balance_to_pay;

                                            return $total;
                                    } else if ($data->payment_status == 1) {
                                            $totalpaid = $data->total_amount + $data->shipping_charge;

                                            return 'INR ' . $totalpaid . '/-';
                                    } else {

                                            return 'INR ' . 0 . '/-';
                                    }
                            },
                                ),
                                array('name' => 'Balance Amount',
                                    'value' => function($data) {
                                    $enqy = ProductEnquiry::model()->findByAttributes(array('order_id' => $data->id));

                                    if (($enqy != '') && ($enqy != 0)) {
                                            $total = $enqy->balance_to_pay;

                                            return $total;
                                    } else {

                                            return 0;
                                    }
                            },
                                ),
                                array('name' => 'order_date',
                                    'value' => function($data) {
                                    $timestamp = strtotime($data->order_date);
                                    echo date('d/m/Y H:i:s', $timestamp);
                            },
                                    'type' => 'raw',
                                ),
                                array('name' => 'ship_address_id',
                                    'type' => 'raw',
                                    'value' => function($data) {

                                    if ($data->ship_address_id != 0) {
                                            $shipp_add = UserAddress::model()->findByPk($data->ship_address_id);
                                            $result .= $shipp_add->first_name . ' ' . $shipp_add->last_name . '<br />';
                                            $result .= $shipp_add->address_1 . ' ' . $shipp_add->address_2 . '<br />';
                                            $result .= $shipp_add->city . ' ' . $shipp_add->postcode . '<br />';
                                            $result .= Countries::model()->findByPk($shipp_add->country)->country_name . ' ' . States::model()->findByPk($shipp_add->state)->state_name . '<br />';
                                            $result .= '+' . Countries::model()->findByPk($shipp_add->country)->phonecode . ' ';
                                            $result .= $shipp_add->contact_number . '<br />';
                                    } else {
                                            $shipp_add = UserAddress::model()->findByAttributes(array('userid' => $data->user_id, 'default_shipping_address' => 1));
                                            $result .= $shipp_add->first_name . ' ' . $shipp_add->last_name . '<br />';
                                            $result .= $shipp_add->address_1 . ' ' . $shipp_add->address_2 . '<br />';
                                            $result .= $shipp_add->city . ' ' . $shipp_add->postcode . '<br />';
                                            $result .= Countries::model()->findByPk($shipp_add->country)->country_name . ' ' . States::model()->findByPk($shipp_add->state)->state_name . '<br />';
                                            $result .= '+' . Countries::model()->findByPk($shipp_add->country)->phonecode . ' ';
                                            $result .= $shipp_add->contact_number . '<br />';
                                    }
                                    return $result;
                            },
                                ),
                                array('name' => 'bill_address_id',
                                    'type' => 'raw',
                                    'value' => function($data) {

                                    if ($data->ship_address_id != 0) {
                                            $bill_add = UserAddress::model()->findByPk($data->bill_address_id);
                                            $result1 .= $bill_add->first_name . ' ' . $bill_add->last_name . '<br />';
                                            $result1 .= $bill_add->address_1 . ' ' . $bill_add->address_2 . '<br />';
                                            $result1 .= $bill_add->city . ' ' . $bill_add->postcode . '<br />';
                                            $result1 .= Countries::model()->findByPk($bill_add->country)->country_name . ' ' . States::model()->findByPk($bill_add->state)->state_name . '<br />';
                                            $result1 .= '+' . Countries::model()->findByPk($bill_add->country)->phonecode . ' ';
                                            $result1 .= $bill_add->contact_number . '<br />';
                                    } else {
                                            $bill_add = UserAddress::model()->findByAttributes(array('userid' => $data->user_id, 'default_billing_address' => 1));
                                            $result1 .= $bill_add->first_name . ' ' . $bill_add->last_name . '<br />';
                                            $result1 .= $bill_add->address_1 . ' ' . $bill_add->address_2 . '<br />';
                                            $result1 .= $bill_add->city . ' ' . $bill_add->postcode . '<br />';
                                            $result1 .= Countries::model()->findByPk($bill_add->country)->country_name . ' ' . States::model()->findByPk($bill_add->state)->state_name . '<br />';
                                            $result1 .= '+' . Countries::model()->findByPk($bill_add->country)->phonecode . ' ';
                                            $result1 .= $bill_add->contact_number . '<br />';
                                    }
                                    return $result1;
                            },
                                ),
                                'comment',
                                array('name' => 'payment_mode',
                                    'value' => function($data) {
                                    if ($data->payment_mode == 1) {
                                            return 'Wallet';
                                    } else if ($data->payment_mode == 2) {
                                            return 'Netbanking';
                                    } else if ($data->payment_mode == 3) {
                                            return 'Paypal';
                                    } else if ($data->payment_mode == 4) {
                                            return 'Wallet, Netbanking';
                                    } else if ($data->payment_mode == 5) {
                                            return 'Wallet, Paypal';
                                    } else {
                                            return 'Not Paid';
                                    }
                            },
                                ),
                                'admin_comment',
                                array('name' => 'transaction_id',
                                    'value' => function($data) {
                                    if ($data->transaction_id == 0) {
                                            return 'Nill';
                                    } else {
                                            return $data->transaction_id;
                                    }
                            },
                                ),
                                array('name' => 'payment_status',
                                    'value' => function($data) {
                                    if ($data->payment_status == 1) {
                                            return 'Success';
                                    } else if ($data->payment_status == 2) {
                                            return 'Failed Transaction';
                                    } else {
                                            return 'Not Paid';
                                    }
                            },
                                ),
                            ),
                        ));
                        ?>


                        <section class="content-header">
                                <h1>Order History</h1>
                                <br />
                                <a href="/admin.php/products/orderHistory/create/id/<?php echo $model->id; ?>" class="btn  btn-laksyah manage">Add New History</a>&nbsp;&nbsp;&nbsp;<a href="/admin.php/products/orderHistory/invoice/id/<?php echo $model->id; ?>" class="btn  btn-laksyah manage">Invoice</a>
                        </section>





                        <?php
                        $enq_details = ProductEnquiry::model()->findByPk(array('order_id' => $model->id));
                        if ($enq_details) {
                                $pdt_details = Products::model()->findByAttributes(array('id' => $enq_details->product_id));
                                $payment_details = MakePayment::model()->findAllByAttributes(array('product_code' => $pdt_details->product_code, 'userid' => $enq_details->user_id, 'enq_id' => $model->id));
                                ?>
                                <h1>Payment Details</h1>
                                <table border = "1">
                                        <thead>
                                                <tr>
                                                        <td><strong>Sl.No</strong></td>
                                                        <td><strong>Product Amount</strong></td>
                                                        <td><strong>Paid amount</strong></td>
                                                        <td><strong>Balance amount</strong></td>
                                                        <td><strong>Date</strong></td>
                                                        <td><strong>Transaction Id</strong></td>
                                                        <td><strong>Payment Status</strong></td>
                                                </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                if (!empty($payment_details)) {
                                                        $i = 1;
                                                        foreach ($payment_details as $payment_detail) {
                                                                ?>
                                                                <tr>
                                                                        <td><?= $i; ?></td>
                                                                        <td><?= $currentpayment_details->pay_amount; ?></td>
                                                                        <td><?= $payment_detail->total_amount; ?></td>
                                                                        <td><?= $currentpayment_details->pay_amount - $payment_detail->total_amount; ?></td>
                                                                        <td><?= $payment_detail->date; ?></td>
                                                                        <td><?= $payment_detail->transaction_id; ?></td>
                                                                        <td><?= $payment_detail->status == 1 ? 'paid' : 'Not Paid'; ?></td>
                                                                </tr>
                                                                <?php
                                                                $i++;
                                                        }
                                                }
                                                ?>
                                        </tbody>
                                </table>

                                <?php
                        }
                        ?>








                        <?php
                        $this->widget('booster.widgets.TbGridView', array(
                            'type' => ' bordered condensed hover',
                            'id' => 'order-products-grid',
                            'dataProvider' => $history->search(),
                            'columns' => array(
                                'date',
                                array('name' => 'status',
                                    'value' => function($data) {
                                    return OrderStatus::model()->findByPk($data->order_status)->title;
                            },
                                    'type' => 'raw',
                                ),
                                'order_status_comment',
                                array('name' => 'shipping_type',
                                    'value' => function($data) {
                                    return MasterShippingTypes::model()->findByPk($data->shipping_type)->shipping_type;
                            },
                                    'type' => 'raw',
                                ),
                                'tracking_id',
                                array(
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '',
                                ),
                            ),
                        ));
                        ?>
                        <?php if ($model->laksyah_gift == 1) { ?>

                                <div id="order-products-grid" class="grid-view">
                                        <div class="summary">Displaying 1-1 of 1 result.</div>
                                        <table class="items table table-bordered table-condensed table-hover">
                                                <thead>
                                                        <tr>
                                                                <th id="order-products-grid_c0"><a class="sort-link" >Date <span class="caret"></span></a></th><th id="order-products-grid_c1"><a class="sort-link" href="/admin.php/products/order/view/id/446/OrderHistory_sort/status">Gift Card Name <span class="caret"></span></a></th><th id="order-products-grid_c2"><a class="sort-link" href="/admin.php/products/order/view/id/446/OrderHistory_sort/order_status_comment">Amount <span class="caret"></span></a></th><th id="order-products-grid_c3"><a class="sort-link" href="/admin.php/products/order/view/id/446/OrderHistory_sort/shipping_type">Gift Delivery  Type <span class="caret"></span></a></th>
                                                                <th id="order-products-grid_c3"><a class="sort-link" href="/admin.php/products/order/view/id/446/OrderHistory_sort/shipping_type">Genarate Gift Code <span class="caret"></span></a></th>
                                                        </tr>
                                                </thead>
                                                <tbody>
                                                <h4>Gift Card Details</h4>
                                                <tr class="odd">
                                                        <td><?php echo $model->order_date; ?></td>
                                                        <td><?php echo GiftCard::model()->findByPk($model->gift_card_id)->name; ?></td>
                                                        <td><?php echo GiftCard::model()->findByPk($model->gift_card_id)->amount; ?></td>
                                                        <td><?php echo $model->laksyah_gift_transportaion == 1 ? 'Delivery By Email' : 'Delivery By Post' ?></td>
                                                        <td> Click here <a style="font-weight: bold; color: #cc006a;" href="<?php echo Yii::app()->request->baseUrl; ?>/admin.php/giftcard/userGiftscardHistory/create/user_id/<?php echo $model->user_id; ?>/gift_id/<?php echo $model->gift_card_id; ?>">Generate Gift Code</a></td>
                                                </tr>
                                                </tbody>
                                        </table>
                                        <div class="keys" style="display:none" title="/admin.php/products/order/view/id/446"><span>31</span></div>
                                </div>
                        <?php } else { ?>
                                <section class="content-header">
                                        <h1>Products</h1>
                                </section>
                                <?php
                                $this->widget('booster.widgets.TbGridView', array(
                                    'type' => ' bordered condensed hover',
                                    'id' => 'order-products-grid',
                                    'dataProvider' => $products->search(),
                                    'columns' => array(
                                        array('name' => 'product_id',
                                            'value' => function($data) {
                                            return '<a href="' . Yii::app()->baseUrl . '/admin.php/products/products/view/id/' . $data->product_id . '" target="_blank">' . $data->product->product_name . '</a>';
                                    },
                                            'type' => 'raw',
                                        ),
                                        array('name' => 'Product Code',
                                            'value' => function($data) {
                                            return $data->product->product_code;
                                    },
                                            'type' => 'raw',
                                        ),
                                        array('name' => 'Product Options',
                                            'value' => function($data) {
                                            if ($data->option_id != 0) {
                                                    $option_details = OptionDetails::model()->findByPk($data->option_id);
                                                    if ($option_details->color_id != 0) {
                                                            return 'Color : <div style="width:20px; float: right; height:20px; background-color:' . OptionCategory::model()->findByPk($option_details->color_id)->color_code . '"></div>' . OptionCategory::model()->findByPk($option_details->color_id)->color_name;
                                                    }
                                                    if ($option_details->size_id != 0) {
                                                            return 'Size : ' . OptionCategory::model()->findByPk($option_details->size_id)->size;
                                                    }
                                            } else {
                                                    return 'No Product Options are Mentioned';
                                            }
                                    },
                                            'type' => 'raw',
                                        ),
                                        array('name' => 'quantity',
                                            'value' => function($data) {
                                            if ($data->quantity == 0) {
                                                    return 1;
                                            } else {
                                                    return $data->quantity;
                                            }
                                    },
                                            'type' => 'raw',
                                        ),
                                        array('name' => 'amount',
                                            'value' => function($data) {
                                            return 'INR ' . $data->amount . '/-';
                                    }
                                        ),
                                        array(
                                            'htmlOptions' => array('nowrap' => 'nowrap'),
                                            'class' => 'booster.widgets.TbButtonColumn',
                                            'template' => '',
                                        ),
                                    ),
                                ));
                                ?>
                        <?php } ?>
                </div>
        </div>
</div>