<?php
$id = $_GET['id'];
?>
<section class="content-header">
        <h1>
                Order                <small>Manage</small>
        </h1>
        <ol class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl . '/managermode.php/products/order/admin'; ?>"><i class="fa fa-dashboard"></i>  Order</a></li>
                <li class="active">Manage</li>
        </ol>
</section>
<div class="col-xs-12 form-page">
        <div class="box">
                <div class="box-body table-responsive no-padding">
                        <?php
                        $model->history_status = $id;
                        $this->widget('booster.widgets.TbGridView', array(
                            'type' => ' bordered condensed hover',
                            'id' => 'order-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'columns' => array(
                                array('name' => 'id', 'header' => 'Order Id',
                                    'value' => function($data) {
                                    return'<a href="' . Yii::app()->baseUrl . '/managermode.php/products/order/view/id/' . $data->id . '" >SLOR' . $data->id . '</a>';
                            },
                                    'type' => 'raw',
                                ),
                                array('name' => 'user_id',
                                    'filter' => CHtml::listData(UserDetails::model()->findAll(), 'id', 'first_name'),
                                    'value' => function($data) {
                                    return'<a href="' . Yii::app()->baseUrl . '/managermode.php/user/UserDetails/view/id/' . $data->user_id . '" target="_blank">' . $data->user->first_name . ' ' . $data->user->last_name . '</a>';
                            },
                                    'type' => 'raw',
                                ),
                                array('name' => 'total_amount',
                                    'value' => function($data) {
                                    $totalamt = $data->total_amount + $data->shipping_charge;
                                    return $totalamt;
                            },
                                ),
                                array('name' => 'total_amount',
                                    'header' => 'Amount Paid',
                                    'value' => function($data) {
                                    $oid = $data->id;
                                    $enqy = ProductEnquiry::model()->findByAttributes(array('order_id' => $oid));
                                    if ($enqy != '') {
                                            $paidamt = $data->total_amount + $data->shipping_charge - $enqy->balance_to_pay;
                                    } else {
                                            $paidamt = $data->total_amount + $data->shipping_charge;
                                    }
                                    return $paidamt;
                            },
                                ),
                                array('name' => 'payment_mode',
                                    'filter' => array('1' => 'Wallet', '2' => 'Netbanking', '3' => 'Paypal', '4' => 'Wallet, Netbanking', '5' => 'NEFT Direct Transfer'),
                                    'value' => function($data) {
                                    if ($data->payment_mode == 1) {
                                            return 'Wallet';
                                    } else if ($data->payment_mode == 2) {
                                            return 'Netbanking';
                                    } else if ($data->payment_mode == 3) {
                                            return 'Paypal';
                                    } else if ($data->payment_mode == 4) {
                                            return 'Wallet, Netbanking';
                                    } else if ($data->payment_mode == 5) {
                                            return 'NEFT Direct Transfer';
                                    } else {
                                            return 'Not Paid';
                                    }
                            },
                                ),
                                array('name' => 'payment_status',
                                    'filter' => array('0' => 'Not Paid', '1' => 'Paid', '2' => 'Payment Failure'),
                                    'value' => function($data) {

                                    if ($data->payment_status == 0) {
                                            return 'Not paid';
                                    } else if ($data->payment_status == 1) {
                                            $oid = $data->id;
                                            $enqy = ProductEnquiry::model()->findByAttributes(array('order_id' => $oid));
                                            if ($enqy != '') {
                                                    if ($enqy->balance_to_pay == 0) {
                                                            return 'Payment Success / Full Payment';
                                                    } else {
                                                            return 'Payment Success / Partial Payment';
                                                    }
                                            } else {
                                                    return 'Payment Success';
                                            }
                                    } else if ($data->payment_status == 2) {
                                            return 'Payment Failed ';
                                    } else if ($data->payment_status == 3) {
                                            return 'Partial Payment Done ';
                                    }
                            },
                                ),
                                array('name' => 'transaction_id',
                                    'value' => function($data) {
                                    if ($data->transaction_id == 0) {
                                            return 'Nill';
                                    } else {
                                            return $data->transaction_id;
                                    }
                            },
                                ),
                                array('name' => 'history_status',
                                    'filter' => array('1' => 'ORDER PLACED', '2' => 'DELIVERY ADDRESS ADDED', '3' => 'ADVANCE PAYMENT RECEIVED', '4' => 'FULL PAYMENT RECEIVED', '5' => 'MEASUREMENT ADDED', '9' => 'PAYMENT FAILURE', '10' => 'ORDER SHIPPED', '11' => 'ITEM READY FOR QUALITY CHECK', '12' => 'WORK IN PROGRESS', '13' => 'ORDER READY FOR QC CHECK', '14' => 'QC CHECKED AND APPROVED', '16' => 'INVOICE ISSUED', '17' => 'ORDER READY FOR DISPATCH', '19' => 'SHIPMENT DELIVERED', '20' => 'ORDER COMPLETED', '21' => 'QUALITY CHECK PASSED', '22' => 'BALANCE PAYMENT RECEIVED', '23' => 'ITEM READY FOR PICK UP', '24' => 'ITEM READY FOR DISPATCH', '25' => 'ITEM SHIPPED', '26' => 'PUT ON HOLD', '27' => 'ORDER CANCELLED', '28' => 'REFUNDED', '29' => 'DECLINED', '30' => 'DELAYED', '31' => 'CREDIT NOTE ISSUED', '32' => 'VERIFICATION REQUIRED', '33' => 'ORDER READY FOR PICK UP', '34' => 'ORDER PICKED UP', '35' => 'DIGITAL GIFT CARD EMAILED', '37' => 'PLEASE PAY BALANCE AMOUNT', '38' => 'MAKE PAYMENT'),
                                    'value' => function($data) {
                                    if ($data->history_status == 1) {
                                            return 'ORDER PLACED';
                                    } else if ($data->history_status == 2) {
                                            return 'DELIVERY ADDRESS ADDED';
                                    } else if ($data->history_status == 3) {
                                            return 'ADVANCE PAYMENT RECEIVED';
                                    } else if ($data->history_status == 4) {
                                            return 'FULL PAYMENT RECEIVED';
                                    } else if ($data->history_status == 5) {
                                            return 'MEASUREMENT ADDED';
                                    } else if ($data->history_status == 9) {
                                            return 'PAYMENT FAILURE';
                                    } else if ($data->history_status == 10) {
                                            return 'ORDER SHIPPED';
                                    } else if ($data->history_status == 11) {
                                            return 'ITEM READY FOR QUALITY CHECK';
                                    } else if ($data->history_status == 12) {
                                            return 'WORK IN PROGRESS';
                                    } else if ($data->history_status == 13) {
                                            return 'ORDER READY FOR QC CHECK';
                                    } else if ($data->history_status == 14) {
                                            return 'QC CHECKED AND APPROVED';
                                    } else if ($data->history_status == 16) {
                                            return 'INVOICE ISSUED';
                                    } else if ($data->history_status == 17) {
                                            return 'ORDER READY FOR DISPATCH';
                                    } else if ($data->history_status == 19) {
                                            return 'SHIPMENT DELIVERED';
                                    } else if ($data->history_status == 20) {
                                            return 'ORDER COMPLETED';
                                    } else if ($data->history_status == 21) {
                                            return 'QUALITY CHECK PASSED';
                                    } else if ($data->history_status == 22) {
                                            return 'BALANCE PAYMENT RECEIVED';
                                    } else if ($data->history_status == 23) {
                                            return 'ITEM READY FOR PICK UP';
                                    } else if ($data->history_status == 24) {
                                            return 'ITEM READY FOR DISPATCH';
                                    } else if ($data->history_status == 25) {
                                            return 'ITEM SHIPPED';
                                    } else if ($data->history_status == 26) {
                                            return 'PUT ON HOLD';
                                    } else if ($data->history_status == 27) {
                                            return 'ORDER CANCELLED';
                                    } else if ($data->history_status == 28) {
                                            return 'REFUNDED';
                                    } else if ($data->history_status == 29) {
                                            return 'DECLINED';
                                    } else if ($data->history_status == 30) {
                                            return 'DELAYED';
                                    } else if ($data->history_status == 31) {
                                            return 'CREDIT NOTE ISSUED';
                                    } else if ($data->history_status == 32) {
                                            return 'VERIFICATION REQUIRED';
                                    } else if ($data->history_status == 33) {
                                            return 'ORDER READY FOR PICK UP';
                                    } else if ($data->history_status == 34) {
                                            return 'ORDER PICKED UP';
                                    } else if ($data->history_status == 35) {
                                            return 'DIGITAL GIFT CARD EMAILED';
                                    } else if ($data->history_status == 37) {
                                            return 'PLEASE PAY BALANCE AMOUNT';
                                    } else if ($data->history_status == 38) {
                                            return 'MAKE PAYMENT';
                                    }
                            },
                                ),
                                array('name' => 'status',
                                    'value' => function($data) {

                                    $latest_order_history = OrderHistory::model()->findByAttributes(array('order_id' => $data->id), array('order' => 'id DESC'));

                                    $latest_stats = OrderStatus::model()->findByPk($latest_order_history->order_status)->title;
                                    if ($latest_stats) {
                                            echo $latest_stats;
                                    } else {
                                            if ($data->status == 1) {
                                                    return 'Order Placed , Not Delivered to customer';
                                            } else if ($data->status == 2) {
                                                    return 'Order Success';
                                            } else if ($data->payment_status == 3) {
                                                    return 'Order Failed';
                                            } else {
                                                    return 'Order Not Placed';
                                            }
                                            return 'INR ' . $data->payment_status . '/-';
                                    }
                            },
                                ),
                                array('name' => 'order_date',
                                    'value' => function($data) {
                                    $timestamp = strtotime($data->order_date);
                                    echo date('d/m/Y H:i:s', $timestamp);
                            },
                                    'type' => 'raw',
                                ),
                                array('name' => 'laksyah_gift',
                                    'filter' => array('1' => 'Gift Card', '0' => 'Products'),
                                    'header' => 'Product Type',
                                    'value' => function($data) {

                            },
                                ),
                                array(
                                    'header' => '<font color="#61625D">View</font>',
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{view}',
                                ),
                                array(
                                    'header' => '<font color="#61625D">Print</font>',
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{print}',
                                    'buttons' => array(
                                        'print' => array(
                                            'url' => 'Yii::app()->request->baseUrl."/managermode.php/products/order/print/id/$data->id"',
                                            'label' => '<i class="fa fa-print" style="font-size:20px;padding:2px;"></i>',
                                            'options' => array(
                                                'data-toggle' => 'tooltip',
                                                'title' => 'Print',
                                                'target' => '_blank',
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                        ));
                        ?>
                </div>
        </div>
</div>
