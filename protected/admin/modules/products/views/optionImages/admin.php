<section class="content-header">
        <h1>
                Product Option Images

        </h1>
        <ol class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/admin.php/site/home"><i class="fa fa-dashboard"></i>Dashboard</a></li>
                <li class="active"> Products</li>
        </ol>

</section>
<a href="<?php echo Yii::app()->request->baseUrl . '/admin.php/products/optionImages/create'; ?>" class='btn  btn-laksyah manage'>Add Products Option Images</a>
<div class="col-xs-12 form-page">
        <div class="box">
                <div class="box-body table-responsive no-padding">
                        <?php
                        $this->widget('booster.widgets.TbGridView', array(
                            'type' => ' bordered condensed hover',
                            'id' => 'option-images-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'columns' => array(
                                array(
                                    'name' => 'image',
                                    'value' => function($data) {
                                            if ($data->image == "") {
                                                    return;
                                            } else {
                                                    $folder = Yii::app()->Upload->folderName(0, 1000, $data->id);
                                                    return '<img width="75" style="border: 2px solid #d2d2d2;" src="' . Yii::app()->request->baseUrl . '/uploads/producttoption/' . $folder . '/' . $data->id . '/' . $data->id . '.' . $data->image . '" />';
                                            }
                                    },
                                    'type' => 'raw'
                                ),
                                array(
                                    'name' => 'product_id',
                                    'value' => function($data) {
                                            $product = Products::model()->findByPk($data->product_id);
                                            return $product->product_name . '(' . $product->product_code . ')';
                                    },
                                    'type' => 'raw'
                                ),
                                array(
                                    'name' => 'product_id',
                                    'value' => function($data) {
                                            $product = OptionCategory::model()->findByPk($data->color_id);
                                            return $product->color_name . '(' . $product->color_code . ')';
                                    },
                                    'type' => 'raw'
                                ),
//                                'color_id',
//                                'status',
                                array(
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{update}{delete}',
                                ),
                            ),
                        ));
                        ?>
                </div>
        </div>
</div>
