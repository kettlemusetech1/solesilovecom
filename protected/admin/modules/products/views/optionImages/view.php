<?php
/* @var $this OptionImagesController */
/* @var $model OptionImages */

$this->breadcrumbs=array(
	'Option Images'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OptionImages', 'url'=>array('index')),
	array('label'=>'Create OptionImages', 'url'=>array('create')),
	array('label'=>'Update OptionImages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OptionImages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OptionImages', 'url'=>array('admin')),
);
?>

<h1>View OptionImages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'product_id',
		'color_id',
		'status',
	),
)); ?>
