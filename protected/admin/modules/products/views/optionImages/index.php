<?php
/* @var $this OptionImagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Option Images',
);

$this->menu=array(
	array('label'=>'Create OptionImages', 'url'=>array('create')),
	array('label'=>'Manage OptionImages', 'url'=>array('admin')),
);
?>

<h1>Option Images</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
