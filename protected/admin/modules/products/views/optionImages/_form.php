<?php
/* @var $this OptionImagesController */
/* @var $model OptionImages */
/* @var $form CActiveForm */
?>

<div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'option-images-form',
            'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>
        <br/>
        <div class="">
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'product_id', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo CHtml::activeDropDownList($model, 'product_id', CHtml::listData(Products::model()->findAll(['condition' => 'status=1']), 'id', 'product_name'), array('empty' => '--Select--', 'class' => 'form-control')); ?>

                        </div> <?php echo $form->error($model, 'product_id'); ?>
                </div>
                <br/><br/>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'color_id', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10">
                                <?php
                                if ($model->color_id != 0) {
                                        $cat = CHtml::listData(OptionDetails::model()->findAllByAttributes(array('product_id' => $model->product_id)), 'color_id', 'color_id');
                                } else {
                                        $cat = array();
                                }
                                $imageOptions = OptionDetails::model()->findAllByAttributes(array('product_id' => $model->product_id));
                                foreach ($imageOptions as $imageOption) {
                                        $color_det = OptionCategory::model()->findByPk($imageOption->color_id);
                                        if ($model->color_id == $color_det->id) {
                                                $sel = 'selected';
                                        } else {
                                                $sel = '';
                                        }
                                        $data .= "<option $sel value='" . $imageOption->color_id . "'>" . $color_det->color_name . "</option>";
                                }
//                                echo CHtml::activeDropDownList($model, 'color_id', $cat, array('empty' => '--Select--', 'class' => 'form-control', 'options' => array('id' => array('selected' => 'selected'))));
                                ?>
                                <select class="form-control" name="OptionImages[color_id]" id="OptionImages_color_id">
                                        <option value="">--Select--</option>
                                        <?php echo $data; ?>
                                </select>
                        </div>
                        <?php echo $form->error($model, 'color_id'); ?>
                </div>

                <br/><br/>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'Thumb Image ( image size : 322 X 500 )', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->fileField($model, 'image', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                                <?php
                                if ($model->image != '' && $model->id != "") {
                                        $folder = Yii::app()->Upload->folderName(0, 1000, $model->id);
                                        echo '<img width="125" style="border: 2px solid #d2d2d2;" src="' . Yii::app()->baseUrl . '/uploads/producttoption/' . $folder . '/' . $model->id . '/small' . '.' . $model->image . '" />';
                                }
                                ?>
                        </div>
                        <?php echo $form->error($model, 'image'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'Gallery Images ( image size : 1508 X 2015 )', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10">
                                <?php
                                $this->widget('CMultiFileUpload', array(
                                    'name' => 'gallery_images',
                                    'accept' => 'jpeg|jpg|gif|png', // useful for verifying files
                                    'duplicate' => 'Duplicate file!', // useful, i think
                                    'denied' => 'Invalid file type', // useful, i think
                                ));
                                ?>

                                <?php
                                if (!$model->isNewRecord) {
                                        $folder = Yii::app()->Upload->folderName(0, 1000, $model->id);

                                        // $path = Yii::getPathOfAlias('webroot') . '/uploads/products/' . $folder . '/' . $model->id . '/gallery/big';

                                        $path = Yii::getPathOfAlias('webroot') . '/uploads/producttoption/' . $folder . '/' . $model->id . '/gallery/big';


                                        $path2 = Yii::getPathOfAlias('webroot') . '/uploads/producttoption/' . $folder . '/' . $model->id . '/gallery/';


                                        foreach (glob("{$path}/*") as $file) {

                                                $info = pathinfo($file);
                                                $file_name = basename($file, '.' . $info['basename']);

                                                //  var_dump($file_name);



                                                if ($file != '') {
                                                        $arry = explode('/', $file);
                                                        echo '<div style="float:left;margin:5px;position:relative;">'
                                                        . '<a style="position:absolute;top:43%;left:40%;color:red;" href="' . Yii::app()->baseUrl . '/admin.php/products/optionImages/NewDelete?id=' . $model->id . '&path=' . $file_name . '"><i class="glyphicon glyphicon-trash"></i></a>'
                                                        . ' <img style="width:100px;height:100px;" src="' . Yii::app()->baseUrl . '/uploads/producttoption/' . $folder . '/' . $model->id . '/gallery/' . end($arry) . '"> </div>';
                                                }
                                        }
                                }
                                ?>
                        </div>
                        <?php echo $form->error($model, 'gallery_images'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10">
                                <?php echo $form->dropDownList($model, 'status', array('1' => 'Enabled', '0' => 'Disabled'), array('class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'status'); ?>
                </div>

        </div>
        <div class="form-group btns">
                <label>&nbsp;</label><br/>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-secondary btn-single pull-right', 'style' => 'border-radius:0px;padding: 10px 50px;')); ?>
        </div>

        <?php $this->endWidget(); ?>

</div><!-- form -->

<script>
        $(document).ready(function () {
                $('#OptionImages_product_id').on('change', function () {

                        var product_id = $(this).val();

                        $.ajax({
                                'url': baseurl + 'products/OptionImages/ImageOptions',
                                'type': "POST",
                                'dataType': 'html',
                                'data': {product_id: product_id},
                                success: function (data) {
                                        if (data == 0) {
                                                alert('There is no color Added On this Product.');
                                        } else {
                                                $('#OptionImages_color_id').html(data);
                                        }
                                }

                        });
                });
                if ($('#OptionImages_product_id').val() != "") {
                        $.ajax({
                                'url': baseurl + 'products/OptionImages/ImageOptions',
                                'type': "POST",
                                'dataType': 'html',
                                'data': {product_id: $('#OptionImages_product_id').val()},
                                success: function (result) {

                                }

                        });
                }
        });

</script>