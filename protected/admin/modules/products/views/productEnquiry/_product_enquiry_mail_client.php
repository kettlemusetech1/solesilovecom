<?php echo $this->renderPartial('//site/mail/_email_header'); ?>


<?php
$product_det = Products::model()->findByPk($enq_data->product_id);

$category_exp = explode(',', $product_det->category_id);

$final_cat = $category_exp[1];
?>               <tr><td>

        </td></tr>
<tr><td>
                <p style="padding: 5px 9px 6px 9px;">Hi <?php echo $enq_data->name; ?>,<br/><br/>Greetings from SolesiLove!</p>
                <p style="padding: 5px 9px 6px 9px;">Many thanks for the order confirmation.</p>
                <p style="padding: 5px 9px 6px 9px;">To proceed further with this order you may kindly click the below link <?= $eid == 3 ? '"MAKE PAYMENT"' : '"ADD MEASUREMENT"'; ?> to fill in your <?= $eid == 3 ? 'payment details' : 'measurements'; ?>.</p>


        </td></tr>
<tr>
        <td>
                <table cellspacing="0" cellpadding="0" border="0" width="776" style="    font-family: 'Open Sans',arial, sans-serif;font-size: 13px;">
                        <thead>
                                <tr>
                                        <th align="left" width="325" bgcolor="#EAEAEA" style="    font-family: 'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Your Order Details:</th>
                                        <th width="10"></th>
                                        <th align="left" width="325" bgcolor="#EAEAEA" style="font-family:'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em"></th>
                                </tr>
                        </thead>
                        <tbody>
                                <?php
                                if ($enq_data->order_id != 0) {
                                        ?>
                                        <tr>
                                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                        Order No.</td>

                                                <td>
                                                        &nbsp;
                                                </td>
                                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                        SLOR<?php echo $enq_data->order_id; ?>



                                                </td>

                                        </tr>

                                        <?php
                                }
                                ?>
                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                Product Name & Code</td>

                                        <td>
                                                &nbsp;
                                        </td>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                <?php echo Products::model()->findByPk($enq_data->product_id)->product_name; ?> - <?php echo Products::model()->findByPk($enq_data->product_id)->product_code; ?>


                                        </td>

                                </tr>

                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                Category</td>

                                        <td>
                                                &nbsp;
                                        </td>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                <?php echo ProductCategory::model()->findByPk($final_cat)->category_name; ?>



                                        </td>

                                </tr>



                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                Product Price</td>

                                        <td>
                                                &nbsp;
                                        </td>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                Rs. <?php echo Products::model()->findByPk($enq_data->product_id)->price; ?>

                                        </td>

                                </tr>

                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                Shipping Charges</td>

                                        <td>
                                                &nbsp;
                                        </td>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                Rs. <?php echo $enq_data->shipping_charge; ?>


                                        </td>

                                </tr>

                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                                Total Amount (Including shipping charges)</td>

                                        <td>
                                                &nbsp;
                                        </td>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                Rs. <?php echo $enq_data->total_amount; ?></td>

                                </tr>




                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                Billing Information

                                        </td>
                                        <td>
                                                &nbsp;
                                        </td>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                <?php echo UserAddress::model()->findByPk($enq_data->billing_address)->address_1 . '<br/>'; ?>
                                                <?php echo UserAddress::model()->findByPk($enq_data->billing_address)->city . '<br/>'; ?>
                                                <?php echo UserAddress::model()->findByPk($enq_data->billing_address)->state . '<br/>'; ?>
                                                <?php echo UserAddress::model()->findByPk($enq_data->billing_address)->postcode . '<br/>'; ?>
                                                <?php $country = UserAddress::model()->findByPk($enq_data->billing_address)->country; ?>
                                                <?php echo Countries::model()->findByPk($country)->country_name; ?>


                                        </td>

                                </tr>

                                <tr>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                Shipping Information


                                        </td>
                                        <td>
                                                &nbsp;
                                        </td>
                                        <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                                <?php echo UserAddress::model()->findByPk($enq_data->shipping_address)->address_1 . '<br/>'; ?>
                                                <?php echo UserAddress::model()->findByPk($enq_data->shipping_address)->city . '<br/>'; ?>
                                                <?php echo UserAddress::model()->findByPk($enq_data->shipping_address)->state . '<br/>'; ?>
                                                <?php echo UserAddress::model()->findByPk($enq_data->shipping_address)->postcode . '<br/>'; ?>
                                                <?php $country = UserAddress::model()->findByPk($enq_data->shipping_address)->country; ?>
                                                <?php echo Countries::model()->findByPk($country)->country_name; ?>





                                        </td>

                                </tr>



                        </tbody>
                </table>

                <p style="font-size:12px;margin:0 0 10px 0"></p>
        </td>
</tr>

<tr>
        <td>
                <br/>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px; text-align:center;">
                        To proceed further with this order, we request you to click the below link <?= $eid == 3 ? '"Make Payment"' : '"Add Measurement"'; ?> and you would be redirected to our <?= $eid == 3 ? 'payment' : 'measurement'; ?> page.
                </p>
        </td>
</tr>

<tr><td><a href="<?php echo $this->siteURL(); ?><?php echo $model->link; ?>" style="    text-transform: uppercase;
           background-color: #ec9721;
           border-radius: 0;
           outline: none;
           height: 40px;
           line-height: 40px;
           padding: 9px 28px;
           border: solid 1px #ec9721;
           border-radius: 4px;
           margin-left: 37%;
           text-align: center;
           text-decoration: none;
           color: #fff;font-weight:bold"><?= $eid == 3 ? 'MAKE PAYMENT' : 'ADD MEASUREMENT'; ?></a></td></tr>

<tr><td>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;color: #abaaaa; text-align:center;">* This is an automatically generated email, please do not reply to this email.</p>

                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;color: #abaaaa; text-align:center;">* If you need assistance, please call +91 914 220 2222 during office hours: Mon to Sat 9:30am to 6:30pm IST or send an email to support@laksyah.com</p>
        </td>
</tr>



<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>