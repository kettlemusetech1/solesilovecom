<?php echo $this->renderPartial('//site/mail/_email_header'); ?>
<tr><td>
                <h4 style="padding: 5px 9px 6px 9px;">Dear Admin,</h4>
                <h5 style="padding: 5px 9px 6px 9px;">The <?= $model->status == 3 ? 'Make Payment' : 'Add Measurement'; ?> link sent to <?php echo $enq_data->name; ?>. </h5>
        </td></tr>
<td>
        <table cellspacing="0" cellpadding="0" border="0" width="776" style="    font-family: 'Open Sans',arial, sans-serif;font-size: 13px;">
                <thead>
                        <tr>
                                <th align="left" width="325" bgcolor="#EAEAEA" style="    font-family: 'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Coustomor Details:</th>
                                <th width="10"></th>
                                <th align="left" width="325" bgcolor="#EAEAEA" style="font-family:'Open Sans',arial, sans-serif;font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Product: <?php echo Products::model()->findByPk($enq_data->product_id)->product_name; ?> - <?php echo Products::model()->findByPk($enq_data->product_id)->product_code; ?></th>
                        </tr>
                </thead>
                <tbody>


                        <tr>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                        Name


                                </td>

                                <td>
                                        &nbsp;
                                </td>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                        <?php echo $enq_data->name; ?>


                                </td>

                        </tr>
                        <tr>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                        Email


                                </td>
                                <td>
                                        &nbsp;
                                </td>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                        <?php echo $enq_data->email; ?>


                                </td>

                        </tr>
                        <tr>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                        Phone Number


                                </td>
                                <td>
                                        &nbsp;
                                </td>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                        <?php echo $enq_data->phone; ?>


                                </td>

                        </tr>
                        <tr>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                        Country


                                </td>
                                <td>
                                        &nbsp;
                                </td>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">
                                        <?php echo Countries::model()->findByPk($enq_data->country)->country_name; ?>



                                </td>

                        </tr>
                        <tr>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                        Requirement


                                </td>
                                <td>
                                        &nbsp;
                                </td>
                                <td valign="top" style="font-size:13px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea">

                                        <?php echo $enq_data->requirement; ?>


                                </td>

                        </tr>


                </tbody>
        </table>
        <br>


        <br>

        <br>
        <p style="font-size:12px;margin:0 0 10px 0"></p>
</td>
</tr>


<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>