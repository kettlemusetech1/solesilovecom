<?php $ctype = $_GET['ctype'];  
$cat= $_GET['cat'];  
if($cat==1)
{
$bk="Enquiry Placed";
}
else if($cat==2)
{
$bk="Measurement Initiated";
}
else if($cat==3)
{
$bk="Payment Initiated";
}
if($ctype==3)
{
$tit="Gift Card Enquiry";
}
else if($ctype==2)
{
$tit="Non Celebrity Enquiry";
}
else if($ctype==1)
{
$tit="Celebrity Enquiry";
}

?>
<section class="content-header">
        <h1>
                <?php echo $tit; ?><?php if($bk) { echo "-".$bk;} ?>               <small>Manage</small>
        </h1>
        <ol class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl . '/admin.php/products/productEnquiry/admin'; ?>"><i class="fa fa-dashboard"></i> <?php echo $tit; ?> </a></li>
                <li class="active">Manage</li>
        </ol>
</section>
<?php if(Yii::app()->user->hasFlash('success')):?>

<div class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <?php echo Yii::app()->user->getFlash('success'); ?> </div>
<?php endif; ?>
<!--<a href="<?php echo Yii::app()->request->baseUrl . '/admin.php/products/productEnquiry/create'; ?>" class='btn  btn-laksyah manage'>Add <?php echo $tit; ?> </a>-->
<div class="col-xs-12 form-page">
        <div class="box">
                <div class="panel panel-default">
                        <?php
                        if ($ctype == 1) {
                                $model->enquiry_type = $ctype;
                        } else {
                                $model->enquiry_type = $ctype;
                        }
$model->status=$cat;
                        $this->widget('booster.widgets.TbGridView', array(
                            'type' => ' bordered condensed hover',
                            'id' => 'product-enquiry-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'columns' => array(
                                   array('name' => 'id', 'header'=>'Enquiry Id',
                                    'value' => function($data) {
                                            return $data->id;
                                    },
                                    'type' => 'raw',
                                ),
                                'name',

                                'email',
                                'phone',
                                array('name' => 'country',
                                    'value' => function($data) {
                                            return $data->country0->country_name;
                                    },
                                    'type' => 'raw',
                                ),
                                array('name' => 'product_id',
                                    'value' => function($data) {
                                            return $data->product->product_name;
                                    },
                                    'type' => 'raw',
                                ),
                                array('name' => 'status',
                                    'value' => function($data) {
                                            if($data->status==3)
                                              {
                                              return 'Payment Initiate';
                                              }
                                              else if($data->status==2)
                                              {
                                              return 'Measurement Initiate';
                                              }
                                              else if($data->status==1)
                                              {
                                              return 'Enquiry Placed';
                                              }
                                    },
                                    'type' => 'raw',
                                ),
                                'doc',
                                /*
                                  'requirement',
                                  'product_id',
                                  'doc',
                                  'dou',
                                  'user_id',
                                 */
                                array(
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{update}',
                                ),

                                 array(
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{delete}',
                                ),

                               array(
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{Order}',
                                    'buttons' => array(
                                        'Order' => array(
                                            'url' => 'Yii::app()->request->baseUrl."/admin.php/products/productEnquiry/foraddress/id/".$data->id',
                                            'visible' => '$data->shipping_address == 0 && $data->enquiry_type == 1',
                                            'label' => '<i class="fa fa-envelope" aria-hidden="true"></i>',
                                            'options' => array(
                                                'data-toggle' => 'tooltip',
                                                'title' => 'Add address notification',
                                            ),
                                        ),
                                    ),
                                ),


                            ),
                        ));
                        ?>
                </div>
        </div>
</div>
