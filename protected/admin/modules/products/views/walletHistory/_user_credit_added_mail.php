<?php echo $this->renderPartial('//site/mail/_email_header'); ?>
<tr>
        <td style="padding:40px 20px; font-family:'Open Sans',arial, sans-serif; font-size:13px"><p>Hi <?php echo $user_model->first_name; ?><span>
                                <?php echo $user_model->last_name; ?>,<br/><br/>Hello from Soles iLove!</p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">Credit Note  $<?php echo $entry_amount->amount; ?>
                        has been added to your Soles iLove account. Your "My Credit" balance is $<?php echo $user_model->wallet_amt; ?></p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;">
                        <?php
                        if ($entry_amount->creditnote_no) {
                                echo "Credit note no. SLCN $entry_amount->creditnote_no ,";
                        }
                        ?>
                        <?php
                        // if ($entry_amount->order_no) {
                        //         echo "Order no. SLOR $entry_amount->order_no ,";
                        // }
                        ?>
                        <?php
                        // if ($entry_amount->invoice_no) {
                        //         echo "Invoice no. SLOR $entry_amount->invoice_no";
                        // }
                        ?>
                </p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;"> Your "My Credit" balance is $<?php echo $user_model->wallet_amt; ?></p>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;"><a href="<?php echo $this->siteURL() . '/CreditHistory' ?>" style="text-transform: uppercase;background-color: #f47721;border-radius: 0;outline: none;border: none;height: 40px;line-height: 40px;padding: 0px 10px;padding-left: 30px;padding-right: 30px; color:#fff; text-decoration:none; display:inline-block;">VIEW MY CREDIT</a></p>



                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;font-style:italic;color:#acacb1; text-align: center">* This is an automatically generated email, please do not reply to this email.</p>
        </td>
</tr>
<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>