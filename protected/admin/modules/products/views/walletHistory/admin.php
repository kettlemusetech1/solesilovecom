<?php
/* @var $this WalletHistoryController */
/* @var $model WalletHistory */
?>
<style>
        .table th, td{
                text-align: center;
        }
        .table td{
                text-align: center;
        }
</style>


<div class="page-title">

        <div class="title-env">
                <h1 style="float: left;" class="title">WalletHistory</h1>
                <p style="float: left;margin-top: 8px;margin-left: 11px;" class="description">Manage WalletHistory</p>
        </div>

        <div class="breadcrumb-env">

                <ol class="breadcrumb bc-1" >
                        <li>
                                <a href="<?php echo Yii::app()->request->baseurl . '/site/home'; ?>"><i class="fa-home"></i>Home</a>
                        </li>

                        <li class="active">

                                <strong>Manage WalletHistory</strong>
                        </li>
                </ol>

        </div>

</div>

<div class="row">


        <div class="col-sm-12">

                <!--<a href="<?php echo Yii::app()->request->baseUrl . '/managermode.php/products/walletHistory/create'; ?>" class='btn  btn-laksyah manage'>Add Credit History</a>-->

                <div class="panel panel-default">
                        <?php
                        // $model->credit_debit = 1;
                        $this->widget('booster.widgets.TbGridView', array(
                            'type' => ' bordered condensed hover',
                            'id' => 'wallet-history-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'columns' => array(
//            		'id',
                                array('name' => 'user_id',
                                    'type' => 'raw',
                                    'value' => function($data) {

                                    $user_id = UserDetails::model()->findByPk($data->user_id);
                                    if ($data->user_id != 0) {
                                            return $user_id->first_name . ' ' . $user_id->last_name . '(USER ID:' . $user_id->id . ')';
                                    } else {
                                            return 'User Not SPecified';
                                    }
                            },
                                ),
                                array('name' => 'balance_amt',
                                    'type' => 'raw',
                                    'value' => function($data) {
                                    $user_id = UserDetails::model()->findByPk($data->user_id);
                                    if ($user_id->wallet_amt) {
                                            return $data->balance_amt;
                                    }
                            },
                                ),
                                array(
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{details}',
                                    'buttons' => array(
                                        'details' => array(
                                            //'url' => '#',
                                            'url' => '$data->user_id',
                                            // 'visible' => '$data->credit_debit == 1',
                                            // 'label' => '<i class="fa fa-table" aria-hidden="true"></i>',
                                            'label' => '<button type="button" class="btn btn-default history_btn"   data-toggle="modal" data-target="#history_modal">View</button>',
                                            'options' => array(
                                                'data-toggle' => 'tooltip',
                                                'title' => 'View Details',
                                            ),
                                        ),
                                    ),
                                ),
                            /*  'balance_amt',
                              'ids',
                              'field1',
                              'field2',
                              'payment_method',
                              'transaction_id',
                              'unique_code',

                              array(
                              'htmlOptions' => array('nowrap' => 'nowrap', 'heading' => 'View',
                              ),
                              'class' => 'booster.widgets.TbButtonColumn',
                              'template' => '{view}',
                              ), */
                            ),
                        ));
                        ?>
                </div>

        </div>


</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="history_modal">
        <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">History</h4>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                        </div>
                </div>
        </div>
</div>
<script>
        $(document).ready(function() {
                $('.history_btn').click(function(e) {
                        e.preventDefault();
                        uid = $(this).parent().attr('href');

                        var url = '<?= Yii::app()->request->baseUrl; ?>/managermode.php/products/walletHistory/view/id/' + uid;
                        $('#history_modal').modal('show')
                                .find('.modal-body')
                                .load(url);

                });
        });
</script>
