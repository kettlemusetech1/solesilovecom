<?php echo $this->renderPartial('//site/mail/_email_header'); ?>

<tr><td align="center">
                <?php
                $user_id = Order::model()->findByPk($model->order_id);
                $enquiry = ProductEnquiry::model()->findByAttributes(array('order_id' => $model->order_id));
                $user_det = UserDetails::model()->findByPk($user_id->user_id);
                ?>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Hi <?php echo $user_det->first_name; ?> <?php echo $user_det->last_name; ?>, </h4>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Greetings from Solesilove!</h4>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Your order # LKOR<?php echo $order->id; ?> has been updated to the following status:</h4>

                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Current Status: <?php echo OrderStatus::model()->findByPk($model->order_status)->title; ?></h4>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Comments: <?php echo $model->order_status_comment; ?></h4>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Updated Time: <?php echo $model->date; ?></h4>
                <?php
                if ((OrderStatus::model()->findByPk($model->order_status)->id == 10 ) || (OrderStatus::model()->findByPk($model->order_status)->id == 25)) {
                        if ($model->shipping_type == 1) {
                                $track = "https://track.aftership.com/dtdc/$model->tracking_id";
                        } else if ($model->shipping_type == 2) {
                                $track = "https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=$model->tracking_id";
                        } else if ($model->shipping_type == 3) {
                                $track = "https://www.aramex.com/track/results?ShipmentNumber=$model->tracking_id";
                        }
                        ?>

                        <a href="<?php echo $track; ?>" name="yt0"  style="     text-transform: uppercase;
                           background-color: #ec9721;
                           border-radius: 0;
                           outline: none;
                           height: 40px;
                           line-height: 40px;
                           padding: 9px 28px;
                           border: solid 1px #ec9721;
                           /* width: 360px; */
                           border-radius: 4px;
                           text-align: center;
                           text-decoration: none;
                           color: #fff;
                           /* display: inline-block; */
                           /* margin: 0 auto; */">Track Shipment </a>

                        <?php
                }  else {
                        ?>
                        <a href="<?php echo $this->siteURL(); ?>/index.php/Myaccount/Myordernew/id/<?php echo $model->order_id; ?>" name="yt0"  style="text-transform: uppercase;
                           background-color: #ec9721;
                           border-radius: 0;
                           outline: none;
                           height: 40px;
                           line-height: 40px;
                           padding: 9px 28px;
                           border: solid 1px #ec9721;
                           /* width: 360px; */
                           border-radius: 4px;
                           text-align: center;
                           text-decoration: none;
                           color: #fff;
                           /* display: inline-block; */
                           /* margin: 0 auto; */">View Order</a>


                        <?php
                }
                ?>



        </td></tr>
<tr><td>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;color: #abaaaa; text-align:center;">* This is an automatically generated email, please do not reply to this email.</p>

                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;color: #abaaaa; text-align:center;">* If you need assistance, please send an email to info@solesilove.com</p>
        </td>
</tr>


<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>
