<style>
        .nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
                color: #fff;
                background-color: #7e8c7e;
                border: 1px solid #ddd;
                border-bottom-color: transparent;
                cursor: default;
        }
        .nav-tabs {
                border-bottom: 1px solid #7e8c7e;
        }
        .errorMessage {
                color: red;
        }
</style>
<div class="form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'products-form',
            'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
            'enableAjaxValidation' => false,
        ));
        ?>
        <?= $form->errorSummary($model); ?>
        <p class="note">Fields with <span class="required">*</span> are required.</p>
        <div class="hasapge">
                <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Basic Product Details</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Product Description</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Price & SEO Contents</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                        <br/>
                                        <!--                <div class="form-group">
                                        <?php //echo$form->labelEx($model, 'main_category', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->dropDownList($model, 'main_category', array('1' => "Men", '2' => "Women"), array('class' => 'form-control')); ?></div>
                                        <?php //echo $form->error($model, 'main_category'); ?>
                                        </div>-->
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'category_id', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10">
                                                        <?php
                                                        if (!$model->isNewRecord) {
                                                            if (!empty($model->category_id)) {
                                                                $ids = explode(',', $model->category_id);
                                                                $selected = array();
                                                                foreach ($ids as $id) {
                                                                    $selected[$id] = array('selected' => true);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <?php echo $form->hiddenField($model, 'category_id'); ?>
                                                        <div class="col-sm-10"><?php
                                                                $this->widget('application.admin.components.CatSelect', array(
                                                                    'type' => 'category',
                                                                    'field_val' => $model->category_id,
                                                                    'category_tag_id' => 'Products_category_id', /* id of hidden field */
                                                                    'form_id' => 'product-category-form',
                                                                ));
                                                                ?></div>
                                                </div>
                                                <?php echo $form->error($model, 'category_id'); ?>
                                        </div>
                                        <div class="form-group" >
                                                <?php echo $form->labelEx($model, 'product_name', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->textField($model, 'product_name', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control slug', 'autocomplete' => 'off')); ?>
                                                </div>
                                                <?php echo $form->error($model, 'product_name'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'product_code', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->textField($model, 'product_code', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control')); ?>
                                                </div>
                                                <?php echo $form->error($model, 'product_code'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php //echo $form->labelEx($model, 'canonical_name', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"> <?php echo $form->hiddenField($model, 'canonical_name', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control')); ?>
                                                </div>
                                                <?php // echo $form->error($model, 'canonical_name'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'brand', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"> <?php echo CHtml::activeDropDownList($model, 'brand', CHtml::listData(Brands::model()->findAll(), 'id', 'brand_name'), array('empty' => '--Select Brand--', 'class' => 'form-control')); ?>
                                                </div>
                                                <?php echo $form->error($model, 'brand'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'Main Image ( image size : 274 X 292 )', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->fileField($model, 'main_image', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                                                        <?php
                                                        if ($model->main_image != '' && $model->id != "") {
                                                            $folder = Yii::app()->Upload->folderName(0, 1000, $model->id);
                                                            echo '<img width="125" style="border: 2px solid #d2d2d2;" src="' . Yii::app()->baseUrl . '/uploads/products/' . $folder . '/' . $model->id . '/small' . '.' . $model->main_image . '" />';
                                                        }
                                                        ?>
                                                </div>
                                                <?php echo $form->error($model, 'main_image'); ?>
                                        </div>
                                        <!--                                        <div class="form-group">
                                        <?php echo $form->labelEx($model, 'Hover Images ( image size : 322 X 500 )', array('class' => 'col-sm-2 control-label')); ?>
                                                                                        <div class="col-sm-10"><?php echo $form->fileField($model, 'hover_image', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
                                        <?php
                                        if ($model->hover_image != '' && $model->id != "") {
                                            $folder = Yii::app()->Upload->folderName(0, 1000, $model->id);
                                            echo '<img width="125" style="border: 2px solid #d2d2d2;" src="' . Yii::app()->baseUrl . '/uploads/products/' . $folder . '/' . $model->id . '/hover/hover.' . $model->hover_image . '" />';
                                        }
                                        ?>
                                                                                        </div>
                                        <?php echo $form->error($model, 'hover_image'); ?>
                                                                                </div>-->
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'Gallery Images ( image size : 1000 X 902 )', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10">
                                                        <?php
                                                        $this->widget('CMultiFileUpload', array(
                                                            'name' => 'gallery_images',
                                                            'accept' => 'jpeg|jpg|gif|png', // useful for verifying files
                                                            'duplicate' => 'Duplicate file!', // useful, i think
                                                            'denied' => 'Invalid file type', // useful, i think
                                                        ));
                                                        ?>
                                                        <?php
                                                        if (!$model->isNewRecord) {
                                                            $folder = Yii::app()->Upload->folderName(0, 1000, $model->id);
                                                            $path = Yii::getPathOfAlias('webroot') . '/uploads/products/' . $folder . '/' . $model->id . '/gallery/big';
                                                            $path2 = Yii::getPathOfAlias('webroot') . '/uploads/products/' . $folder . '/' . $model->id . '/gallery/';
                                                            foreach (glob("{$path}/*") as $file) {

                                                                $info = pathinfo($file);
                                                                $file_name = basename($file, '.' . $info['basename']);
                                                                if ($file != '') {
                                                                    $arry = explode('/', $file);
                                                                    echo '<div style="float:left;margin:5px;position:relative;">'
                                                                    . '<a style="position:absolute;top:43%;left:40%;color:red;" href="' . Yii::app()->baseUrl . '/admin.php/products/products/NewDelete?id=' . $model->id . '&path=' . $file_name . '"><i class="glyphicon glyphicon-trash"></i></a>'
                                                                    . ' <img style="width:100px;height:100px;" src="' . Yii::app()->baseUrl . '/uploads/products/' . $folder . '/' . $model->id . '/gallery/' . end($arry) . '"> </div>';
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                </div>
                                                <?php echo $form->error($model, 'gallery_images'); ?>
                                        </div>
                                        <!--                <div class="form-group">
                                        <?php //echo $form->labelEx($model, 'description', array('class' => 'col-sm-2 control-label')); ?>
                                                                <div class="col-sm-10"><?php
                                        //$this->widget('application.admin.extensions.eckeditor.ECKEditor', array(
                                        //    'model' => $model,
                                        //     'attribute' => 'description',
                                        //  ));
                                        ?>
                                                                </div>
                                        <?php //echo $form->error($model, 'description'); ?>
                                                        </div>-->
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->dropDownList($model, 'status', array('1' => "Enabled", '0' => "Disabled"), array('class' => 'form-control')); ?>
                                                </div>
                                                <?php echo $form->error($model, 'status'); ?>
                                        </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                        <br/>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'product_details', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php
                                                        $this->widget('application.admin.extensions.eckeditor.ECKEditor', array(
                                                            'model' => $model,
                                                            'attribute' => 'product_details',
                                                        ));
                                                        ?>
                                                </div>
                                                <?php echo $form->error($model, 'product_details'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'Product Features', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php
                                                        $this->widget('application.admin.extensions.eckeditor.ECKEditor', array(
                                                            'model' => $model,
                                                            'attribute' => 'product_details1',
                                                        ));
                                                        ?>
                                                </div>
                                                <?php echo $form->error($model, 'product_details1'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'Product Size And Fitting', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php
                                                        $this->widget('application.admin.extensions.eckeditor.ECKEditor', array(
                                                            'model' => $model,
                                                            'attribute' => 'size_fitting',
                                                        ));
                                                        ?>
                                                </div>
                                                <?php echo $form->error($model, 'size_fitting'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'Product Delivery Details', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php
                                                        $this->widget('application.admin.extensions.eckeditor.ECKEditor', array(
                                                            'model' => $model,
                                                            'attribute' => 'delivery',
                                                        ));
                                                        ?>
                                                </div>
                                                <?php echo $form->error($model, 'delivery'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'Product Styling', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php
                                                        $this->widget('application.admin.extensions.eckeditor.ECKEditor', array(
                                                            'model' => $model,
                                                            'attribute' => 'styling',
                                                        ));
                                                        ?>
                                                </div>
                                                <?php echo $form->error($model, 'styling'); ?>
                                        </div>
                                        <?php
                                        if (!is_array($model->toy_style)) {
                                            $toy_style = explode(',', $model->toy_style);
                                        } else {
                                            $toy_style = $model->toy_style;
                                        }


                                        $product_toy_style = array();

                                        foreach ($toy_style as $value1) {
                                            $product_toy_style[$value1] = array('selected' => 'selected');
                                        }
                                        ?>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'Toe Style', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo CHtml::activeDropDownList($model, 'toy_style', CHtml::listData(ToyStyle::model()->findAll(), 'id', 'title'), array('class' => 'form-control', 'multiple' => true, 'options' => $product_toy_style)); ?>
                                                </div>
                                                <?php echo $form->error($model, 'toy_style'); ?>
                                        </div>
                                        <?php
                                        if (!is_array($model->width_fittings)) {
                                            $width_fit = explode(',', $model->width_fittings);
                                        } else {
                                            $width_fit = $model->width_fittings;
                                        }


                                        $product_width_fit = array();

                                        foreach ($width_fit as $value) {
                                            $product_width_fit[$value] = array('selected' => 'selected');
                                        }
                                        ?>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'width_fittings', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo CHtml::activeDropDownList($model, 'width_fittings', CHtml::listData(WidthFitting::model()->findAll(), 'id', 'title'), array('class' => 'form-control', 'multiple' => true, 'options' => $product_width_fit)); ?>
                                                </div>
                                                <?php echo $form->error($model, 'width_fittings'); ?>
                                        </div>
                                      

                                       <!-- <div class="form-group">
                                                <?php echo$form->labelEx($model, 'featured', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->dropDownList($model, 'featured', array('1' => "Yes", '0' => "No"), array('class' => 'form-control')); ?></div>
                                                <?php echo $form->error($model, 'featured'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo$form->labelEx($model, 'new', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->dropDownList($model, 'new', array('1' => "Yes", '0' => "No"), array('class' => 'form-control')); ?></div>
                                                <?php echo $form->error($model, 'new'); ?>
                                        </div>-->

                                        <?php
                                        if (!is_array($model->related_products)) {
                                            $related = explode(',', $model->related_products);
                                        } else {
                                            $related = $model->related_products;
                                        }


                                        $product_related = array();

                                        foreach ($related as $value2) {
                                            $product_related[$value2] = array('selected' => 'selected');
                                        }
                                        ?>
                                        <!--<div class="form-group">-->
                                        <?php //echo $form->labelEx($model, 'related_products', array('class' => 'col-sm-2 control-label')); ?>
                                        <!--                                                <div class="col-sm-10"><?php echo CHtml::activeDropDownList($model, 'related_products', CHtml::listData(Products::model()->findAll(['limit' => 100]), 'id', 'product_name'), array('class' => 'form-control', 'multiple' => true, 'options' => $product_related)); ?>
                                                                                        </div>-->
                                        <?php //echo $form->error($model, 'toy_style'); ?>
                                        <!--</div>-->

                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages">
                                        <br/>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'quantity', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->textField($model, 'quantity', array('class' => 'form-control')); ?>
                                                </div>
                                                <?php echo $form->error($model, 'quantity'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo$form->labelEx($model, 'stock_availability', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->dropDownList($model, 'stock_availability', array('1' => "Yes", '0' => "No"), array('class' => 'form-control')); ?></div>
                                                <?php echo $form->error($model, 'stock_availability'); ?>
                                        </div>

                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'price', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->textField($model, 'price', array('class' => 'form-control')); ?>
                                                </div>
                                                <?php echo $form->error($model, 'price'); ?>
                                        </div>

                                        <div class="form-group disty">
                                                <div class="col-sm-2 control-label">
                                                        <?php echo $form->labelEx($model, 'discount_type'); ?>
                                                </div>
                                                <div class="col-sm-10">
                                                        <?php echo $form->dropDownList($model, 'discount_type', array('0' => "Select Type", '1' => "Fixed", '2' => "Percentage"), array('class' => 'form-control')); ?>
                                                        <?php echo $form->error($model, 'discount_type'); ?>
                                                </div>
                                        </div>
                                        <div class="form-group dis">
                                                <div class="col-sm-2 control-label">
                                                        <?php echo $form->labelEx($model, 'discount_rate'); ?>
                                                </div>
                                                <div class="col-sm-10">
                                                        <?php echo $form->textField($model, 'discount_rate', array('class' => 'form-control')); ?>
                                                        <?php echo $form->error($model, 'discount_rate'); ?>
                                                </div>
                                        </div>
                                        <div class="form-group">

                                                <?php echo $form->labelEx($model, 'meta_title', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->textField($model, 'meta_title', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control')); ?>
                                                </div>
                                                <?php echo $form->error($model, 'meta_title'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'meta_description', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->textArea($model, 'meta_description', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control')); ?>
                                                </div>
                                                <?php echo $form->error($model, 'meta_description'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php echo $form->labelEx($model, 'meta_keywords', array('class' => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-10"><?php echo $form->textField($model, 'meta_keywords', array('size' => 60, 'maxlength' => 225, 'class' => 'form-control')); ?>
                                                </div>
                                                <?php echo $form->error($model, 'meta_keywords'); ?>
                                        </div></div>
                        </div>

                </div>



                <div class="box-footer">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-laksyah pos')); ?>
                </div>
                <?php $this->endWidget(); ?>

        </div>
</div>
<script>
    $(document).ready(function () {
            $('.slug').keyup(function () {
                    $('#Products_canonical_name').val(slug($(this).val()));
            });
    });
    var slug = function (str) {
            var $slug = '';
            var trimmed = $.trim(str);
            $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
                    replace(/-+/g, '-').
                    replace(/^-|-$/g, '');
            return $slug.toLowerCase();
    };
</script>

<script type="text/javascript">
    $(document).ready(function () {
            $('#Products_toy_style').multiselect({
                    selectAllText: ' Select all',
                    includeSelectAllOption: true
            });
            $('#Products_width_fittings').multiselect({
                    selectAllText: ' Select all',
                    includeSelectAllOption: true
            });
            $('#Products_related_products').multiselect({
                    selectAllText: ' Select all',
                    includeSelectAllOption: true
            });
    });</script>