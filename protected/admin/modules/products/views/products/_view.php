<div class="view">
        <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
        <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
        <?php echo CHtml::encode($data->category_id); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('product_name')); ?>:</b>
        <?php echo CHtml::encode($data->product_name); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('product_code')); ?>:</b>
        <?php echo CHtml::encode($data->product_code); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('main_image')); ?>:</b>
        <?php echo CHtml::encode($data->main_image); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('gallery_images')); ?>:</b>
        <?php echo CHtml::encode($data->gallery_images); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
        <?php echo CHtml::encode($data->description); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('meta_title')); ?>:</b>
        <?php echo CHtml::encode($data->meta_title); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('meta_description')); ?>:</b>
        <?php echo CHtml::encode($data->meta_description); ?>
        <br />
</div>