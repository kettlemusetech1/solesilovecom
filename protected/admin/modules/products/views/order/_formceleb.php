<style>
        .multiselect-native-select{
                display :block;
                width: 100%;
        }
        .multiselect-native-select .btn-group{
                width: 100%;
                display: block;
        }
        button.multiselect.dropdown-toggle.btn.btn-default {
                width: 100%;
                text-align: left;
                background: transparent;
                border-radius: 0px;
        }
        .btn-group {
                margin: 0px;
        }
        .open>.dropdown-menu {
                display: block;
                height: 500px;
                overflow-y: scroll;
        }
        .detals h4{
                font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
                font-weight: bold;
                font-size: 14px;
                text-align: right;
        }
        tr {
                border-bottom: 1px solid #ccc;
        }
        thead {
                background-color: #ccc;
        }
        table {
                width: 100%;
                margin-top: 20px;
                border: 1px solid #ccc;
                margin-bottom: 30px;
        }
        .form-control {
                margin-bottom: 15px;
        }
        .billing-det h3 {
                font-size: 21px;
                font-weight: bold;
                margin-bottom: 6px;
                text-decoration: underline;
        }
        .address-head {
                height: 90px;
        }
        @media (min-width: 768px){
                .form-horizontal .control-label {
                        text-align: left;
                        padding-left: 43px;
                }
        }
        a.calculate_shipping {
                color: blue;
                font-size: 15px;
                text-decoration: underline;
                /* margin-bottom: 19px; */
        }
        .not_ext{
                display: none;

        }
        .ship_not_ext{
                display: none;

        }

</style>
<form>
        <div id="addproducts" class="modal fade" role="dialog">
                <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                                <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Celeb Product</h4>
                                </div>
                                <!--<form  method="POST" if="product_form">-->
                                <div class="modal-body">

                                        <div class="p_color">

                                        </div>
                                        <div class="p_size">

                                        </div>
                                        <div class="form-group">
                                                <label class="col-sm-2 control-label" for="Order_total_amount">Quantity</label>
                                                <div class="col-sm-10">
                                                        <select class="form-control" required name="quantity" id="quantity">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                        </select>
                                                </div>

                                        </div>
                                </div>
                                <div class="modal-footer">
                                        <button type="button" class="btn btn-default product_sub_btn" >Submit Product</button>
                                </div>
                                <!--</form>-->
                        </div>

                </div>
        </div>
</form>
<div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'order-form',
            'htmlOptions' => array('class' => 'form-horizontal'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>
        <div class="form-group">
                <div class="row">
                        <div class="col-md-2">
                                <?php echo $form->labelEx($model, 'Customer', array('class' => 'control-label')); ?>
                        </div>

                        <div class="col-sm-4">

                                <?php
                                $models = UserDetails::model()->findAllByAttributes(array('email_verification' => 1));
                                $data = array();
                                // $data[0] = 'New User';
                                foreach ($models as $model1) {
                                        $data[$model1->id] = $model1->first_name . ' ' . $model1->last_name . '  (USER ID : #' . $model1->id . ' - ' . $model1->email . ' )';
                                }

                                echo $form->dropDownList($model, 'user_id', $data, array('empty' => '--Select User--', 'class' => 'form-control', 'required' => 'required', 'options' => array('id' => array('selected' => 'selected'))));
                                ?>
                                <?php echo $form->error($model, 'user_id'); ?></div>
                </div>
        </div>

        <div class="olduser" style="display:none">
                <div class="detals">
                        <div class="row">
                                <div class="col-xs-2">
                                        <h4>Name: </h4>
                                </div>
                                <div class="col-xs-10">
                                        <span id="fname"></span> <span id="lname"></span>
                                </div>
                        </div>
                        <!--                        <div class="row">
                                                        <div class="col-xs-2">
                                                                <h4>Last Name: </h4>
                                                        </div>
                                                        <div class="col-xs-10">
                                                                <span id="lname"></span>
                                                        </div>
                                                </div>-->
                        <div class="row">
                                <div class="col-xs-2">
                                        <h4>Country: </h4>
                                </div>
                                <div class="col-xs-10">
                                        <span id="country"></span>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-xs-2">
                                        <h4>Email:</h4>
                                </div>
                                <div class="col-xs-10">
                                        <span id="email"></span>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-xs-2">
                                        <h4>Mobile Number: </h4>
                                </div>
                                <div class="col-xs-10">
                                        <span id="mobile"></span>
                                </div>
                        </div>
                </div>

        </div>
        <div class="newuser" style="display:none">


                <div class="form-group">
                        <div class="row">
                                <?php // echo $form->labelEx($user, 'title', array('class' => 'control-label col-sm-1')); ?>
                                <div class="col-sm-2">
                                        <?php echo $form->labelEx($user, 'first_name', array('class' => 'control-label ')); ?>
                                </div>

                                <div class="col-sm-1">
                                        <?php echo $form->dropDownList($user, 'title', array('Ms' => "Ms", 'Mrs' => "Mrs", 'Mr' => "Mr"), array('class' => 'form-control')); ?>


                                </div>
                                <div class="col-sm-3">
                                        <?php echo $form->textField($user, 'first_name', array('class' => 'form-control')); ?>
                                        <?php echo $form->error($user, 'first_name'); ?>
                                </div>
                                <div class="col-sm-2">
                                        <?php echo $form->labelEx($user, 'last_name', array('class' => '')); ?>
                                </div>

                                <div class="col-sm-4">
                                        <?php echo $form->textField($user, 'last_name', array('class' => 'form-control')); ?>
                                        <?php echo $form->error($user, 'last_name'); ?>
                                </div>
                        </div>

                </div>
                <div class="form-group">
                        <div class="row">
                                <div class="col-sm-2">
                                        <?php echo $form->labelEx($user, 'country', array('class' => 'control-label ')); ?>
                                </div>
                                <div class="col-sm-4">
                                        <?php // $country = Countries::model()->findAll(); ?>
                                        <?php echo CHtml::activeDropDownList($user, 'country', CHtml::listData(Countries::model()->findAll(), 'id', 'country_name'), array('empty' => '--Select Country--', 'class' => 'form-control ', 'options' => array(99 => array('selected' => 'selected')))); ?>
                                        <?php echo $form->error($user, 'country'); ?>
                                </div>
                                <div class="col-sm-2">
                                        <?php echo $form->labelEx($user, 'email', array('class' => '')); ?>
                                </div>
                                <div class="col-sm-4">
                                        <?php echo $form->textField($user, 'email', array('class' => 'form-control')); ?>
                                        <?php echo $form->error($user, 'email'); ?>
                                </div>
                        </div>
                </div>
                <div class="form-group">
                        <div class="row">
                                <div class="col-sm-2">
                                        <label class="control-label" for="Order_total_amount">Country Code</label>
                                </div>
                                <div class="col-sm-4">
                                        <?php $countrycode = Countries::model()->findAll(); ?>
                        <!--                        <select required="" class="form-control" required="required" name="countrycode" id="Order_user_id">
                                                <option value="">--Select A Country--</option>
                                        <?php foreach ($countrycode as $co) { ?>
                                                                                                                                                                                                                <option value="<?php echo $co->id; ?>"><?php echo $co->country_name; ?>(+<?php echo $co->phonecode; ?>)</option>
                                        <?php } ?>
                                        </select>-->
                                        <?php
                                        $phonecode = Countries::model()->findByPk(99)->phonecode;
                                        $state_options = array();

                                        $states = Countries::model()->findAll();
                                        if (!empty($states)) {
                                                $state_options[""] = "--Select--";
                                                foreach ($states as $state) {
                                                        $state_options[$state->phonecode] = '+' . $state->phonecode;
                                                }
                                        } else {
                                                $state_options[""] = "--Code--";
                                                $state_options[0] = "Other";
                                        }
                                        ?>
                                        <?php echo CHtml::activeDropDownList($user, 'phonecode', $state_options, array('class' => 'form-control aik', 'options' => array($phonecode => array('selected' => 'selected')))); ?>
                                        <?php echo $form->error($user, 'phonecode'); ?>
                                </div>


                                <div class="col-sm-2">
                                        <?php echo $form->labelEx($user, 'phone_no_2', array('class' => '')); ?>
                                </div>
                                <div class="col-sm-4">
                                        <?php echo $form->textField($user, 'phone_no_2', array('class' => 'form-control')); ?>
                                        <?php echo $form->error($user, 'phone_no_2'); ?>
                                </div>
                        </div>
                </div>
        </div>
        <div class="form-group">
                <div class="row">
                        <div class="col-md-2">
                                <?php echo $form->labelEx($model, 'Product', array('class' => 'control-label')); ?>
                        </div>

                        <div class="col-sm-4">

                                <?php $products = Products::model()->findAll(array('condition' => 'stock_availability = 1 AND status = 1 and enquiry_sale = 0')); ?>
                                <select required class="form-control" required="required" name="productid" id="productid">
                                        <option value="">--Select Product--</option>
                                        <?php foreach ($products as $pdts) { ?>
                                                <option value="<?php echo $pdts->id; ?>"><?php echo $pdts->product_name; ?>(<?php echo $pdts->product_code; ?>)</option>
                                        <?php } ?>
                                </select>


                                <?php //echo $form->error($model, 'user_id'); ?></div>
                </div>
        </div>
        <div class="row">
                <div class="col-xs-6">
                        <div class="p_color">

                        </div>
                </div>
        </div>

        <div class="col-xs-12 col-sm-6">
                <div class="row">
                        <div class="billing-det" style="">
                                <div class="col-xs-12 address-head"><h3>Billing Details</h3></div>
                                <div class="form-group">
                                        <?php // echo $form->labelEx($user, 'title', array('class' => 'control-label col-sm-1')); ?>
                                        <?php echo $form->labelEx($billing, '[bill]first_name', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($billing, '[bill]first_name', array('class' => 'form-control', 'required' => true)); ?>
                                                <?php echo $form->error($billing, '[bill]first_name'); ?>
                                        </div>
                                        <?php echo $form->labelEx($billing, '[bill]last_name', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($billing, '[bill]last_name', array('class' => 'form-control', 'required' => true)); ?>
                                                <?php echo $form->error($billing, '[bill]last_name'); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo $form->labelEx($billing, '[bill]address_1', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($billing, '[bill]address_1', array('class' => 'form-control', 'required' => true)); ?>
                                                <?php echo $form->error($billing, '[bill]address_1'); ?>
                                        </div>
                                        <?php echo $form->labelEx($billing, '[bill]address_2', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($billing, '[bill]address_2', array('class' => 'form-control')); ?>
                                                <?php echo $form->error($billing, '[bill]address_2'); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo $form->labelEx($billing, '[bill]country', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php // $country = Countries::model()->findAll(); ?>
                                                <?php echo CHtml::activeDropDownList($billing, '[bill]country', CHtml::listData(Countries::model()->findAll(), 'id', 'country_name'), array('empty' => '--Select Country--', 'class' => 'form-control ', 'required' => true)); ?>
                                                <?php echo $form->error($billing, '[bill]country'); ?>
                                        </div>
                                        <?php echo $form->labelEx($billing, '[bill]state', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <div class="cn_ext">
                                                        <?php
                                                        //  echo Yii::app()->session['user']['country'];
                                                        $state_options = array();
                                                        if (Yii::app()->session['user']['country'] != 0) {
                                                                $states = States::model()->findAllByAttributes(array('country_id' => Yii::app()->session['user']['country']));
                                                                if (!empty($states)) {
                                                                        $state_options[""] = "--Select--";
                                                                        foreach ($states as $state) {
                                                                                $state_options[$state->state_name] = $state->state_name;
                                                                        }
                                                                } else {
                                                                        $state_options[""] = "--Select--";
                                                                        $state_options[0] = "Other";
                                                                }
                                                        } else {
                                                                $state_options[""] = '--select--';
                                                        }
                                                        ?>
                                                        <?php echo CHtml::activeDropDownList($billing, '[bill]state', $state_options, array('class' => 'form-control aik', 'required' => true, 'options' => array('id' => array('selected' => 'selected')))); ?>
                                                </div>
                                                <div class="not_ext">
                                                        <input placeholder="state " class="form-control aik" name="UserAddress[bill][state1]" id="UserAddress_bill_state1"  type="text" maxlength="111">

                                                </div>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo $form->labelEx($billing, '[bill]city', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($billing, '[bill]city', array('class' => 'form-control', 'required' => true)); ?>
                                                <?php echo $form->error($billing, '[bill]city'); ?>
                                        </div>
                                        <?php echo $form->labelEx($billing, '[bill]postcode', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($billing, '[bill]postcode', array('class' => 'form-control', 'required' => true)); ?>
                                                <?php echo $form->error($billing, '[bill]postcode'); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="col-sm-4 control-label" for="Order_total_amount">Country Code</label>
                                        <div class="col-sm-8">
                                                <?php $countrycode = Countries::model()->findAll(); ?>
                                <!--                        <select required="" class="form-control" required="required" name="countrycode" id="Order_user_id">
                                                        <option value="">--Select A Country--</option>
                                                <?php foreach ($countrycode as $co) { ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <option value="<?php echo $co->id; ?>"><?php echo $co->country_name; ?>(+<?php echo $co->phonecode; ?>)</option>
                                                <?php } ?>
                                                </select>-->
                                                <?php
                                                $phonecode = Countries::model()->findByPk(99)->phonecode;
                                                $state_options = array();

                                                $states = Countries::model()->findAll();
                                                if (!empty($states)) {
                                                        $state_options[""] = "--Select--";
                                                        foreach ($states as $state) {
                                                                $state_options[$state->phonecode] = '+' . $state->phonecode;
                                                        }
                                                } else {
                                                        $state_options[""] = "--Code--";
                                                        $state_options[0] = "Other";
                                                }
                                                ?>
                                                <?php echo CHtml::activeDropDownList($billing, '[bill]phonecode', $state_options, array('class' => 'form-control aik', 'required' => true, 'options' => array($phonecode => array('selected' => 'selected')))); ?>
                                                <?php echo $form->error($billing, '[bill]phonecode'); ?>
                                        </div>


                                        <?php echo $form->labelEx($billing, '[bill]contact_number', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($billing, '[bill]contact_number', array('class' => 'form-control', 'required' => true)); ?>
                                                <?php echo $form->error($billing, '[bill]contact_number'); ?>
                                        </div>

                                </div>
                        </div>

                </div>
        </div>
        <div class="col-xs-12 col-sm-6">
                <div class="row">
                        <div class="billing-det" style="">
                                <div class="col-xs-12 address-head"><h3>Shipping Details</h3>
                                        <label class=""><input type="checkbox" name="billing_same" class="check_same" value="1"> Same as Billing Address</label>
                                </div>
                                <div class="form-group">
                                        <?php // echo $form->labelEx($user, 'title', array('class' => 'control-label col-sm-1')); ?>
                                        <?php echo $form->labelEx($shipping, '[ship]first_name', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($shipping, '[ship]first_name', array('class' => 'form-control')); ?>
                                                <?php echo $form->error($shipping, '[ship]first_name'); ?>
                                        </div>
                                        <?php echo $form->labelEx($shipping, '[ship]last_name', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($shipping, '[ship]last_name', array('class' => 'form-control')); ?>
                                                <?php echo $form->error($shipping, '[ship]last_name'); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo $form->labelEx($shipping, '[ship]address_1', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($shipping, '[ship]address_1', array('class' => 'form-control')); ?>
                                                <?php echo $form->error($shipping, '[ship]address_1'); ?>
                                        </div>
                                        <?php echo $form->labelEx($shipping, '[ship]address_2', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($shipping, '[ship]address_2', array('class' => 'form-control')); ?>
                                                <?php echo $form->error($shipping, '[ship]address_2'); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo $form->labelEx($shipping, '[ship]country', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php // $country = Countries::model()->findAll(); ?>
                                                <?php echo CHtml::activeDropDownList($shipping, '[ship]country', CHtml::listData(Countries::model()->findAll(), 'id', 'country_name'), array('empty' => '--Select Country--', 'class' => 'form-control ')); ?>
                                                <?php echo $form->error($shipping, '[ship]country'); ?>
                                        </div>
                                        <?php echo $form->labelEx($shipping, '[ship]state', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <div class="ship_cn_ext">
                                                        <?php
                                                        //  echo Yii::app()->session['user']['country'];
                                                        $state_options = array();
                                                        if (Yii::app()->session['user']['country'] != 0) {
                                                                $states = States::model()->findAllByAttributes(array('status' => 1));
                                                                if (!empty($states)) {
                                                                        $state_options[""] = "--Select--";
                                                                        foreach ($states as $state) {
                                                                                $state_options[$state->state_name] = $state->state_name;
                                                                        }
                                                                } else {
                                                                        $state_options[""] = "--Select--";
                                                                        $state_options[0] = "Other";
                                                                }
                                                        } else {
                                                                $state_options[""] = '--select--';
                                                                $states = States::model()->findAllByAttributes(array('status' => 1));
                                                                if (!empty($states)) {
                                                                        $state_options[""] = "--Select--";
                                                                        foreach ($states as $state) {
                                                                                $state_options[$state->state_name] = $state->state_name;
                                                                        }
                                                                } else {
                                                                        $state_options[""] = "--Select--";
                                                                        $state_options[0] = "Other";
                                                                }
                                                        }
                                                        ?>
                                                        <?php echo CHtml::activeDropDownList($shipping, '[ship]state', $state_options, array('class' => 'form-control aik', 'options' => array('id' => array('selected' => 'selected')))); ?>
                                                </div>
                                                <div class="ship_not_ext">
                                                        <input placeholder="state " class="form-control aik" name="UserAddress[ship][state1]" id="UserAddress_ship_state1" type="text" maxlength="111">

                                                </div>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <?php echo $form->labelEx($shipping, '[ship]city', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($shipping, '[ship]city', array('class' => 'form-control')); ?>
                                                <?php echo $form->error($shipping, '[ship]city'); ?>
                                        </div>
                                        <?php echo $form->labelEx($shipping, '[ship]postcode', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($billing, '[ship]postcode', array('class' => 'form-control')); ?>
                                                <?php echo $form->error($shipping, '[ship]postcode'); ?>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label class="col-sm-4 control-label" for="Order_total_amount">Country Code</label>
                                        <div class="col-sm-8">
                                                <?php $countrycode = Countries::model()->findAll(); ?>
                                <!--                        <select required="" class="form-control" required="required" name="countrycode" id="Order_user_id">
                                                        <option value="">--Select A Country--</option>
                                                <?php foreach ($countrycode as $co) { ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <option value="<?php echo $co->id; ?>"><?php echo $co->country_name; ?>(+<?php echo $co->phonecode; ?>)</option>
                                                <?php } ?>
                                                </select>-->
                                                <?php
                                                $phonecode = Countries::model()->findByPk(99)->phonecode;
                                                $state_options = array();

                                                $states = Countries::model()->findAll();
                                                if (!empty($states)) {
                                                        $state_options[""] = "--Select--";
                                                        foreach ($states as $state) {
                                                                $state_options[$state->phonecode] = '+' . $state->phonecode;
                                                        }
                                                } else {
                                                        $state_options[""] = "--Code--";
                                                        $state_options[0] = "Other";
                                                }
                                                ?>
                                                <?php echo CHtml::activeDropDownList($shipping, '[ship]phonecode', $state_options, array('class' => 'form-control aik', 'options' => array($phonecode => array('selected' => 'selected')))); ?>
                                                <?php echo $form->error($shipping, '[ship]phonecode'); ?>
                                        </div>


                                        <?php echo $form->labelEx($shipping, '[ship]contact_number', array('class' => 'control-label col-sm-4')); ?>
                                        <div class="col-sm-8">
                                                <?php echo $form->textField($shipping, '[ship]contact_number', array('class' => 'form-control')); ?>
                                                <?php echo $form->error($shipping, '[ship]contact_number'); ?>
                                        </div>

                                </div>
                        </div>

                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Shipping Charge', array('class' => 'col-sm-2 control-label')); ?>
                <div class="col-sm-10">
                        <h4 class="shipping_charge"></h4><h5><a href="javascript:void(0)" class="calculate_shipping">Click here calculate shipping</a></h5>
                        <?php echo $form->textField($model, 'shipping_charge', array('class' => 'form-control')); ?>

                </div>

        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Total Amount', array('class' => 'col-sm-2 control-label')); ?>
                <div class="col-sm-10"><h4 class="total_amount">&nbsp;&nbsp;&nbsp;</h4>
                </div>

        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Amount Paid', array('class' => 'col-sm-2 control-label')); ?>
                <div class="col-sm-10"><?php echo $form->textField($model, 'total_amount', array('class' => 'form-control')); ?>
                </div>
                <?php echo $form->error($model, 'total_amount'); ?>
        </div>
        <!--        <div class="form-group">
        <?php //echo $form->labelEx($model, 'order_date', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'order_date', array('class' => 'form-control')); ?>
                        </div>
        <?php //echo $form->error($model, 'order_date'); ?>
                </div>-->
        <!--        <div class="form-group">
        <?php //echo $form->labelEx($model, 'ship_address_id', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'ship_address_id', array('class' => 'form-control')); ?>
                        </div>
        <?php //echo $form->error($model, 'ship_address_id'); ?>
                </div>
                <div class="form-group">
        <?php //echo $form->labelEx($model, 'bill_address_id', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'bill_address_id', array('class' => 'form-control')); ?>
                        </div>
        <?php //echo $form->error($model, 'bill_address_id'); ?>
                </div>-->
        <div class="form-group">
                <?php echo $form->labelEx($model, 'Order Comments', array('class' => 'col-sm-2 control-label')); ?>
                <div class="col-sm-10"><?php echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
                </div>
                <?php echo $form->error($model, 'comment'); ?>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'payment_mode', array('class' => 'col-sm-2 control-label')); ?>
                <div class="col-sm-10"><?php echo $form->dropDownList($model, 'payment_mode', array('1' => "Wallet", '2' => "Payment Gateway", '3' => "Paypal", '4' => "Wallet + Netbanking", '5' => "NEFT Direct Transaction"), array('class' => 'form-control')); ?>
                </div>
                <?php echo $form->error($model, 'payment_mode'); ?>
        </div>
        <!--    <div class="form-group">
        <?php //echo $form->labelEx($model, 'admin_comment', array('class' => 'col-sm-2 control-label')); ?>
                <div class="col-sm-10"><?php //echo $form->textArea($model, 'admin_comment', array('rows' => 6, 'cols' => 50, 'class' => 'form-control'));                ?>
                </div>
        <?php //echo $form->error($model, 'admin_comment'); ?>
            </div>-->
        <div class="form-group">
                <?php echo $form->labelEx($model, 'transaction_id', array('class' => 'col-sm-2 control-label')); ?>
                <div class="col-sm-10"><?php echo $form->textField($model, 'transaction_id', array('class' => 'form-control')); ?>
                </div>
                <?php echo $form->error($model, 'transaction_id'); ?>
        </div>

        <!-- <div class="form-group">
        <?php echo $form->labelEx($model, 'payment_status', array('class' => 'col-sm-2 control-label')); ?>
                 <div class="col-sm-10"><?php echo $form->dropDownList($model, 'payment_status', array('1' => "Success", '2' => "Failed"), array('class' => 'form-control')); ?>
                 </div>
        <?php echo $form->error($model, 'payment_status'); ?>
         </div>-->
        <!--    <div class="form-group">
        <?php //echo $form->labelEx($model, 'admin_status', array('class' => 'col-sm-2 control-label')); ?>
                <div class="col-sm-10"><?php //echo CHtml::activeDropDownList($model, 'admin_status', CHtml::listData(OrderStatus::model()->findAll(), 'id', 'title'), array('empty' => '--Select--', 'class' => 'form-control'));                  ?>
                </div>
        <?php //echo $form->error($model, 'admin_status'); ?>
            </div>-->

        <!-- <div class="form-group">
        <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-2 control-label')); ?>
                 <div class="col-sm-10"><?php echo $form->dropDownList($model, 'status', array('1' => "Enabled", '0' => "Disabled"), array('class' => 'form-control')); ?>
                 </div>
        <?php echo $form->error($model, 'status'); ?>
         </div>-->



        <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-success ')); ?>
        </div>

        <?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
        $(document).ready(function() {


                $('#Order_user_id').multiselect({
                        selectAllText: ' Select all',
                        includeSelectAllOption: true,
                        enableFiltering: true,
                        filterPlaceholder: 'Search',
                        enableCaseInsensitiveFiltering: true,
                });
                $('#UserDetails_country').multiselect({
                        selectAllText: ' Select all',
                        includeSelectAllOption: true,
                        enableFiltering: true,
                        filterPlaceholder: 'Search',
                        enableCaseInsensitiveFiltering: true,
                });
                $('#productid').multiselect({
                        selectAllText: ' Select all',
                        includeSelectAllOption: true,
                        enableFiltering: true,
                        filterPlaceholder: 'Search',
                        enableCaseInsensitiveFiltering: true,
                });
                $('#UserDetails_country').multiselect({
                        selectAllText: ' Select all',
                        includeSelectAllOption: true,
                        enableFiltering: true,
                        filterPlaceholder: 'Search',
                        enableCaseInsensitiveFiltering: true,
                });
//                $(".delete_product").click(function () {
                $(".getproduct").on("click", ".pdata>table>tbody>tr>td>.delete_product", function() {
                        var value = $(this).attr("cid");
                        $.ajax({
                                type: "POST",
                                url: baseurl + "ajax/UpdateProductDataCeleb",
                                data: {value: value}
                        }).done(function(data) {

                                $('.getproduct').html(data);
                                //  $("#addproducts").modal('hide');
                        });
                });

                $(".getproduct").on("click", ".pdata>table>tbody>tr>td>.create_enquiry", function() {
                        var value = $(this).attr("cid");
                        $.ajax({
                                type: "POST",
                                url: baseurl + "ajax/CreateProductDataCeleb",
                                data: {value: value}
                        }).done(function(data) {

                                $('.getproduct').html(data);
                                //  $("#addproducts").modal('hide');
                        });
                });


                $(".product_sub_btn").click(function() {
                        var value = $("#Order_user_id").val();
                        if (value == "") {

                        } else {

                                var cux_name = $("#Order_user_id").val();
                                var fname = $("#UserDetails_first_name").val();
                                var lname = $("#UserDetails_last_name").val();
                                var country = $("#UserDetails_country").val();
                                var email = $("#UserDetails_email").val();
                                var mobile = $("#UserDetails_phone_no_2").val();
                                var phonecode = $("#UserDetails_phonecode").val();
                                var title = $("#UserDetails_title").val();
                                var product = $("#productid").val();
                                var color = $("#product_color").val();
                                var size = $("#product_size").val();
                                var quantity = $("#quantity").val();
                                $.ajax({
                                        type: "POST",
                                        url: baseurl + "ajax/PutProductDataCeleb",
                                        data: {cux_name: cux_name, phonecode: phonecode, title: title, fname: fname, lname: lname, country: country, email: email, mobile: mobile, product: product, color: color, size: size, quantity: quantity}}
                                ).done(function(data) {
                                        var obj = jQuery.parseJSON(data);
                                        $('.getproduct').html(obj.cartdata);
//                                        $('#Order_user_id').html(obj.userdata);
                                        $("#addproducts").modal('hide');
                                        $(".newuser").hide();
//
                                        //                                        if (data == 0) {
//
//                                                $('#UserAddress_bill_state').attr('required', false);
//                                                $('#UserAddress_bill_state1').attr('required', true);
//                                                $('.cn_ext').hide();
//                                                $('.not_ext').show();
//                                        } else {
//                                                $('#UserAddress_bill_state').attr('required', true);
//                                                $('#UserAddress_bill_state1').attr('required', false);
//                                                $('.not_ext').hide();
//                                                $('.cn_ext').show();
//
//                                                $('#UserAddress_bill_state').html(data);
//                                        }
                                });
                        }
                });
                $(".manage").click(function() {
                        var value = $("#Order_user_id").val();
                        if (value == "") {
                                alert("Please Select a Customer");
                                return false;
                        } else {
                                if (value == 0) {
                                        var fname = $("#UserDetails_first_name").val();
                                        var lname = $("#UserDetails_last_name").val();
                                        var country = $("#UserDetails_country").val();
                                        var email = $("#UserDetails_email").val();
                                        var mobile = $("#UserDetails_phone_no_2").val();
                                        if (fname == "") {
                                                alert("First Name Must be filled out");
                                                return false;
                                        } else if (lname == "") {
                                                alert("Last Name Must be filled out");
                                                return false;
                                        } else if (country == "") {
                                                alert("Country  Must be filled out");
                                                return false;
                                        } else if (email == "") {
                                                alert("Email Must be filled out");
                                                return false;
                                        } else if (mobile == "") {
                                                alert("Mobile Number Must be filled out");
                                                return false;
                                        }
                                } else {
                                        //                                        $(".p_color").html(" ");
//                                        $(".p_size").html(" ");
//                                        $('#product_form')[0].reset();
//                                        $("#productid").val(" ");
                                        return true;
                                }
                        }
                });
                $('#UserAddress_bill_country').change(function() {
                        var country = $(this).val();
                        if (country != '') {

                                $.ajax({
                                        type: "POST",
                                        url: baseurl + "ajax/selectState",
                                        data: {country: country}
                                }).done(function(data) {
                                        if (data == 0) {

                                                $('#UserAddress_bill_state').attr('required', false);
                                                $('#UserAddress_bill_state1').attr('required', true);
                                                $("#UserAddress_bill_state").val(" ");
                                                $('.cn_ext').hide();
                                                $('.not_ext').show();
                                        } else {
                                                $('#UserAddress_bill_state').attr('required', true);
                                                $('#UserAddress_bill_state1').attr('required', false);
                                                $("#UserAddress_bill_state1").val(" ");
                                                $('.not_ext').hide();
                                                $('.cn_ext').show();
                                                $('#UserAddress_bill_state').html(data);
                                        }
                                });
                        } else {


                                $('#UserAddress_bill_state').html("<option value=''>--Select--</option>");
                        }
                });
                $('#UserAddress_country').change(function() {
                        var country = $(this).val();
                        if (country != '') {
                                $.ajax({
                                        type: "POST",
                                        url: baseurl + "ajax/selectState",
                                        data: {country: country}
                                }).done(function(data) {
                                        if (data == 0) {

                                                $('#UserAddress_state').attr('required', false);
                                                $('#UserAddress_bill_state').attr('required', true);
                                                $('.cn_ext').hide();
                                                $('.not_ext').show();
                                        } else {

                                                $('#UserAddress_state').attr('required', true);
                                                $('#UserAddress_bill_state').attr('required', false);
                                                $('.not_ext').hide();
                                                $('.cn_ext').show();
                                                $('#UserAddress_state').html(data);
                                        }
                                });
                        } else {
                                $('#UserAddress_state').html("<option value=''>--Select--</option>");
                        }
                });
                $('#UserAddress_ship_country').change(function() {
                        var country = $(this).val();
                        if (country != '') {
                                $.ajax({
                                        type: "POST",
                                        url: baseurl + "ajax/selectState",
                                        data: {country: country}
                                }).done(function(data) {
                                        if (data == 0) {
                                                $('.ship_cn_ext').hide();
                                                $('.ship_not_ext').show();
                                        } else {


                                                $('.ship_not_ext').hide();
                                                $('.ship_cn_ext').show();
                                                $('#UserAddress_ship_state').html(data);
                                        }
                                });
                        } else {
                                $('#UserAddress_ship_state').html("<option value=''>--Select--</option>");
                        }
                });
                $('#UserDetails_country').change(function() {
                        var country = $(this).val();
                        if (country != '') {
                                $.ajax({
                                        type: "POST",
                                        url: baseurl + "ajax/SelectPhonecode",
                                        data: {country: country}
                                }).done(function(data) {
                                        $('#UserDetails_phonecode').html(data);
                                });
                        } else {
                                $('#UserDetails_phonecode').html("<option value=''>--Code--</option>");
                        }
                });
                $('#UserAddress_bill_country').change(function() {
                        var country = $(this).val();
                        if (country != '') {
                                $.ajax({
                                        type: "POST",
                                        url: baseurl + "ajax/SelectPhonecode",
                                        data: {country: country}
                                }).done(function(data) {
                                        $('#UserAddress_bill_phonecode').html(data);
                                });
                        } else {
                                $('#UserAddress_bill_phonecode').html("<option value=''>--Code--</option>");
                        }
                });
                $('#UserAddress_ship_country').change(function() {
                        var country = $(this).val();
                        if (country != '') {
                                $.ajax({
                                        type: "POST",
                                        url: baseurl + "ajax/SelectPhonecode",
                                        data: {country: country}
                                }).done(function(data) {
                                        $('#UserAddress_ship_phonecode').html(data);
                                });
                        } else {
                                $('#UserAddress_ship_phonecode').html("<option value=''>--Code--</option>");
                        }
                });
                $('#productid').change(function() {
                        $('.p_color').html("");
                        $('.p_size').html("");
                        var val = $(this).val();
                        if (val != '') {
                                $.ajax({
                                        type: "POST",
                                        url: baseurl + "ajax/GetProductColor",
                                        data: {val: val}
                                }).done(function(data) {
                                        $('.p_color').html(data);
                                });
                        } else {
                                $('.p_color').html("<p>No Color Option Found");
                        }
                });
                //                $('#product_color').change(function () {
                $("#addproducts").on("change", ".p_color>.form-group>.col-sm-10>#product_color", function() {

                        var color = $(this).val();
                        var product = $('#productid').val();
                        if (color != '') {
                                $.ajax({
                                        type: "POST",
                                        url: baseurl + "ajax/GetProductSize",
                                        data: {product: product, color: color}
                                }).done(function(data) {
                                        $('.p_size').html(data);
                                });
                        } else {
                                $('.p_size').html("");
                        }
                });
                $('#Order_user_id').change(function() {
                        var value = $(this).val();
                        if (value != '') {
                                if (value == 0) {
                                        $(".newuser").show();
                                        $(".olduser").hide();
                                } else {
                                        $.ajax({
                                                type: "POST",
                                                url: baseurl + "ajax/SelectUser",
                                                data: {value: value}
                                        }).done(function(data) {
                                                var obj = jQuery.parseJSON(data);
                                                $("#fname").html(obj.title + ' ' + obj.fname);
                                                $("#lname").html(obj.lname);
                                                $("#country").html(obj.country);
                                                $("#email").html(obj.email);
                                                $("#mobile").html("+" + obj.phonecode + " " + obj.mobile);
                                                $(".newuser").hide();
                                                $(".olduser").show();
                                        });
                                }
                        } else {
                                $(".newuser").hide();
                                $(".olduser").hide();
                                //                                $('#UserDetails_phonecode').html("<option value=''>--Code--</option>");
                        }
                });

                $('.calculate_shipping').click(function() {
                        var country = $("#UserAddress_ship_country").val();
                        var user = $("#Order_user_id").val();
                        var product = $("#productid").val();
                        if (user != 0 && user != "") {
                                if (country != 0) {

                                        $.ajax({
                                                type: "POST",
                                                url: baseurl + "ajax/GetShippingChargeCeleb",
                                                data: {country: country, user: user, product: product}
                                        }).done(function(data) {

                                                var obj = jQuery.parseJSON(data);
                                                //$(".shipping_charge").html(obj.shippingcharge);
                                                $("#Order_shipping_charge").val(obj.total_shipping_rate);
                                                $(".total_amount").html(obj.granttotal);
//                                                $("#lname").html(obj.lname);
//                                                $("#country").html(obj.country);
//                                                $("#email").html(obj.email);
//                                                $("#mobile").html("+" + obj.phonecode + " " + obj.mobile);
//                                                $(".newuser").hide();
//                                                $(".olduser").show();
                                        });
                                } else {
                                        alert("Please fill the Billing Address and Shipping Address");
                                        //                                $('#UserDetails_phonecode').html("<option value=''>--Code--</option>");
                                }
                        } else {
                                alert("Please Add a new user");
                        }
                });
                $('.check_same').click(function() {
                        var bill_fname = $("#UserAddress_bill_first_name").val();
                        var bill_lname = $("#UserAddress_bill_last_name").val();
                        var bill_add1 = $("#UserAddress_bill_address_1").val();
                        var bill_add2 = $("#UserAddress_bill_address_2").val();
                        var bill_country = $("#UserAddress_bill_country").val();
                        var bill_state = $("#UserAddress_bill_state").val();
                        var bill_city = $("#UserAddress_bill_city").val();
                        var bill_postcode = $("#UserAddress_bill_postcode").val();
                        var bill_phonecode = $("#UserAddress_bill_phonecode").val();
                        var bill_phone = $("#UserAddress_bill_contact_number").val();

                        if ($('.check_same').is(":checked"))
                        {


                                $("#UserAddress_ship_first_name").val(bill_fname);
                                $("#UserAddress_ship_last_name").val(bill_lname);
                                $("#UserAddress_ship_address_1").val(bill_add1);
                                $("#UserAddress_ship_address_2").val(bill_add2);
                                $("#UserAddress_ship_country").val(bill_country);
                                var bill_state1 = $("#UserAddress_bill_state1").val();
                                if (bill_state1 != " ") {

                                        $("#UserAddress_ship_state").val(" ");
                                        $("#UserAddress_ship_state1").val(bill_state1);
                                        $(".ship_cn_ext").hide();
                                        $(".ship_not_ext").show();

                                } else {
                                        $("#UserAddress_ship_state1").val(" ");
                                        $("#UserAddress_ship_state").val(bill_state);
                                        $(".ship_cn_ext").show();
                                        $(".ship_not_ext").hide();

                                }

                                $("#UserAddress_ship_city").val(bill_city);
                                $("#UserAddress_ship_postcode").val(bill_postcode);
                                $("#UserAddress_ship_phonecode").val(bill_phonecode);
                                $("#UserAddress_ship_contact_number").val(bill_phone);
                        } else {
                                $("#UserAddress_ship_first_name").val("");
                                $("#UserAddress_ship_last_name").val("");
                                $("#UserAddress_ship_address_1").val("");
                                $("#UserAddress_ship_address_2").val("");
                                $("#UserAddress_ship_country").val("");
                                $("#UserAddress_ship_state").val("");
                                $("#UserAddress_ship_city").val("");
                                $("#UserAddress_ship_postcode").val("");
                                $("#UserAddress_ship_phonecode").val("");
                                $("#UserAddress_ship_contact_number").val("");
                        }
                });

        });
</script>