<section class="content-header">
        <h1>View Order #<?php echo $model->id; ?></h1>
        <ol class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl . '/admin.php/products/order/admin'; ?>"><i class="fa fa-dashboard"></i>  Order</a></li>
                <li class="active">Manage</li>
        </ol>
</section> 
<!--<a href="<?php echo Yii::app()->request->baseUrl . '/admin.php/products/order/create'; ?>" class='btn  btn-success manage'>Add Order</a>-->
<div class="col-xs-12 form-page">
        <div class="box">
                <div class="box-body table-responsive no-padding">

                        <?php
                        $this->widget('booster.widgets.TbDetailView', array(
                            'data' => $model,
                            'attributes' => array(
//                    'id',
                                array('name' => 'user_id',
                                    'value' => function($data) {
                                    return $data->user->first_name . ' ' . $data->user->last_name;
                            },
                                ),
                                array('name' => 'Sub Total',
                    'header'=>'Subtotal(Total Prdoduct price)',
                        'value' => function($data) {
                            $get_datas = OrderProducts::model()->findAllByAttributes(['order_id'=>$data->id]);
                            $get_result = 0;
                            foreach($get_datas as $get_data){
                                $get_result += $get_data->amount;
                            }
                             return 'AUD ' . $get_result . '/-';
                            // $enqy = ProductEnquiry::model()->findByAttributes(array('order_id' => $data->id));
                            // if (($enqy != '') && ($enqy != 0)) {
                            //     $total = $enqy->total_amount + $enqy->shipping_charge;

                            //     return 'AUD ' . $total . '/-';
                            // } else {

                            //     return 'AUD ' . $get_result . '/-';
                            // }
                        },
                    ),
                      array('name' => 'Tax(GST)',
                     // 'heading'=>'Subtotal(Total Prdoduct price)',
                        'value' => function($data) {
                            $get_datas = Settings::model()->findByAttributes(['id'=>1]);
                            $get_result = round($data->total_amount * $get_datas->tax_rate /100);
                            return 'AUD ' .$get_result. '/-';;
                        },
                    ),
                    array('name' => 'shipping_charge',
                      'heading'=>'Subtotal(Total Prdoduct price)',
                        'value' => function($data) {
                           
                            return 'AUD ' .$data->shipping_charge. '/-';;
                        },
                    ),
                      array('name' => 'total_amount',
                        'value' => function($data) {
                            $enqy = ProductEnquiry::model()->findByAttributes(array('order_id' => $data->id));
                            if (($enqy != '') && ($enqy != 0)) {
                                $total = $enqy->total_amount + $enqy->shipping_charge;

                                return 'AUD ' . $total . '/-';
                            } else {

                                return 'AUD ' . $data->total_amount . '/-';
                            }
                        },
                    ),
                                array('name' => 'Amount Paid',
                                    'value' => function($data) {
                                    $enqy = ProductEnquiry::model()->findByAttributes(array('order_id' => $data->id));
                                    if (($enqy != '') && ($enqy != 0)) {

                                            $total = $enqy->total_amount + $enqy->shipping_charge - $enqy->balance_to_pay;

                                            return 'AUD ' . $total . '/-';
                                    } else if ($data->payment_status == 1) {
                                            $totalpaid = $data->total_amount + $data->shipping_charge;

                                            return 'AUD ' . $totalpaid . '/-';
                                    } else {

                                            return 'AUD ' . 0 . '/-';
                                    }
                            },
                                ),
                            //     array('name' => 'Balance Amount',
                            //         'value' => function($data) {
                            //         $enqy = ProductEnquiry::model()->findByAttributes(array('order_id' => $data->id));

                            //         if (($enqy != '') && ($enqy != 0)) {
                            //                 $total = $enqy->balance_to_pay;

                            //                 return 'AUD ' . $total . '/-';
                            //         } else {

                            //                 return 'AUD ' . 0 . '/-';
                            //         }
                            // },
                                // ),
                                'order_date',
                            //     array('name' => 'order_date',
                            //         'value' => function($data) {
                            //         $timestamp = strtotime('Y/m/d H:i A',$data->order_date);
                            //         return $timestamp;
                            // },
                            //         'type' => 'raw',
                            //     ),
                                array('name' => 'ship_address_id',
                                    'type' => 'raw',
                                    'value' => function($data) {

                                    if ($data->ship_address_id != 0) {
                                            $shipp_add = UserAddress::model()->findByPk($data->ship_address_id);
                                            $result .= $shipp_add->first_name . ' ' . $shipp_add->last_name . '<br />';
                                            $result .= $shipp_add->address_1 . ' ' . $shipp_add->address_2 . '<br />';
                                            $result .= $shipp_add->city . ' ' . $shipp_add->postcode . '<br />';
                                            $result .= Countries::model()->findByPk($shipp_add->country)->country_name . ' ' . States::model()->findByPk($shipp_add->state)->state_name . '<br />';
                                            $result .= '+' . Countries::model()->findByPk($shipp_add->country)->phonecode . ' ';
                                            $result .= $shipp_add->contact_number . '<br />';
                                    } else {
                                            $shipp_add = UserAddress::model()->findByAttributes(array('userid' => $data->user_id, 'default_shipping_address' => 1));
                                            $result .= $shipp_add->first_name . ' ' . $shipp_add->last_name . '<br />';
                                            $result .= $shipp_add->address_1 . ' ' . $shipp_add->address_2 . '<br />';
                                            $result .= $shipp_add->city . ' ' . $shipp_add->postcode . '<br />';
                                            $result .= Countries::model()->findByPk($shipp_add->country)->country_name . ' ' . States::model()->findByPk($shipp_add->state)->state_name . '<br />';
                                            $result .= '+' . Countries::model()->findByPk($shipp_add->country)->phonecode . ' ';
                                            $result .= $shipp_add->contact_number . '<br />';
                                    }
                                    return $result;
                            },
                                ),
                                array('name' => 'bill_address_id',
                                    'type' => 'raw',
                                    'value' => function($data) {

                                    if ($data->ship_address_id != 0) {
                                            $bill_add = UserAddress::model()->findByPk($data->bill_address_id);
                                            $result1 .= $bill_add->first_name . ' ' . $bill_add->last_name . '<br />';
                                            $result1 .= $bill_add->address_1 . ' ' . $bill_add->address_2 . '<br />';
                                            $result1 .= $bill_add->city . ' ' . $bill_add->postcode . '<br />';
                                            $result1 .= Countries::model()->findByPk($bill_add->country)->country_name . ' ' . States::model()->findByPk($bill_add->state)->state_name . '<br />';
                                            $result1 .= '+' . Countries::model()->findByPk($bill_add->country)->phonecode . ' ';
                                            $result1 .= $bill_add->contact_number . '<br />';
                                    } else {
                                            $bill_add = UserAddress::model()->findByAttributes(array('userid' => $data->user_id, 'default_billing_address' => 1));
                                            $result1 .= $bill_add->first_name . ' ' . $bill_add->last_name . '<br />';
                                            $result1 .= $bill_add->address_1 . ' ' . $bill_add->address_2 . '<br />';
                                            $result1 .= $bill_add->city . ' ' . $bill_add->postcode . '<br />';
                                            $result1 .= Countries::model()->findByPk($bill_add->country)->country_name . ' ' . States::model()->findByPk($bill_add->state)->state_name . '<br />';
                                            $result1 .= '+' . Countries::model()->findByPk($bill_add->country)->phonecode . ' ';
                                            $result1 .= $bill_add->contact_number . '<br />';
                                    }
                                    return $result1;
                            },
                                ),
                                 //'comment',
                                array('name' => 'payment_mode',
                                    'value' => function($data) {
                                    if ($data->payment_mode == 1) {
                                            return 'Wallet';
                                    } else if ($data->payment_mode == 2) {
                                            return 'Netbanking';
                                    } else if ($data->payment_mode == 3) {
                                            return 'Paypal';
                                    } else if ($data->payment_mode == 4) {
                                            return 'Wallet, Netbanking';
                                    } else if ($data->payment_mode == 5) {
                                            return 'Wallet, Paypal';
                                    } else {
                                            return 'Not Paid';
                                    }
                            },
                                ),
                            //     'admin_comment',
                                array('name' => 'transaction_id',
                                    'value' => function($data) {
                                    if ($data->transaction_id == 0) {
                                            return 'Nill';
                                    } else {
                                            return $data->transaction_id;
                                    }
                            },
                                ),
                                array('name' => 'payment_status',
                                    'value' => function($data) {
                                    if ($data->payment_status == 1) {
                                            return 'Success';
                                    } else if ($data->payment_status == 2) {
                                            return 'Failed Transaction';
                                    } else {
                                            return 'Not Paid';
                                    }
                            },
                                ),
                              array('name' => 'status',
                              'value' => function($data) {
                              if ($data->status == 1) {
                              return 'Order Placed , Not Delivered to customer';
                              } else if ($data->status == 2) {
                              return 'Order Success';
                              } else if ($data->payment_status == 3) {
                              return 'Order Failed';
                              } else {
                              return 'Order Not Placed';
                              }
                              },
                              ), 
                            ),
                        ));
                        ?>


                        
        </div>
        <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'brands-form',
            'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>
        <br/>
        <div class="">

               <div class="form-group">
                        <?php echo $form->labelEx($model, 'payment_status', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->dropDownList($model, 'payment_status', array('1' => 'Success', '0' => 'Not Pay','2'=>'Failed'), array('class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'payment_status'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'transaction_id', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'transaction_id', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control slug')); ?></div>
                        <?php echo $form->error($model, ' transaction_id'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'payment_mode', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->dropDownList($model, 'payment_mode', array('1' => 'Wallet', '2' => 'Gateway','3'=>'Paypal'), array('class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'payment_mode'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->dropDownList($model, 'status', array('1' => 'Enabled', '0' => 'Disabled'), array('class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'status'); ?>
                </div>


        </div>
        <div class="form-group btns">
                <label>&nbsp;</label><br/>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-secondary btn-single pull-right', 'style' => 'border-radius:0px;
                                padding: 10px 50px;
                                                  ')); ?>
        </div>

        <?php $this->endWidget(); ?>

</div><!-- form -->
</div>