<?php echo $this->renderPartial('//site/mail/_email_header'); ?>

<tr><td align="center">
                <?php
                $user_id = Order::model()->findByPk($model->order_id);
                $enquiry = ProductEnquiry::model()->findByAttributes(array('order_id' => $model->order_id));
                $user_det = UserDetails::model()->findByPk($user_id->user_id);
                ?>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Hi <?php echo $user_det->first_name; ?> <?php echo $user_det->last_name; ?>, </h4>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Greetings from SolesiLove!</h4>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Your order # SLOR<?php echo $order->id; ?> has been updated to the following status:</h4>

                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Current Status: <?php echo OrderStatus::model()->findByPk($model->order_status)->title; ?></h4>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Comments: <?php echo $model->order_status_comment; ?></h4>
                <h4 style="padding: 5px 9px 6px 9px;font-style:italic; text-align:center">Updated Time: <?php echo $model->date; ?></h4>
                <?php
                if ((OrderStatus::model()->findByPk($model->order_status)->id == 10 ) || (OrderStatus::model()->findByPk($model->order_status)->id == 25)) {
                        if ($model->shipping_type == 1) {
                                $track = "https://track.aftership.com/dtdc/$model->tracking_id";
                        } else if ($model->shipping_type == 2) {
                                $track = "https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=$model->tracking_id";
                        } else if ($model->shipping_type == 3) {
                                $track = "https://www.aramex.com/track/results?ShipmentNumber=$model->tracking_id";
                        }
                        ?>

                        <a href="<?php echo $track; ?>" name="yt0"  style="     text-transform: uppercase;
                           background-color: #ec9721;
                           border-radius: 0;
                           outline: none;
                           height: 40px;
                           line-height: 40px;
                           padding: 9px 28px;
                           border: solid 1px #ec9721;
                           /* width: 360px; */
                           border-radius: 4px;
                           text-align: center;
                           text-decoration: none;
                           color: #fff;
                           /* display: inline-block; */
                           /* margin: 0 auto; */">Track Shipment </a>

                        <?php
                } else if (OrderStatus::model()->findByPk($model->order_status)->id == 19) {
                        ?>
                        <a href="<?php echo $this->siteURL(); ?>/index.php/products/category?name=women" name="yt0"  style="     text-transform: uppercase;
                           background-color: #ec9721;
                           border-radius: 0;
                           outline: none;
                           height: 40px;
                           line-height: 40px;
                           padding: 9px 28px;
                           border: solid 1px #ec9721;
                           /* width: 360px; */
                           border-radius: 4px;
                           text-align: center;
                           text-decoration: none;
                           color: #fff;
                           /* display: inline-block; */
                           /* margin: 0 auto; */">Shop Now </a>

                        <?php
                } else if (OrderStatus::model()->findByPk($model->order_status)->id == 37) {


                        $celib_history = new CelibStyleHistory;
                        $celib_history->enq_id = $enquiry->id;
                        $celib_history->status = 3;
                        $celib_history->pay_amount = $enquiry->balance_to_pay;
                        if ($celib_history->save()) {
                                $celib_history_update = CelibStyleHistory::model()->findByPk($celib_history->id);
                                $enc_enq_id = $enquiry->id;
                                $enc_celib_history_id = $celib_history->id;
                                $getToken1 = $this->encrypt_decrypt('encrypt', 'enquiry_id=' . $enc_enq_id . ',history_id=' . $enc_celib_history_id);
                                $celib_history_update->link = Yii::app()->request->baseUrl . '/index.php/Myaccount/MakepaymentPartial?p=' . $getToken1;
                                $celib_history_update->save();
                        }
                        ?>
                        <a href="<?php echo $this->siteURL(); ?>/index.php/<?php echo $celib_history_update->link; ?>" name="yt0"  style="text-transform: uppercase;
                           background-color: #ec9721;
                           border-radius: 0;
                           outline: none;
                           height: 40px;
                           line-height: 40px;
                           padding: 9px 28px;
                           border: solid 1px #ec9721;
                           /* width: 360px; */
                           border-radius: 4px;
                           text-align: center;
                           text-decoration: none;
                           color: #fff;
                           /* display: inline-block; */
                           /* margin: 0 auto; */">Make Payment </a>

                        <?php
                } else if (OrderStatus::model()->findByPk($model->order_status)->id == 38) {
                        ?>
                        <a href="<?php echo $this->siteURL(); ?>/index.php/Myaccount/MakepaymentDirect" name="yt0"  style="text-transform: uppercase;
                           background-color: #ec9721;
                           border-radius: 0;
                           outline: none;
                           height: 40px;
                           line-height: 40px;
                           padding: 9px 28px;
                           border: solid 1px #ec9721;
                           /* width: 360px; */
                           border-radius: 4px;
                           text-align: center;
                           text-decoration: none;
                           color: #fff;
                           /* display: inline-block; */
                           /* margin: 0 auto; */">Make Payment </a>

                        <?php
                } else {
                        ?>
                        <a href="<?php echo $this->siteURL(); ?>/index.php/Myaccount/Myordernew/id/<?php echo $model->order_id; ?>" name="yt0"  style="text-transform: uppercase;
                           background-color: #ec9721;
                           border-radius: 0;
                           outline: none;
                           height: 40px;
                           line-height: 40px;
                           padding: 9px 28px;
                           border: solid 1px #ec9721;
                           /* width: 360px; */
                           border-radius: 4px;
                           text-align: center;
                           text-decoration: none;
                           color: #fff;
                           /* display: inline-block; */
                           /* margin: 0 auto; */">View Order</a>


                        <?php
                }
                ?>



        </td></tr>
<tr><td>
                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;color: #abaaaa; text-align:center;">* This is an automatically generated email, please do not reply to this email.</p>

                <p style=" font-family:'Open Sans',arial, sans-serif; font-size:13px;color: #abaaaa; text-align:center;">* If you need assistance, please call +91 914 220 2222 during office hours: Mon to Sat 9:30am to 6:30pm IST or send an email to support@laksyah.com</p>
        </td>
</tr>


<?php echo $this->renderPartial('//site/mail/_email_footer'); ?>
