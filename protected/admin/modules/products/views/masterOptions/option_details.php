<style>
        .option_image {
                width: 125px;
        }
        .option_image img {
                width: 100%;
        }
        .row.options_data {
                padding: 15px 10px;
        }
        .add_delete {
                padding-top: 27px;
                padding-left: 20px;
        }
        .add_delete i {
                width: 35px;
                height: 35px;
                background-color: #ccc;
                text-align: center;
                line-height: 35px;
                margin-right: 10px;
                border-radius: 50px;
        }
        .inner_option_data {
                /* margin-bottom: 17px; */
                padding-top: 30px;
        }
</style>
<section class="content-header">
        <h1>  Product Options

                <small>Create</small>
        </h1>
        <ol class="breadcrumb">
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/admin.php/site/home"><i class="fa fa-dashboard"></i>Dashboard</a></li>
                <li class="active">Create Product Options</li>
        </ol>
</section>
<a class="btn btn-laksyah" href="<?php echo Yii::app()->request->baseurl . '/admin.php/products/masterOptions/admin'; ?>" id="add-note">
        Manage Product Options
</a>
<section class="content">

        <div class="box box-info">

                <div class="box-body">


                        <div class="form">


                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'options-detail-form',
                                    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
                                    // Please note: When you enable ajax validation, make sure the corresponding
                                    // controller action is handling ajax validation correctly.
                                    // There is a call to performAjaxValidation() commented in generated controller code.
                                    // See class documentation of CActiveForm for details on this.
                                    'enableAjaxValidation' => false,
                                ));
                                ?>

                                <?php echo $form->errorSummary($model); ?>


                                <br/>
                                <div class="form-group">
                                        <label class="col-sm-2 control-label">Product Name</label><div class="col-sm-10"><span style="text-transform: capitalize;"> <?php echo $productOptions->product->product_name; ?></span></div>
                                </div>
                                <input type="hidden" value="<?php echo $options->master_option_id; ?>" id="master_option_id"/>
                                <?php
                                if ($model->isNewRecord) {


                                    $check_type = OptionDetails::model()->findAll(['condition' => 'master_option_id=' . $options->master_option_id]);

                                    if (!empty($check_type)) {
                                        $t = 1;
                                        foreach ($check_type as $type) {
                                            if ($type->color_id != 0) {
                                                if ($t == 1) {
                                                    $types.=$type->color_id;
                                                } else {
                                                    $types.=',' . $type->color_id;
                                                }
                                            } else if ($type->size_id != 0) {
                                                if ($t == 1) {
                                                    $types.=$type->size_id;
                                                } else {
                                                    $types.=',' . $type->size_id;
                                                }
                                            }


                                            $t++;
                                        }

                                        if ($types != "") {
                                            $condition = ' and id not in(' . $types . ') ';
                                        }
                                    } else {
                                        $condition = '';
                                    }
                                } else {
                                    $condition = '';
                                }
                                ?>
                                <?php
                                //var_dump($model->size_id);
                                //exit;
                                ?>
                                <?php if ($productOptions->option_type_id == 1) { ?>
                                    <div class="form-group">
                                            <?php echo $form->labelEx($model, 'color_id', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">

                                                    <?php echo CHtml::activeDropDownList($model, 'color_id', CHtml::listData(OptionCategory::model()->findAll(['condition' => 'option_type_id=1 ' . $condition]), 'id', 'color_name'), array('empty' => '--Select--', 'class' => 'form-control')); ?>

                                                    <?php echo $form->error($model, 'color_id'); ?>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            <?php echo $form->labelEx($model, 'image (Choose color combination image - 274 * 292)', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                    <?php echo $form->fileField($model, 'image', array('class' => 'form-control')); ?>
                                                    <div class="option_image"></div>
                                                    <?php echo $form->error($model, 'image'); ?>
                                            </div>
                                    </div>


                                <?php } else if ($productOptions->option_type_id == 2) { ?>
                                    <div class="form-group">
                                            <?php echo $form->labelEx($model, 'size_id', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                    <?php echo CHtml::activeDropDownList($model, 'size_id', CHtml::listData(OptionCategory::model()->findAll(['condition' => 'option_type_id=2 ' . $condition]), 'id', 'size'), array('empty' => '--Select--', 'class' => 'form-control')); ?>

                                                    <?php echo $form->error($model, 'size_id'); ?>
                                            </div>
                                    </div>


                                <?php } else if ($productOptions->option_type_id == 3) { ?>
                                    <div class="form-group">
                                            <?php echo $form->labelEx($model, 'color_id', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                    <?php echo CHtml::activeDropDownList($model, 'color_id', CHtml::listData(OptionCategory::model()->findAll(['condition' => 'option_type_id=1']), 'id', 'color_name'), array('empty' => '--Select--', 'class' => 'form-control')); ?>

                                                    <?php echo $form->error($model, 'color_id'); ?>
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            <?php echo $form->labelEx($model, 'image (Choose color combination image - 274 * 292 )', array('class' => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                    <?php echo $form->fileField($model, 'image', array('class' => 'form-control')); ?>
                                                    <div class="option_image"></div>
                                                    <?php echo $form->error($model, 'image'); ?>
                                            </div>
                                    </div>







                                <?php } ?>
                                <?php if ($optionid != '') { ?>
                                    <div class="row options_data">
                                            <div class="col-xs-10">
                                                    <div class="row">
                                                            <?php if ($productOptions->option_type_id == 3) { ?>
                                                                <div class="col-xs-12 col-sm-3">
                                                                        <div class="row">

                                                                                <?php echo $form->labelEx($model, 'size_id', array('class' => 'col-sm-12 control-label text-left')); ?>
                                                                        </div>
                                                                </div>
                                                            <?php } ?>
                                                            <div class="col-xs-12 col-sm-3">
                                                                    <div class="row">

                                                                            <?php echo $form->labelEx($model, 'Width Fittings', array('class' => 'col-sm-12 control-label text-left')); ?>
                                                                    </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                    <div class="row">

                                                                            <?php echo $form->labelEx($model, 'stock', array('class' => 'col-sm-12 control-label text-left')); ?>
                                                                    </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                    <div class="row">

                                                                            <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-12 control-label text-left')); ?>
                                                                    </div>
                                                            </div>
                                                            <div class="option_det">
                                                                    <div class="inner_option_data">
                                                                            <?php if ($productOptions->option_type_id == 3) { ?>
                                                                                <div class="col-xs-12 col-sm-3">
                                                                                        <div class="row">

                                                                                                <?php //echo $form->labelEx($model, 'size_id', array('class' => 'col-sm-12 control-label text-left'));   ?>
                                                                                                <div class="col-sm-12">
                                                                                                        <?php echo CHtml::activeDropDownList($model, 'size_id', CHtml::listData(OptionCategory::model()->findAll(['condition' => 'option_type_id=2']), 'id', 'size'), array('class' => 'form-control option_sizes', 'options' => array('size_id' => array('selected' => 'selected')))); ?>

                                                                                                        <?php echo $form->error($model, 'size_id'); ?>
                                                                                                </div>

                                                                                        </div>
                                                                                </div>
                                                                            <?php } ?>

                                                                            <div class="col-xs-12 col-sm-3">
                                                                                    <div class="row">

                                                                                            <?php //echo $form->labelEx($model, 'Width Fittings', array('class' => 'col-sm-12 control-label text-center'));   ?>
                                                                                            <div class="col-sm-12">
                                                                                                    <?php echo CHtml::activeDropDownList($model, 'width_id', CHtml::listData(WidthFitting::model()->findAll(), 'id', 'title'), array('class' => 'form-control', 'empty' => '--Select--',)); ?>

                                                                                                    <?php echo $form->error($model, 'width_id'); ?>
                                                                                            </div>

                                                                                    </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-3">
                                                                                    <div class="row">

                                                                                            <?php //echo $form->labelEx($model, 'stock', array('class' => 'col-sm-12 control-label text-center'));   ?>
                                                                                            <div class="col-sm-12">
                                                                                                    <?php echo $form->textField($model, 'stock', array('class' => 'form-control')); ?>

                                                                                                    <?php echo $form->error($model, 'stock'); ?>
                                                                                            </div>

                                                                                    </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-3">
                                                                                    <div class="row">

                                                                                            <?php //echo $form->labelEx($model, 'status', array('class' => 'text-center col-sm-12 control-label'));   ?>
                                                                                            <div class="col-sm-12">
                                                                                                    <?php echo $form->dropdownList($model, 'status', array('1' => 'In Stock', '0' => 'Out of Stock'), array('class' => 'form-control')); ?>

                                                                                                    <?php echo $form->error($model, 'status'); ?>
                                                                                            </div>

                                                                                    </div>
                                                                            </div>
                                                                            <br/>
                                                                    </div>
                                                            </div>
                                                    </div>
                                            </div>


                                    </div>
                                <?php } else { ?>
                                    <div class="row options_data">
                                            <div class="col-xs-10">
                                                    <div class="row">
                                                            <?php if ($productOptions->option_type_id == 3) { ?>
                                                                <div class="col-xs-12 col-sm-3">
                                                                        <div class="row">

                                                                                <?php echo $form->labelEx($model, 'size_id', array('class' => 'col-sm-12 control-label text-left')); ?>
                                                                        </div>
                                                                </div>
                                                            <?php } ?>
                                                            <div class="col-xs-12 col-sm-3">
                                                                    <div class="row">

                                                                            <?php echo $form->labelEx($model, 'Width Fittings', array('class' => 'col-sm-12 control-label text-left')); ?>
                                                                    </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                    <div class="row">

                                                                            <?php echo $form->labelEx($model, 'stock', array('class' => 'col-sm-12 control-label text-left')); ?>
                                                                    </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                    <div class="row">

                                                                            <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-12 control-label text-left')); ?>
                                                                    </div>
                                                            </div>
                                                            <div class="option_det">
                                                                    <div class="inner_option_data">
                                                                            <?php if ($productOptions->option_type_id == 3) { ?>
                                                                                <div class="col-xs-12 col-sm-3">
                                                                                        <div class="row">

                                                                                                <?php //echo $form->labelEx($model, 'size_id', array('class' => 'col-sm-12 control-label text-left'));   ?>
                                                                                                <div class="col-sm-12">
                                                                                                        <?php echo CHtml::activeDropDownList($model, 'size_id[]', CHtml::listData(OptionCategory::model()->findAll(['condition' => 'option_type_id=2']), 'id', 'size'), array('class' => 'form-control option_sizes', 'options' => array('size_id' => array('selected' => 'selected')))); ?>

                                                                                                        <?php echo $form->error($model, 'size_id'); ?>
                                                                                                </div>

                                                                                        </div>
                                                                                </div>
                                                                            <?php } ?>

                                                                            <div class="col-xs-12 col-sm-3">
                                                                                    <div class="row">

                                                                                            <?php //echo $form->labelEx($model, 'Width Fittings', array('class' => 'col-sm-12 control-label text-center'));   ?>
                                                                                            <div class="col-sm-12">
                                                                                                    <?php echo CHtml::activeDropDownList($model, 'width_id[]', CHtml::listData(WidthFitting::model()->findAll(), 'id', 'title'), array('class' => 'form-control', 'empty' => '--Select--')); ?>

                                                                                                    <?php echo $form->error($model, 'width_id'); ?>
                                                                                            </div>

                                                                                    </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-3">
                                                                                    <div class="row">

                                                                                            <?php //echo $form->labelEx($model, 'stock', array('class' => 'col-sm-12 control-label text-center'));   ?>
                                                                                            <div class="col-sm-12">
                                                                                                    <?php echo $form->textField($model, 'stock[]', array('class' => 'form-control', 'value' => 1)); ?>

                                                                                                    <?php echo $form->error($model, 'stock'); ?>
                                                                                            </div>

                                                                                    </div>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-3">
                                                                                    <div class="row">

                                                                                            <?php //echo $form->labelEx($model, 'status', array('class' => 'text-center col-sm-12 control-label'));   ?>
                                                                                            <div class="col-sm-12">
                                                                                                    <?php echo $form->dropdownList($model, 'status[]', array('1' => 'In Stock', '0' => 'Out of Stock'), array('class' => 'form-control')); ?>

                                                                                                    <?php echo $form->error($model, 'status'); ?>
                                                                                            </div>

                                                                                    </div>
                                                                            </div>
                                                                            <br/>
                                                                    </div>
                                                            </div>
                                                    </div>
                                            </div>
                                            <div class="col-xs-2">

                                                    <div class="add_delete">
                                                            <div class="col-xs-12">
                                                                    <a id="add_data" href="javascript:void(0)"><i class="fa fa-plus"></i></a>
                                                                    <a id="del_data" href="javascript:void(0)"><i class="fa fa-minus"></i></a>
                                                            </div>
                                                    </div>
                                            </div>

                                    </div>

                                <?php } ?>

                                <div class="form-group btns">
                                        <label>&nbsp;</label><br/>
                                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Save', array('class' => 'btn btn-laksyah pull-right')); ?>
                                </div>

                                <?php $this->endWidget(); ?>

                        </div><!-- form -->

                </div>
                <div class="box-body table-responsive no-padding">
                    <div class="form-group btns">
                                        <label>&nbsp;</label><br/>
                                       <button type="button" class="btn btn-laksyah pull-right delete_all">Delete Selected Items</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="float:right;margin-right:15px;font-size: 15px;">Select All <input type="checkbox" style="width: 20px;
    height: 20px;" name='select_all' class="check_all"/></span>
                                </div>
                        <?php
                        if ($productOptions->option_type_id == 1 || $productOptions->option_type_id == 3) {
                            $color = true;
                        } else {
                            $color = false;
                        }
                        if ($productOptions->option_type_id == 2 || $productOptions->option_type_id == 3) {
                            $size = true;
                        } else {
                            $size = false;
                        }
                        $this->widget('booster.widgets.TbGridView', array(
                            'type' => ' bordered condensed hover',
                            'id' => 'options-detail-grid',
                            'dataProvider' => $options->search(),
                            'filter' => $options,
                            'columns' => array(
                                array(
                                    'name' => 'color_id',
                                    'value' => '$data->color->color_name',
                                    'filter' => CHtml::listData(OptionCategory::model()->findAll(['condition' => 'option_type_id=1']), 'id', 'color_name'),
                                    'visible' => $color,
                                ),
                                array(
                                    'name' => 'size_id',
                                    'value' => '$data->size->size',
                                    'filter' => CHtml::listData(OptionCategory::model()->findAll(['condition' => 'option_type_id=2']), 'id', 'size'),
                                    'visible' => $size,
                                ),
                                array(
                                    'name' => 'width_id',
                                    'value' => function($data) {
                                        $get_data = WidthFitting::model()->findByPk($data->width_id);
                                        return $get_data->title;
                                    },
                                    'filter' => CHtml::listData(WidthFitting::model()->findAll(['condition' => 'status =1']), 'id', 'title'),
                                    'visible' => true,
                                ),
                                'stock',
                                array(
                                    'name' => 'status',
                                    'filter' => array('1' => 'In Stock', '0' => 'Out of Stock'),
                                    'value' => function($data) {
                                return $data->status == 1 ? 'In Stock' : 'Out of Stock';
                            }
                                ),
                                array(
                                    'header'=>'Edit',
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{update}{delete}',
                                    'buttons' => array(
                                        'update' => array(
                                            'url' => 'Yii::app()->request->baseUrl . "/admin.php/products/masterOptions/OptionDetails/id/".$data->master_option_id."/optionid/" . $data->id',
                                            'label' => '<i class="glyphicon glyphicon-trash" style="font-size: 20px;"> </i>',
                                            'options' => array(
                                                'data-toggle' => 'tooltip',
                                                'title' => 'update',
                                            ),
                                        ),
                                        'delete' => array(
                                            'url' => 'Yii::app()->request->baseUrl . "/admin.php/products/masterOptions/OptionsDelete/id/" . $data->id."?option=".$data->master_option_id',
                                            'label' => '<i class="glyphicon glyphicon-trash" style="font-size: 20px;"> </i>',
                                            'options' => array(
                                                'data-toggle' => 'tooltip',
                                                'title' => 'delete',
                                            ),
                                        ),
                                        
                                        
                                        
                                    ),),
                                    array(
                                    'header' => '<font color="#61625D">Delete</font>',
                                    'htmlOptions' => array('nowrap' => 'nowrap'),
                                    'class' => 'booster.widgets.TbButtonColumn',
                                    'template' => '{multiple}',
                                    'buttons' => array(
                                         
                                        
                                        'multiple' => array(
                                            'url'=>'"#".$data->id',
                                         //   'url' => 'Yii::app()->request->baseUrl."/managermode.php/products/order/print/id/$data->id"',
                                            'label' => '<input type="checkbox" class="multiple_data" name="multiple_data[]" />',
                                            'options' => array(
                                                'data-toggle' => 'tooltip',
                                                'title' => 'Multiple Delete',
                                                'target' => '_blank',
                                                'value'=>$data->id,
                                                'class'=>'multi_delete',
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                        ));
                        ?>
                </div>
        </div>

</section><!-- form -->
<script>
    $(document).ready(function(){
        $( "body").on( "click", ".delete_all", function() {
            var vaal = '';
        $( ".multiple_data" ).each(function() {
                    vaal += $(this).val() + '#';
                     
                   
                });
                if(vaal == ''){
                    alert('No Items Selected');
                    return false;
                }
                $.ajax({
                'url': baseurl + '/admin.php/products/MasterOptions/MultipleDelete',
                'type': "POST",
                //'dataType': 'html',
                'data': {option_id: vaal},
                success: function (result) {


                   $.fn.yiiGridView.update('options-detail-grid');
                }

            });
        });
        $( "#options-detail-grid").on( "click", ".multi_delete", function() {
        // $('.multi_delete').click(function(event){
            if($(this).find('input').is(':checked')){
             var vaal = $(this).attr('href');
           var newval = vaal.replace('#', '');
           $(this).find('input').val(newval);
           //$(this).find('input').prop('checked', true);
              
          
            }else{
                  $(this).find('input').val('');
            }
        });
        $('.check_all').click(function(event){
           if($(this).is(':checked')){
                $('.multi_delete').find('input').prop('checked', true);
                $( ".odd" ).each(function() {
                    var vaal = $(this).find('.multi_delete').attr('href');
                     var newval = vaal.replace('#', '');
                      $(this).find('input').val(newval);
                   
                });
                $( ".even" ).each(function() {
                    var vaal = $(this).find('.multi_delete').attr('href');
                     var newval = vaal.replace('#', '');
                      $(this).find('input').val(newval);
                   
                });
           }else{
               $( ".odd" ).each(function() {
                    var vaal = $(this).find('.multi_delete').attr('href');
                     var newval = vaal.replace('#', '');
                      $(this).find('input').val('');
                   
                });
                $( ".even" ).each(function() {
                    var vaal = $(this).find('.multi_delete').attr('href');
                     var newval = vaal.replace('#', '');
                      $(this).find('input').val('');
                   
                });
              
               $('.multi_delete').find('input').prop('checked', false);
           }
         
          
           //$(this).find('input').prop('checked', true);
        });
        
    });
    
</script>
<script>
    var counter = 1;
    var content = $('.option_det').html();
    $('#del_data').hide();
    $('#add_data').click(function () {
        if (counter < 50)
        {
            if (counter >= 1) {
                $('#del_data').show();
            }
            $('.option_det').append(content);
            counter++;
        }
    });
    $('#del_data').click(function () {
        if (counter > 2) {

            counter--;
            $(".option_det div.inner_option_data:last-child").remove();
        } else {
            $('#del_data').hide();
            counter--;
            $(".option_det div.inner_option_data:last-child").remove();
        }

    });
</script>
<script>
    var baseurl = "<?php print Yii::app()->request->baseUrl; ?>";
    $(document).ready(function () {
        $('#OptionDetails_color_id').on('change', function () {
            var color_id = $(this).val();
            var master_id = $('#master_option_id').val();
            ProductOptionType(color_id, master_id);
        });

        function ProductOptionType(color_id, master_id) {

            $.ajax({
                'url': baseurl + '/admin.php/products/MasterOptions/ProductTypeOptions',
                'type': "POST",
                'dataType': 'html',
                'data': {color_id: color_id, master_id: master_id},
                success: function (result) {


                    //                    $('.option_sizes').html(result);
                    $('.option_sizes').show();
                }

            });
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#OptionDetails_color_id').change(function () {
            var color_id = $(this).val();
            var product_id = <?php echo $productOptions->product->id; ?>;
            if (color_id != '' && product_id != '') {
                $.ajax({
                    type: "POST",
                    url: baseurl + "/index.php/ajax/selectOptionImage",
                    data: {color_id: color_id, product_id: product_id}
                }).done(function (data) {
                    if (data != '') {
                        $(".option_image").html(data);
                        //
                    } else {

                    }
                });
            } else {

            }
        });
        var color_id = $('#OptionDetails_color_id').val();
        var product_id = <?php echo $productOptions->product->id; ?>;
        if (color_id != '' && product_id != '') {
            $.ajax({
                type: "POST",
                url: baseurl + "/index.php/ajax/selectOptionImage",
                data: {color_id: color_id, product_id: product_id}
            }).done(function (data) {
                if (data != '') {
                    $(".option_image").html(data);
//
                } else {

                }
            });
        } else {

        }

    });</script>