<style>
        .multiselect-native-select{
                display :block;
                width: 100%;
        }
        .multiselect-native-select .btn-group{
                width: 100%;
                display: block;
        }
        button.multiselect.dropdown-toggle.btn.btn-default {
                width: 100%;
                text-align: left;
                background: transparent;
                border-radius: 0px;
        }
        .btn-group {
                margin: 0px;
        }
        .open>.dropdown-menu {
                display: block;
                height: 250px;
                overflow-y: scroll;
        }
        .detals h4{
                font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
                font-weight: bold;
                font-size: 14px;
                text-align: right;
        }
        
        .form-control {
                margin-bottom: 15px;
        }
        .billing-det h3 {
                font-size: 21px;
                font-weight: bold;
                margin-bottom: 6px;
                text-decoration: underline;
        }
        .address-head {
                height: 90px;
        }
        @media (min-width: 768px){
                .form-horizontal .control-label {
                        text-align: left;
                        padding-left: 43px;
                }
        }
        a.calculate_shipping {
                color: blue;
                font-size: 15px;
                text-decoration: underline;
                /* margin-bottom: 19px; */
        }
        .not_ext{
                display: none;

        }
        .ship_not_ext{
                display: none;
        }
        
        .grid-view .filters input, .grid-view .filters select#MasterOptions_product_id {
    margin-bottom: 0;
    width: auto;
}
input.form-control.multiselect-search {
    min-width: 465px;
}
.open>.dropdown-menu {
    display: block;
    height: 250px;
    overflow-y: scroll;
    min-width: 570px;
}
.skin-laksyah .main-footer {
    border-top-color: #d2d6de;
    min-height: 500px;
}
</style>
<section class="content-header">
    <h1>
        Product Options

    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/admin.php/site/home"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li class="active">Product Options</li>
    </ol>
</section>



<div class="col-sm-12">

    <a class="btn btn-laksyah" href="<?php echo Yii::app()->request->baseurl . '/admin.php/products/masterOptions/create'; ?>" id="add-note">
        Add Product Options
    </a>
    <div class="col-xs-12 form-page">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <?php
                $this->widget('booster.widgets.TbGridView', array(
                    'type' => ' bordered condensed hover',
                    'id' => 'master-options-grid',
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'columns' => array(
                        array(
                            'name' => 'product_id',
                            'value' => '$data->product->product_name',
                            'filter' => CHtml::listData(Products::model()->findAll(['condition' => 'status=1','order'=>'product_name']), 'id', 'product_name')
                        ),
                        array(
                            'name' => 'option_type_id',
                            'value' => '$data->optionType->type',
                            'filter' => CHtml::listData(OptionType::model()->findAll(['condition' => 'field1=1']), 'id', 'type')
                        ),
                        array(
                            'htmlOptions' => array('nowrap' => 'nowrap'),
                            'class' => 'booster.widgets.TbButtonColumn',
                            'template' => '{update}{delete}',
                            'buttons' => array(
                                'update' => array(
                                    'url' => 'Yii::app()->request->baseUrl . "/admin.php/products/masterOptions/OptionDetails/id/" . $data->id',
                                    'label' => '<i class="glyphicon glyphicon-pencil" style="font-size: 20px;"> </i>',
                                    'options' => array(
                                        'data-toggle' => 'tooltip',
                                        'title' => 'update',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ));
                ?>
            </div>

        </div>

    </div>
</div>

<script>
        $(document).ready(function () {
                        $('#MasterOptions_product_id').multiselect({
                        selectAllText: ' Select all',
                        includeSelectAllOption: true,
                        enableFiltering: true,
                        enableCaseInsensitiveFiltering: true,
                        filterPlaceholder: 'Search',
                });
                
               
        });
         setInterval(function(){
                 
                         $('#MasterOptions_product_id').multiselect({
                        selectAllText: ' Select all',
                        includeSelectAllOption: true,
                        enableFiltering: true,
                        enableCaseInsensitiveFiltering: true,
                        filterPlaceholder: 'Search',
                });
                }, 3000);
                
        </script>
