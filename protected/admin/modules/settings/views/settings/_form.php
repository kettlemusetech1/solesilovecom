<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>
<style>.form-group {
                margin-bottom: 18px;
                padding-bottom: 40px;
        }</style>
<div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'settings-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        ));
        ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>
        <br/>
        <div class="">
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'email', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 111, 'class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'email'); ?>
                </div>
                <br/>

                <div class="form-group">
                        <?php echo $form->labelEx($model, 'email_1', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'email_1', array('size' => 60, 'maxlength' => 111, 'class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'email_1'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'phone', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'phone', array('size' => 60, 'maxlength' => 111, 'class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'phone'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'free_shipping_limit', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'free_shipping_limit', array('size' => 60, 'maxlength' => 111, 'class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'free_shipping_limit'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'shipping_charge', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'shipping_charge', array('size' => 60, 'maxlength' => 111, 'class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'shipping_charge'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($model, 'tax_rate', array('class' => 'col-sm-2 control-label')); ?>
                        <div class="col-sm-10"><?php echo $form->textField($model, 'tax_rate', array('size' => 60, 'maxlength' => 111, 'class' => 'form-control')); ?>
                        </div>
                        <?php echo $form->error($model, 'tax_rate'); ?>
                </div>


        </div>
        <div class="form-group btns">
                <label>&nbsp;</label><br/>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-secondary btn-single pull-right', 'style' => 'border-radius:0px;padding: 10px 50px;')); ?>
        </div>

        <?php $this->endWidget(); ?>

</div><!-- form -->