<?php
/* @var $this SettingsController */
/* @var $data Settings */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_1')); ?>:</b>
	<?php echo CHtml::encode($data->email_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('free_shipping_limit')); ?>:</b>
	<?php echo CHtml::encode($data->free_shipping_limit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shipping_charge')); ?>:</b>
	<?php echo CHtml::encode($data->shipping_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tax_rate')); ?>:</b>
	<?php echo CHtml::encode($data->tax_rate); ?>
	<br />


</div>