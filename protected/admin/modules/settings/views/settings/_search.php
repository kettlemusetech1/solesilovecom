<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email_1'); ?>
		<?php echo $form->textField($model,'email_1',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>14,'maxlength'=>14,'class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'free_shipping_limit'); ?>
		<?php echo $form->textField($model,'free_shipping_limit',array('class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shipping_charge'); ?>
		<?php echo $form->textField($model,'shipping_charge',array('class' => 'form-control')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tax_rate'); ?>
		<?php echo $form->textField($model,'tax_rate',array('class' => 'form-control')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->